//'use strict';

var newDBClient = require('mongodb').MongoClient,    
    chalk = require('chalk'),
    uuid = require('node-uuid'),
    assert = require('assert');

var newUrl = 'mongodb://localhost:27017/crmclone';

var count = 0;

var streets = [
'Абельмановская улица',
'Абельмановская Застава, площадь',
'Абрамцевская просека',
'Бабаевская улица',
'Бабьегородский 1-й переулок',
'Бабьегородский 2-й переулок',
'Вавилова, улица',
'Вагоноремонтная улица',
'Вадковский переулок',
'Габричевского, улица',
'Гаврикова улица',
'Гагарина, площадь',
'Давыдковская улица',
'Давыдовский переулок',
'Даев переулок',
'Жебрунова, улица',
'Железногорская 1-я улица',
'Железногорская 2-я улица',
'Забелина, улица',
'Заболотье, улица',
'Заваруевский переулок',
'Ибрагимова, улица',
'Ивана Бабушкина, улица',
'Ивана Сусанина, улица',
'Кабельная 1-я улица',
'Кабельная 2-я улица',
'Кабельная 3-я улица',
'Лавочкина, улица',
'Лавров переулок',
'Лаврский переулок',
'Магаданская улица',
'Магазинный тупик',
'Магистральная 1-я улица',
'Набережная улица (Лианозово).',
'Набережная улица (Посёлок Рублёво).',
'Нагатинская набережная',
'Обводное шоссе',
'Оболенский переулок',
'Оборонная улица',
'Павелецкая набережная',
'Павелецкая площадь',
'Павелецкий 1-й проезд',
'Рабочая улица',
'Рабочий 1-й переулок',
'Рабфаковский переулок',
'Саввинская набережная',
'Савёлкинский проезд',
'Савёловская Линия, улица',
'Таганрогская улица',
'Таганская площадь',
'Таганская улица',
'Уваровский переулок',
'Угличская улица',
'Угловой переулок',
'Федеративный проспект',
'Фёдора Полетаева, улица',
'Фёдорова, улица',
'Хабаровская улица',
'Хавская улица',
'Халтуринская улица',
'Цандера, улица',
'Цариков переулок',
'Цветной бульвар',
'Чавеса, улица',
'Чагинская улица',
'Чапаева, улица',
'Шаболовка, улица',
'Шарикоподшипниковская улица',
'Шарля де Голля, площадь',
'Щёлковский проезд',
'Щёлковское шоссе',
'Щемиловский 1-й переулок',
'Элеваторная улица',
'Элеваторный переулок',
'Электрический переулок',
'Югорский проезд',
'Южная улица',
'Южнобутовская улица',
'Яблоневая аллея',
'Яблонный переулок',
'Яблочкова, улица'];

var COLLECTIONS = {
    organizations: 'organizations',
    projects: 'projects',
    proposals: 'proposals',
    users: 'users',
}

var usersArray = new Array();

newDBClient.connect(newUrl, function(err, newDB) {
    console.log(log(chalk.green.bold('CONNECTED SUCCESS!')));    

    updateUsers(newDB);    
    updateProposals(newDB);    
});

var updateUsers = function(newDB){
    console.log(chalk.blue.bold('\nUSERS UPDATE:'));
    var userCount = 0;
    newDB.collection(COLLECTIONS.users).find()
        .toArray(function(err, data) {
            data.forEach(function(elem){
                elem.lastName = (elem.lastName && elem.lastName.length>2) ? elem.lastName[2].toUpperCase() + '.' : elem.lastName;
                elem.fullName = elem.firstName + ' ' + elem.lastName;
                var newPhone = '89';
                for(var i=2; i<elem.phone.length; i++){
                    orderNum = Math.floor(getRandom(2, elem.phone.length));
                    newPhone += elem.phone[orderNum] + '';
                }
                elem.phone = newPhone;
                elem.needsSMS = false;
                elem.email = 'user_' + (userCount+1) + '@office.ru';
                elem.password = '$2a$10$apJDDq6M2wncD1DzaXqqXutHEDQiOkzrqaYa.lPlkGst8h/hnhgnO';

                usersArray[elem._id] = elem;
                
                newDB.collection(COLLECTIONS.users).save(elem, function(err, result){
                    if (!err){
                        console.log(log(elem.fullName + ' ' + chalk.green.bold('SUCCESS!')));
                    }
                    else{
                        console.log(log(chalk.red.bold('FAIL!\n' + err)));
                    }
                });

                console.log(log(elem.fullName + ', ' + elem.email + ', ' + elem.phone));
                
                userCount++;            
        });
    });
}

var updateProposals = function(newDB){
    console.log(chalk.blue.bold('\nPROPOSALS UPDATE:'));
    newDB.collection(COLLECTIONS.proposals).find()
        .toArray(function(err, data) {
            data.forEach(function(elem){

                console.log(log('заявка №' + elem.number));
                

                console.log(log('update address'));

                var addressNum = Math.floor(getRandom(1,streetsCnt));
                var houseNum = Math.floor(getRandom(1,15));
                var propsalName = streets[addressNum] + ', д. ' + houseNum;
                var testAddress = 'Москва, ' + propsalName;                          

                 while(isUsed(propsalName)) {
                    addressNum = Math.floor(getRandom(1,streetsCnt));
                    houseNum = Math.floor(getRandom(1,15));
                    
                    propsalName = streets[addressNum] + ', д. ' + houseNum;
                    testAddress = 'Москва, ' + propsalName;
                    countEq++;
                }
                
                elem.name = propsalName;                
                elem.address = testAddress;
                if (elem.mapAddress){
                    elem.mapAddress.foundPlace = elem.address;
                    elem.mapAddress.queryPlace = elem.address;
                }
                usedStreets.push(propsalName);                

                if (elem.tasks){
                    //console.log(log('update tasks'));

                    for (var i = 0, len = elem.tasks.length; i < len; i++) {
                        var task = elem.tasks[i];

                        if (task.owner){
                            var userOwner = usersArray[task.owner._id];
                            if (userOwner){                                    
                                    task.owner = userOwner;
                                }
                                else{
                                    console.log(log(chalk.red.bold('ERROR:') + ' user ' + task.owner._id + ' not found'));
                                    task.owner = null;
                                }
                            
                        }

                        if (task.executor){
                            var userExecutor = usersArray[task.executor._id];
                            if (userExecutor){
                                    task.executor = userExecutor;
                                }
                                else{
                                    console.log(log(chalk.red.bold('ERROR:') + ' user ' + task.executor._id + ' not found'));
                                    task.owner = null;
                                }
                        }

                        if (elem.currentTask.id == task.id){
                            elem.currentTask = task;
                        }
                    }
                }                


                if (elem.customFields != null && elem.customFields.value != null){
                    //console.log(log('update customFields'));

                    for (var i = 0, len = elem.customFields.value.length; i < len; i++) {
                        var customField = elem.customFields.value[i];

                        if (customField.id == "cc8ab470-9a9e-11e4-80e6-17c87fc056ce"){
                            customField.value = "8-901-234-56-78";                            
                        }

                        if (customField.id == "b5676540-9a9e-11e4-80e6-17c87fc056ce"){
                            customField.value = "ООО";                            
                        }

                        elem.customFields.value[i] = customField;
                    }
                } 

                if (elem.documents){
                    //console.log(log('update documents'));

                    for (var i = 0, len = elem.documents.length; i < len; i++) {
                        var document = elem.documents[i];
                        document.link = "https://yadi.sk";
                    }
                } 

                if (elem.manager){
                   // console.log(log('update manager'));
                    var userManager = usersArray[elem.manager._id];
                    if (userManager){
                        elem.manager = userManager;
                        elem.owner = userManager;
                        elem.coordinator = userManager;
                    }
                }              

                newDB.collection(COLLECTIONS.proposals).save(elem, function(err, result){
                    if (!err){
                        console.log(log(elem.number + ' ' + chalk.green.bold('SUCCESS!')));
                    }
                    else{
                        console.log(log(chalk.red.bold('FAIL!')));
                    }
                });
                count++;
            });
        });
}
    
var streetsCnt = streets.length;
var usedStreets = [];

function getRandom(min, max){
  return Math.random() * (max - min) + min;
}

var countEq = 0;
function isUsed(address){    
    usedStreets.forEach(function(elem){
        if (elem == address){            
            return true;
        }
    });
    return false;
}

var log = function(msg) {
    return chalk.cyan.bold('[' + new Date().toLocaleTimeString() + '] ') + msg;
}