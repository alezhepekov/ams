var newDBClient = require('mongodb').MongoClient,
    oldDBClient = require('mongodb').MongoClient,
    chalk = require('chalk'),
    uuid = require('node-uuid'),
    assert = require('assert');

var COLLECTIONS = {
    organizations: 'organizations',
    projects: 'projects',
    proposals: 'proposals',
    users: 'users',
}

var projectName = 'Инвестпроекты';
var sourceProjectTableName = 'projects_invest';

var newUsers = [];

var tmpObjects = {
    org: null,
    proj: null
}


var newUrl = 'mongodb://localhost:27017/crmclone',
    oldUrl = 'mongodb://localhost:27017/abndim';

newDBClient.connect(newUrl, function(err, newDB) {
    oldDBClient.connect(oldUrl, function(err, oldDB) {
        init(newDB, oldDB);
    });
});


var init = function(newDB, oldDB) {
    //dropDB(newDB);
    createOrganization(newDB, createProject, function() {
        exportDB(newDB, oldDB);
    });
}

var exportDB = function(newDB, oldDB) {
    //exportUsers(newDB, oldDB);
    exportProposals(newDB, oldDB);
}

var exportUsers = function(newDB, oldDB) {
    console.log(chalk.blue.bold('\nUSERS EXPORT:'));
    oldDB.collection(COLLECTIONS.users).find().toArray(function(err, data) {
        data.forEach(function(user) {
            exportUser(newDB, user);
        });
    });
}

var exportProposals = function(newDB, oldDB, callback) {
    console.log(chalk.blue.bold('\nPROPOSALS EXPORT:'));

    //oldDB.collection(COLLECTIONS.projects).find().toArray(function(err, data) {
    oldDB.collection(sourceProjectTableName).find().toArray(function(err, data) {
        console.log(chalk.blue.bold('\nPROPOSALS count:' + data.length));

        data.forEach(function(proposal) {
            exportProposal(newDB, proposal);
        });
    });
}

var exportProposal = function(newDB, proposal) {
    var msg = 'Creating of proposal ' + chalk.cyan(proposal.data.projectNo) + '...',
        newTasks = exportTasks(proposal),
        newCurrentTask = newTasks.length > 0 ? newTasks[newTasks.length - 1] : {};

    var existedCnt = 0;
    var successCnt = 0;
    var failCnt = 0;
    var projectId = '' + tmpObjects.proj._id;

    newDB.collection(COLLECTIONS.proposals).find({
        number: proposal.data.projectNo,
        projectId: projectId,
        "started.year": proposal.createdAt.getFullYear()}
    ).toArray(function(err, data){
        if (data.length > 0){
            console.log(log(msg + chalk.green.bold('SUCCESS!')));
            existedCnt++;
        }
        else{
            newDB.collection(COLLECTIONS.proposals).insert({
                started: proposal.createdAt,
                projectId: projectId,
                name: proposal.data.title,
                number: proposal.data.projectNo,
                documents: proposal.data.files.map(function(file) {
                    return {
                        id: uuid.v1(),
                        text: file.caption,
                        link: file.url
                    }
                }),
                address: proposal.data.title,
                mapAddress: {
                    point : {
                        lat : proposal.data.coords[0],
                        lon : proposal.data.coords[1]
                    },
                    precision : "exact",
                    country : "Россия",
                    state : "",
                    city : "",
                    foundPlace : "",
                    queryPlace : proposal.data.title
                },
                currentTask: newCurrentTask,
                status: {
                    title: proposal.data.status,
                    className: getStatusClass(proposal.data.status)
                },
                manager: findUser(proposal.createdBy),
                owner: findUser(proposal.createdBy),
                description: proposal.data.description,
                customFields: createCustomFields(proposal),
                tasks: newTasks,
            }, function(err, result) {
                if (!err) {
                    console.log(log(msg + chalk.green.bold('SUCCESS!')));
                    successCnt++;
                } else {
                    console.log(log(msg + chalk.red.bold('FAIL!\nERROR: ' + err)));
                    failCnt++;
                }
            });
        }

        console.log('SUCCESS-CNT: ' + chalk.green.bold(successCnt) + ', EXISTED-CNT: ' + chalk.yellow.bold(existedCnt) +
            ', FAIL-CNT: ' + chalk.red.bold(failCnt));
    });
}

var exportTasks = function(proposal) {
    var result = [];
    proposal.data.tasks.forEach(function(task) {
        result.push({
            id: uuid.v1(),
            creationDate: task.createdAt,
            dueDate: task.due,
            finished: task.completed,
            finishingDate: task.completedAt,
            owner: findUser(task.createdBy),
            executor: findUser(task.assignedTo),
            description: task.description,
            status: {
                title: task.type,
                className: getStatusClass(task.type)
            }
        });
    });
    return result;
}

var createStatuses = function() {
    return ['все',
        'заявка',
        'осмотр',
        'оценка',
        'проверка',
        'исправление',
        'проверка после исправления',
        'замечания от СРО',
        'замечания от ДГИМ',
        'согласование в ДГИМ',
        'согласование в СРО',
        'повторное согласование в СРО',
        'исправление ошибок',
        'подготовка экспертизы',
        'повторная экспертиза',
        'сдача',
        'печать',
        'доставка',
        'выполнена',
        'отложена',
        'отменена',
    ].map(function(statusName) {
        return {
            title: statusName,
            className: getStatusClass(statusName)
        }
    });
}

var getStatusClass = function(name) {
    switch (name) {
        case 'выполнена':
            return 'success';
        case 'оценка':
            return 'primary';
        case 'осмотр':
            return 'default';
        case 'отложена':
            return 'warning';
        case 'согласование в ДГИМ':
            return 'danger';
        default:
            return 'info';
    }
}

var findUser = function(oldUser) {
    if (oldUser == null)
        return;
    return newUsers.filter(function(user) {
        return user.email === oldUser.email;
    })[0];
}

var createCustomFields = function(proposal) {
    return {
        id: 'e957e780-9a9e-11e4-80e6-17c87fc056ce',
        value: [{
            id: 'cc8ab470-9a9e-11e4-80e6-17c87fc056ce',
            value: proposal.data.phone
        }, {
            id: 'b5676540-9a9e-11e4-80e6-17c87fc056ce',
            value: proposal.data.description
        }, {
            id: '635fbd10-9a99-11e4-80e6-17c87fc056ce',
            value: [{
                id: '580b67d0-9a93-11e4-80e6-17c87fc056ce',
                value: proposal.data.type
            }, {
                id: '7fa2b8e0-9a96-11e4-80e6-17c87fc056ce',
                value: proposal.data.area
            }, {
                id: 'bd43da30-9a96-11e4-80e6-17c87fc056ce',
                value: proposal.data.currentUse
            }, {
                id: '1e8cadc0-9a98-11e4-80e6-17c87fc056ce',
                value: proposal.data.municipDistrict
            }, {
                id: '4',
                value: proposal.data.municipRegion
            }, {
                id: '714cf460-9a99-11e4-80e6-17c87fc056ce',
                value: proposal.data.floor
            }, {
                id: 'd51825b0-9a9d-11e4-80e6-17c87fc056ce',
                value: proposal.data.placement
            }, {
                id: 'ef7fa360-9a9d-11e4-80e6-17c87fc056ce',
                value: proposal.data.separateEntrance
            }, {
                id: '05d54250-9a9e-11e4-80e6-17c87fc056ce',
                value: proposal.data.entrancePlacement
            }, {
                id: '43b54160-9a9e-11e4-80e6-17c87fc056ce',
                value: proposal.data.condition
            }, {
                id: '5b3e3030-9a9e-11e4-80e6-17c87fc056ce',
                value: ''
            }, {
                id: '62717330-9a9e-11e4-80e6-17c87fc056ce',
                value: ''
            }, {
                id: '6becaa60-9a9e-11e4-80e6-17c87fc056ce',
                value: ''
            }, {
                id: '72ea3760-9a9e-11e4-80e6-17c87fc056ce',
                value: ''
            }, {
                id: '7b6c8fa0-9a9e-11e4-80e6-17c87fc056ce',
                value: ''
            }, {
                id: '82058e70-9a9e-11e4-80e6-17c87fc056ce',
                value: {
                    day: 10,
                    month: 12,
                    year: 2014
                }
            }, {
                id: '905d9580-9a9e-11e4-80e6-17c87fc056ce',
                value: proposal.data.marketPrice
            }, {
                id: '9b945f60-9a9e-11e4-80e6-17c87fc056ce',
                value: proposal.data.landMarketPrice
            }, {
                id: 'a3c1e0e0-9a9e-11e4-80e6-17c87fc056ce',
                value: ''
            }]
        }]
    }
}

var exportUser = function(newDB, user) {
    var msg = 'Creating of user ' + chalk.cyan(user.name) + '...';


    newDB.collection(COLLECTIONS.users).find({email: user.email}).toArray(function(err, data){
        if (data.length > 0){
            newUsers.push(data[0]);
            console.log(log(msg + chalk.green.bold('SUCCESS!')));
        }
        else{
                newDB.collection(COLLECTIONS.users).insert({
                organizationId: '' + tmpObjects.org._id,
                firstName: user.name.split(' ')[0],
                lastName: user.name.split(' ')[1],
                fullName: user.name,
                email: user.email,
                position: user.roles[0],
                active: true,
                phone: '89000000000',
                password: '$2a$10$apJDDq6M2wncD1DzaXqqXutHEDQiOkzrqaYa.lPlkGst8h/hnhgnO', //12345678
                roles: convertRoles(user.roles),
            }, function(err, result) {
                if (!err) {
                    newUsers.push(result[0]);
                    console.log(log(msg + chalk.green.bold('SUCCESS!')));
                } else {
                    console.log(log(msg + chalk.red.bold('FAIL!\nERROR: ' + err)));
                }
            });
        }
    });
}

var convertRoles = function(oldRoles) {
    var result = [];
    oldRoles.forEach(function(oldRole) {
        tmpObjects.proj.roles.forEach(function(newRole) {
            if (oldRole === newRole.name) {
                result.push({
                    roleId: newRole.id,
                    projectId: '' + tmpObjects.proj._id
                });
            }
        });
    });

    return result;
}


var removeCollection = function(db, collectionName) {
    var msg = 'Deleting of collection ' + chalk.cyan(collectionName) + '... ';
    db.collection(collectionName).remove({}, function(err, numberRemoved) {
        if (!err) {
            console.log(log(msg + chalk.green.bold('SUCCESS!')));
        } else {
            console.log(log(msg + chalk.red.bold('FAIL!\nERROR: ' + err)));
        }
    });
}

var createRoles = function() {
    var result = [];
    ['ассистент', 'курьер', 'оценщик', 'супервайзер', 'координатор', 'СРО', 'ДГИМ'].forEach(function(roleName) {
        result.push({
            id: uuid.v1(),
            name: roleName,
            seeAllProposals: false,
            seeAllProjects: false,
            editAllProposals: false,
            editAllTasks: false,
            completeAllTasks: false,
            seeStatistic: false,
            dataExport: false,
        });
    });
    return result;
}

var log = function(msg) {
    return chalk.cyan.bold('[' + new Date().toLocaleTimeString() + '] ') + msg;
}

var dropDB = function(db) {
    removeCollection(db, COLLECTIONS.users);
    removeCollection(db, COLLECTIONS.organizations);
    removeCollection(db, COLLECTIONS.projects);
    removeCollection(db, COLLECTIONS.proposals);
}

var createOrganization = function(db, callback, createProjectCallback) {
    var msg = 'Creating of organization ' + chalk.cyan('"abndim"') + '...';

    db.collection(COLLECTIONS.organizations).find({name: 'abndim'}).toArray(function(err, data){
        if (data.length > 0){
            console.log(log(msg + chalk.green.bold('SUCCESS!')));
                    tmpObjects.org = data[0];
                    createProject(db, createProjectCallback);
        }
        else{
            db.collection(COLLECTIONS.organizations).insert({
                name: 'abndim',
                description: '---'
            }, function(err, result) {
                if (!err) {
                    console.log(log(msg + chalk.green.bold('SUCCESS!')));
                    tmpObjects.org = result[0];
                    createProject(db, createProjectCallback);
                } else {
                    console.log(log(msg + chalk.red.bold('FAIL!\nERROR: ' + err)));
                }
            });
        }

    });


}

var createProject = function(db, callback) {
    var msg = 'Creating of project ' + chalk.cyan(projectName) + '...';

    db.collection(COLLECTIONS.projects).find({name: projectName}).toArray(function(err, data){
     if (data.length > 0){
        console.log(log(msg + chalk.green.bold('SUCCESS!')));
        tmpObjects.proj = data[0];
        callback(db);
        }
        else{
            db.collection(COLLECTIONS.projects).insert({
                started: new Date(),
                name: projectName,
                description: '---',
                number: '',
                organizationId: '' + tmpObjects.org._id,
                documents: [],
                address: '',
                mapAddress: {},
                proposalTemplate: createProposalTemplate(),
                templates: createTemplates(),
                statuses: createStatuses(),
                roles: createRoles(),
            }, function(err, result) {
                if (!err) {
                    console.log(log(msg + chalk.green.bold('SUCCESS!')));
                    tmpObjects.proj = result[0];
                    callback(db);
                } else {
                    console.log(log(msg + chalk.red.bold('FAIL!\nERROR: ' + err)));
                }
            });
        }
    });
}

var createTemplates = function() {
    return JSON.parse('[{"fields":[{"entityType":"list-field","entitySubtype":"constant-list","entityName":"Тип","emptyListType":"text-fields-list","listItems":[{"id":0,"value":"ПСН"},{"id":1,"value":"Торговое"},{"id":2,"value":"Офис"},{"id":3,"value":"Склад"},{"id":4,"value":"Общепит"},{"id":5,"value":"ОСЗ"}],"entity":"","placeholder":"","title":"Тип","id":"580b67d0-9a93-11e4-80e6-17c87fc056ce"},{"entityType":"text-field","entitySubtype":"","entityName":"Площадь","emptyListType":"","listItems":[],"entity":"","placeholder":"Площадь (кв. м)","title":"Площадь","id":"7fa2b8e0-9a96-11e4-80e6-17c87fc056ce"},{"entityType":"text-field","entitySubtype":"","entityName":"Текущее использование","emptyListType":"","listItems":[],"entity":"","placeholder":"Текущее использование","title":"Текущее использование","id":"bd43da30-9a96-11e4-80e6-17c87fc056ce"},{"entityType":"list-field","entitySubtype":"constant-list","entityName":"Округ","emptyListType":"text-fields-list","listItems":[{"id":0,"value":"ЮАО"},{"id":1,"value":"Другое"}],"entity":"","placeholder":"","title":"Округ","id":"1e8cadc0-9a98-11e4-80e6-17c87fc056ce"},{"entityType":"list-field","entitySubtype":"constant-list","entityName":"Район","emptyListType":"text-fields-list","listItems":[{"id":0,"value":"Даниловский"},{"id":1,"value":"Другой"}],"entity":"","placeholder":"","title":"Район","id":4},{"entityType":"text-field","entitySubtype":"","entityName":"Этаж","emptyListType":"","listItems":[],"entity":"","placeholder":"Этаж","title":"Этаж","id":"714cf460-9a99-11e4-80e6-17c87fc056ce"},{"entityType":"list-field","entitySubtype":"constant-list","entityName":"Линия","emptyListType":"text-fields-list","listItems":[{"id":0,"value":"первая"},{"id":1,"value":"внутриквартальная"}],"entity":"","placeholder":"","title":"Линия","id":"d51825b0-9a9d-11e4-80e6-17c87fc056ce"},{"entityType":"list-field","entitySubtype":"constant-list","entityName":"Отд. вход","emptyListType":"","listItems":[{"id":0,"value":"есть"},{"id":1,"value":"единая входная группа"}],"entity":"","placeholder":"","title":"Отд. вход","id":"ef7fa360-9a9d-11e4-80e6-17c87fc056ce"},{"entityType":"list-field","entitySubtype":"constant-list","entityName":"Расположение входа","emptyListType":"","listItems":[{"id":0,"value":"первая"},{"id":1,"value":"внутриквартальная"}],"entity":"","placeholder":"","title":"Расположение входа","id":"05d54250-9a9e-11e4-80e6-17c87fc056ce"},{"entityType":"text-field","entitySubtype":"","entityName":"Отделка","emptyListType":"","listItems":[],"entity":"","placeholder":"Отделка","title":"Отделка","id":"43b54160-9a9e-11e4-80e6-17c87fc056ce"},{"entityType":"text-field","entitySubtype":"","entityName":"Кадастровый №","emptyListType":"","listItems":[],"entity":"","placeholder":"Кадастровый №","title":"Кадастровый №","id":"5b3e3030-9a9e-11e4-80e6-17c87fc056ce"},{"entityType":"text-field","entitySubtype":"","entityName":"Кадастровый № з/у","emptyListType":"","listItems":[],"entity":"","placeholder":"Кадастровый № з/у","title":"Кадастровый № з/у","id":"62717330-9a9e-11e4-80e6-17c87fc056ce"},{"entityType":"text-field","entitySubtype":"","entityName":"№ документоконтроля","emptyListType":"","listItems":[],"entity":"","placeholder":"№ документоконтроля","title":"№ документоконтроля","id":"6becaa60-9a9e-11e4-80e6-17c87fc056ce"},{"entityType":"text-field","entitySubtype":"","entityName":"№ заключения СРО","emptyListType":"","listItems":[],"entity":"","placeholder":"№ заключения СРО","title":"№ заключения СРО","id":"72ea3760-9a9e-11e4-80e6-17c87fc056ce"},{"entityType":"text-field","entitySubtype":"","entityName":"№ отчета","emptyListType":"","listItems":[],"entity":"","placeholder":"№ отчета","title":"№ отчета","id":"7b6c8fa0-9a9e-11e4-80e6-17c87fc056ce"},{"entityType":"date-field","entitySubtype":"date","entityName":"Дата отчёта","emptyListType":"","listItems":[],"entity":"","placeholder":"","title":"Дата отчёта","id":"82058e70-9a9e-11e4-80e6-17c87fc056ce"},{"entityType":"text-field","entitySubtype":"","entityName":"Стоимость (без НДС)","emptyListType":"","listItems":[],"entity":"","placeholder":"Стоимость (без НДС)","title":"Стоимость (без НДС)","id":"905d9580-9a9e-11e4-80e6-17c87fc056ce"},{"entityType":"text-field","entitySubtype":"","entityName":"Стоимость з/у (без НДС)","emptyListType":"","listItems":[],"entity":"","placeholder":"Стоимость з/у (без НДС)","title":"Стоимость з/у (без НДС)","id":"9b945f60-9a9e-11e4-80e6-17c87fc056ce"},{"entityType":"text-field","entitySubtype":"","entityName":"Примечания","emptyListType":"","listItems":[],"entity":"","placeholder":"Примечания","title":"Примечания","id":"a3c1e0e0-9a9e-11e4-80e6-17c87fc056ce"}],"name":"Результаты осмотра и оценки","id":"635fbd10-9a99-11e4-80e6-17c87fc056ce"}]');
}

var createProposalTemplate = function() {
    return JSON.parse('{"fields":[{"entityType":"text-field","entitySubtype":"","entityName":"Описание, арендатор","emptyListType":"","listItems":[],"entity":"","placeholder":"Описание, арендатор","title":"Описание, арендатор","id":"b5676540-9a9e-11e4-80e6-17c87fc056ce"},{"entityType":"text-field","entitySubtype":"","entityName":"Контактный телефон","emptyListType":"","listItems":[],"entity":"","placeholder":"Номер телефона","title":"Контактный телефон","id":"cc8ab470-9a9e-11e4-80e6-17c87fc056ce"},{"entityType":"entity-field","entitySubtype":"","entityName":"Результаты осмотра и оценки","emptyListType":"","listItems":[],"entityId":"635fbd10-9a99-11e4-80e6-17c87fc056ce","placeholder":"","title":"Результаты осмотра и оценки","id":4}],"name":"Новый шаблон","id":"e957e780-9a9e-11e4-80e6-17c87fc056ce"}');
}
