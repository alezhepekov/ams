// Generated on 2014-11-06 using generator-angular 0.9.8
'use strict';
var path = require('path');
// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

module.exports = function (grunt) {

    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);

    // Time how long tasks take. Can help when optimizing build times
    require('time-grunt')(grunt);

    // Configurable paths for the application
    var appConfig = {
        app: require('./bower.json').appPath || 'app',
        dist: 'dist'
    };

    // Define the configuration for all the tasks
    grunt.initConfig({

        // Project settings
        yeoman: appConfig,

        // Watches files for changes and runs tasks based on the changed files
        watch: {
            bower: {
                files: ['bower.json'],
                tasks: ['wiredep']
            },
            js: {
                files: ['<%= yeoman.app %>/scripts/{,**/}*.js'],
                //tasks: ['newer:jshint:all', 'karma'],
                tasks: ['newer:jshint:all'],
                options: {
                    livereload: '<%= connect.options.livereload %>'
                }
            },
            jsxCompenents: {
                files: ['<%= yeoman.app %>/scripts/common/ui-components/src/{,**/}*'],
                tasks: ['clean:uiComponents', 'react:components', 'less', 'concat:uiComponents', 'cssmin:uiComponents'],
            },
            jsxControllers: {
                files: ['<%= yeoman.app %>/scripts/controllers/src/{,**/}*'],
                tasks: ['react:controllers'],
            },
            jsTest: {
                files: ['test/spec/{,*/}*.js'],
                //tasks: ['newer:jshint:test', 'karma']
                tasks: ['newer:jshint:test']
            },
            styles: {
                files: ['<%= yeoman.app %>/styles/{,*/}*.css'],
                tasks: ['newer:copy:styles', 'autoprefixer']
            },
            gruntfile: {
                files: ['Gruntfile.js']
            },
            livereload: {
                options: {
                    livereload: '<%= connect.options.livereload %>'
                },
                files: [
                    '<%= yeoman.app %>/{,*/}*.html',
                    '.tmp/styles/{,*/}*.css',
                    '<%= yeoman.app %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
                ]
            }
        },
        express: {
            backend: {
                options: {
                    server: path.resolve('./server/app')
                }
            }
        },
        // Looks like it isn't needed any more
        connect: {
            options: {
                port: 9000,
                // Change this to '0.0.0.0' to access the server from outside.
                hostname: 'localhost',
                livereload: 35729
            },
            livereload: {
                options: {
                    open: true,
                    middleware: function (connect) {
                        return [
                            connect.static('.tmp'),
                            connect().use(
                                '/bower_components',
                                connect.static('./bower_components')
                            ),
                            connect.static(appConfig.app)
                        ];
                    }
                }
            },
            test: {
                options: {
                    port: 9001,
                    middleware: function (connect) {
                        return [
                            connect.static('.tmp'),
                            connect.static('test'),
                            connect().use(
                                '/bower_components',
                                connect.static('./bower_components')
                            ),
                            connect.static(appConfig.app)
                        ];
                    }
                }
            },
            dist: {
                options: {
                    open: true,
                    base: '<%= yeoman.dist %>'
                }
            }
        },
        // Make sure code styles are up to par and there are no obvious mistakes
        jshint: {
            options: {
                jshintrc: '.jshintrc',
                reporter: require('jshint-stylish')
            },
            all: {
                src: [
                    'Gruntfile.js',
                    '<%= yeoman.app %>/scripts/{,*/}*.js'
                ]
            },
            test: {
                options: {
                    jshintrc: '.jshintrc'
                },
                src: ['test/spec/controllers/{,*/}*.js']
            },
            jsx :{
                src: [
                        'app/scripts/controllers/src/*.jsx',
                        'app/scripts/common/ui-components/src/*/**.jsx'
                ],
            },
            server: {
                src: [
                    'server/*.js',
                    'server/bin/*.js',
                    'server/db-manager/*.js',
                    'server/config/*.js',
                    'server/routes/*.js',
                    'server/test/**.js'
                ]
            }
        },

        // Empties folders to start fresh
        clean: {
            dist: {
                files: [{
                    dot: true,
                    src: [
                        '.tmp',
                        '<%= yeoman.dist %>/{,*/}*',
                        '!<%= yeoman.dist %>/.git*'
                    ]
                }]
            },
            uiComponents: ['<%= yeoman.app %>/scripts/common/ui-components/dist/**/*'],
            server: '.tmp'
        },

        // Add vendor prefixed styles
        autoprefixer: {
            options: {
                browsers: ['last 1 version']
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: '.tmp/styles/',
                    src: '{,*/}*.css',
                    dest: '.tmp/styles/'
                }]
            }
        },

        // Automatically inject Bower components into the app
        wiredep: {
            app: {
                src: ['<%= yeoman.app %>/index.html'],
                ignorePath: /\.\.\//
            }
        },

        // Renames files for browser caching purposes
        filerev: {
            dist: {
                src: [
                    '<%= yeoman.dist %>/scripts/{,*/}*.js',
                    '<%= yeoman.dist %>/styles/{,*/}*.css',
                    '<%= yeoman.dist %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}',
                    '<%= yeoman.dist %>/styles/fonts/*'
                ]
            }
        },

        // Reads HTML for usemin blocks to enable smart builds that automatically
        // concat, minify and revision files. Creates configurations in memory so
        // additional tasks can operate on them
        useminPrepare: {
            html: '<%= yeoman.app %>/index.html',
            options: {
                dest: '<%= yeoman.dist %>',
                flow: {
                    html: {
                        steps: {
                            js: ['concat', 'uglifyjs'],
                            css: ['cssmin']
                        },
                        post: {}
                    }
                }
            }
        },

        // Performs rewrites based on filerev and the useminPrepare configuration
        usemin: {
            html: ['<%= yeoman.dist %>/{,*/}*.html'],
            css: ['<%= yeoman.dist %>/styles/{,*/}*.css'],
            options: {
                assetsDirs: ['<%= yeoman.dist %>', '<%= yeoman.dist %>/images']
            }
        },

        svgmin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= yeoman.app %>/images',
                    src: '{,*/}*.svg',
                    dest: '<%= yeoman.dist %>/images'
                }]
            }
        },

        htmlmin: {
            dist: {
                options: {
                    collapseWhitespace: true,
                    conservativeCollapse: true,
                    collapseBooleanAttributes: true,
                    removeCommentsFromCDATA: true,
                    removeOptionalTags: true
                },
                files: [{
                    expand: true,
                    cwd: '<%= yeoman.dist %>',
                    src: ['*.html', 'views/{,*/}*.html'],
                    dest: '<%= yeoman.dist %>'
                }]
            }
        },

        // ng-annotate tries to make the code safe for minification automatically
        // by using the Angular long form for dependency injection.
        ngAnnotate: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '.tmp/concat/scripts',
                    src: ['*.js', '!oldieshim.js'],
                    dest: '.tmp/concat/scripts'
                }]
            }
        },

        // Replace Google CDN references
        cdnify: {
            dist: {
                html: ['<%= yeoman.dist %>/*.html']
            }
        },

        // Copies remaining files to places other tasks can use
        copy: {
            dist: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= yeoman.app %>',
                    dest: '<%= yeoman.dist %>',
                    src: [
                        '*.{ico,png,txt}',
                        '.htaccess',
                        '*.html',
                        'views/{,*/}*.html',
                        'images/{,*/}*.{webp}',
						'images/*',
                        'fonts/*',
						'scripts/common/libs/tv-main-built.js',
						'styles/tv-styles.css'
                    ]
                }, {
					expand: true,
					dot: true,
					cwd: '<%= yeoman.app %>',
					dest: '<%= yeoman.dist %>/fonts/',
					src: [
						//TODO: Make copy only files without folder's path
						'bower_components/bootstrap/fonts/*'
					],
				}, {
                    expand: true,
                    cwd: '.tmp/images',
                    dest: '<%= yeoman.dist %>/images',
                    src: ['generated/*']
                }, {
                    expand: true,
                    cwd: 'bower_components/bootstrap/dist',
                    src: 'fonts/*',
                    dest: '<%= yeoman.dist %>'
                }]
            },
            styles: {
                expand: true,
                cwd: '<%= yeoman.app %>/styles',
                dest: '.tmp/styles/',
                src: '{,*/}*.css'
            }
        },

        // Run some tasks in parallel to speed up the build process
        concurrent: {
            server: [
                'copy:styles',
                'react',
                'less'
            ],
            test: [
                'copy:styles'
            ],
            dist: [
                'copy:styles',
                'svgmin'
            ]
        },

        // Test settings
        /*karma: {
            unit: {
                configFile: 'test/karma.conf.js',
                singleRun: true
            }
        },*/
        react: {
            components: {
                files: [{
                    expand: true,
                    cwd: '<%= yeoman.app %>/scripts/common/ui-components/src',
                    src: ['**/*.jsx'],
                    dest: '<%= yeoman.app %>/scripts/common/ui-components/dist',
                    ext: '.js'
                }]
            },
            controllers: {
                files: [{
                    expand: true,
                    cwd: '<%= yeoman.app %>/scripts/controllers/src',
                    src: ['**/*.jsx'],
                    dest: '<%= yeoman.app %>/scripts/controllers/dest',
                    ext: '.js'
                }]
            }

        },
        less: {
            dynamicCompiling: {
                files: [{
                    expand: true,
                    cwd: '<%= yeoman.app %>/scripts/common/ui-components/src',
                    src: ['**/*.less'],
                    dest: '<%= yeoman.app %>/scripts/common/ui-components/dist',
                    ext: '.css'
                }]
            }
        },
		comments: {
			js: {
				options: {
				  singleline: true,
				  multiline: true
				},
				src: ['<%= yeoman.app %>/scripts/controllers/dest/*.js']
			}
		},
        cssmin: {
            uiComponents: {
                options: {
                    banner: '/* UI Componetns styles */'
                },
                files: {
                    '<%= yeoman.app %>/scripts/common/ui-components/dist/ui-components.css': ['<%= yeoman.app %>/scripts/common/ui-components/dist/**/*.css']
                }
            }
        },
        concat: {
            uiComponents: {
                options: {
                    banner: '/* UI Componetns */'
                },
                files: {
                    '<%= yeoman.app %>/scripts/common/ui-components/dist/ui-components.js': ['<%= yeoman.app %>/scripts/common/ui-components/dist/**/*.js']
                }
            }
        }
    });


    grunt.registerTask('serve', 'Compile then start a connect web server', function (target) {
        if (target === 'dist') {
            return grunt.task.run(['build', 'connect:dist:keepalive']);
        }

        grunt.task.run([
            'clean:server',
            'wiredep',
            'concurrent:server',
            'concat',
            'cssmin:uiComponents',
            'autoprefixer',
            'connect:livereload',
            'watch'
        ]);
    });

    grunt.registerTask('server', 'DEPRECATED TASK. Use the "serve" task instead', function (target) {
        grunt.log.warn('The `server` task has been deprecated. Use `grunt serve` to start a server.');
        grunt.task.run(['serve:' + target]);
    });

    grunt.registerTask('test', [
        'clean:server',
        'react',
        'concurrent:test',
        'autoprefixer',
        'connect:test',
        //'karma',
    ]);

    grunt.registerTask('build', [
        'clean',
        'react',
        'less',
		'comments',
        'wiredep',
        'useminPrepare',
        'concurrent:dist',
        'concat',
        'cssmin:uiComponents',
        'autoprefixer',
        'concat',
        'ngAnnotate',
        'copy:dist',
        'cdnify',
        'cssmin',
        'uglify',
        'filerev',
        'usemin'//,
        //'htmlmin'
    ]);
    /*Builds project and runs backend*/
    grunt.registerTask('backend', ['express', 'express-keepalive']);

    grunt.registerTask('default', [
        'newer:jshint',
        'test',
        'build'
    ]);
};
