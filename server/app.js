var express = require('express'),
    logger = require('morgan'),
    cookieParser = require('cookie-parser'),
    expressJwt = require('express-jwt'),
    jwt = require('jsonwebtoken'),
    bodyParser = require('body-parser'),
    config = require('./config'),
    db = require('./db-manager')(),
    loggerDB = require('./db-manager')('mongodb://localhost:27017/crmclone-log'),
    graylog2 = require("graylog2"),
    projectsREST = require('./routes/projects'),
    usersREST = require('./routes/users'),
	rolesREST = require('./routes/roles'),
    organizationsREST = require('./routes/organizations'),
    proposalsREST = require('./routes/proposals'),
    commentsREST = require('./routes/comments'),
    feedbacksREST = require('./routes/feedbacks'),
	mapsREST = require('./routes/maps'),
    reportsREST = require('./routes/reports'),
    tvsREST = require('./routes/tvs'),
    tv2sREST = require('./routes/tv2s'),
    app = express(),
    winston = require('winston'),
    fs = require('fs'),
    loggerPath,
    multer  = require('multer'),
    pdf = require('html-pdf');
    //htmlDocx = require('html-docx-js');

/*Access control*/
app.all("*", function(req, res, next) {
    "use strict";
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS");
    res.header("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,Cache-Control,Authorization");
    next();
});

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

var path = require('path'),
    nodemailer = require('nodemailer'),
    smtpTransport = require('nodemailer-smtp-transport'),
    mime = require('mime');

templatesDir = path.resolve(__dirname, 'templates');
emailTemplates = require('email-templates');

Promise = require('promise');
diff = require('deep-diff').diff;

CONFIG = {
    mailUser: 'noreply@abn-consult.ru',
    mailPassword: '123456Qq'
};

transporter = nodemailer.createTransport(smtpTransport({
    host: 'smtp.yandex.ru',
    port: 465,
    auth: {
        user: CONFIG.mailUser,
        pass: CONFIG.mailPassword
    },
    secure: true
}));

domainAddr = 'http://localhost';
//Использовать в случае возникновения проблем
defaultAddr = 'boltenkov@f-case.ru';

//Планировщик
var schedule = require('node-schedule');
//SMS-шлюз
sms = require('./sms');

//TODO: Получать из DB
var statusesForCheck = [
    {id: '217805e2-b83c-11e4-b249-cb8db56ea811', title: 'оценка'},
    {id: '217805eb-b83c-11e4-b249-cb8db56ea811', title: 'согласование в СРО'}
];

//Люди для уведомления
var notifyUsers = {
    'abn': [
        'Андрей Савенков',
        'Алексей Савельев',
        'Павел Шмырёв',
        'Алексей Корягин',
        'Дмитрий Никитин',
        'Максим Болтенков'
    ],
    businessKrug: [
        'Андрей Савенков',
        'Алексей Савельев',
        'Павел Шмырёв',
        'Елена Лопатина',
        'Мигран Назаретян',
        'Алексей Корягин',
        'Дмитрий Никитин',
        'Максим Болтенков'
    ]
};

/*
    Функция получения пользователей по списку заданному пользователем
* */
var loadRealUsers = function(usersList) {
    var res = [];
    return new Promise(function(resolve, reject) {
        db.find('users', {}, function(err, users) {
            if (!err) {
                usersList.forEach(function(userName) {
                    var user = users.filter(function(u) {
                        return u.fullName === userName;
                    })[0];
                    if (user) {
                        res.push(user);
                    }
                });
                resolve(res)
                return res;
            } else {
                console.log('Error users retrieving');
                reject('users not found');
            }
        }, null);
    });
};

/*
    Функция проверки статусов
    Запускается каждую 1, 31 миунут, с 8-18-и часов, с понедельника по пятницу
* */
var checkStatuses = schedule.scheduleJob('1,30 8-18 * * 1-5', function(){
    //Вытаскиваем все заявки
    var criteria = { projectId: '55a3708e043dcaac1570122b' };
    var projectName = 'МОСИНЖПРОЕКТ';
    db.find('proposals', criteria, function(err, proposals) {
        if (!err) {
            var today = new Date();
            var logSmsNotification = function(err, doc) {
                var logEntry = {
                    projectName: projectName,
                    userEmail: 'system@f-case.ru',
                    userName: 'Система',
                    host: domainAddr,
                    time: new Date(),
                    type: types['smsNotification'],
                    objectId: (doc) ? doc._id.toHexString() : '',
                    objectName: (doc) ? doc.number : ''
                };

                if (!err) {
                    grayLogBusiness.log('Sms-уведомление по заявке успешно отправлено', logEntry);
                } else {
                    grayLogBusiness.log('Не удалось отправить sms-уведомление по заявке', logEntry);
                }
            };

            var abnId = getOrganizationId('АБН');
            var businessKrugId = getOrganizationId('Бизнес-КРУГ');

            //Загружаем списки реальных пользователей один раз за сессию
            var notifyRealUsers = {
                'abn': [],
                'businessKrug': []
            };

            var usersRetrieving = [
                loadRealUsers(notifyUsers.abn),
                loadRealUsers(notifyUsers.businessKrug)
            ];

            Promise.all(usersRetrieving).then(function(values){
                notifyRealUsers.abn = values[0];
                notifyRealUsers.businessKrug = values[1];
            }).then(function() {
                statusesForCheck.forEach(function(st) {
                    var standardDurationDays = getStandardDuration(st.title);
                    proposals.forEach(function(prop) {
                        if (prop.tasks && prop.tasks.length>0) {
                            var task = prop.tasks.filter(function(t){
                                return t.status.id === st.id;
                            })[0];

                            if (task && task.creationDate) {
                                var creationDate = new Date(task.creationDate);
                                var finishingDate = (task.finishingDate) ? new Date(task.finishingDate) : undefined;
                                var dueDate = (task.dueDate) ? task.dueDate : undefined;
                                var planedDueDate = (dueDate) ? dueDate : creationDate.setDate(creationDate.getDate()+standardDurationDays);

                                //Статус не закрыт
                                if (!(task.finished && finishingDate)) {
                                    var diff1 = planedDueDate - today;
                                    var restDaysNum = (diff1 > 0) ? Math.ceil(diff1 / (1000 * 3600 * 24)) : 0;
                                    if (restDaysNum === 1 && !task.smsNotified) {
                                        //Получаем список людей для отправки sms-уведомлений
                                        var realUsers = [];
                                        switch(prop.orgId) {
                                            default: case abnId: {
                                                realUsers = notifyRealUsers.abn;
                                                break;
                                            }
                                            case businessKrugId: {
                                                realUsers = notifyRealUsers.businessKrug;
                                                break;
                                            }
                                        }

                                        if (realUsers && realUsers.length > 0) {
                                            var propNumber = prop.number;
                                            //var propName = prop.address;
                                            var statusName = task.status.title;

                                            var smsText = projectName + ', №' + propNumber + ':\r\n' +
                                                'до окончания этапа [' + statusName + '] остался 1 день';

                                            var smsAny = false;
                                            realUsers.forEach(function(user) {
                                                if (user.phone) {
                                                    sms(user.phone, smsText);
                                                    console.log((new Date()) + '. SMS: 1 day rest for proposal: ' + prop.number + ', ' + user.fullName);
                                                    smsAny = true;
                                                }
                                            });

                                            if (smsAny) {
                                                //Обновляем заявку
                                                //TODO: Возможно сделать разделение по тем кому отправлено sms-уведомление
                                                task.smsNotified = true;
                                                db.replaceOne('proposals', prop._id, prop, logSmsNotification);
                                            }
                                        }
                                    }
                                    //Если нужно будет оповещать о просрочке
                                    //var overDaysNum = (diff1 > 0) ? 0 : Math.ceil( Math.abs(diff1) / (1000 * 3600 * 24));
                                }
                            }
                        }
                    });
                });
            });
        } else {
            console.log(err);
        }
    }, null);
});

RenderTemplate = function(locals) {
    this.locals = locals;
    this.send = function(err, html, text) {
        if (err) {
            console.log(err);
        } else {
            transporter.sendMail({
                from: 'CRM DGI15 <' + CONFIG.mailUser + '>',
                to: locals.email,
                subject: locals.subject,
                html: html,
                text: text
            }, function(err, responseStatus) {
                if (err) {
                    console.log(err);
                } else {
                    console.log(responseStatus.response);
                }
            });
        }
    };
    this.batch = function(batch) {
        batch(this.locals, templatesDir, this.send);
    };
};

/*sendNotification = function(to, subject, message) {
    transporter.sendMail({
        from: 'CRM DGI <dgi15@abn-consult.ru>',
        to: to,
        subject: subject,
        text: message
    });
}*/

/**
 * Returns function< which sends bad request with given response;
 * @param   {Object} res
 * @returns {Function}
 */
send400 = function (res) {
    return function () {
        res.status(400);
        res.json({
            message: 'Bad request'
        });
        res.end();
    };
};

var grayLogApi = new graylog2.graylog({
    servers: [
        { 'host': '5.189.140.28', port: 12201 }
    ],
    hostname: 'localhost',
    facility: 'crm.local.api',
    bufferSize: 1350
});

grayLogApi.on('error', function (error) {
    console.error('Error while trying to write to grayLogApi:', error);
});

grayLogBusiness = new graylog2.graylog({
    servers: [
        { 'host': '5.189.140.28', port: 12201 }
    ],
    hostname: 'localhost',
    facility: 'crm.local.business',
    bufferSize: 1350
});

grayLogApi.on('error', function (error) {
    console.error('Error while trying to write to grayLogBusiness:', error);
});

Object.prototype.clone = function() {
    if (null == this || "object" != typeof this) return this;
    var copy = this.constructor();
    for (var attr in this) {
        if (this.hasOwnProperty(attr)) copy[attr] = this[attr];
    }
    return copy;
};

isEmpty = function(obj) {
    if (obj == undefined) return true;
    for(var key in obj) {
        if (obj.hasOwnProperty(key)) {
            return false;
        }
    }
    return true;
};

getIpAddress = function (ipStr) {
    var delimiter = ipStr.indexOf(':');
    return (delimiter > 0) ? ipStr.substr(0, delimiter) : ipStr;
};

//TODO: Вынести все Id в конфигурационный файл или считывать из БД
//+ вынести в Rest для инициализации в клиенте

organizations = [
    {id: '54c53693a8a6e6d9a0866e0a', name: 'АБН', root: true},
    {id: '54c5eaa45079053c0350d085', name: 'ДГИМ', root: true},
    {id: '55372317852193b41657450f', name: 'СРО', root: true},
    {id: '5555c43c52c591d01b76ab3c', name: 'ДГС', root: false},
    {id: '5583e61d8c80873021f31b50', name: 'УГС', root: false},
    {id: '55a52e10e3d3a94818105980', name: 'Бизнес-КРУГ', root: false},
    {id: '55a66bd3369a9b8c1d8ae45a', name: 'Мосинжпроект', root: true},
    {id: '55ad04a658e88f78186a5ab6', name: 'ДГП', root: true},
    {id: '55b5f095dd994d081b4defda', name: 'МПСИ', root: false}
];

projects = [
    {id: '54c53693a8a6e6d9a0866e0b', name: 'ФЗ-159', orgMode: false},
    {id: '54c5eb025079053c0350d086', name: 'Инвестпроекты', orgMode: false},
    {id: '54c5ffeafedbcc2003db68c1', name: 'Аренда', orgMode: false},
    {id: '54f98d32c5f295d81fab6e3b', name: 'ЕИРЦ', orgMode: false},
    {id: '5510284bca989d5812930b21', name: 'ФЗ-178', orgMode: false},
    {id: '5555c4a152c591d01b76ab51', name: 'Гаражи. Продажа', orgMode: false},
    {id: '5555c4d652c591d01b76ab5f', name: 'Гаражи. Аренда', orgMode: false},
    {id: '5620de94bae766d41774ae9f', name: 'Гаражи. Залоги', orgMode: false},
    {id: '5583e7db8c80873021f31b6a', name: 'УГС (Компас)', orgMode: false},
    {id: '55a3708e043dcaac1570122b', name: 'МОСИНЖПРОЕКТ', orgMode: true},
    {id: '55b5f20fdd994d081b4df0a5', name: 'МПСИ', orgMode: false},
    {id: '55a79230d9853f100fc236ec', name: 'ОБМЕН', orgMode: false},
    {id: '5625f1b823bc3e840bcdb0e7', name: '108-15Б', orgMode: false},
    {id: '563b6343652913780c24dde2', name: 'ФЗ-178 [2]', orgMode: false},
    {id: '5662e295294b86c4876ee7fd', name: 'ФЗ-178 [2]', orgMode: true}
];

defaultDuration = 1;
statusesForReport = [
    {name: 'оценка', standardDuration: 1},
    {name: 'согласование в СРО', standardDuration: 1},
    {name: 'повторное согласование в СРО', standardDuration: 1},
    {name: 'замечания от СРО', standardDuration: 1},
    {name: 'согласование в ДГИМ', standardDuration: 1},
    {name: 'повторное согласование в ДГИМ', standardDuration: 1},
    {name: 'замечания от ДГИМ', standardDuration: 1},
    {name: 'проверка', standardDuration: 1},
    {name: 'исправление', standardDuration: 1},
    {name: 'проверка после исправления', standardDuration: 1}
];

getStandardDuration = function(name) {
    var status = statusesForReport.filter(function(st) {
        return st.name == name;
    })[0];
    return (status) ? status.standardDuration : defaultDuration;
};

getOrganizationName = function (orgId) {
    var org = organizations.filter(function(o) {
        return o.id === orgId;
    })[0];
    return (org) ? org.name : 'unknown';
};

getOrganizationId = function (orgName) {
    var org = organizations.filter(function(o) {
        return o.name === orgName;
    })[0];
    return (org) ? org.id : '';
};

getProjectName = function (projectId) {
    var project = projects.filter(function(p){
        return p.id === projectId;
    })[0];
    return (project) ? project.name : 'unknown';
};

isRootOrg = function(orgId) {
    var org = organizations.filter(function(o) {
        return o.id === orgId;
    })[0];
    return (org) ? org.root : false;
};

isProjectOrgMode = function(projectId) {
    var project = projects.filter(function(p){
        return p.id === projectId;
    })[0];
    return (project) ? project.orgMode : false;
};

getProposalNum = function(proposalId) {
    return new Promise(function(resolve, reject) {
        db.findOne('proposals', db.idFromHexString(proposalId), function (err, doc) {
            if (!err) {
                if (!doc.number)
                    doc.number = '-';

                if (doc.projectId==='5510284bca989d5812930b21')
                    doc.number += '-178';
                else if (doc.projectId==='563b6343652913780c24dde2')
                    doc.number += '-178-2';
                else if (doc.projectId==='5625f1b823bc3e840bcdb0e7')
                    doc.number += '-108-15Б';
                else if (doc.projectId==='5555c4a152c591d01b76ab51')
                    doc.number += '-ГП';
                else if (doc.projectId==='5555c4d652c591d01b76ab5f')
                    doc.number += '-ГА';

                resolve(doc.number);
            } else {
                reject('not found');
            }
        });
    });
};

types = {
    login: 'login',
    smsCheck: 'smsCheck',
    view: 'view',

    viewProposalsList: 'viewProposalsList',
    saveProposal: 'saveProposal',
    deleteProposal: 'deleteProposal',
    pushToCrm: 'pushToCrm',
    pushFromCrm: 'pushFromCrm',

    uploadFileCloud: 'uploadFileCloud',
    downloadFileCloud: 'downloadFileCloud',

    newComment: 'newComment',

    smsNotification: 'smsNotification',

    viewTv: 'viewTv'
};

commentTypes = [
    {name: 'Обычно', code: 'success'},
    {name: 'Обычно2', code: 'info'},
    {name: 'Важно', code: 'warning'},
    {name: 'Критично', code: 'danger'}
];

getCommentTypeStr = function(code) {
    var res = 'Обычно';
    commentTypes.forEach(function(type) {
        if (type.code === code) {
            res = type.name;
        }
    });
    return res;
};

formUserEntryLog = function(req, type) {
    var body = (req.body) ? req.body.clone() : {};
    if (body.password)
        delete body['password'];

    return {
        url: req.url,
        userEmail: (req.user) ? req.user.email : '',
        userName: (req.user) ? req.user.fullName : '',
        userPosition: (req.user) ? req.user.position : '',
        userOrganizationId: (req.user) ? req.user.organizationId : '',
        userOrganization: (req.user) ? getOrganizationName(req.user.organizationId) : '',
        userAgent: (req.headers) ? req.headers['user-agent'] : '',
        ip: (req.headers['x-forwarded-for']) ? getIpAddress(req.headers['x-forwarded-for']) : req._remoteAddress,
        host: (req.headers) ? req.headers['host'] : '',
        origin: (req.headers) ? req.headers['origin'] : '',
        referer: (req.headers) ? req.headers['referer'] : '',
        time: new Date(),
        body: body,
        type: (type) ? type : ''
    };
};

function getUserHome() {
    return process.env[(process.platform == 'win32') ? 'USERPROFILE' : 'HOME'];
}

var createLoggerDir = function() {
    if (process.platform === 'win32') {
        loggerPath = getUserHome() + '/crm_logger/';
    } else {
        loggerPath = getUserHome() + '/crm_logger/';
    }
    if (!fs.existsSync(loggerPath)) {
        fs.mkdirSync(loggerPath);
    }
};
createLoggerDir();

var winstonLogger = new(winston.Logger)({
    transports: [
        new(winston.transports.File)({
            name: 'info-file',
            filename: loggerPath + 'crm-info.log',
            level: 'info'
        }),
        new(winston.transports.File)({
            name: 'error-file',
            filename: loggerPath + 'crm-error.log',
            level: 'error'
        })
    ]
});

var winstonLog = function(req, logType, error) {
    var msg;
    var errStack = null;
    var logUser = null;
    if (req && logType) {
        if (req.user && req.user.fullName && req.user.email) {
            msg = 'QUERY: ( Method: ' + req.method + ';    URL: ' + req.url + ';    USER: ' + req.user.fullName + ' (' + req.user.email + ');)';
        } else {
            msg = 'QUERY: ( Method: ' + req.method + ';    URL: ' + req.url + ')';
        }
        if (error) {
            errStack = error.stack;
            msg += 'ERROR: ' + errStack;
        }
        if(req.user){
            logUser = {
                name: req.user.fullName,
                email: req.user.email,
                roles: req.user.roles,
                position: req.user.position,
                organizationId: req.user.organizationId,
                organizationName: getOrganizationName(req.user.organizationId)
            }
        }

        var body = (req.body) ? req.body.clone() : {};
        if (body.password)
            delete body['password'];

        var logEntryDb = {
            message: msg,
            user: logUser,
            url: req.url,
            method: req.method,
            type: logType,
            ip: (req.headers['x-forwarded-for']) ? getIpAddress(req.headers['x-forwarded-for']) : req._remoteAddress,
            time: new Date(),
            error: errStack,
            body: body
        };

        var logEntry = {
            message: msg,
            userName: (req.user) ? req.user.fullName : '',
            userEmail: (req.user) ? req.user.email : '',
            userPosition: (req.user) ? req.user.position : '',
            userOrganizationId: (req.user) ? req.user.organizationId : '',
            userOrganization: (req.user) ? getOrganizationName(req.user.organizationId) : '',
            url: req.url,
            method: req.method,
            type: logType,
            ip: (req.headers['x-forwarded-for']) ? getIpAddress(req.headers['x-forwarded-for']) : req._remoteAddress,
            time: new Date(),
            error: errStack,
            body: body
        };

        winstonLogger.log(logType, msg);
        grayLogApi.log(msg, logEntry);
        loggerDB.insertOne('log', logEntryDb, function(err){
            if(err){
                console.log(err);
            }
        });
    }
};

/* Secret key */
var secret = 'MyCatDidSomeStepsOnKeyboard';

app.use('/api/', expressJwt({
    secret: secret
}).unless({
    path: ['/api/users/login','/api/users/smscheck', '/api/projects/pushFromCrm']
}));

app.use(logger('dev'));
app.use(multer({inMemory: true}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser());
app.set('view engine', 'jade');



app.use(function(req, res, next) {
    winstonLog(req, 'info');
    next();
});

app.post('/push2DIM',function(req,res,next){
    req.url = '/api/projects/pushFromCrm';
    next();
});

var downloadPdf = function(req, res) {
    if (!(req.body.htmlContent && req.body.htmlContent.length!=0)) {
        return;
    }
    var html = req.body.htmlContent;

    var fileName = 'File.pdf';
    if (!(req.body.docName && req.body.docName.length!=0)) {
        fileName = req.body.docName + '.pdf';
    }

    var options = { format: 'Letter' };
    var fileNamePath = './' + req.body.docName + '.pdf';
    pdf.create(req.body.htmlContent, options).toFile(fileNamePath, function(err, result) {
        if (err) return console.log(err);
        console.log(result); // { filename: '/app/test.pdf' }

        var filename = path.basename(result.filename);
        var mimetype = mime.lookup(result.filename);

        res.setHeader('Content-disposition', 'attachment; filename=' + filename);
        res.setHeader('Content-type', mimetype);

        var filestream = fs.createReadStream(result.filename);
        filestream.pipe(res);
    });
    //var mimetype = 'application/pdf';
    /*pdf.create(html).toStream(function(err, res) {
        if (err) return console.log(err);
        //var filestream = fs.createReadStream(file);
        res.pipe(fs.createWriteStream('./' + fileName));
    });*/


};

app.post('/utils/downloadPdf', downloadPdf);

app.use('/api/projects', projectsREST(db));
app.use('/api/users', usersREST(db, jwt, secret));
app.use('/api/roles', rolesREST(db));
app.use('/api/organizations', organizationsREST(db));
app.use('/api/projects', proposalsREST(db));
app.use('/api/comments', commentsREST(db));
app.use('/api/feedbacks', feedbacksREST(db));
app.use('/api/projects', mapsREST(db));
app.use('/api/reports', reportsREST(db));
app.use('/api/tvs', tvsREST(db));
app.use('/api/tv2s', tv2sREST(db));


//catch 404 and forward to error handler
// app.use(function(req, res, next) {
//     'use strict';
//     console.log("404");
//     var err = new Error('Not Found');
//     err.status = 404;
//     next(err);
// });

// app.use(function(err, req, res, next) {
//     if (err) {
//         winstonLog(req, 'error', err);
//         res.status(404);
//         next(err);
//     } else {
//         next();
//     }
// });


module.exports = app;
