var nconf = require('nconf'),
	path = require('path');

nconf.argv();
/* parse test.config.json if --test flag added */
if(nconf.get('test')){
    nconf.env()
    .file({ file: path.join(__dirname,'test.config.json') });
}else{
    nconf.env()
    .file({ file: path.join(__dirname,'config.json') });
}



module.exports = nconf;