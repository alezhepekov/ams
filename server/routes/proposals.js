'use strict';
var express = require('express');
var apiFactory = require('./restFactory');
var clean = require('../db-manager/cleaners');
var ObjectId = require('mongodb').ObjectID;
var db = require('../db-manager');
var request = require('request');
var SOAP = require('../SOAP');
var uuid = require('node-uuid');

var buildOptions = function(req) {
    var options = {
        limit: 20,
        skip: 0,
        sort: {
            started: -1
        }
    };
    if (req.query.page) {
        options.skip = --req.query.page * 20;
    }
    if (req.query.sortBy) {
        options.sort = {}
        options.sort[req.query.sortBy] = 1;
        if (req.query.reversed === 'true') {
            options.sort[req.query.sortBy] = -1;
        }
    }
    return options;
};

function UTCToLocalTime( utc ) {
    var timezoneOffset = ( new Date() ).getTimezoneOffset() * 60000;
    return new Date( utc - timezoneOffset );
}

//var now = UTCToLocalTime( Date.now() ).toISOString().replace( /T/, " " ).replace( /Z/, "" ); // .replace( /\..+/, "" )

var proposalsAPIFactory = function(db) {

    /**
     * To enable full txt search, run
     * db.proposals.ensureIndex({name: "text", number:"text", address: "text", decription:"text"},{default_language:"russian"})
     */

    var _getAllProposals = function(req, res, next, options) {
        db.findOne('projects', db.idFromHexString(req.params.projectId), function(err, project) {
            try {
                if (!err) {
                    var role = null;
                    if (!req.user.isRoot) {
                        var userRole = req.user.roles.filter(function (r) {
                            return r.projectId == req.params.projectId;
                        })[0];
                        /*if user has no role in the project and is not root, he cannot see proposals*/
                        if (userRole == undefined) {
                            res.status(403);
                            res.end();
                            return;
                        }
                        role = project.roles.filter(function (r) {
                            return r.id == userRole.roleId;
                        })[0];
                        /* If user has no role in the project and isn't root, send him 403 */
                        if (role == null) {
                            res.status(403);
                            res.end();
                            return;
                        }
                    }


                    var criterion = {$and: [], $or: []};
                    criterion.projectId = req.params.projectId;
                 
                    if ( req.query.searchString ) {
	                    var searchString = req.query.searchString;
	                    var re = searchString.replace( " ", ".*" );
                        re = ".*" + re + ".*";
                        
                        var mongoDBQuery = {};
                        if ( req.query.searchContext === "Project*" ) {
                             mongoDBQuery = {
                                $or: [
                                    { name: {
                                        $regex : re,
                                        $options: "i"
                                    } },
                                    { number: {
                                        $regex : re,
                                        $options: "i"
                                    } }                                   
                                ]
                            };

                        }
                        else if ( req.query.searchContext === "Project*.Document*" ) {
                             mongoDBQuery = {
                                documents: {
                                    $elemMatch: {
                                        name: {
                                            $regex : re,
                                            $options: "i"
                                        }
                                    }
                                } };
                        }
                        else if ( req.query.searchContext === "Project*.Task*" ) {
                             mongoDBQuery = {                             
                                tasks: {
                                    $elemMatch: {
                                        "status.name": {
                                            $regex : re,
                                            $options: "i"
                                        }
                                    }
                                }
                             };
                        }
                        else {
                            console.error("Недопустимый контекст поиска");
                            res.status(404);
                            res.end();
                            return;
                        }                     

                        criterion.$and.push( mongoDBQuery );
                    }

                    if ( req.query.projectType ) {
                        criterion["type.id"] = req.query.projectType;
                    }

                    if ( req.query.manager ) {
                        criterion["manager._id"] = req.query.manager;
                    }
                     
                    if ( req.query.owner ) {
                        criterion["owner._id"] = req.query.owner;
                    }
              
                    var dateCreatedStart = null;
                    var dateCreatedEnd = null;
                    if ( req.query.dateCreatedStart ) {
                        dateCreatedStart = req.query.dateCreatedStart;
                    }
                    if ( req.query.dateCreatedEnd ) {                      
                        dateCreatedEnd = req.query.dateCreatedEnd;                                        
                    }
                    if ( dateCreatedStart && dateCreatedEnd ) {
                        criterion.$and.push({
                            started: {
                                $gte: dateCreatedStart,
                                $lt: dateCreatedEnd
                            }
                        });
                    } else if ( dateCreatedStart ) {
                        criterion.$and.push({
                            started: {
                                $gte: dateCreatedStart
                            }
                        });
                    } else if ( dateCreatedEnd ) {
                        criterion.$and.push({
                            started: {
                                $lt: dateCreatedEnd
                            }
                        });
                    }

                    var dateFinishedStart = null;
                    var dateFinishedEnd = null;
                    if ( req.query.dateFinishedStart ) {
                        dateFinishedStart = req.query.dateFinishedStart;
                    }
                    if ( req.query.dateFinishedEnd ) {
                        dateFinishedEnd = req.query.dateFinishedEnd;
                    }
                    if ( dateFinishedStart && dateFinishedEnd ) {
                        criterion.$and.push({
                            dueDate: {
                                $gte: dateFinishedStart,
                                $lt: dateFinishedEnd
                            }
                        });
                    } else if ( dateFinishedStart ) {
                        criterion.$and.push({
                            dueDate: {
                                $gte: dateFinishedStart
                            }
                        });
                    } else if ( dateFinishedEnd ) {
                        criterion.$and.push({
                            dueDate: {
                                $lt: dateFinishedEnd
                            }
                        });
                    }

                    if (criterion.$and.length==0)
                        delete criterion.$and;

                    if (criterion.$or.length==0)
                        delete criterion.$or;

                    _getProposals(req, res, next, options, criterion);

                } else {
                    res.status(500);
                    res.end();
                }
            } catch (e) {
                next(e);
            }
        });
    }

    var _getProposals = function(req, res, next, options, criterion){
        db.find('proposals', criterion, function(err, proposals) {
            try {
                if (!err) {
                    db.collection("proposals").count(criterion, function(err, q) {
                        try {
                            if (!err) {
                                res.json({
                                    proposals: proposals,
                                    quantity: q
                                });
                            } else {
                                res.status(500);
                                res.end();
                            }
                        } catch (e) {
                            next(e);
                        }
                    });
                } else {
                    res.status(500);
                    res.end();
                }
            } catch (e) {
                next(e);
            }
        }, options);
    }

    var getProposals = function(req, res, next) {
        _getAllProposals(req, res, next, buildOptions(req));
        var logEntry = formUserEntryLog(req, types['viewProposalsList']);
        logEntry.objectId = (req.params.projectId) ? req.params.projectId : '';
        var projectName = (req.params.projectId) ? getProjectName(req.params.projectId) : '';
        logEntry.projectName = projectName;
        grayLogBusiness.log('Просмотр всех заявок проекта ' + projectName, logEntry);
    }

    var getAllProposals = function(req, res, next) {
        _getAllProposals(req, res, next, null);
        var logEntry = formUserEntryLog(req, types['viewProposalsList']);
        logEntry.objectId = (req.params.projectId) ? req.params.projectId : '';
        var projectName = (req.params.projectId) ? getProjectName(req.params.projectId) : '';
        logEntry.projectName = projectName;
        grayLogBusiness.log('Просмотр всех заявок проекта ' + projectName, logEntry);
    }

    var saveProposal = function(req, res) {
        var doc = req.body;

        doc.projectGroupList = [];
        doc.projectGroupList.push(doc.owner);

        var tmparr = [];
        tmparr.push(doc.owner.email);
	    //TODO: Разделять проект на DGI15, MIP и AMS и выбирать соответствующий алгоритм
        for (var i=0; i<doc.tasks.length; i++)
        {
            doc.tasks[i].creationDate = ( ( doc.tasks[i].creationDate && doc.tasks[i].creationDate.length > 0 ) ? ( doc.tasks[i].creationDate ) : ( null ) );
            doc.tasks[i].dueDate = ( ( doc.tasks[i].dueDate && doc.tasks[i].dueDate.length > 0 ) ? ( doc.tasks[i].dueDate ) : ( null ) );
            doc.tasks[i].owner = doc.owner;

            if(doc.tasks[i].executor && tmparr.indexOf(doc.tasks[i].executor.email) < 0) {
                doc.projectGroupList.push(doc.tasks[i].executor);
                tmparr.push(doc.tasks[i].executor.email);
            }
            //console.log(doc);
        }

        //TODO: Сделать по другому фильтрацию для ДГИМ
        var dgimId = getOrganizationId('ДГИМ');

        var logEntry = formUserEntryLog(req, types['saveProposal']);
        logEntry.objectId = doc._id;
        var projectName = (req.params.projectId) ? getProjectName(req.params.projectId) : '';
        logEntry.projectName = projectName;

        var onSuccInsert = function(err, doc) {
            if (!err) {
                res.status(200);
                res.json(doc);
                var msg = ''
                var docNum = (doc.number) ? doc.number : '-';
                if ('POST' == req.method) {
                    msg = 'Успешное сохранение заявки №' + docNum;
                } else if ('PUT' == req.method) {
                    msg = 'Успешное обновление заявки №' + docNum;
                }
                grayLogBusiness.log(msg, logEntry);
            } else {
                res.status(500);
                res.json({
                    message: 'Internal Server Error'
                });
                grayLogBusiness.log('Ошибка сервера во время сохранения заявки', logEntry);
            }
        };

        try {
            var cleaned = doc;
            /*var cleaned = clean.proposal(doc);
            if (cleaned.currentTask && cleaned.currentTask.status && cleaned.currentTask.status.title === 'согласование в ДГИМ' && cleaned.currentTask.finished &&
                cleaned.projectId!='5555c4a152c591d01b76ab51' && cleaned.projectId!='5555c4d652c591d01b76ab5f') { //temp solution for garages
                console.log('status update');
                cleaned.status = {
                    "next": [
                        "21e1232a-a781-11e4-86d2-91fe118cc3c1",
                        "21e12327-a781-11e4-86d2-91fe118cc3c1"
                    ],
                    "autoset": true,
                    "className": "primary",
                    "roles": [{
                        "right": "view",
                        "role": "оценщик"
                    }, {
                        "right": "view",
                        "role": "ассистент"
                    }, {
                        "right": "view",
                        "role": "курьер"
                    }, {
                        "right": "edit",
                        "role": "супервайзер"
                    }, {
                        "right": "edit",
                        "role": "координатор"
                    }, {
                        "right": "view",
                        "role": "СРО"
                    }, {
                        "right": "view",
                        "role": "ДГИМ"
                    }, {
                        "right": "edit",
                        "role": "проверяющий"
                    }],
                    "id": "21e12328-a781-11e4-86d2-91fe118cc3c1",
                    "title": "согласование в СРО"
                }
            }*/

            var findChanges = function (err, doc) {
                logEntry.changes = diff(doc, cleaned);
                var tasksChanges = logEntry.changes.filter(function(c) { //currentTask
                    return c.path[0] == 'tasks';// && c.path[2] == 'executor';
                });

                var tasksExecutorChanges = tasksChanges.filter(function(c) { //currentTask
                    return c.path[2] == 'executor';
                });

                //Добавили новую задачу или изменили текущую
                if (tasksExecutorChanges && tasksExecutorChanges.length > 0) {
                    //Определяем измененную задачу
                    var changedTaskId = tasksChanges.filter(function(c) {
                        return c.path[2] == 'id';
                    })[0];

                    //TODO: Сделать вариант множественного изменения (может быть несколько измененных задач одновременно)
                    if (changedTaskId) {
                        var changedTask = cleaned.tasks.filter(function(c) {
                            return c.id == changedTaskId.rhs;
                        })[0];

                        if (changedTask) {
                            var orgMode = isProjectOrgMode(req.params.projectId);
                            var propNumber = cleaned.number;
                            var propName = (orgMode) ? cleaned.name : '';

                            var phoneNum = (changedTask.executor.organizationId!==dgimId) ? changedTask.executor.phone : '';
                            var description = changedTask.description;
                            var statusName = changedTask.status.title;

                            var smsText = projectName + ', №' + propNumber + (propName ? '. ' + propName : '') + '\r\n';
                            smsText += (description.length > 0)
                                ? 'На вас назначено новое задание [' + statusName + ']: ' + description
                                : 'На вас назначено новое задание [' + statusName + '].';

                            if (phoneNum) {
                                sms(phoneNum, smsText);
                            }
                        }
                    }
                }
            };

            if ('POST' == req.method) {
                db.insertOne('proposals', cleaned, onSuccInsert);
            } else if ('PUT' == req.method) {
                logEntry.body = {};
                //Ищем изменения
                db.findOne('proposals', db.idFromHexString(cleaned._id), findChanges);
                //Обновляем заявку
                db.replaceOne('proposals', db.idFromHexString(cleaned._id), cleaned, onSuccInsert);
            }
            if (cleaned.currentTask && !cleaned.currentTask.finished && cleaned.currentTask.status && cleaned.currentTask.status.sendSMS) {
                if (cleaned.currentTask.executor && cleaned.currentTask.executor.organizationId!==dgimId &&
                    cleaned.currentTask.executor.phone) {
                    var text = "Заявке " + cleaned.number + " присвоен статус " + cleaned.status.title;
                    sms(cleaned.currentTask.executor.phone, text);
                }
            }
        } catch (ex) {
            console.log(ex);
            res.status(400);
            res.json({
                message: ex
            });
            grayLogBusiness.log('Ошибка сервера во время сохранения заявки: ' + ex, logEntry);
        }
    };

    var deleteProposal = function(req, res) {
        var logEntry = formUserEntryLog(req, types['deleteProposal']);
        logEntry.objectId = (req.params.id) ? req.params.id : '';
        var projectName = (req.params.projectId) ? getProjectName(req.params.projectId) : '';
        logEntry.projectName = projectName;
        db.findOne('projects', db.idFromHexString(req.params.projectId), function(err, project) {
            var id = db.idFromHexString(req.params.id);
            try {
                if (!err) {
                    var userRole = req.user.roles.filter(function(r) {
                        return r.projectId == req.params.projectId;
                    })[0];
                    /*if user has no role in the project and is not root, he cannot remove proposals*/
                    if (!userRole && !req.user.isRoot) {
                        res.status(403);
                        res.end();
                        return;
                    }

                    var role = undefined;
                    if (!req.user.isRoot) {
                        role = project.roles.filter(function(r) {
                            return r.id == userRole.roleId;
                        })[0];
                    }

                    if (req.user.isRoot || (role && role.removeProposals)) {
                        var onSuccDelete = function(err, n) {
                            if (!err) {
                                res.status(200);
                                res.json({
                                    ok: true
                                });
                                grayLogBusiness.log('Успешное удаление заявки', logEntry);
                            } else {
                                res.status(500);
                                res.json({
                                    ok: false
                                });
                                grayLogBusiness.log('Удаление заявки не удалось', logEntry);
                            }
                        };

                        db.deleteOne('proposals', id, onSuccDelete);
                    }
                } else {
                    res.status(500);
                    res.end();
                    grayLogBusiness.log('Ошибка сервера во время удаления заявки: ', logEntry);
                    return;
                }
            } catch (ex) {
                res.status(500);
                res.end();
                grayLogBusiness.log('Ошибка сервера во время удаления заявки: ' + ex, logEntry);
                return;
            }
        });
    };

    /*var pushToCrm = function(req, res) {
        var id = db.idFromHexString(req.params.id);
        var proposal = req.body;
        var logEntry = formUserEntryLog(req, types['pushToCrm']);
        var projectName = (req.params.projectId) ? getProjectName(req.params.projectId) : '';
        logEntry.projectName = projectName;
        logEntry.objectId = id;

        proposal.started = new Date(proposal.started);
        if(proposal.currentTask) {
            if (proposal.currentTask.creationDate)
                proposal.currentTask.creationDate = new Date(proposal.currentTask.creationDate);
            if (proposal.currentTask.dueDate)
                proposal.currentTask.dueDate = new Date(proposal.currentTask.dueDate);
        }
        if(proposal.tasks) {
            proposal.tasks.forEach(function(task) {
                if (task.creationDate) {
                    task.creationDate = new Date(task.creationDate);
                }
                if (task.dueDate) {
                    task.dueDate = new Date(task.dueDate);
                }
            });
        }

        var onSuccUpdate = function(err, doc) {
            if (!err) {
                if (!doc.number)
                    doc.number = '-';

                if (doc.projectId==='5510284bca989d5812930b21')
                    doc.number += '-178';
                else if (doc.projectId==='563b6343652913780c24dde2')
                    doc.number += '-178-2';
                else if (doc.projectId==='5625f1b823bc3e840bcdb0e7')
                    doc.number += '-108-15Б';
                else if (doc.projectId==='5555c4a152c591d01b76ab51')
                    doc.number += '-ГП';
                else if (doc.projectId==='5555c4d652c591d01b76ab5f')
                    doc.number += '-ГА';

                console.log('UPDATED HERE');
                SOAP.submitProposalToDGI(doc, function(error, resp, body) {
                    if (err || !resp) {
                        res.status(500);
                        res.end();
                        return;
                    }
                    if (200 == resp.statusCode) {
                        res.status(200);
                        res.json(doc);
                        res.end();
                        //TODO: Дополнительно сохранять номер в CRM
                        grayLogBusiness.log('Успешная отправка заявки №' + doc.number + ' в CRM', logEntry);
                    } else {
                        res.status(500);
                        res.end();
                        grayLogBusiness.log('Ошибка сервера при отправке заявки №' + doc.number + ' в CRM', logEntry);
                    }
                });
            } else {
                res.status(500);
                res.end();
            }
        }

        if (!proposal.DgiProjectNo) {
            SOAP.requestDGIProjectNo(function(s) {
                proposal.DgiProjectNo = s;
                db.replaceOne('proposals', id, proposal, onSuccUpdate);
            });
        } else {
            db.replaceOne('proposals', id, proposal, onSuccUpdate);
        }

    };

    var pushFromCrm = function(req, res) {
        var id = db.idFromHexString(req.body.DgiID);
        var logEntry = formUserEntryLog(req, types['pushFromCrm']);
        logEntry.objectId = id;
        var projectName = (req.params.projectId) ? getProjectName(req.params.projectId) : '';
        logEntry.projectName = projectName;

        var onSuccFind = function(err, proposal) {

            var onSuccUserFind = function(err, users) {
                if (users.length !== 1) {
                    res.status(400).send("Пользователь не найден");
                } else {
                    var executor = users[0];
                    var inspectionDate = Date.parse(req.body.inspectionDate);
                    if (!inspectionDate) {
                        inspectionDate = new Date();
                    }
                    var inspectionComment = req.body.InspectionComment;


                    // Find inspection task and update it
                    for (var i = 0; i < proposal.tasks.length; i++) {
                        if (proposal.tasks[i].status.title === 'осмотр' && !proposal.tasks[i].finished) {
                            var nextId = proposal.tasks[i].status.next[0];
                            proposal.tasks[i].finished = true;
                            proposal.tasks[i].finishingDate = inspectionDate;
                            proposal.tasks[i].executor = executor;
                            proposal.tasks[i].description = inspectionComment;
                            proposal.currentTask = proposal.tasks[i];
                        }
                    }

                    // Find project, to get next status
                    var onProjectFind = function(err, project) {
                        if (!err) {

                            var onSuccProposalUpdate = function(err) {
                                if (!err) {
                                    res.status(200);
                                    res.end();
                                    grayLogBusiness.log('Успешное получение заявок из CRM', logEntry);
                                } else {
                                    res.status(500);
                                    res.end();
                                    grayLogBusiness.log('Ошибка сервера при получении заявок из CRM', logEntry);
                                }
                            };

                            var status = project.statuses.filter(function(st) {
                                return st.id == nextId;
                            })[0];
                            proposal.status = status;

                            if (!proposal.documents) {
                                proposal.documents = [];
                            }
                            if (req.body.DocsLocation) {
                                var docs = {};
                                docs.id = uuid.v1();
                                docs.link = req.body.DocsLocation;
                                docs.text = "документы";
                                docs.user = req.body.AssistantName;
                                docs.date = new Date();
                                proposal.documents.push(docs);
                            }
                            if (req.body.PhotosLocation) {
                                var docs = {};
                                docs.id = uuid.v1();
                                docs.link = req.body.PhotosLocation;
                                docs.text = "фото";
                                docs.user = req.body.AssistantName;
                                docs.date = new Date();
                                proposal.documents.push(docs);
                            }
                            // Save proposal
                            db.replaceOne('proposals', id, proposal, onSuccProposalUpdate);

                        } else {
                            res.status(500).send("Внутренняя ошибка сервера");
                        }
                    };

                    db.findOne('projects', db.idFromHexString(proposal.projectId), onProjectFind);
                }
            }

            if (!proposal) {
                res.status(404).send("Заявка не определена");
                return;
            }

            if (!err && proposal.DgiProjectNo === req.body.ProjectNo) {
                var name0 = req.body.AssistantName.split(' ')[0];
                var name1 = req.body.AssistantName.split(' ')[1];
                // Find user to check his existing
                var criteria = {
                    $or: [{
                        firstName: name0,
                        lastName: name1
                    }, {
                        firstName: name1,
                        lastName: name0
                    }]
                };
                db.find('users', criteria, onSuccUserFind);
            } else {
                res.status(400);
                if (err){
                    res.send(err);
                }
                else{
                    res.send("Некорректный идентификатор проекта");
                }
            }
        };

        db.findOne('proposals', id, onSuccFind);
    };*/

    var logUploadFileCloud = function (req, res) {
        var msg = 'Загрузка файла на Cloud';
        var logEntry = formUserEntryLog(req, types['uploadFileCloud']);
        //logEntry.projectName = (req.params.projectId) ? getProjectName(req.params.projectId) : '';
        logEntry.projectName = (req.body.projectId) ? getProjectName(req.body.projectId) : '';
        logEntry.objectId = (req.body.id) ? req.body.id : '';
        var pGetProposalNum = getProposalNum(req.body.id);
        pGetProposalNum.then(function(proposalNum) {
            logEntry.objectName = '№' + proposalNum;
            if (req.body.result) {
                switch(req.body.result) {
                    case 'noAuth': {
                        msg = 'Пользователь пытался загрузить файл на Cloud не авторизовавшись';
                        logEntry.type += 'NoAuth';
                        break;
                    }
                    case 'error': {
                        msg = 'Ошибка при загрузке файла на Cloud';
                        logEntry.type += 'Error';
                        break;
                    }
                    case 'doneAll': {
                        msg = 'Успешная загрузка всех файлов на Cloud';
                        logEntry.type += 'DoneAll';
                        break;
                    }
                    case 'done': msg = 'Успешная загрузка файла на Cloud'; break;
                }
            }
            logEntry.fileUrl = (req.body.fileUrl) ? req.body.fileUrl : '';
            logEntry.fileName = (req.body.fileName) ? req.body.fileName : '';
            logEntry.fileSize = (req.body.fileSize) ? req.body.fileSize : ''; //in bytes
            grayLogBusiness.log(msg, logEntry);

            res.status(200).send("Successful upload file to Cloud!");
        });
    };

    var logDownloadFileCloud = function (req, res) {
        var msg = 'Скачивание файла с Cloud';
        var logEntry = formUserEntryLog(req, types['downloadFileCloud']);
        //logEntry.projectName = (req.params.projectId) ? getProjectName(req.params.projectId) : '';
        logEntry.projectName = (req.body.projectId) ? getProjectName(req.body.projectId) : '';
        logEntry.objectId = (req.body.id) ? req.body.id : '';
        var pGetProposalNum = getProposalNum(req.body.id);
        pGetProposalNum.then(function(proposalNum) {
            logEntry.objectName = '№' + proposalNum;
            if (req.body.result) {
                switch(req.body.result) {
                    case 'noAuth': {
                        msg = 'Пользователь пытался скачать файл с Cloud не авторизовавшись';
                        logEntry.type += 'NoAuth';
                        break;
                    }
                    case 'error': {
                        msg = 'Ошибка при скачивании файла с Cloud';
                        logEntry.type += 'Error';
                        break;
                    }
                    case 'error2': {
                        msg = 'Ошибка при скачивании файла с Cloud 2';
                        logEntry.type += 'Error2';
                        break;
                    }
                    case 'doneAnother': {
                        msg = 'Успешное скачивание файла с другого ресурса';
                        logEntry.type = 'downloadFileAnother';
                        break;
                    }
                    case 'done': msg = 'Успешное скачивание файла с Cloud'; break;
                }
            }
            logEntry.fileUrl = (req.body.fileUrl) ? req.body.fileUrl : '';
            logEntry.fileName = (req.body.fileName) ? req.body.fileName : '';
            logEntry.fileSize = (req.body.fileSize) ? req.body.fileSize : ''; //in bytes
            grayLogBusiness.log(msg, logEntry);

            res.status(200).send("Successful download file from Cloud!");
        }, null);
    };

    var saveDoc = function (req, res) {
        var msg = 'Сохранение файла из редактора';
        var logEntry = formUserEntryLog(req, types['saveDoc']);
        //logEntry.projectName = (req.params.projectId) ? getProjectName(req.params.projectId) : '';
        logEntry.projectName = (req.params.projectId) ? getProjectName(req.params.projectId) : '';
        logEntry.objectId = (req.params.id) ? req.params.id : '';
        var pGetProposalNum = getProposalNum(req.params.id);
        pGetProposalNum.then(function(proposalNum) {
            logEntry.objectName = '№' + proposalNum;
            if (req.body.result) {
                switch(req.body.result) {
                    case 'noAuth': {
                        msg = 'Пользователь пытался загрузить файл на Cloud не авторизовавшись';
                        logEntry.type += 'NoAuth';
                        break;
                    }
                    case 'error': {
                        msg = 'Ошибка при загрузке файла на Cloud';
                        logEntry.type += 'Error';
                        break;
                    }
                    case 'doneAll': {
                        msg = 'Успешная загрузка всех файлов на Cloud';
                        logEntry.type += 'DoneAll';
                        break;
                    }
                    case 'done': msg = 'Успешная загрузка файла на Cloud'; break;
                }
            }
            logEntry.fileUrl = (req.body.fileUrl) ? req.body.fileUrl : '';
            logEntry.fileName = (req.body.fileName) ? req.body.fileName : '';
            logEntry.fileSize = (req.body.fileSize) ? req.body.fileSize : ''; //in bytes
            grayLogBusiness.log(msg, logEntry);

            res.status(200).send("Successful save file from editor to server");
        });
    };



    var router = express.Router();
    router.get('/:projectId/proposals', getProposals);
    router.get('/:projectId/proposals/all', getAllProposals);
    router.get('/:projectId/proposals/:id', apiFactory.GET(db, 'proposals', null));
    router.post('/:projectId/proposals/:id/saveDoc', saveDoc);
    router.post('/:projectId/proposals/', saveProposal);
    router.put('/:projectId/proposals/:id', saveProposal);
    router.delete('/:projectId/proposals/:id', deleteProposal);
   //router.post('/:projectId/proposals/:id/pushToCrm', pushToCrm);
   // router.post('/pushFromCrm', pushFromCrm);
    //router.post('/:projectId/proposals/:id/uploadFileCloud', logUploadFileCloud);
    //router.post('/:projectId/proposals/:id/downloadFileCloud', logDownloadFileCloud);
    router.post('/uploadFileCloud', logUploadFileCloud);
    router.post('/downloadFileCloud', logDownloadFileCloud);
    //router.get('/downloadPdf', downloadPdf);
    return router;
};

module.exports = proposalsAPIFactory;
