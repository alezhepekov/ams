'use strict';
var express = require('express');
var clean = require('../db-manager/cleaners');
var apiFactory = require('./restFactory');

function organizationsRESTFactory(db) {
    var collectionName= 'organizations';
    var router = express.Router();
    var validate = function(org){
        return {ok: true};
    };

    /* GET all items in collectionName */
    router.get('/', apiFactory.GETAll(db, 'organizations'));

    /* GET one item from collectionName by id */
    router.get('/:id', apiFactory.GET(db, 'organizations'));

    /* POST(add) item to collectionName */
    router.post('/', apiFactory.POST(db, 'organizations', null, clean.organization));

    /* PUT (update) item in collectionName by id */
    router.put('/:id',apiFactory.PUT(db, 'organizations', null, clean.organization));

    /* DELETE item from collectionName by id*/
    router.delete('/:id', apiFactory.DELETE(db, 'organizations'));

    return router;
}

module.exports = organizationsRESTFactory;
