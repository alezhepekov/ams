'use strict';

function GenericAPIFactory() {
    /**
     * Generic API's GET request handler factory
     * @param   {Object}   db             instance of DbManager
     * @param   {String}   collectionName name of mongo collection
     * @param   {Function} beforeSendHook Applied to data before sending response to user.
     * @returns {Function} GET request handler, compatible with express.
     */
    this.GET = function (db, collectionName, beforeSendHook) {
        return function (req, res) {
            console.log("GET " + req.url);

            var id = "";
            if ( collectionName === "projects" ) {
                id = req.params.id;
            }
            else {
                id = db.idFromHexString(req.params.id, send400(res));        
            }

            var logEntry = formUserEntryLog(req, types['view']);
            logEntry.objectId = id;

            var onSuccFind = function (err, doc) {
                if (!err) {
                    if (doc !== null) {
                        if (beforeSendHook) {
                            doc = beforeSendHook(doc, req);
                        }
                        res.status(200);
                        res.json(doc);

                        var msg = '';
                        switch(collectionName) {
                            case 'users': {
                                msg = 'Просмотр пользователя ' + doc.fullName;
                                logEntry.type += 'User';
                                break;
                            }
                            //case 'projects': msg = 'Просмотр проекта ' + doc.name; break;
                            case 'proposals': {
                                var docNum = (doc.number) ? doc.number : '-';
                                msg = 'Просмотр заявки №'+ docNum;
                                logEntry.type += 'Proposal';
                                break;
                            }
                            default: {
                                msg = 'Просмотр объекта ' + id + ' в ' + collectionName;
                                logEntry.type += 'Object';
                            }
                        }
                        if (collectionName != 'projects')
                            grayLogBusiness.log(msg, logEntry);

                    } else {
                        res.status(404);
                        res.json({
                            message: 'Not Found'
                        });
                    }
                } else {
                    res.status(500);
                    res.json(err);
                }
            };

            db.findOne(collectionName, id, onSuccFind);
        };
    };

    /**
     * Generic API's GET request handler factory
     * @param   {Object}   db             DbManager instance
     * @param   {String}   collectionName name of mongo collection
     * @param   {Function} beforeSendHook Applied to data before send to client
     * @param   {Function} buildCriteria  Build request dependent mongo criteria
     * @returns {Function} GET request handler, compatible with express.
     */
    this.GETAll = function (db, collectionName, beforeSendHook, buildCriteria, buildOptions) {
        beforeSendHook = beforeSendHook || false;
        return function (req, res) {
            console.log("GET " + req.url);

            var criteria;
            if (buildCriteria) {
                criteria = buildCriteria(req);
            } else {
                criteria = {};
            }
            var onSuccessCount = function (err, n) {
                var onSuccFind = function (err, docs) {
                    if (!err) {
                        var result = {};
                        result[collectionName] = docs;
                        result.empty = (n === 0);
                        result.quantity = n;
                        if (beforeSendHook) {
                            result = beforeSendHook(result, req);
                        }
                        res.status(200);
                        res.json(result);
                    } else {
                        res.status(500);
                        res.send(err);
                    }
                };
                if (buildOptions) {
                    db.find(collectionName, criteria, onSuccFind, buildOptions(req));
                } else {
                    db.find(collectionName, criteria, onSuccFind);
                }
            };
            db.collection(collectionName).count(criteria,onSuccessCount);
            var logEntry = formUserEntryLog(req, types['view']);
            var msg = '';
            switch(collectionName) {
                case 'users': {
                    msg = (req.url==='/manage') ? 'Администрирование пользователей' : '';
                    logEntry.type = 'ManageUsers';
                    break;
                }
                case 'organizations': {
                    msg = 'Просмотр списка организаций';
                    logEntry.type += 'OrgsList';
                    break;
                }
                case 'projects': {
                    msg = 'Просмотр списка проектов';
                    logEntry.type += 'ProjectsList';
                    break;
                }
                default: {
                    msg = 'Просмотр списка объектов ' + collectionName;
                    logEntry.type += 'ObjectsList';
                }
            }
            if (msg.length > 0)
                grayLogBusiness.log(msg, logEntry);
        };
    };
    /**
     * Generic API's POST request handler factory
     * @param   {Object}   db               DbManager instance
     * @param   {String}   collectionName   name of mongo collection
     * @param   {Function} beforeSendHook   Applied to data before sending response to user.
     * @param   {Function} beforeInsertHook Called before any DB write transaction. Do validation here, if an excepton thrown, 400 is send to client.
     * @returns {Function} POST request handler, compatible with express.
     */
    this.POST = function (db, collectionName, beforeSendHook, beforeInsertHook) {
        return function (req, res) {
            console.log("POST " + req.url);

            // POST /downloadFileCloud
            // POST /api/projects/downloadFileCloud
            if (
                req.url === "/downloadFileCloud"
                || req.url === "/api/projects/downloadFileCloud"
            ) {               
                res.status(200);
                res.json({});
                return;
            }

            var id = "";
            if ( collectionName === "projects" ) {
                id = req.params.id;
            }
            else if ( collectionName === "comments" ) {
            }
            else {
                id = db.idFromHexString(req.params.id, send400(res));        
            }

            var doc = req.body;

            var onSuccInsert = function (err, doc) {
                if (!err) {
                    res.status(200);                   
                    // if (beforeSendHook) {
                    //     doc = beforeSendHook(doc, req);
                    // }
                    // if (collectionName=='comments') {
                    //     //Получаем номер заявки по id
                    //     var pGetProposalNum = getProposalNum(doc.projectID);
                    //     pGetProposalNum.then(function(proposalNum) {
                    //         var now = new Date();
                    //         var projectName = (doc.projectID) ? getProjectName(doc.projectID) : '';

                    //         var locals = {
                    //             subject: 'Заявка №' +  proposalNum + ' (' + projectName + '). Новый комментарий',
                    //             fullName: (req.user.fullName) ? req.user.fullName : 'Пользователь',
                    //             proposalNum: proposalNum,
                    //             projectName: projectName,
                    //             proposalLink: domainAddr + '/#/projects/' + doc.projectID,
                    //             comment: doc.message,
                    //             commentType: doc.type,
                    //             commentTypeStr: getCommentTypeStr(doc.type),
                    //             dateTimeNotification: now.toLocaleDateString() + ' ' + now.toLocaleTimeString()
                    //         };

                    //         emailTemplates(templatesDir, function(err, template) {
                    //             if (err) {
                    //                 console.log(err);
                    //             } else {
                    //                 if (req.body.notificationList && req.body.notificationList.length > 0) {
                    //                     //Загружаем шаблон один раз и рассылаем сообщения по всему списку адресатов
                    //                     template('newcomment', true, function (err, batch) {
                    //                         req.body.notificationList.forEach(function(user) {
                    //                             locals.email = (user.email) ? user.email : defaultAddr;
                    //                             var render = new RenderTemplate(locals);
                    //                             render.batch(batch);
                    //                         });
                    //                     });
                    //                 } else {
                    //                     //Отсылаем одно письмо на дефолтный адрес
                    //                     template('newcomment', locals, function(err, html, text) {
                    //                         if (err) {
                    //                             console.log(err);
                    //                         } else {
                    //                             transporter.sendMail({
                    //                                 from: 'CRM DGI <dgi15@abn-consult.ru>',
                    //                                 to: defaultAddr,
                    //                                 subject: locals.subject,
                    //                                 html: html,
                    //                                 text: text
                    //                                 }, function(err, responseStatus) {
                    //                                 if (err) {
                    //                                     console.log(err);
                    //                                 } else {
                    //                                     console.log(responseStatus.message);
                    //                                 }
                    //                             });
                    //                         }
                    //                     });
                    //                 }
                    //             }
                    //         });

                    //         var logEntry = formUserEntryLog(req, types['newComment']);
                    //         logEntry.objectId = doc._id;
                    //         logEntry.projectName = projectName;

                    //         db.findOne('comments', doc._id, function(err, doc1) {
                    //             if (!err) {
                    //                 if (doc1 !== null) {
                    //                     doc1.notificationTime = now;
                    //                     db.replaceOne('comments', doc1._id, doc1, function(err, doc2) {
                    //                         //TODO: Возможно добавить сюда логирование
                    //                     });
                    //                 }
                    //             } else {
                    //                 res.status(500);
                    //                 res.json(err);
                    //             }
                    //         });

                    //         grayLogBusiness.log("Добавление нового комментария", logEntry);
                    //     });
                    // }                   
                    res.json(doc);
                } else {
                    res.status(500);
                    res.json({
                        message: 'Internal Server Error'
                    });
                }
            };

            try {             
                db.insertOne(collectionName, doc, onSuccInsert);              
            } catch (ex) {
                res.status(400);
                res.json({
                    message: ex
                });
            }

        };
    };
    /**
     * Generic API's PUT request handler factory.
     * @param   {Object}   db               DbManager instance
     * @param   {String}   collectionName   name of mongo collection
     * @param   {Function} beforeSendHook   Applied to data before sending response to user.
     * @param   {Function} beforeInsertHook Called before any DB write transaction. Do validation here, if an excepton thrown, 400 is send to client.
     * @returns {Function} PUT request handler, compatible with express.
     */
    this.PUT = function (db, collectionName, beforeSendHook, beforeInsertHook) {
        return function (req, res) {
            console.log("PUT " + req.url);

            var id = "";
            if ( collectionName === "projects" ) {
                id = req.params.id;
            }
            else {
                id = db.idFromHexString(req.params.id, send400(res));        
            }

            var newObject = req.body;
            var onSuccUpdate = function (err, doc) {
                if (!err) {
                    res.status(200);
                    if (beforeSendHook) {
                        res.json(beforeSendHook(doc, req));
                    } else {
                        res.json(doc);
                    }
                } else {
                    res.status(500);
                    res.json({
                        message: 'Internal Server Error'
                    });
                }
            };

            try {            
                db.replaceOne(collectionName, id, newObject, onSuccUpdate);            
            } catch (ex) {
                res.status(400);
                res.json({
                    message: ex
                });
            }

        };
    };
    /**
     * Generic API's DELETE request handler factory
     * @param   {Object}   db             DbManagerInstance
     * @param   {String}   collectionName name of mongo collection
     * @returns {Function} DELETE request handler, compatible with express.
     */
    this.DELETE = function (db, collectionName) {
        return function (req, res) {
            console.log("DELETE " + req.url);
            var id = "";
            if ( collectionName === "projects" ) {
                id = req.params.id;
            }
            else {
                id = db.idFromHexString(req.params.id, send400(res));        
            }

            var onSuccDelete = function (err, n) {
                if (!err) {
                    res.status(200);
                    res.json({
                        ok: true
                    });
                } else {
                    res.status(500);
                    res.json({
                        ok: false
                    });
                }
            };       

            try {            
                db.deleteOne(collectionName, id, onSuccDelete);
            } catch (ex) {
                res.status(400);
                res.json({
                    message: ex
                });
            }
        };
    };
};


module.exports = new GenericAPIFactory();
