'use strict';
var express = require('express');
var apiFactory = require('./restFactory');
var clean = require('../db-manager/cleaners');
var ObjectId = require('mongodb').ObjectID;
var db = require('../db-manager');
var request = require('request');
var uuid = require('node-uuid');

var reportsAPIFactory = function(db) {

    /**
     * To enable full txt search, run
     * db.proposals.ensureIndex({name: "text", number:"text", address: "text", decription:"text"},{default_language:"russian"})
     */

    var _getStatusReport = function(req, res, next) {
        db.findOne('projects', db.idFromHexString(req.params.projectId), function(err, project) {
            try {
                if (!err) {

                    var criteria = {};
                    criteria.projectId = req.params.projectId;
                    if (req.query.manager) {
                        criteria["manager._id"] = db.idFromHexString(req.query.manager);
                    }
                    if (req.query.coordinator) {
                        criteria["coordinator._id"] = db.idFromHexString(req.query.coordinator);
                    }
                    if (req.query.owner) {
                        criteria["owner._id"] = db.idFromHexString(req.query.owner);
                    }
                    if (req.query.executor) {
                        criteria["currentTask.executor._id"] = db.idFromHexString(req.query.executor);
                        criteria["currentTask.finished"] = false;
                    }
                    if (req.query.status) {
                        criteria['status.title'] = req.query.status;
                    }

                    var matchQuery = {
                        $match: {
                            projectId: req.params.projectId
                        }
                    };
                    var dateCreatedStart = "";
                    var dateCreatedEnd = "";
                    if (req.query.dateCreatedStart) {
                        dateCreatedStart = new Date(req.query.dateCreatedStart);
                    }
                    if (req.query.dateCreatedEnd) {
                        dateCreatedEnd = new Date(req.query.dateCreatedEnd);
                        dateCreatedEnd.setDate(dateCreatedEnd.getDate()+1);
                    }
                    if (dateCreatedStart && dateCreatedEnd) {
                        matchQuery = {
                            $match: {
                                projectId: req.params.projectId,
                                started: {
                                    $gte: dateCreatedStart,
                                    $lt: dateCreatedEnd
                                }
                            }
                        };
                    } else if (dateCreatedStart) {
                        matchQuery = {
                            $match: {
                                projectId: req.params.projectId,
                                started: {
                                    $gte: dateCreatedStart
                                }
                            }
                        };
                    } else if (dateCreatedEnd) {
                        matchQuery = {
                            $match: {
                                projectId: req.params.projectId,
                                started: {
                                    $lt: dateCreatedEnd
                                }
                            }
                        };
                    }

                    try{
                        var statuses = project.statuses;
                        var statusPipline = [
                            matchQuery,
                            {
                                $group: {
                                    _id: "$status.title",
                                    count: {$sum : 1}
                                }
                            },
                            {
                                $project: {
                                    "status" : "$_id",
                                    "count" : 1
                                }
                            }
                        ];

                        db.aggregate('proposals', statusPipline, function(err, data) {
                            if (!err) {
                                res.json({
                                    statuses : data.map(function(v){
                                        return {
                                            name: v.status,
                                            dataLabels: {enabled: v.count > 0},
                                            y: v.count
                                        };
                                    })
                                });
                            } else {
                                console.log(err);
                                res.status(500);
                                res.end();
                            }
                        });
                    } catch(e) {
                        next(e);
                    }
                } else {
                    res.status(500);
                    res.end();
                }
            } catch (e) {
                next(e);
            }
        });
    };

    var _getSupervisorReport = function(req, res, next) {
        db.findOne('projects', db.idFromHexString(req.params.projectId), function(err, project) {
            try {
                if (!err) {

                    var criteria = {};
                    criteria.projectId = req.params.projectId;

                    try{

                      var statuses = project.statuses;

                        var oneWeekAgo = new Date();
                        oneWeekAgo.setDate(oneWeekAgo.getDate() - 20);

                      var statusPipline = [
                    {
                        $match: {
                            projectId: req.params.projectId,
                            "currentTask.creationDate" : {"$gte" : oneWeekAgo}
                            }
                        },
                        {$group: {
                            _id : {status : "$status.title", supervisor : {$concat : ["$manager.lastName", " ", "$manager.firstName"]}},
                            proposals : {$sum : 1}
                        }},
                        {$group : {
                            _id : {status : "$_id.status" },
                            supervisors : { $addToSet : {supervisor : "$_id.supervisor", proposals : "$proposals"} }}}];

                      db.aggregate('proposals', statusPipline, function(err, data){
                        if (!err) {
                                    res.json({
                                            data : data
                                        });
                        }
                          else{
                            console.log(err);
                            res.status(500);
                            res.end();
                        }
                      });


                    }catch(e){
                        next(e);
                    }

                } else {
                    res.status(500);
                    res.end();
                }
            } catch (e) {
                next(e);
            }
        });
    };

    var getDate = function(strDate){
        var dateParts = strDate.split(".");

        var date = new Date(dateParts[2], (dateParts[1] - 1), dateParts[0]);

        return date;
    }

    var _getDgimReport = function(req, res, next) {
        db.findOne('projects', db.idFromHexString(req.params.projectId), function(err, project) {
            try {
                if (!err) {

                    var startDate = "";
                    var endDate = "";
                    if (req.body.startDate) {
                        startDate = parseDate(req.body.startDate);
                        startDate.setHours(0, -startDate.getTimezoneOffset(), 0, 0);
                        startDate = startDate.toISOString();
                    }
                    if (req.body.endDate) {
                        endDate = parseDate(req.body.endDate);
                        endDate.setHours(0, -endDate.getTimezoneOffset(), 0, 0);
                        endDate = endDate.toISOString();
                    }

                    var currentTaskFilter = {};
                    if (startDate && endDate) {
                        currentTaskFilter = {
                            $match: {
                                "status.title" : {"$in":["выполнена","доставка"]},
                                projectId: req.params.projectId,
                                "currentTask.finished" : true,
                                "currentTask.finishingDate": {
                                    "$gte": startDate,
                                    "$lte": endDate
                                }
                            }
                        }
                    } else if (startDate) {
                        currentTaskFilter = {
                            $match: {
                                "status.title" : {"$in":["выполнена","доставка"]},
                                projectId: req.params.projectId,
                                "currentTask.finished" : true,
                                "currentTask.finishingDate": {
                                    "$gte": startDate
                                }
                            }
                        }
                    } else if (endDate) {
                        currentTaskFilter = {
                            $match: {
                                "status.title" : {"$in":["выполнена","доставка"]},
                                projectId: req.params.projectId,
                                "currentTask.finished" : true,
                                "currentTask.finishingDate": {
                                    "$lte": endDate
                                }
                            }
                        }
                    } else {
                        currentTaskFilter = {
                            $match: {
                                "status.title" : {"$in":["выполнена","доставка"]},
                                projectId: req.params.projectId,
                                "currentTask.finished" : true
                            }
                        }
                    }
                    var pipeline = [currentTaskFilter,
                        {
                            "$project":{
                                "data.number": 1,
                                "name": 1,
                                "customFields" :1,
                                "data.files": 1,
                                "currentTask.finishingDate":
                                    {"$ifNull": ["$currentTask.finishingDate", "$currentTask.creationDate"]}
                            }
                        }
                    ];

                    db.aggregate('proposals', pipeline, function(err, data){
                        if (!err) {
                            res.json({
                                    result : data
                                });
                        } else {
                            console.log(err);
                            res.status(500);
                            res.end();
                        }
                      });

                } else {
                    res.status(500);
                    res.end();
                }
            } catch (e) {
                next(e);
            }
        });
    };

    var _getExportReport = function(req, res, next) {
        db.findOne('projects', db.idFromHexString(req.params.projectId), function(err, project) {
            try {
                if (!err) {

                    var pipeline = [
                        {
                            "$match":{
                                "started": {
                                    "$gte": getDate(req.body.startDate),
                                    "$lte": getDate(req.body.endDate)
                                }
                            }
                        },
                        {
                        "$project":{
                            'name' : 1,
                            'number':1
                            ,'address':1
                            ,'description':1
                            ,'status.title':1
                            ,'mapAddress.point':1
                            ,'documents':1
                            ,'currentTsk':1
                            ,'owner.fullName':1
                            ,'customFields' : 1
                        }
                    }];

                    db.aggregate('proposals', pipeline, function(err, data){
                        if (!err) {
                            res.json({
                                result : data
                            });
                        }
                        else{
                            console.log(err);
                            res.status(500);
                            res.end();
                        }
                    });



                } else {
                    res.status(500);
                    res.end();
                }
            } catch (e) {
                next(e);
            }
        });
    };

    function parseDate(input) {
        //TODO: Вынести в helper
        var parts = input.split('.');
        return new Date(parts[2], parts[1]-1, parts[0]);
    }

    var _getTasksExcelReport = function(req, res, next) {
        db.findOne('projects', db.idFromHexString(req.params.projectId), function(err, project) {
            try {
                if (!err) {

                    var startDate = "";
                    var endDate = "";
                    if (req.query.startDate) {
                        startDate = parseDate(req.query.startDate);
                        startDate.setHours(0, -startDate.getTimezoneOffset(), 0, 0);
                    }
                    if (req.query.endDate) {
                        endDate = parseDate(req.query.endDate);
                        endDate.setHours(0, -endDate.getTimezoneOffset(), 0, 0);
                    }

                    var taskFilter = {};
                    if (startDate && endDate) {
                        taskFilter = {
                            "$match": {
                                "tasks.finished": true,
                                "tasks.dueDate": {
                                    $gte: startDate,
                                    $lte: endDate
                                }
                            }
                        };
                    } else if (startDate) {
                        taskFilter = {
                            "$match": {
                                "tasks.finished": true,
                                "tasks.dueDate": {
                                    $gte: startDate
                                }
                            }
                        };
                    } else if (endDate) {
                        taskFilter = {
                            "$match": {
                                "tasks.finished": true,
                                "tasks.dueDate": {
                                    $lte: endDate
                                }
                            }
                        };
                    } else {
                        taskFilter = {
                            "$match": {
                                "tasks.finished": true
                            }
                        };
                    }

                    var pipeline = [
                        {
                            "$project": {
                                "projectCreatedAt": "$started",
                                "projectCreatedBy": "$owner.fullName",
                                "projectNo": "$number",
                                "address": "$address",
                                "projectStatus": "$status.title",
                                "projectNoCRM": "$DgiProjectNo",
                                "tasks": "$tasks",
                                "customFields": 1
                            }
                        }, taskFilter,
                        {
                            "$unwind": "$tasks"
                        },
                        {
                            "$project": {
                                "projectCreatedAt": 1,
                                "projectCreatedBy": 1,
                                "projectNo": 1,
                                "address": 1,
                                "projectStatus": 1,
                                "projectNoCRM": 1,
                                "taskType": "$tasks.status.title",
                                "taskAssignedTo": "$tasks.executor.fullName",
                                "taskDue": "$tasks.dueDate",
                                "taskCreatedBy": "$tasks.owner.fullName",
                                "taskCompletedAt": "$tasks.finishingDate",
                                "taskDescription": "$tasks.description",
                                "customFields": "$customFields.value"
                            }
                        }
                    ];

                    db.aggregate('proposals', pipeline, function(err, data){
                        if (!err) {
                            res.json({
                                result : data
                            });
                        }
                        else{
                            console.log(err);
                            res.status(500);
                            res.end();
                        }
                    });



                } else {
                    res.status(500);
                    res.end();
                }
            } catch (e) {
                next(e);
            }
        });
    };


    var _getDurationReport = function(req, res, next){
        //var dateStart = new Date('2014-07-10T20:00:00.000Z');

        var durationPipeline = [{
            "$match": {
                "status.title" :"выполнена",
                "projectId": req.params.projectId
                /*,"started": {
                    "$gt":{"$date":dateStart}
                }*/
            }
        },{
            "$unwind": "$tasks"
        }, {
            "$group": {
                "_id": {"_id":"$_id","number":"$number"},
                "tasksTypes": {
                    "$push": "$tasks.status.title"
                },
                "tasksCreatedAt": {
                    "$push": "$tasks.creationDate"
                },
                "tasksCompletedAt": {
                    "$push": "$tasks.finishingDate"
                },
                "projectCompletedAt": {
                    "$first": "$tasks.finishingDate"
                }
            }
        } , {
            "$project": {
                "_id":"$_id._id",
                "projectNo":"$_id.number",
                "tasksTypes":1,
                "tasksCreatedAt":1,
                "tasksCompletedAt":1,
                projectCompletedAt:1
            }
        }, {"$sort":{"projectCompletedAt":-1}}
        ];

        db.aggregate('proposals', durationPipeline, function(err, data){
            if (!err) {
                res.json({
                    result : data
                });
            }
            else{
                console.log(err);
                res.status(500);
                res.end();
            }
        });
    };

    var getCorrectDayLocalName = function(daysNum) {
        return (daysNum % 10 == 1 && (daysNum > 20 || daysNum < 10)) ? 'день' : ((daysNum % 10 < 5 && daysNum > 20) ? 'дня' : (daysNum>1 && daysNum<5) ? 'дня' : 'дней');
    };

    var _getStatusesDurationReport = function(req, res, next){

        var projectId = db.idFromHexString(req.params.projectId, send400(res));
        var orgMode = isProjectOrgMode(req.params.projectId);

        var onSuccFind = function(err, doc) {
            if (!err) {
                //Получаем реальные статусы по конфигурации
                var realStatuses = [];
                statusesForReport.forEach(function (s) {
                   var status = doc.statuses.filter(function(st){
                       return st.title === s.name;
                   })[0];

                    if (status) {
                        realStatuses.push(status);
                    }
                });

                //TODO: Реализовать получение статистики запросами к mongodb
                var result = [];
                //Вытаскиваем все заявки
                var criteria = { projectId: req.params.projectId };
                db.find('proposals', criteria, function(err, proposals) {
                    if (!err) {
                        var today = new Date();
                        realStatuses.forEach(function(st) {
                            var statusTable = {statusName: st.title, statusClassName: st.className, proposals: []};
                            var standardDurationDays = getStandardDuration(st.title);
                            proposals.forEach(function(prop) {
                                var durationDaysNum = 0;
                                var restDaysNum = 0;
                                var overDaysNum = 0;

                                var durationPercent = 0;
                                var restPercent = 0;
                                var overPercent = 0;

                                var creationDate;
                                var finishingDate;
                                var finished;

                                if (prop.tasks && prop.tasks.length>0) {
                                    var task = prop.tasks.filter(function(t){
                                        return t.status.id === st.id;
                                    })[0];

                                    if (task && task.creationDate) {
                                        finished = task.finished;
                                        creationDate = new Date(task.creationDate);
                                        finishingDate = (task.finishingDate) ? new Date(task.finishingDate) : undefined;
                                        var dueDate = (task.dueDate) ? task.dueDate : undefined;
                                        var planedDueDate = (dueDate) ? dueDate : creationDate.setDate(creationDate.getDate()+standardDurationDays);

                                        var diff1;
                                        //Статус закрыт
                                        if (finished && finishingDate) {
                                            var durationTime = finishingDate - creationDate;
                                            durationDaysNum = Math.ceil(durationTime / (1000 * 3600 * 24));
                                            diff1 = planedDueDate - finishingDate;
                                        }
                                        //Статус не закрыт
                                        else {
                                            //Прошло время с момента создания
                                            var durationTime = today - creationDate;
                                            durationDaysNum = Math.ceil(durationTime / (1000 * 3600 * 24));
                                            diff1 = planedDueDate - today;
                                        }

                                        restDaysNum = (diff1 > 0) ? Math.ceil(diff1 / (1000 * 3600 * 24)) : 0;
                                        overDaysNum = (diff1 > 0) ? 0 : Math.ceil( Math.abs(diff1) / (1000 * 3600 * 24));

                                        //Обобщенный расчет
                                        var totalDaysNum = durationDaysNum + restDaysNum + overDaysNum;
                                        durationPercent = Math.ceil(durationDaysNum / totalDaysNum * 100);
                                        restPercent = (restDaysNum > 0) ? 100 - durationPercent : 0;
                                        overPercent = (overDaysNum > 0) ? 100 - durationPercent : 0;
                                    }
                                }

                                statusTable.proposals.push({
                                    id: prop._id,
                                    projectId: prop.projectId,
                                    name: prop.number + '. ' + (orgMode ? prop.name : prop.address),

                                    durationDaysNum: durationDaysNum,
                                    durationText: st.title + ': ' + durationDaysNum + ' ' + getCorrectDayLocalName(durationDaysNum),
                                    durationPercent: durationPercent + '%',

                                    restDaysNum: restDaysNum,
                                    restText: (restDaysNum > 0) ? 'осталось ' + restDaysNum + ' ' + getCorrectDayLocalName(restDaysNum) : '',
                                    restPercent: restPercent + '%',

                                    overDaysNum: overDaysNum,
                                    overText: (overDaysNum > 0) ? 'просрочена на ' + overDaysNum + ' ' + getCorrectDayLocalName(overDaysNum) : '',
                                    overPercent: overPercent + '%',

                                    creationDate: creationDate,
                                    finishingDate: finishingDate,
                                    planedDueDate: planedDueDate,
                                    finished: finished
                                });
                            });
                            result.push(statusTable);
                        });

                        res.json({
                            result: result
                        });
                    } else {
                        console.log(err);
                        res.status(500);
                        res.end();
                    }
                }, null);
            } else {
                console.log(err);
                res.status(500);
                res.end();
            }
        };

        db.findOne('projects', projectId, onSuccFind);
    };

    //Получаем среднюю длительность статусов
    var _getStatusesAvgDurationReport = function(req, res, next){
        var projectId = db.idFromHexString(req.params.projectId, send400(res));
        var onSuccFind = function(err, proj) {
            if (!err) {

                var statusesTable = {};
                var unknownStatuses = [];

                proj.statuses.forEach(function(st) {
                    statusesTable[st.id] = {
                        name: st.title, className: st.className, totalCount: 0,
                        totalDuration: 0, minDuration: 900000000000000000, maxDuration: 0, avgDuration: 0,
                        percentage: 0
                    }
                });

                //TODO: Реализовать получение статистики запросами к mongodb
                //Получаем все выполненные заявки
                var criteria = { projectId: req.params.projectId };
                criteria.$and = [ {"status.title": "выполнена"} ];
                db.find('proposals', criteria, function(err, proposals) {
                    if (!err) {
                        proposals.forEach(function(prop) {
                            if (prop.tasks && prop.tasks.length>0) {
                                prop.tasks.forEach( function(task) {
                                    if (task && task.creationDate && task.finishingDate) {
                                        var creationDate = new Date(task.creationDate);
                                        var finishingDate = new Date(task.finishingDate);
                                        var totalDuration = Math.abs(finishingDate - creationDate);

                                        var stId = task.status.id;
                                        if (task.status && statusesTable[stId]) {
                                            statusesTable[stId].totalCount++;
                                            statusesTable[stId].totalDuration += totalDuration;

                                            if (totalDuration <= statusesTable[stId].minDuration) {
                                                statusesTable[stId].minDuration = totalDuration;
                                            } else {
                                                statusesTable[stId].maxDuration = totalDuration;
                                            }
                                        } else {
                                            //Неизвестный статус
                                            unknownStatuses.push(task.status);
                                            console.log('unknown status: ' + task.status);
                                        }
                                    }
                                });
                            }
                        });

                        proj.statuses.forEach(function(st) {
                            var status = statusesTable[st.id];
                            if (st.title==='выполнена' || status.totalDuration === 0 ) {
                                delete statusesTable[st.id];
                            } else {
                                status.avgDuration = (status.totalCount > 0) ? status.totalDuration / status.totalCount : 0;

                                //Переводим в часы
                                status.minDuration = (status.minDuration > 0) ? Math.ceil(status.minDuration / (1000 * 3600)) : 0;
                                status.maxDuration = (status.maxDuration > 0) ? Math.ceil(status.maxDuration / (1000 * 3600)) : 0;
                                status.avgDuration = (status.avgDuration > 0) ? Math.ceil(status.avgDuration / (1000 * 3600)) : 0;
                            }

                        });

                        //Сортируем по среднему времени выполнения
                        var result = [];
                        for (var stId in statusesTable) {
                            if (statusesTable[stId].name) {
                                result.push(statusesTable[stId]);
                            } else {
                                delete statusesTable[stId];
                            }
                        }
                        result.sort(function(a, b) {return a.avgDuration - b.avgDuration});

                        //Расчитываем проценты
                        var sum = 0;
                        result.forEach(function(st) {
                            sum += st.avgDuration;
                        });

                        result.forEach(function(st) {
                            st.percentage = (st.avgDuration / sum) * 100;
                        });

                        var sumPercent = 0;
                        result.forEach(function(st) {
                            sumPercent += st.percentage;
                        });

                        var over = sumPercent - 100;
                        if (over > 0) {
                            result[result.length-1].percentage - 1;
                        }

                        res.json({
                            result: result
                        });
                    } else {
                        console.log(err);
                        res.status(500);
                        res.end();
                    }
                }, null);
            } else {
                console.log(err);
                res.status(500);
                res.end();
            }
        };

        db.findOne('projects', projectId, onSuccFind);
    };

    var _getPlanReport = function(req, res, next){

        var planPipeline = [{
            "$match": {
                 "status.title": {
                     "$nin": ["выполнена","отложена","отменена"]
                 }
             }
         }, {
             "$unwind": "$tasks"
         }, {
             "$group": {
                 "_id": "$_id",
                 "lastTask": {
                     "$first": "$tasks"
                 }
                 , "projectNo": {"$first": "$number"}
                 , "status": {"$first": "$status.title"}
             }
         }, {
             "$project": {
                 "taskStartedAt" : {"$substr":["$lastTask.creationDate", 0, 10]},
                 "projectNo": 1,
                 "status":1
             }
         }];


        db.aggregate('proposals', planPipeline, function(err, data){
            if (!err) {

                res.json({
                    result : data
                });
            }
            else{
                console.log(err);
                res.status(500);
                res.end();
            }
        });
    };

    var getStatusReport = function(req, res, next) {
        _getStatusReport(req, res, next);
    };

    var getSupervisorReport = function(req, res, next) {
        _getSupervisorReport(req, res, next);
    };

    var getDgimReport = function(req, res, next) {
        _getDgimReport(req, res, next);
    };

    var getTasksExcelReport = function(req, res, next) {
        _getTasksExcelReport(req, res, next);
    };

    var getDurationReport = function(req, res, next) {
        _getDurationReport(req, res, next);
    };

    var getStatusesDurationReport = function(req, res, next) {
        _getStatusesDurationReport(req, res, next);
    };

    var getStatusesAvgDurationReport = function(req, res, next) {
        _getStatusesAvgDurationReport(req, res, next);
    };

    var getPlanReport = function(req, res, next) {
        _getPlanReport(req, res, next);
    };

    var getExportReport = function(req, res, next) {
        _getExportReport(req, res, next);
    };

    var router = express.Router();
    router.get('/status/:projectId', getStatusReport);
    router.get('/supervisor/:projectId', getSupervisorReport);
    router.get('/duration/:projectId', getDurationReport);
    router.get('/statusesDuration/:projectId', getStatusesDurationReport);
    router.get('/statusesAvgDuration/:projectId', getStatusesAvgDurationReport);
    router.get('/plan/:projectId', getPlanReport);
    router.post('/dgim/:projectId', getDgimReport);
    router.post('/tasksExcel/:projectId', getTasksExcelReport);
    router.post('/export/:projectId', _getExportReport);

    return router;
};

module.exports = reportsAPIFactory;
