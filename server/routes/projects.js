"use strict";
var express = require("express");
var apiFactory = require("./restFactory");
var db = require("../db-manager")();

function projectsRESTFactory(db) {
    var collectionName = "projects";
    var router = express.Router();
  
    router.get("/", apiFactory.GETAll(db, collectionName, null, null));
    router.get("/:id", apiFactory.GET(db, collectionName));
    router.post("/:id", apiFactory.POST(db, collectionName, null, null));
    router.put("/:id", apiFactory.PUT(db, collectionName, null, null));
    router.delete("/:id", apiFactory.DELETE(db, collectionName));

    return router;
}

module.exports = projectsRESTFactory;
