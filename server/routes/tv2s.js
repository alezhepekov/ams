'use strict';

var express = require('express');
var db = require('../db-manager');
var month = new Array('января','февряля','марта','апреля','мая','июня','июля','августа','сентября','октября','ноября','декабря');

var tv2sAPIFactory = function (db) {
    var getTv2Show = function(req, res) {

        function getGraphStartDate(dt)
        {
            var date = new Date(dt);
            return date.toLocaleDateString();
        }

        function getStartDate(dt)
        {
            var date = new Date(dt);
            return date.toLocaleString();
        }

        function getEndDate(dt, days)
        {
            days = days || 1;
            var date = new Date(dt);

            var i=0;

            while (i <= days) {
                date.setDate(date.getDate() + 1);
                if ((date.getDay() != 0) && (date.getDay() != 6)) {
                    i++;
                }
//                date.setDate(date.getDate() + days);
            }

            return date.toLocaleDateString();
        }

        function getWarkDays(dt1, dt2)
        {
            var tmp = new Date(dt1)
            var date1 = new Date(tmp.toLocaleDateString());
            tmp = new Date(dt2)
            var date2 = new Date(tmp.toLocaleDateString());

            if (date1 > date2) return 0;

            var i=0;
            while (date1 < date2) {
                date1.setDate(date1.getDate() + 1);
                if ((date1.getDay() != 0) && (date1.getDay() != 6)) i++;
            }

            return i;
        }

        function sortTasks(a, b) {
            var dta = new Date(a.creationDate);
            var dtb = new Date(b.creationDate);
            if (dta.getTime() < dtb.getTime())
                return -1;
            if (dta.getTime() > dtb.getTime())
                return 1;
            return 0;
        }

        var logEntry = formUserEntryLog(req, types['viewTv']);
        logEntry.objectId = '54c5eb025079053c0350d086';
        logEntry.projectName = 'Инвестпроекты';
        grayLogBusiness.log('Просмотр доски статистики Инвестпроектов', logEntry);

        var res_data = [];

        var dateCreatedStart = new Date(2015, 0, 1);
        var mycriteria = {
            "projectId" :"54c5eb025079053c0350d086",
            "started": {$gte: dateCreatedStart},
            "status.title": {$nin: ["выполнена", "отменена", "отложена", "согласование в ДГИМ", "повторное согласование в ДГИМ"]} //на эти заявки мы не можем повлиять!!!
        };

        var myoptions = {
            sort: {
                "currentTask.dueDate": -1,
                "started": -1
            }
        };

        db.find('proposals', mycriteria, function(err, proposals) {
            try {
                if (!err) {
                    res.status(200);
                    var indx = 0;
                    proposals.forEach( function(value, index) {

                        if (!value.currentTask)
                            return;

                        //сортируем таски
                        value.tasks.sort(sortTasks);

                        var nowdt = new Date();
                        nowdt = new Date(nowdt.toLocaleDateString());

                        var cssclass = 'done';

                        //время дэдлайна
                        var dueDatestr = "";
                        //начало графика
                        var graphstartdate = getGraphStartDate(value.started);
                        //конец графика
                        var graphenddate = getEndDate(nowdt);
                        //реальное положение дел
                        var tasknow = {};

                        //количество задач;
                        var ts = 0;
                        var tl = value.tasks.length-1;

                        if(value.tasks[tl].finished != false)
                            return; //хреновая анкета...
                        if (value.tasks[0].status.title != 'осмотр')
                            return;

                        var newtasks = [];
                        var newtaskscnt = 0;
                        var layer = true;

                        for(var i=0; i<tl; i++)
                        {
                            var tasknow = {};

                            tasknow.title = value.tasks[i].status.title;
                            tasknow.layer = layer ? 1 : 2;
                            layer = !layer;
                            tasknow.start =  getStartDate(value.tasks[i].creationDate);
                            tasknow.end =  getStartDate(new Date()); //tasknow.end =  getEndDate(new Date());

                            if (value.tasks[i].status.title == 'осмотр') {
                                tasknow.start = getStartDate(value.started);
                                tasknow.end = getStartDate(value.tasks[i].finishingDate); //getEndDate(value.tasks[i].finishingDate);
                            }

                            if (value.tasks[i].status.title == 'оценка') {
                                //дата начала = концу осмотра
                                tasknow.start = getStartDate(value.tasks[i-1].finishingDate);
                                tasknow.end = getStartDate(value.tasks[i].finishingDate); //getEndDate(value.tasks[i].finishingDate);
                                if (value.tasks[i+1].status.title != 'согласование в ДГИМ') {
                                    for(var j=i+2; j<=tl; j++) {
                                        i =  j;
                                        if (value.tasks[i].status.title == 'согласование в ДГИМ') {
                                            i = j-1;
                                            j = 9999;
                                        }
                                    }
                                    if (i != tl) {
                                        tasknow.end = getStartDate(value.tasks[i].finishingDate); //getEndDate(value.tasks[i].finishingDate);
                                    }
                                    else {
                                        tasknow.end = getStartDate(new Date()); //getEndDate(new Date());
                                    }
                                }
                            }

                            if (value.tasks[i].status.title == 'согласование в ДГИМ')
                            {
                                tasknow.start = getStartDate(value.tasks[i-1].finishingDate);//getStartDate(newtasks[newtaskscnt-1].end);
                                tasknow.end = getStartDate(value.tasks[i].finishingDate); //getEndDate(value.tasks[i].finishingDate);

                                if (value.tasks[i+1].status.title != 'согласование в СРО')
                                {
                                    for(var j=i+2; j<=tl; j++)
                                    {
                                        i =  j;
                                        if (value.tasks[i].status.title == 'согласование в СРО') {
                                            i = j-1;
                                            j = 9999;
                                        }
                                    }
                                    //tasknow.end = getStartDate(value.tasks[i].finishingDate);
                                    if (i != tl) {
                                        tasknow.end = getStartDate(value.tasks[i].finishingDate); //getEndDate(value.tasks[i].finishingDate);
                                    }
                                    else {
                                        tasknow.end = getStartDate(new Date()); //getEndDate(new Date());
                                    }
                                }
                            }

                            if (value.tasks[i].status.title == 'согласование в СРО') {
                                tasknow.start = getStartDate(value.tasks[i-1].finishingDate); //getStartDate(newtasks[newtaskscnt-1].end);
                                tasknow.end = getStartDate(value.tasks[i].finishingDate); //getEndDate(value.tasks[i].finishingDate);
                                if (value.tasks[i+1].status.title != 'сдача') {
                                    for(var j=i+2; j<=tl; j++)
                                    {
                                        i =  j;
                                        if (value.tasks[i].status.title == 'сдача') {
                                            i = j-1;
                                            j = 9999;
                                        }
                                    }
                                }
                                if (i != tl) {
                                    tasknow.end = getStartDate(value.tasks[i].finishingDate); //getEndDate(value.tasks[i].finishingDate);
                                }
                                else {
                                    var e20 = getEndDate(tasknow.start, 10);
                                    var e11 = new Date();
                                    var e21 = new Date(e20);

                                    if (e11.getTime() < e21.getTime()) {
                                        tasknow.end = e20;
                                    } else {
                                        tasknow.end = getStartDate(new Date()); //e11
                                    }
                                }
                            }

                            var dd = getWarkDays(tasknow.start, tasknow.end); //количество дней просрочки
                            tasknow.color = '#71AE70';
                            if ((((tasknow.title == 'оценка') || (tasknow.title == 'согласование в СРО')) && (dd > 11)) || ((tasknow.title == 'осмотр') && (dd > 6))) {
                                tasknow.color = '#ee5f5b';
                            }
                            newtasks[newtaskscnt] = tasknow;
                            newtaskscnt++;

                            //последний этап
                            if (i != tl) {
                                if (value.tasks[i].status.title == 'подготовка экспертизы') {
                                    var tasklast = {};
                                    tasklast.layer = layer ? 1 : 2;
                                    layer = !layer;
                                    tasklast.title = 'доставка';
                                    tasklast.start = getStartDate(value.tasks[i].finishingDate);
                                    tasklast.end = getEndDate(new Date()); //getEndDate(new Date());

                                    var dd = getWarkDays(tasklast.start, tasklast.end); //количество дней просрочки
                                    tasklast.color = '#71AE70';
                                    if (dd > 2) {
                                        tasklast.color = '#ee5f5b';
                                    }

                                    newtasks[newtaskscnt] = tasklast;
                                    newtaskscnt++;
                                    break;
                                }
                            }
                        }

                        //текущий таск
                        if ((value.tasks[tl].status.title == 'осмотр') || (value.tasks[tl].status.title == 'оценка')
                                || (value.tasks[tl].status.title == 'согласование в ДГИМ') || (value.tasks[tl].status.title == 'согласование в СРО'))
                        {
                            var tasknow = {};

                            tasknow.title = value.tasks[tl].status.title;
                            tasknow.layer = layer ? 1 : 2;
                            layer = !layer;

                            if (value.tasks[tl].status.title == 'осмотр') {
                                tasknow.start = getStartDate(value.started);

                                var e10 = getEndDate(new Date());
                                var e20 = getEndDate(tasknow.start, 5);
                                var e11 = new Date(e10);
                                var e21 = new Date(e20);
                                if (e11.getTime() < e21.getTime()) {
                                    tasknow.end = e20;
                                } else {
                                    tasknow.end = getStartDate(new Date()); //e11;
                                }
                            } else {
                                tasknow.start = getStartDate(value.tasks[tl - 1].finishingDate); //getStartDate(newtasks[newtaskscnt-1].end);

                                if (value.tasks[tl].status.title != 'согласование в ДГИМ') {
                                    var e10 = getEndDate(new Date());
                                    var e20 = getEndDate(tasknow.start, 10);
                                    var e11 = new Date(e10);
                                    var e21 = new Date(e20);
                                    if (e11.getTime() < e21.getTime()) {
                                        tasknow.end = e20;
                                    } else {
                                        tasknow.end = getStartDate(new Date()); //e11;
                                    }
                                } else {
                                    tasknow.end = getEndDate(new Date());
                                }
                            }

                            var dd = getWarkDays(new Date(), tasknow.end);
                            tasknow.color = '#71AE70';
                            if (dd == 0) {
                                tasknow.color = '#ee5f5b';
                            }
                            newtasks[newtaskscnt] = tasknow;
                            newtaskscnt++;
                        } else { //еще идет предыдущая задача
                            if ((value.tasks[tl].status.title != 'подготовка экспертизы') && (newtasks[newtaskscnt - 1].title != 'доставка')) {
                                newtasks[newtaskscnt - 1].end = getEndDate(new Date());

                                var dd = getWarkDays(newtasks[newtaskscnt - 1].start, newtasks[newtaskscnt - 1].end);
                                newtasks[newtaskscnt - 1].color = '#71AE70';
                                if ((((newtasks[newtaskscnt - 1].title == 'оценка') || (newtasks[newtaskscnt - 1].title == 'согласование в СРО')) && (dd > 10)) || ((newtasks[newtaskscnt - 1].title == 'осмотр') && (dd > 5))) {
                                    newtasks[newtaskscnt - 1].color = '#ee5f5b';
                                }
                            }
                        }

                        //просрочка
                        if (newtasks[newtaskscnt - 1].color == '#ee5f5b') {
                            cssclass = 'danger';
                        }

                        //если остался один день
                        var dt1 = new Date();
                        var dt2 = new Date(newtasks[newtaskscnt - 1].end);
                        var dd = (dt2 - dt1) / (1000 * 60 * 60 * 24);
                        if ((dd > 0) && (dd <=1) && (newtasks[newtaskscnt - 1].color != '#ee5f5b') && ((newtasks[newtaskscnt - 1].title == 'оценка') || (newtasks[newtaskscnt - 1].title == 'согласование в СРО') || (newtasks[newtaskscnt - 1].title == 'осмотр') || (newtasks[newtaskscnt - 1].title == 'доставка'))) {
                            cssclass = 'warning';
                        }

                        graphenddate = getGraphStartDate(getEndDate(newtasks[newtaskscnt-1].end));

                        dueDatestr = dt2.getDate()+" "+month[dt2.getMonth()]+" "+ dt2.getFullYear();

                        res_data[indx] = {
                            ProjectID: value.projectId,
                            ObjID: value._id,
                            AgreementNo: value.number != "" ? value.number: " ",
                            Address: value.address,
                            CoordManager: "Исполнитель: "+((value.currentTask.executor) ? value.currentTask.executor.fullName : "-"),
                            Appraiser: ""+((("manager" in value) && (value.manager != null)) ? value.manager.fullName : "-")+";"+((("coordinator" in value) && value.coordinator != null) ? value.coordinator.fullName : "-"),
                            Started: new Date(value.started).getDate() + " " + month[new Date(value.started).getMonth()]+" "+(value.owner ? value.owner.fullName : "-"),
                            PlannedReadyDateTime: dueDatestr,
                            status: value.status.title,
                            cssclass: cssclass,
                            //tasks: value.tasks,
                            ntasks: newtasks,
                            graphstartdate: graphstartdate,
                            graphenddate: graphenddate
                        };
                        indx++;
                    });
                    res.json(res_data);
                } else {
                    res.status(500);
                    res.send(err);
                }
                res.end();
            }
            catch (e) {
                next(e);
            }
        }, myoptions);
    };

    var router = express.Router();
    router.get('/', getTv2Show);
    return router;
};

module.exports = tv2sAPIFactory;
