'use strict';
var express = require('express');
var bcrypt = require('bcrypt-nodejs');
var clean = require('../db-manager/cleaners');
var validator = require('validator');
var apiFactory = require('./restFactory');
var sms = require('../sms');
/**
 * implements RESTful api for users mongodb collection
 */
function usersRESTFactory(db, jwt, secret) {
    /* Name of mongo collection */
    var collectionName = 'users';

    var router = express.Router();

    /**
     * Encrypts password into bcrypt hash
     * @param   {String} password
     * @returns {String} salt hash
     */
    var encryptPassword = function(password) {
        var salt = bcrypt.genSaltSync(10);
        return bcrypt.hashSync(password, salt);
    };

    /**
     * Returns "cut" user object, firstName, lastName and _id
     * @param   {Object} u user object
     * @returns {Object} "cut" user object
     */
    var briefUser = function(user) {
        var r = {};
        r._id = user._id;
        r.firstName = user.firstName;
        r.lastName = user.lastName;
        return r;
    };

    /**
     * Removes password from user object
     * @param   {Object} user user object
     * @returns {Object} user object without password
     */
    var rmPassword = function(user) {
        delete user.password;
        return user;
    };

    /**
     * Returns function< which sends bad request with given response;
     * @param   {Experss response} res
     * @returns {Function}
     */
    var send400 = function(res) {
        return function() {
            res.status(400);
            res.json({
                message: 'Bad request'
            });
            res.end();
        };
    };

    /**
     * Allows user to log in with login(email or phone number) and password
     * @param {Object} req Express request object
     * @param {Object} res Express response object
     */
    router.post('/login/', function(req, res, next) {
        var logEntry = formUserEntryLog(req, types['login']);
        db.findUserByLogin(req.body.login, function(err, user) {
            if (!err) {
                var checkPass;
                try{
                    checkPass = user && bcrypt.compareSync(req.body.password, user.password) && user.active;
                } catch (e) {
                    next(e);
                    res.status(500);
                    res.end();
                    return;
                }
                if (!user || !checkPass || !user.active) {
                    res.status(401);
                    res.json({
                        message: 'Wrong user or password'
                    });
                    logEntry.userEmail = (req.body.login) ? req.body.login : '';
                    logEntry.type += 'Error';
                    grayLogBusiness.log("Неправильный логин или пароль", logEntry);
                    return;
                } else {
                    if (user.needsSMS) {
                        var credentials = {};
                        credentials.expires = new Date(new Date().getTime() + 60 * 10 * 1000);
                        credentials.code = Math.floor(Math.random() * (9999 - 1000 + 1) + 1000);
                        credentials.user = user;
                        db.insertOne('credentials', credentials, function(err, doc) {
                            if (!err) {
                                res.status(202);
                                res.json({
                                    id: credentials._id
                                });
                                res.end();
                                sms(user.phone, "Код для входа:" + credentials.code);
                            } else {
                                res.status(500);
                                res.end();
                                next();
                            }
                        });
                    } else {
                        var token = jwt.sign(user, secret, {
                            expiresInMinutes: 60 * 5
                        });
                        res.json({
                            token: token,
                            profile: rmPassword(user)
                        });
                        logEntry.userEmail = (user) ? user.email : '';
                        logEntry.userName = (user) ? user.fullName : '';
                        logEntry.userPosition = (user) ? user.position : '';
                        logEntry.userOrganizationId = (user) ? user.organizationId : '';
                        logEntry.userOrganization = (user) ? getOrganizationName(user.organizationId) : '';
                        logEntry.objectId = (user) ? user._id.toHexString() : '';
                        grayLogBusiness.log("Корректный вход", logEntry);
                        return;
                    }
                }
            } else {
                res.status(500);
                res.json(err);
                res.end();
            }
        });
    });

    /**
     * Allows users with sms code authentication to login with code from sms
     */
    router.post('/smscheck', function(req, res) {
        var logEntry = formUserEntryLog(req, types['smsCheck']);
        var id = db.idFromHexString(req.body.id, send400(res));
        db.findOne('credentials', id, function(err, doc) {
            if (!err) {
                console.log(id);
                if (!doc) {
                    res.status(404);
                    res.end();
                    return;
                }

                logEntry.userEmail = (doc.user) ? doc.user.email : '';
                logEntry.userName = (doc.user) ? doc.user.fullName : '';
                logEntry.userPosition = (doc.user) ? doc.user.position : '';
                logEntry.userOrganizationId = (doc.user) ? doc.user.organizationId : '';
                logEntry.userOrganization = (doc.user) ? getOrganizationName(doc.user.organizationId) : '';
                logEntry.objectId = (doc.user) ? doc.user._id.toHexString() : '';

                if (doc.code == req.body.code && doc.expires > new Date()) {
                    res.status(200);
                    var token = jwt.sign(doc.user, secret, {
                        expiresInMinutes: 60 * 5
                    });
                    res.json({
                        token: token,
                        profile: rmPassword(doc.user)
                    });
                    res.end();
                    grayLogBusiness.log("Корректный вход с помощью SMS", logEntry);
                    return;
                } else {
                    res.status(401);
                    logEntry.type += 'Error';
                    grayLogBusiness.log("Некорректный вход с помощью SMS", logEntry);
                    res.end();
                }
                db.deleteOne('credentials', id, function(err, n) {
                    if (err) {
                        console.log(err);
                    }
                });
            } else {
                res.status(500);
                res.end();
            }
        });
    });

    /**
     * Allows user to see own profile.
     * @param {Object} req Express request object
     * @param {Object} res Express response object
     */
    router.get('/profile/', function(req, res) {
        var id;
        id = db.idFromHexString(req.user._id, send400(res));

        var onSuccFind = function(err, user) {
            if (!err) {
                res.status(200);
                if (user !== null) {
                    /* PASSWORDS(even hashes) shoud not leak to client */
                    res.json(rmPassword(user));
                } else {
                    res.status(404);
                    res.json({
                        message: 'Not Found'
                    });
                }
            } else {
                res.status(500);
                res.json(err);
            }
        };

        db.findOne('users', id, onSuccFind);
    });

    /**
     * Allows user to modify own profile.
     * @param {Object} req Express request object
     * @param {Object} res Express response object
     */
    router.put('/profile/', function(req, res) {
        /* Check if id is valid 24-digit hex string, otherwise send Bad Request */
        var id;
        id = db.idFromHexString(req.body._id, send400(res));

        var newUser = req.body;

        var onSuccUpdate = function(err, user) {
            if (!err) {
                delete user.password;
                res.status(200)
                    .json(user);
            } else {
                res.status(500)
                    .end();
            }
        };
        try {
            newUser = clean.profile(newUser)
            db.updateOne('users', id, newUser, onSuccUpdate);
        } catch (ex) {
            res.status(400);
            res.json({
                message: ex
            });
        }
    });

    /**
     * Allows user to change own password. Old one is needed.
     * @param {Object} req Express request object
     * @param {Object} res Express response object
     */
    router.put('/profile/password', function(req, res) {

        var onSuccUpdate = function(err, user) {
            if (!err) {
                res.status(200);
                res.end();
            } else {
                res.status(500);
                res.end();
            }
        };
        /* If user was found, replaces his password and calls db.replaceOne */
        var onSuccFind = function(err, foundUser) {
            if (!err) {
                /*Check user's old password.*/
                if (!foundUser || !(bcrypt.compareSync(req.body.old, foundUser.password))) {
                    /* Send 401 to users with incorrect password.*/
                    res.send(401, 'Wrong password');
                    res.end();
                    return;
                } else {
                    /* Replace password */
                    foundUser.password = req.body.new;
                    foundUser.confirm = req.body.confirm;
                    /* Validate modified user */
                    try {
                        foundUser = clean.user(foundUser);
                        foundUser.password = encryptPassword(foundUser.password);
                        /* Replace old one in db*/
                        db.replaceOne('users', foundUser._id, foundUser, onSuccUpdate);
                    } catch (ex) {
                        res.status(400);
                        res.json({
                            message: ex
                        });
                    }
                }
            } else {
                res.status(500);
                res.json(err);
                res.end();
            }
        };

        if (!validator.isAlphanumeric(req.body.new)) {
            res.status(400);
            res.json({
                message: 'Пароль может состоять только из английских букв и цифр!'
            });
            res.end();
            return;
        }

        db.findUserByLogin(req.user.email, onSuccFind);
    });


    /**
     * Allows root to change any password. Old one is needed.
     * @param {Object} req Express request object
     * @param {Object} res Express response object
     */
    router.put('/:id/password', function(req, res) {
        var onSuccUpdate = function(err, user) {
            if (err) {
                res.status(500);
                res.end();
            } else {
                res.status(200);
                res.end();
            }
        };
        /* If user was found, replaces his password and calls db.replaceOne */
        var onSuccFind = function(err, foundUser) {
            if (!err) {
                /*Check if requester is root.*/
                if (!req.user.isRoot) {
                    /* Send 401 to users with incorrect password.*/
                    res.status(403);
                    res.end();
                    return;
                } else {
                    /* Replace password */
                    foundUser.password = req.body.password;
                    foundUser.confirm = req.body.password;
                    /* Validate modified user */
                    try {
                        foundUser = clean.user(foundUser);
                        foundUser.password = encryptPassword(foundUser.password);
                        /* Replace old one in db*/
                        db.replaceOne('users', foundUser._id, foundUser, onSuccUpdate);
                    } catch (ex) {
                        res.status(400);
                        res.json({
                            message: ex
                        });
                    }
                }
            } else {
                res.status(500);
                res.json(err);
                res.end();
            }
        };

        if (!validator.isAlphanumeric(req.body.password)) {
            res.status(400);
            res.json({
                message: 'Пароль может состоять только из английских букв и цифр!'
            });
            res.end();
            return;
        }

        db.findOne('users', db.idFromHexString(req.params.id), onSuccFind);
    });

    /**
     * Retrieve all users(passwords are deleted)
     * @param {Object} req Express request object
     * @param {Object} res Express response object
     */
    router.get('/', apiFactory.GETAll(db, 'users'));

    router.get('/manage', apiFactory.GETAll(db, 'users'));

    /**
     * Retrieve brief info(_id, firstName, lastName) about all users.
     * @param {Object} req Express request object
     * @param {Object} res Express response object
     */
    router.get('/brief', apiFactory.GETAll(db, 'users'));

    /**
     * Retrieve user by id.
     * @param {Object} req Express request object
     * @param {Object} res Express response object
     */
    router.get('/:id', apiFactory.GET(db, 'users', rmPassword));

    /**
     * Retrieve brief info(_id, firstName, lastName) about user with given id.
     * @param {Object} req Express request object
     * @param {Object} res Express response object
     */
    router.get('/:id/brief', apiFactory.GET(db, 'users', briefUser));

    /**
     * Add new user.
     * @param {Object} req Express request object
     * @param {Object} res Express response object
     */
    router.post('/', function(req, res) {
        var user = req.body;
        var onSuccInsert = function(err, user) {
            if (!err) {
                res.status(200);
                res.json(rmPassword(user));
            } else {
                res.status(500);
                res.json({
                    message: 'Something went wrong'
                });
            }
        };

        /*Validate and save doc to mongo */

        try {
            user = clean.user(user)
                /* Encrypt password */
            user.password = encryptPassword(user.password);
            delete user.confirm;
            db.insertOne(collectionName, user, onSuccInsert);
        } catch (ex) {
            res.status(400);
            res.json({
                message: ex
            });
        }
    });

    /**
     * Update user explicitly.
     * @param {Object} req Express request object
     * @param {Object} res Express response object
     */
    router.put('/:id', function(req, res) {
        /* Check if id is valid 24-digit hex string, otherwise send Bad Request */
        var id;
        id = db.idFromHexString(req.params.id, send400(res));

        var newUser = req.body;

        var onSuccUpdate = function(err, user) {
            if (!err) {
                res.status(200);
                res.json(rmPassword(user));
            } else {
                res.status(500);
                res.end();
            }
        };
        try {
            clean.profile(newUser);
            newUser.fullName = newUser.firstName + ' ' + newUser.lastName;
            db.updateOne(collectionName, id, newUser, onSuccUpdate);
        } catch (ex) {
            res.status(400);
            res.json({
                message: ex
            });
        }

    });

    /**
     * Delete user from collection.
     * @param {Object} req Express request object
     * @param {Object} res Express response object
     */
    router.delete('/:id', apiFactory.DELETE(db, 'users'));

    return router;
}

module.exports = usersRESTFactory;
