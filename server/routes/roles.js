'use strict';
var express = require('express');
var apiFactory = require('./restFactory');
var clean = require('../db-manager/cleaners');

var rolesAPIFactory = function (db) {
    var router = express.Router();
    router.get('/', apiFactory.GETAll(db,'roles', null));
    router.get('/:id', apiFactory.GET(db, 'roles', null));
    router.post('/',apiFactory.POST(db,'roles', null, null));
    router.put('/:id',apiFactory.PUT(db,'roles', null, null));
    router.delete('/:id',apiFactory.DELETE(db,'roles'));
    return router;
};

module.exports = rolesAPIFactory;
