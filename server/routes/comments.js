'use strict';
var express = require('express');
var apiFactory = require('./restFactory');
var db = require('../db-manager')();

var buildCriteria = function(req) {
    try {
        if (req.query.propId) {
            return {
                proposalId: req.query.propId
            };
        } else {
            return {};
        }
    }catch(e){
        next(e);
    }
};

function commentsRESTFactory(db) {
    var collectionName = 'comments';
    var router = express.Router();
    var validate = function(pr) {
        return {
            ok: true
        };
    };

    /* GET all items in collectionName */
    router.get('/', apiFactory.GETAll(db, 'comments', null, buildCriteria));

    /* GET one item from collectionName by id */
    router.get('/:id', apiFactory.GET(db, 'comments'));

    /* POST(add) item to collectionName */
    router.post('/', apiFactory.POST(db, 'comments', null, null));

    /* PUT (update) item in collectionName by id */
    router.put('/:id', apiFactory.PUT(db, 'comments', null, null));

    /* DELETE item from collectionName by id*/
    router.delete('/:id', apiFactory.DELETE(db, 'comments'));

    return router;
}

module.exports = commentsRESTFactory;