'use strict';
var express = require('express');
var apiFactory = require('./restFactory');
var db = require('../db-manager')();

var buildCriteria = function(req) {
    try {
        if (req.query.propId) {
            return {
                proposalId: req.query.propId
            };
        } else {
            return {};
        }
    }catch(e){
        next(e);
    }
};



function feedbacksRESTFactory(db) {
    var collectionName = 'feedbacks';
    var router = express.Router();
    var validate = function(pr) {
        return {
            ok: true
        };
    };

    var saveFeedback = function (req, res) {
        var msg = 'Отправка обратной связи';
        var logEntry = formUserEntryLog(req, types['sendFeedback']);
        //logEntry.projectName = (req.params.projectId) ? getProjectName(req.params.projectId) : '';
        logEntry.projectName = (req.params.projectId) ? getProjectName(req.params.projectId) : '';
        logEntry.objectId = (req.params.id) ? req.params.id : '';

        grayLogBusiness.log(msg, logEntry);
        res.status(200).send("Successful save feedback");
    }

    /* GET all items in collectionName */
    router.get('/', apiFactory.GETAll(db, 'feedbacks', null, buildCriteria));

    /* GET one item from collectionName by id */
    router.get('/:id', apiFactory.GET(db, 'feedbacks'));

    /* POST(add) item to collectionName */
    //router.post('/', saveFeedback);
    router.post('/', apiFactory.POST(db, 'feedbacks', null, null));

    /* PUT (update) item in collectionName by id */
    router.put('/:id', apiFactory.PUT(db, 'feedbacks', null, null));

    /* DELETE item from collectionName by id*/
    router.delete('/:id', apiFactory.DELETE(db, 'feedbacks'));

    return router;
}

module.exports = feedbacksRESTFactory;
