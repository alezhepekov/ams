'use strict';

var express = require('express');
var db = require('../db-manager');
var month = new Array('января','февряля','марта','апреля','мая','июня','июля','августа','сентября','октября','ноября','декабря');

var tvsAPIFactory = function (db) {
    var getTvShow = function(req, res) {
        var logEntry = formUserEntryLog(req, types['viewTv']);
        logEntry.objectId = '54c5eb025079053c0350d086';
        logEntry.projectName = 'Инвестпроекты';
        grayLogBusiness.log('Просмотр доски статистики Инвестпроектов', logEntry);

        var res_data = [];

        var dateCreatedStart = new Date(2015, 0, 1);
        var mycriteria = {
            "projectId" :"54c5eb025079053c0350d086",
            "started": {$gte: dateCreatedStart},
            "status.title": {$nin: ["выполнена", "отменена"]}
        };

        var myoptions = {
            /*sort: {
                started: -1
            }*/
        };

        db.find('proposals', mycriteria, function(err, proposals) {
            try {
                if (!err) {
                    res.status(200);
                    var indx = 0;
                    proposals.forEach( function(value, index) {

                        if (!value.currentTask)
                            return;

                        if ("finishingDate" in value.currentTask)
                            var dd = new Date(value.currentTask.finishingDate);
                        var dt = ("finishingDate" in value.currentTask) ? " - "+dd.getDate()+" "+month[dd.getMonth()]+" "+dd.getFullYear()+" в "+dd.getHours()+":"+dd.getMinutes() : " ";

                        res_data[indx] = {
                            ProjectID: value.projectId,
                            AgreementNo: value.number != "" ? value.number: " ",
                            Customer: value.address,
                            AddressFull: "",
                            ObjectDescription: value.status.title+(value.currentTask.finished ? dt : " "),
                            AppraisalResults: null,
                            //ThirdPartyName: '((("coordinator" in value) && value.coordinator != null) ? value.coordinator.fullName : "-")',
                            CoordManager: "Исполнитель: "+((value.currentTask.executor) ? value.currentTask.executor.fullName : "-"),
                            Appraiser: ""+((("manager" in value) && (value.manager != null)) ? value.manager.fullName : "-")+";"+((("coordinator" in value) && value.coordinator != null) ? value.coordinator.fullName : "-")+";"+(value.owner ? value.owner.fullName : "-"),
                            //ReportSignedBy: '(("manager" in value) && (value.manager != null)) ? value.manager.fullName : "-"' ,
                            DocumentsPassedForAppraisalAt: "/Date("+new Date(value.started).getTime()+")/",
                            PlannedReadyDateTime: value.currentTask ? ("dueDate" in value.currentTask) ? "/Date("+new Date(value.currentTask.dueDate).getTime()+")/" : null : null,
                            status: value.status.title,
                            mysortOrder: value.status.title == "выполнена" ? "/Date("+(new Date(value.started).getTime()-1000000000000)+")/" : (value.status.title != "отменена" && value.status.title != "отложена") ? value.currentTask ? ("dueDate" in value.currentTask) ? (new Date(value.currentTask.dueDate).getTime()+86400000)<(new Date().getTime()) ? "/Date("+(new Date(value.started).getTime()+1429822800000)+")/" : "/Date("+new Date(value.started).getTime()+")/" : "/Date("+new Date(value.started).getTime()+")/" : "/Date("+new Date(value.started).getTime()+")/" : "/Date("+new Date(value.started).getTime()+")/"
                        };
                        indx++;
                    });
                    res.json(res_data);
                } else {
                    res.status(500);
                    res.send(err);
                }
                res.end();
            }
            catch (e) {
                next(e);
            }
        }, myoptions);
    };

    var router = express.Router();
    router.get('/', getTvShow);
    return router;
};

module.exports = tvsAPIFactory;
