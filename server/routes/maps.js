'use strict';
var express = require('express');
var clean = require('../db-manager/cleaners');
var apiFactory = require('./restFactory');
var db = require('../db-manager')();
var numeral = require('numeral');

function mapsRESTFactory(db) {

    var getStatusClass = function(status) {
        switch(status) {
            case 'заявка': return 'info';
            case 'осмотр': return 'warning';
            case 'оценка': return 'warning';
            case 'исправление': return 'danger';
            case 'проверка': return 'warning';
            case 'проверка после исправления': return 'danger';
            case 'доработка': return 'danger';
            case 'согласование в СРО':
            case 'повторное согласование в СРО':
            case 'исправление ошибок':
            case 'согласование в ДГИМ':
            case 'повторное согласование в ДГИМ':
                return 'info';
            case 'сдача':
            case 'подготовка экспертизы':
            case 'повторная экспертиза':
            case 'печать':
            case 'доставка':
            case 'выполнена': return 'success';
            default: return 'default';
        }
    };

    var mapIconPreset = function(status) {
        var iconPresets = {
            'default':'islands#grayStretchyIcon',
            'info':'islands#blueStretchyIcon',
            'warning':'islands#darkOrangeStretchyIcon',
            'danger':'islands#redStretchyIcon',
            'success':'islands#darkGreenStretchyIcon'
        }, statusClass = getStatusClass(status);
        return (statusClass && iconPresets[statusClass]) || 'islands#blueDotIcon';
    };

    var _getProposals = function(req, res, next, criteria){

        var mapPipline = [
            {
                $match: criteria
            },
            {
                $group:
                {
                    _id: null,
                    totalCount: {  $sum: 1  }
                }
            }
        ];

        var statisticData  = {
            totalCount : 0
        };

        db.find('proposals', criteria, function(err, proposals) {
            try {
                if (!err) {
                    var orgMode = isProjectOrgMode(req.params.projectId);
                    db.collection("proposals").count(criteria, function(err, q) {
                        try {
                            if (!err) {

                                statisticData.totalCount = q;

                                numeral.language('ru', {
                                    delimiters: {
                                        thousands: ' ',
                                        decimal: ','
                                    }
                                });

                                numeral.language('ru');

                                var points = proposals.map(function(p){

                                    var customFields = p.customFields && p.customFields.value ? p.customFields.value.filter(function (x){ return x.id == '635fbd10-9a99-11e4-80e6-17c87fc056ce'; }) : null;
                                    var areaField = {};
                                    var marketPriceField = {};
                                    var buildingType = {};

                                    if (customFields && customFields.length > 0){

                                        areaField = customFields[0].value.filter(function (x){ return x.id == '7fa2b8e0-9a96-11e4-80e6-17c87fc056ce'; })
                                        areaField = areaField ? areaField[0] : null;

                                        marketPriceField = customFields[0].value.filter(function (x){ return x.id == '905d9580-9a9e-11e4-80e6-17c87fc056ce'; })
                                        marketPriceField = marketPriceField ? marketPriceField[0] : null;

                                        buildingType = customFields[0].value.filter(function (x){ return x.id == '580b67d0-9a93-11e4-80e6-17c87fc056ce'; })
                                        buildingType = buildingType ? buildingType[0] : null;
                                    }

                                    var marketPricePerSqm = areaField != undefined && marketPriceField != undefined
                                        ? Math.round(parseFloat(marketPriceField.value) / parseFloat(areaField.value)) : 0;

                                    return {
                                        type: 'Feature',
                                        geometry: {
                                            type: 'Point',
                                            coordinates: [ p.mapAddress.point.lat, p.mapAddress.point.lon ]
                                        },
                                        options: {
                                            preset: mapIconPreset(p.status.title)
                                        },
                                        status : p.status.title,
                                        properties : {
                                            clusterCaption: '№'+p.number,
                                            iconContent:'№'+p.number, //.truncate(5),
                                            balloonContentHeader: '№'+p.number,
                                            balloonContentFooter: '<i><a href="/#/projects/' + p.projectId + '/proposals/' + p._id + '" target="_blank">посмотреть&gt;</a></i>',
                                            balloonContentBody: (orgMode ? '<b>' + p.name + '</b><br/>' : '') + '<b>' + p.address + '</b><br/>' +
                                                (!orgMode ? (buildingType ? buildingType.value + ' ': '') + (areaField ? areaField.value + '&nbsp;м<sup>2</sup>':'') + '<br/>' +
                                                    (marketPricePerSqm ? numeral(marketPricePerSqm).format('0,0') +'&nbsp;руб/м<sup>2</sup><br/>' : '') : '') +
                                                '<i>' + p.status.title + '</i>',
                                            dgiArea: areaField ? areaField.value : 0,
                                            dgiType: buildingType ? buildingType.value: '',
                                            dgiStatus: p.status.title,
                                            dgiMarketPricePerSqm: marketPricePerSqm
                                        }
                                    };
                                });

                                res.json({
                                    points : points,
                                    statistic : statisticData
                                });

                                // всего заявок с учетм условий запроса
                                /*
                                db.aggregate('proposals', [ { $match: criteria }, { $group: { _id: null,  totalCount: {  $sum: 1  } } }], function(err, data){
                                    if (!err){
                                        statisticData.totalCount = data[0].totalCount;
                                    }
                                });*/
                            } else {
                                console.log(err)
                                res.status(500);
                                res.end();
                            }
                        } catch (e) {
                            next(e);
                        }
                    });
                } else {
                    console.log(err)
                    res.status(500);
                    res.end();
                }
            } catch (e) {
                next(e);
            }
        });
    };

    var getDate = function(strDate){
        var dateParts = strDate.split(".");

        var date = new Date(dateParts[2], (dateParts[1] - 1), dateParts[0]);

        return date;
    }

    var _getAllPoints = function(req, res, next) {
        //Получаем запись проекта
        db.findOne('projects', db.idFromHexString(req.params.projectId), function(err, project) {
            try {
                if (!err) {

                    var criteria = {};
                    criteria.projectId = req.params.projectId;


                    criteria.$and = [{"mapAddress.point" : {$exists : true}}];
                    criteria.$and.push({"mapAddress.point.lat" : {$exists : true}});
                    criteria.$and.push({"mapAddress.point.lon" : {$exists : true}});

                    var selectedStatuses = [];
                    var selectedObjectTypes = [];
                    var selectedAreaSizeTypes = [];

                    if (req.query.statuses) {
                        if (typeof req.query.statuses === 'string'){
                            selectedStatuses.push(req.query.statuses);
                        }
                        else{
                            selectedStatuses =req.query.statuses;
                        }

                        criteria.$and.push({"status.id" : { "$in" : selectedStatuses} });
                    }

                    if (req.query.startDate) {
                        var startDate = getDate(req.query.startDate);
                        criteria.$and.push({"started" : { "$gte" : startDate} });
                    }

                    if (req.query.endDate) {
                        var endDate = getDate(req.query.endDate);
                        endDate.setDate(endDate.getDate()+1);
                        criteria.$and.push({"started" : { "$lte" : endDate} });
                    }

                    if (req.query.objectTypes) {

                        if (typeof req.query.objectTypes === 'string'){
                            selectedObjectTypes.push(req.query.objectTypes);
                        }
                        else{
                            selectedObjectTypes =req.query.objectTypes;
                        }

                        criteria.$and.push({
                            customFields : {"$exists": true},
                            "customFields.value" : {$elemMatch : { "value" : { $elemMatch : { "value" : { $in : selectedObjectTypes } } }}}
                        });
                    }

                    if (req.query.areaSizeTypes) {

                        if (typeof req.query.areaSizeTypes === 'string'){
                            selectedAreaSizeTypes.push(req.query.areaSizeTypes);
                        }
                        else{
                            selectedAreaSizeTypes =req.query.areaSizeTypes;
                        }

                        var areas = selectedAreaSizeTypes.map(function(x){
                            return x.split("-");
                        });

                        var areaCriteria = {
                            $or : []
                        };

                        criteria.$and.push({customFields : {"$exists": true}});

                        areas.forEach(function(areaRange){
                            areaCriteria.$or.push({
                                "customFields.value":
                                {
                                    $elemMatch:
                                    {
                                        "value":
                                        {
                                            $elemMatch:
                                            {
                                                "id": "7fa2b8e0-9a96-11e4-80e6-17c87fc056ce",
                                                "value":
                                                {
                                                    "$gte" : areaRange[0], "$lte" : areaRange[1]
                                                }
                                            }
                                        }
                                    }
                                }
                            });
                        });

                        criteria.$and.push(areaCriteria);
                    }

                    //Области видимости по организация
                    if (isProjectOrgMode(req.params.projectId)) {
                        if (isRootOrg(req.user.organizationId)) {
                            if (req.query.orgId) {
                                criteria["orgId"] = req.query.orgId;
                            }
                        } else {
                            criteria["orgId"] = req.user.organizationId;
                        }
                    }

                    _getProposals(req, res, next, criteria);

                } else {
                    console.log(err)
                    res.status(500);
                    res.end();
                }
            } catch (e) {
                next(e);
            }
        });
    }

    var getAllPoints = function(req, res, next) {
        _getAllPoints(req, res, next);
    }

    var router = express.Router();
    router.get('/:projectId/map', getAllPoints);

    return router;
}

module.exports = mapsRESTFactory;
