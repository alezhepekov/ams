var et = require('elementtree');
var XML = et.XML;
var ElementTree = et.ElementTree;
var element = et.Element;
var subElement = et.SubElement;
var request = require('request');
fs = require('fs');

function escapeHtml(text) {
    return text
        .replace(/&/g, "&amp;")
        .replace(/</g, "&lt;")
        .replace(/>/g, "&gt;")
        .replace(/"/g, "&quot;")
        .replace(/'/g, "&#039;");
}

var ex = function() {
    this.buildSOAPRequestBody = function(proposal) {
        var xml = fs.readFileSync(__dirname + '/snippet.xml').toString();

        xml = xml.replace(/\{ProjectNo\}/g, proposal.DgiProjectNo);
        var owner = '';
        if (proposal.owner){
          owner = proposal.owner.firstName + ' ' + proposal.owner.lastName;
        }

        xml = xml.replace(/\{CreatedBy\}/g, owner);

        xml = xml.replace(/\{CreatedAt\}/g, proposal.started.replace('.622Z',''));
        var inspectionDate = proposal.tasks.filter(function(t) {
          return t.status.title === 'осмотр' && !t.finished;
        })[0];

        if (inspectionDate) {
            var d = new Date(inspectionDate.dueDate)
            inspectionDate = d.getFullYear() + '-' + (parseInt(d.getMonth(), 10) + 1) + '-' + d.getDate();
        }

        var customer='', contacts='', area = '';
        var fields = (proposal.customFields && proposal.customFields.value) ? proposal.customFields.value : [];

        /*
        for (var i=0; i<fields.length; i++) {
          if (!(fields[i].value instanceof Array)) {
            if (/[0-9-]{5,20}/.test(fields[i].value)) {
              contacts = fields[i].value || '';
            } else {
              customer = fields[i].value || '';
            }
          }
        }*/

        var area = 0;

        //dynamicFields = fields;
        //Обычно первое поле в списке customFields
        //var dynamicFields = fields.filter(function(x){ return x.id == "635fbd10-9a99-11e4-80e6-17c87fc056ce"});
        var dynamicFields = fields.filter(function(x){ return x.id == "4"});    //temp solution
        if (dynamicFields.length > 0){
            dynamicFields = dynamicFields[0].value;

            area = dynamicFields.filter(function(x){ return x.id == "7fa2b8e0-9a96-11e4-80e6-17c87fc056ce"});
            if (area.length > 0) {
                area = area[0].value.replace(',','.');
                //if (area){
                //    area = ' ' + area + ' кв.м,';
                //}
            }
        }

        //my:TotalArea xsi:nil="true"
        customer = fields.filter(function(x){ return x.id == "b5676540-9a9e-11e4-80e6-17c87fc056ce"});
        if (customer.length > 0){
            customer = customer[0].value;
        }

        contacts = fields.filter(function(x){ return x.id == "cc8ab470-9a9e-11e4-80e6-17c87fc056ce"});
        if (contacts.length > 0){
            contacts = contacts[0].value;
        }

        xml = xml.replace(/\{InspectionDate\}/g, inspectionDate);
        var areaStr = (area && area>0) ? area + ' ' : '';
        var address = proposal.address + ' (ДИМ ' + areaStr + '№' + proposal.number + ')';
        xml = xml.replace(/\{Address\}/g, address);
        xml = xml.replace(/\{DgiID\}/g, proposal._id.toString());
        xml = xml.replace(/\{DgiProjectNo\}/g,proposal._id.toString());

        xml = xml.replace(/\{Customer\}/g, customer);
        xml = xml.replace(/\{Contacts\}/g, contacts);

        if (area && area > 0) {
            xml = xml.replace('my:TotalArea xsi:nil="true" /', 'my:TotalArea&gt;' + area + '&lt;/my:TotalArea');
        }

        xml = xml.replace(/\{InspectionContactName\}/g, customer);
        xml = xml.replace(/\{InspectionContactPhone\}/g, contacts);

        xml = xml.replace()

        return xml;
    };

    this.parseDGIProjectNo = function(xml) {
        var eTree = et.parse(xml);
        return eTree.findtext('soap:Body/GetProjectNoResponse/GetProjectNoResult');
    };

    this.submitProposalToDGI = function(proposal, callback) {
        proposal.started = proposal.started.toISOString();
        var xml = buildSOAPRequestBody(proposal).toString();

        var options = {
            uri: 'https://crm.abn-consult.ru/ProjectServices.asmx',
            method: 'POST',
            timeout: 4000,
            headers: {
            'Content-Type': 'text/xml; charset=utf-8',
            'SOAPAction': '"http://www.abn-consult.ru/ProjectServices/2011/SubmitProject"',
            },
            body: xml
        };

        request(options, callback);
    };

    this.requestDGIProjectNo = function(callback) {
        var xml = '<?xml version="1.0" encoding="utf-8"?>' +
            '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">' +
            '<soap:Body>' +
            '<GetProjectNo xmlns="http://www.abn-consult.ru/ProjectServices/2011" />' +
            '</soap:Body>' +
            '</soap:Envelope>'

        var options = {
            uri: 'https://crm.abn-consult.ru/ProjectServices.asmx',
            method: 'POST',
            timeout: 10000,
            headers: {
                'Content-Type': 'text/xml; charset=utf-8',
                'SOAPAction': '"http://www.abn-consult.ru/ProjectServices/2011/GetProjectNo"'
            },
            body: xml
        };
        console.log('REQUEST SEND HERE');

        request(options, function(err, resp, body) {
            console.log('RESPONSE GOT');
            if (!err) {
                callback(parseDGIProjectNo(body));
            } else {
                callback(null);
            }
            });
        };

        return this;
    };

module.exports = ex();
