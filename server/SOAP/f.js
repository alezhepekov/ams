var SOAP = require('./index');
var prop = {
  DgiProjectNo: "2323-12",
  _id: 'abcd12312321dcdbasd32423432',
  owner: {
    _id: "ASDASDASDADA1S241224sadasasd",
    firstName: 'Vasya',
    lastName: 'Pupkin'
  },
  manager: {
    _id: "ASDASDASDADAS1241224sadasasd",
    firstName: 'Vasya',
    lastName: 'Pupkin'
  },
  currentTask: {
    status: {
      title: "Example",
      className: "default"
    }
  },
  tasks: [{
    dueDate: new Date(),
    status: {
      title: "осмотр",
      className: "default"
    }
  }],
  started: new Date(),
  address: "Moscow, Red Square 1",
  customFields: {
    "id": "e957e780-9a9e-11e4-80e6-17c87fc056ce",
    value: [{
      "id": "cc8ab470-9a9e-11e4-80e6-17c87fc056ce",
      "value": "321312"
    }, {
      "id": "b5676540-9a9e-11e4-80e6-17c87fc056ce",
      "value": "ооо\"пелпомена\""
    }, {
      "id": "635fbd10-9a99-11e4-80e6-17c87fc056ce",
      "value": [{
        "id": "580b67d0-9a93-11e4-80e6-17c87fc056ce",
        "value": ""
      }, {
        "id": "7fa2b8e0-9a96-11e4-80e6-17c87fc056ce",
        "value": 232.8
      }, {
        "id": "bd43da30-9a96-11e4-80e6-17c87fc056ce",
        "value": ""
      }, {
        "id": "1e8cadc0-9a98-11e4-80e6-17c87fc056ce",
        "value": ""
      }, {
        "id": "4",
        "value": ""
      }, {
        "id": "714cf460-9a99-11e4-80e6-17c87fc056ce",
        "value": ""
      }, {
        "id": "d51825b0-9a9d-11e4-80e6-17c87fc056ce",
        "value": ""
      }, {
        "id": "ef7fa360-9a9d-11e4-80e6-17c87fc056ce",
        "value": ""
      }, {
        "id": "05d54250-9a9e-11e4-80e6-17c87fc056ce",
        "value": ""
      }, {
        "id": "43b54160-9a9e-11e4-80e6-17c87fc056ce",
        "value": ""
      }, {
        "id": "5b3e3030-9a9e-11e4-80e6-17c87fc056ce",
        "value": ""
      }, {
        "id": "62717330-9a9e-11e4-80e6-17c87fc056ce",
        "value": ""
      }, {
        "id": "6becaa60-9a9e-11e4-80e6-17c87fc056ce",
        "value": ""
      }, {
        "id": "72ea3760-9a9e-11e4-80e6-17c87fc056ce",
        "value": ""
      }, {
        "id": "7b6c8fa0-9a9e-11e4-80e6-17c87fc056ce",
        "value": ""
      }, {
        "id": "82058e70-9a9e-11e4-80e6-17c87fc056ce",
        "value": {
          "day": 10,
          "month": 12,
          "year": 2014
        }
      }, {
        "id": "905d9580-9a9e-11e4-80e6-17c87fc056ce",
        "value": 0
      }, {
        "id": "9b945f60-9a9e-11e4-80e6-17c87fc056ce",
        "value": 0
      }, {
        "id": "a3c1e0e0-9a9e-11e4-80e6-17c87fc056ce",
        "value": ""
      }]
    }]
  }
};

console.log(SOAP.buildSOAPRequestBody(prop));