var MongoClient = require('mongodb').MongoClient,
    objId = require('mongodb').ObjectID,
    config = require('../config');

function DbManager(url) {
    'use strict';
    var self = this;
    self.url = url;
    self.db = null;

    MongoClient.connect(url, function (err, db) {
        if (err !== null) {
            throw new Error('Connection to ' + url + ' failed!');
        }
        console.log('Connected to ' + self.url);

        if (self.db === null) {
            self.db = db;
        }
    });
    /**
     * Get collection by name
     * @param   {String} collection name
     * @returns {Object} MongoDB collection
     */
    self.collection = function (collection) {
        return self.db.collection(collection);
    };

    /**
     * Converts hex string id to Object
     * @param   {String} idHex hex string(_id)
     * @param   {Function} callback, id is invalid.
     * @returns {Object} ObjectId
     */
    self.idFromHexString = function (idHex, onError) {
        try {
            return objId.createFromHexString(idHex);
        } catch (E) {
            if(onError){
                onError();
            }
        }
    };

    /**
     * Finds collection docs filtered by criteria and calls callback
     * @param {String}   collectionName Name of collection
     * @param {Object}   criteria       See node-mongodb-native driver docs
     * @param {Function} callback       Takes two args, err and result(Array of docs)
     * @param {Object}   options        Skip,limit, sort, etc...
     */
    self.find = function (collectionName, criteria, callback, options) {
        var coll = self.collection(collectionName);
        var skip =0, limit = 0, sort = {_id: 1};
        if(!options){
            options = {};
        }
        if(options.skip){
            skip = options.skip;
        }
        if(options.limit){
            limit = options.limit;
        }
        if(options.sort){
            sort = options.sort;
        }
        coll.find(criteria).sort(sort).skip(skip).limit(limit).toArray(callback);
    };

    /**
     * Finds one doc in collection by _id and calls callback
     * @param {String}   collectionName Name of collection
     * @param {Object}   id             See idFromHexString
     * @param {Function} callback       Callback, takes two args, err and doc object
     */
    self.findOne = function (collectionName, id, callback) {
        var coll = self.collection(collectionName);
        var criteria = {
            _id: id
        };
        coll.findOne(criteria, callback);
    };

    /**
     * Inserts One document to collection
     * @param {String}   collectionName Name of collection
     * @param {Object}   doc            Document
     * @param {Function} callback       Takes two args, err and doc object
     */
    self.insertOne = function (collectionName, doc, callback) {
        var coll = self.collection(collectionName);
        coll.save(doc, callback);
    };

    /**
     * Replaces one document(selected by _id) in collection to given one.
     * If doc in db has field, which replace hasn't, it will be lost.
     * @param {String}   collectionName Name of collection
     * @param {Object}   id             See idFromHexString
     * @param {Object}   replace        New document
     * @param {Function} callback       Takes two args, err and doc object
     */
    self.replaceOne = function (collectionName, id, replace, callback) {
        var coll = self.collection(collectionName);
        var criteria = {
            _id: id
        };
        var options = {
            new: true
        };
        delete replace._id;
        coll.findAndModify(criteria, [], replace, options, callback);
    };

    /**
     * Updates one document(selected by _id) in collection with given name
     * Is not the same as replaceOne. If object in db has field, which doc hasn't, is won't be touched.
     * @param {String}   collectionName Name of collection
     * @param {Object}   id             See idFromHexString
     * @param {Object}   doc            Update object.
     * @param {Function} callback       Takes two args, err and doc
     */
    self.updateOne = function(collectionName, id, doc, callback){
        var coll = self.collection(collectionName);
        var criteria = {
            _id: id
        };
        delete doc._id;
        coll.update(criteria, {$set:doc},function(err, result){
            if(!err){
                self.findOne(collectionName, id, callback);
            }else{
                callback(err, null);
            }
        });
    };

    /**
     * Deletes document from collection by id
     * @param {String}   collectionName Name of collection
     * @param {Object}   id             see idFromHexString
     * @param {Function} callback       Takes two args, err and n of deleted docs
     */
    self.deleteOne = function (collectionName, id, callback) {
        var coll = self.collection(collectionName);
        var criteria = {
            _id: id
        };
        coll.remove(criteria, {}, callback);
    };
    /**
     * Finds user in users collection by login.
     * Login may be email or phone number
     * @param {String} login    email or phone
     * @param {Function} callback f(err, usr)
     */
    self.findUserByLogin = function(login, callback){
        var coll = self.collection('users');
        var criteria = {
            $or:[{email:login}, {phone:login}]
        };
        coll.findOne(criteria, callback);
    };

    self.aggregate = function (collectionName, pipline, callback) {
        var coll = self.collection(collectionName);

        coll.aggregate(pipline, callback);
    };
}

module.exports = function (url) {
    'use strict';
    url = url || config.get('connection-string');
    return new DbManager(url);
};
