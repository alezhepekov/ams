/**
 * Module, which contains validation functions for different things.
 * For example, for models.
 */
'use strict';
var validator = require('validator');
var db = require('./index')();

var clean = {};

var namePattern = /^.+$/; /*/^[A-Za-zА-Яа-я\s]{3,20}$/;*/

var positionPattern = /^[0-9A-Za-zА-Яа-я,.:;/ -]{3,50}$/;

var descriptionPattern = /^[0-9A-Za-zА-Яа-я,.:;()/ \s-]{3,250}$/;

/**
 * Cleans user
 * @param   {Object}   user                 user object, which we insert into db.
 * @param   {Boolean}  [checkPassword=true] set true on insert and false on update.
 * @returns {Object}  true if user is valid
 */
clean.user = function(user, checkPassword){
    if(checkPassword === undefined){
        checkPassword = true;
    }
    if(!validator.isHexadecimal(user.organizationId)){
        throw 'Не выбрана организация';
        return false;
    }
    if(!namePattern.test(user.firstName)){
        throw 'Некорректно введено имя';
        return false;
    }
    if(!namePattern.test(user.lastName)){
        throw 'Некорректно введена фамилия';
        return false;
    }
    if(!positionPattern.test(user.position)){
        throw 'Некорректно введена должность';
        return false;
    }
    if(!validator.isEmail(user.email)){
        throw 'Некорректно введен email';
        return false;
    }
    if(!validator.isNumeric(user.phone) || (user.phone.length < 6) || (user.phone.length > 16) ){
        throw 'Некорректно введен номер телефона';
        return false;
    }

    /*if(user.isRoot !== undefined){
        throw 'У нас уже есть админ, и другого нам не надо!';
        return false;
    }*/

    if(checkPassword){
        if (!validator.isAlphanumeric(user.password) || !validator.isAlphanumeric(user.confirm)) {
            throw 'Пароль может состоять только из английских букв и цифр!';
            return false;
        }

        if((user.password !== user.confirm) || (user.password.length < 8)){
            throw 'Пароли не совпадают или они короче 8-ми символов';
            return false;
        }
    }
    if(user.confirm){
        delete user.confirm;
    }
    if(!user.needsSMS){
        user.needsSMS = false;
    }
    return user;
};
/**
 * Alias for clean.user(user, false);
 * @param   {Object}  profile user
 * @returns {Object} true if user is valid
 */
clean.profile = function(profile){
    return clean.user(profile, false);
};

/**
 * Cleans organization
 * @param {Object} org
 * @returns {Object} org
 */
clean.organization = function(org){
    if(!namePattern.test(org.name)){
        throw 'Некорректно введено название';
        return false;
    }
    if(!descriptionPattern.test(org.description)){
        throw 'Некорректно введено описание'
        return false;
    }
    return org;
};
/**
 * Cleans project
 * @param {Object} project
 * @returns {Object} project
 */
clean.project = function(project){
    if(!validator.isHexadecimal(project.organizationId)){
        throw 'Не выбрана организация';
        return false;
    }
    if(!namePattern.test(project.name)){
        throw 'Некорректно введено название';
        return false;
    }
    if(!descriptionPattern.test(project.description)){
        throw 'Некорректно введено описание'
        return false;
    }
    return project;
};
/**
 * Cleans proposal, before insert(Mock for a while)
 * @param proposal
 * @returns {*}
 */
clean.proposal = function(proposal){
    proposal.started = new Date(proposal.started);
    proposal.owner._id = db.idFromHexString(proposal.owner._id);
    if(proposal.manager){
        proposal.manager._id = db.idFromHexString(proposal.manager._id);
    }
    if(proposal.coordinator){
        proposal.coordinator._id = db.idFromHexString(proposal.coordinator._id);
    }
    proposal.tasks = proposal.tasks.map(function(t){
        t.dueDate = new Date(t.dueDate);
        if (t.creationDate== undefined){
            t.creationDate = new Date();
        }
        t.owner._id = db.idFromHexString(t.owner._id);
        if(t.executor){
            t.executor._id = db.idFromHexString(t.executor._id);
        }
        return t;
    });
    proposal.currentTask = proposal.tasks[0];
    for(var i=0; i<proposal.tasks.length; i++){
        if(new Date(proposal.tasks[i].creationDate) > new Date(proposal.currentTask.creationDate)){
            proposal.currentTask = proposal.tasks[i];
        }
    }
    return proposal;
};
/**
 * Cleans role before insert(Also mock);
 * @param role
 * @returns {*}
 */
clean.role = function(role){
    return role;
};
module.exports = clean;
