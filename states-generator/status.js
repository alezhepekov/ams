/*{
 "states": [{
 "status": "Прием учредительных док-во и печати у бывшего руководителя",
 "default": true,
 "className": "default",
 "executer": "",
 "next": [],
 "roles": [{
 "right": "view",
 "role": "оценщик"
 }, {
 "right": "edit",
 "role": "супервайзер"
 }]
 },{
 "status": "Получение ключей",
 "default": true,
 "className": "default",
 "executer": "",
 "next": [],
 "roles": [{
 "right": "view",
 "role": "оценщик"
 }, {
 "right": "edit",
 "role": "супервайзер"
 }]
 },{
 "status": "Обеспечение охраны помещения",
 "default": true,
 "className": "default",
 "executer": "",
 "next": [],
 "roles": [{
 "right": "view",
 "role": "оценщик"
 }, {
 "right": "edit",
 "role": "супервайзер"
 }]
 },{
 "status": "Рассылка уведомлений об отмене доверенностей - Росреестр М и МО",
 "default": true,
 "className": "default",
 "executer": "",
 "next": [],
 "roles": [{
 "right": "view",
 "role": "оценщик"
 }, {
 "right": "edit",
 "role": "супервайзер"
 }]
 },{
 "status": "Рассылка уведомлений об отмене доверенностей - Суд М и МО",
 "default": true,
 "className": "default",
 "executer": "",
 "next": [],
 "roles": [{
 "right": "view",
 "role": "оценщик"
 }, {
 "right": "edit",
 "role": "супервайзер"
 }]
 },{
 "status": "Рассылка уведомлений об отмене доверенностей - ИФНС М и МО",
 "default": true,
 "className": "default",
 "executer": "",
 "next": [],
 "roles": [{
 "right": "view",
 "role": "оценщик"
 }, {
 "right": "edit",
 "role": "супервайзер"
 }]
 },{
 "status": "Рассылка уведомлений об отмене доверенностей - ГИБДД М и МО",
 "default": true,
 "className": "default",
 "executer": "",
 "next": [],
 "roles": [{
 "right": "view",
 "role": "оценщик"
 }, {
 "right": "edit",
 "role": "супервайзер"
 }]
 },{
 "status": "Рассылка уведомлений об отмене доверенностей - ССП М и МО",
 "default": true,
 "className": "default",
 "executer": "",
 "next": [],
 "roles": [{
 "right": "view",
 "role": "оценщик"
 }, {
 "right": "edit",
 "role": "супервайзер"
 }]
 },{
 "status": "Рассылка уведомлений об отмене доверенностей - Публикация в \"Коммерсант\"",
 "default": true,
 "className": "default",
 "executer": "",
 "next": [],
 "roles": [{
 "right": "view",
 "role": "оценщик"
 }, {
 "right": "edit",
 "role": "супервайзер"
 }]
 },{
 "status": "Рассылка уведомлений об отмене доверенностей - Банки",
 "default": true,
 "className": "default",
 "executer": "",
 "next": [],
 "roles": [{
 "right": "view",
 "role": "оценщик"
 }, {
 "right": "edit",
 "role": "супервайзер"
 }]
 },{
 "status": "Публикация в журнале \"Вестник\" о смене ликвидатора",
 "default": true,
 "className": "default",
 "executer": "",
 "next": [],
 "roles": [{
 "right": "view",
 "role": "оценщик"
 }, {
 "right": "edit",
 "role": "супервайзер"
 }]
 },{
 "status": "Прием кадровой документации",
 "default": true,
 "className": "default",
 "executer": "",
 "next": [],
 "roles": [{
 "right": "view",
 "role": "оценщик"
 }, {
 "right": "edit",
 "role": "супервайзер"
 }]
 },{
 "status": "Прием документов о финансово-хозяйственной деятельности",
 "default": true,
 "className": "default",
 "executer": "",
 "next": [],
 "roles": [{
 "right": "view",
 "role": "оценщик"
 }, {
 "right": "edit",
 "role": "супервайзер"
 }]
 },{
 "status": "Получение эл. баз, 1С",
 "default": true,
 "className": "default",
 "executer": "",
 "next": [],
 "roles": [{
 "right": "view",
 "role": "оценщик"
 }, {
 "right": "edit",
 "role": "супервайзер"
 }]
 },{
 "status": "Получение электронных ключей к расчетным счетам",
 "default": true,
 "className": "default",
 "executer": "",
 "next": [],
 "roles": [{
 "right": "view",
 "role": "оценщик"
 }, {
 "right": "edit",
 "role": "супервайзер"
 }]
 },{
 "status": "Получение сведений по всем открытым счетам  Общества у ликвидатора",
 "default": true,
 "className": "default",
 "executer": "",
 "next": [],
 "roles": [{
 "right": "view",
 "role": "оценщик"
 }, {
 "right": "edit",
 "role": "супервайзер"
 }]
 },{
 "status": "Запрос в ИФНС по открытым и закрытым счетам",
 "default": true,
 "className": "default",
 "executer": "",
 "next": [],
 "roles": [{
 "right": "view",
 "role": "оценщик"
 }, {
 "right": "edit",
 "role": "супервайзер"
 }]
 },{
 "status": "Издание председателем комиссии приказа о вступлении в исполнение обязанностей руководителя ЛК и переходе полномочий по управлению предприятием к ЛК",
 "default": true,
 "className": "default",
 "executer": "",
 "next": [],
 "roles": [{
 "right": "view",
 "role": "оценщик"
 }, {
 "right": "edit",
 "role": "супервайзер"
 }]
 }]
 }*/
