var argv = require('minimist')(process.argv.slice(2)),
    dbClient = require('mongodb').MongoClient,
    chalk = require('chalk'),
    fs = require('fs'),
    uuid = require('node-uuid'),
    source,
	resultRoles = [],
    resultSt = [];

var logErr = function(msg, err) {
    console.log(chalk.red.bold('ERROR: ') + chalk.yellow.bold(msg) + chalk.red.bold(' (' + err + ')'));
}

var log = function(msg) {
    console.log(chalk.green.bold('SUCCESS: ') + chalk.cyan.bold(msg));
}

var parseRoles = function(source) {
    var result = [];
    source.roles.forEach(function(role) {
        role.id = uuid.v1();
        result.push(role);
    });
    return result;
}

var parseRules = function(source) {
    var result = [];
    source.tasks.forEach(function(task) {
        task.id = uuid.v1();
        task.title = task.name;
		task.description = task.description;
        delete task.status;
		task.roles = [{
            "right": "edit",
            "role": "Исполнитель"
        }, {
            "right": "edit",
            "role": "Секретарь"
        }, {
            "right": "edit",
            "role": "Супервайзер"
        }];
        result.push(task);
    });
    return result;
}

var parseSteps = function(parsedTasks) {
    var result = [];
    parsedTasks.forEach(function(task) {
        if (task.next) {
            task.next = task.next.map(function(n) {
                var res;
                parsedTasks.forEach(function(r) {
                    if (n === r.title) {
                        res = r.id;
                    }
                });
                return res;
            });
        }
        result.push(task);
    });

    return result;
}

var parse = function(data) {
    try {
        source = JSON.parse(data);
        log('успешно прочитан исходный JSON');
    } catch (err) {
        logErr('не удалось распарсить JSON', err);
    }
	
	try {
        resultRoles = parseRoles(source);
        log('подготвка ролей заверешена');
    } catch (err) {
        logErr('не удалось распарсить объект иходных данных', err);
    }
	
	var parsedTasks = [];
    try {
        parsedTasks = parseRules(source);
        log('подготвка статусов заверешена');
    } catch (err) {
        logErr('не удалось распарсить объект иходных данных', err);
    }
	
    try {
        resultSt = parseSteps(parsedTasks);
        log('все состояния проставлены успешно');
    } catch (err) {
        logErr('не удалось распарсить допустимые состояния', err);
    }
}

var updateStatus = function(status, resultStatuses) {
    var resultStatus = resultStatuses.filter(function(st) {
        return st.title === status.title;
    })[0];
    if (resultStatus) {
        return resultStatus;
    } else {
        return status;
    }
}

var updateStatuses = function(db, projectId, resultStatuses) {
    db.collection('proposals').find().toArray(function(err, data) {
        if (!err) {
            data.forEach(function(p) {
                if (p.projectId === '' + projectId) {
                    var resultTasks = [];

                    p.tasks = p.tasks.map(function(task) {
                        if (task.status) {
                            task.status = updateStatus(task.status, resultStatuses);
                        }
                        return task;
                    });
                    if (p.status) {
                        p.status = updateStatus(p.status, resultStatuses);
                    }

                    db.collection('proposals').save(p, function(e) {
                        if (!e) {
                            log('Успешно обновлены статусы заявки "' + p.name + '"');
                        }
                    });
                }
            });
        } else {
            logErr('Не удалось загрузить заявки', err)
        }
    });
}

var setRoles = function(resultRoles) {
    dbClient.connect(argv.c, function(err, db) {
        if (!err) {
            db.collection('projects').findOne({
                name: argv.p.replace(/_/g, ' ')
            }, function(err, data) {
                if (!err) {
                    data.roles = resultRoles;
                    db.collection('projects').save(data, function(err, data) {
                        if (!err) {
                            log('роли успешно записаны в БД');
                        }
                    });
					//TODO: Do update roles
                    /*if (argv.u != null) {
                        updateRoles(db, data._id, resultStatuses);
                    }*/
                }
            });
        } else {
            logErr('не удалось подключиться к базе данных ( ' + argv.c + ' )', err);
        }
    });
}

var setStatuses = function(resultStatuses) {
    dbClient.connect(argv.c, function(err, db) {
        if (!err) {
            db.collection('projects').findOne({
                name: argv.p.replace(/_/g, ' ')
            }, function(err, data) {
                if (!err) {
                    data.statuses = resultStatuses;
                    db.collection('projects').save(data, function(err, data) {
                        if (!err) {
                            log('статусы успешно записаны в БД');
                        }
                    });
                    if (argv.u != null) {
                        updateStatuses(db, data._id, resultStatuses);
                    }
                }
            });
        } else {
            logErr('не удалось подключиться к базе данных ( ' + argv.c + ' )', err);
        }
    });
}

argv.f = 'ams.json';
argv.c = 'mongodb://localhost:27017/crmclone';
argv.p = 'Управление активами';
argv.u = true;

fs.readFile(argv.f, {
    encoding: 'utf-8'
}, function(err, data) {
    if (!err) {
        parse(data);
		setRoles(resultRoles);
        setStatuses(resultSt);
    } else {
        logErr('не удалось прочитать файл' + argv.f);
    }
});
