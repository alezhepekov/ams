"use strict";

/**
 * @ngdoc overview
 * @name crmApp
 * @description
 * # crmApp
 *
 * Main module of the application.
 */

angular.module("crmApp", [
	"ngAnimate",
	"ngCookies",
	"ngResource",
	"ngRoute",
	"ngSanitize",
	"ngTouch",
	"ui.bootstrap",
	"ui.bootstrap.tpls",
	"ui.router",
	"ui.router.tabs",
	"ui.bootstrap",
	"ui.bootstrap.datetimepicker",
	"ui.bootstrap.collapse",
	"ngTable",
	"froala",
	"angularFileUpload",
	"angular-loading-bar",
	"cfp.loadingBar",
	"angularUUID2"
])
.config(function($routeProvider, $httpProvider, $stateProvider, cfpLoadingBarProvider) {
	$routeProvider
		.when( "/", {
			templateUrl: "views/dashboard.html",
			controller: "DashboardController"
		})
		.when( "/projects", {
			templateUrl: "views/projects.html",
			controller: "ProjectsController"
		})			
		// Проект
		.when( "/projects/new", {
			templateUrl: "views/project.html",
			controller: "ProjectController"
		})
		.when( "/projects/:id/main", {
			templateUrl: "views/project.html",
			controller: "ProjectController"
		})
		.when( "/projects/:id/edit", {
			templateUrl: "views/project.html",
			controller: "ProjectController"
		})
		.when( "/projects/:id/tasks", {
			templateUrl: "views/project/tasks.html",
			controller: "ProjectController"
		})
		.when( "/projects/:id/tasks-edit", {
			templateUrl: "views/project/tasks-edit.html",
			controller: "ProjectController"
		})
		.when( "/projects/:id/docs", {
			templateUrl: "views/project/docs.html",
			controller: "ProjectController"
		})
		.when( "/projects/:id/assets-tangible", {
			templateUrl: "views/project/assets-tangible.html",
			controller: "ProjectController"
		})
		.when( "/projects/:id/assets-intangible", {
			templateUrl: "views/project/assets-intangible.html",
			controller: "ProjectController"
		})
		.when( "/projects/:id/creditors", {
			templateUrl: "views/project/creditors.html",
			controller: "ProjectController"
		})
		.when( "/projects/:id/debtors", {
			templateUrl: "views/project/debtors.html",
			controller: "ProjectController"
		})
		.when( "/projects/:id/comments", {
			templateUrl: "views/project/comments.html",
			controller: "ProjectController"
		})
		.when( "/projects/:id/group", {
			templateUrl: "views/project/group.html",
			controller: "ProjectController"
		})
		.when( "/projects/:id/gantt", {
			templateUrl: "views/project/gantt.html",
			controller: "ProjectController"
		})
		.when( "/projects/:id/mail", {
			templateUrl: "views/project/mail.html",
			controller: "ProjectController"
		})			
		//Управление профилем
		.when( "/login", {
			templateUrl: "views/system/login.html",
			controller: "LogInCtrl"
		})
		.when( "/smscheck/:id", {
			templateUrl: "views/system/smscheck.html",
			controller: "SMSCheckCtrl"
		})
		.when( "/logout", {
			templateUrl: "views/system/logout.html",
			controller: "LogOutCtrl"
		})
		.when( "/help", {
			templateUrl: "views/system/help.html",
			controller: "HelpCtrl"
		})
		.when( "/feedback", {
			templateUrl: "views/system/feedback.html",
			controller: "FeedbackCtrl"
		})
		//Административная панель
		.when( "/admin", {
			templateUrl: "views/admin/admin.html",
			controller: "AdminCtrl"
		})
		.when( "/admin/users", {
			controller: "UsersAdminCtrl",
			templateUrl: "views/admin/users-admin.html"
		})
		.when( "/admin/users/:id/reset", {
			controller: "ResetAdminCtrl",
			templateUrl: "views/admin/reset-admin.html"
		})
		.when( "/admin/users/new", {
			controller: "NewUserCtrl",
			templateUrl: "views/admin/new-user.html"
		})
		.when( "/admin/users/:id/edit", {
			controller: "EditUserCtrl",
			templateUrl: "views/admin/new-user.html"
		})
		.when( "/profile", {
			controller: "UserProfileCtrl",
			templateUrl: "views/admin/profile.html"
		})
		.when( "/admin/orgs", {
			controller: "OrgsAdminCtrl",
			templateUrl: "views/admin/orgs-list.html"
		})
		.when( "/admin/orgs/new", {
			controller: "NewOrgCtrl",
			templateUrl: "views/admin/new-org.html"
		})
		.when( "/admin/orgs/:id/edit", {
			controller: "EditOrgCtrl",
			templateUrl: "views/admin/new-org.html"
		})
		.when( "/admin/forms", {
			/* Needed? */
			templateUrl: "views/admin/forms.html",
			controller: "FormsCtrl"
		})
		.when( "/admin/forms/:id", {
			/* Needed? */
			templateUrl: "views/admin/form.html",
			controller: "FormCtrl"
		})
		.when( "/admin/new-form", {
			/* Needed? */
			templateUrl: "views/admin/form.html",
			controller: "FormCtrl"
		})
		.when( "/admin/project-template", {
			/* Needed? */
			templateUrl: "views/admin/project-template.html",
			controller: "ProjectTemplateCtrl"
		})
		.when( "/admin/task", {
			/* Needed? */
			templateUrl: "views/admin/task.html",
			controller: "TaskCtrl"
		})	
		.when( "/edit-project/:id", {
			/* Needed? */
			templateUrl: "views/admin/new-project.html",
			controller: "EditProjectCtrl"
		})
		.when( "/admin/roles", {
			templateUrl: "views/admin/roles.html",
			controller: "RolesCtrl"
		})
		.when( "/admin/mail-system", {
			templateUrl: "views/admin/mail-system.html",
			controller: "MailSystemCtrl"
		})
		//Отчеты
		.when( "/reports/gantt", {
			templateUrl: "views/reports/report-gantt.html",
			controller: "ReportGanttCtrl"
		})
		.when( "/reports/users", {
			templateUrl: "views/reports/report-users.html",
			controller: "ReportUsersCtrl"
		})		
		.when( "/maps/:id", {
			templateUrl: "views/maps.html",
			controller: "MapsCtrl"
		})
		.when( "/about", {
			templateUrl: "views/system/about.html"
		})
		.otherwise({
			redirectTo: "/"
		});

	cfpLoadingBarProvider.includeBar = true;
	cfpLoadingBarProvider.includeSpinner = false;
})
.run( ["$rootScope", "$location", "Utils", function( $rootScope, location, Utils ) {
	$rootScope.$on("$routeChangeSuccess", function( event, current, previous ) {
		$rootScope.title = "AMS Управление активами";
		$rootScope.projectId = '';
		$rootScope.subTitle = '';

		Utils.checkCloud();
		
		if ( location.path().indexOf( "logout" ) === -1 ) {
			var sidebar = angular.element( ".control-sidebar" );
			if ( sidebar ) {
				sidebar.removeClass( "control-sidebar-open" );
			}
		}
					
		var options = {
			"shape": "circle"
		};

		MaterialAvatar(document.getElementsByClassName("profile-photo"), options);
	});
}]).value("froalaConfig", {
	toolbarInline: false,
	placeholderText: "Enter Text Here"
});