'use strict';

angular.module('crmApp')
    .controller('ReportStatisticsCtrl', function ($scope, $http, $routeParams, $sce, Project, Report, ReportHelper, RolesHelper, CONFIG)  {

    	$scope.projectId = $routeParams.id;
        $scope.tabData = ReportHelper.getTabsConfig($scope.projectId, RolesHelper.isCoordinator($scope.currentUser.roles));

        $scope.statisticsUrl = $sce.trustAsResourceUrl(CONFIG.STATISTICS_ADDRESS);

        $scope.frameWidth = document.documentElement.clientWidth - 100;
        $scope.frameHeight = 768; //document.documentElement.clientHeight - 100;
});