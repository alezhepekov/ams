"use strict";

function UTCToLocalTime( utc ) {
    var timezoneOffset = ( new Date() ).getTimezoneOffset() * 60000;
    return new Date( utc - timezoneOffset );
}

angular.module( "crmApp" ).directive( "onFinishRender", function( $timeout ) {
    return {
        restrict: "A",
        link: function( scope, element, attrs ) {
            if ( scope.$last === true ) {
                $timeout( function() {
                    scope.$emit( attrs.onFinishRender );
                });
            }
        }
    }
});

angular.module( "crmApp" ).filter( "filterTaskActive", function() {
    return function( items ) {
        if ( !items ) {
            return [];
        }

        var result = [];
        for ( var i = 0; i < items.length; i++ ) {
            var currentItem = items[i];
            if ( !currentItem.removed ) {
                result.push( currentItem );
            }
        }
        return result;
    }
});

angular.module( "crmApp" ).filter( "filterCreditorCreation", function() {
    return function( items, dateFrom, dateTo ) {
        if ( !items ) {
            return [];
        }

        var dateFromString = dateFrom;
        var dateToString = dateTo;

        var result = [];
        for ( var i = 0; i < items.length; i++ ) {
            var timestampString = items[i].creationDate;
            timestampString = timestampString.replace( /T/, " " ).replace( /Z/, "" ).replace( /\..+/, "" );

            if ( !dateFromString || !dateToString || dateFromString.length === 0 && dateToString.length === 0 ) {
                result.push( items[i] );
            }
            else if ( dateFromString.length > 0 && dateToString.length > 0 ) {
                if ( timestampString >= dateFromString && timestampString <= dateToString ) {
                    result.push( items[i] );
                }
            }
            else if ( dateFromString.length > 0 ) {
                if ( timestampString >= dateFromString ) {
                    result.push( items[i] );
                }
            }
            else if ( dateFromString.length > 0 ) {
                if ( timestampString <= dateToString ) {
                    result.push( items[i] );
                }
            }
        }

        return result;
    };
});


angular.module( "crmApp" ).filter( "filterDebtorCreation", function() {
    return function( items, dateFrom, dateTo ) {
        if ( !items ) {
            return [];
        }

        var dateFromString = dateFrom;
        var dateToString = dateTo;

        var result = [];
        for ( var i = 0; i < items.length; i++ ) {
            var timestampString = items[i].creationDate;
            timestampString = timestampString.replace( /T/, " " ).replace( /Z/, "" ).replace( /\..+/, "" );

            if ( !dateFromString || !dateToString || dateFromString.length === 0 && dateToString.length === 0 ) {
                result.push( items[i] );
            }
            else if ( dateFromString.length > 0 && dateToString.length > 0 ) {
                if ( timestampString >= dateFromString && timestampString <= dateToString ) {
                    result.push( items[i] );
                }
            }
            else if ( dateFromString.length > 0 ) {
                if ( timestampString >= dateFromString ) {
                    result.push( items[i] );
                }
            }
            else if ( dateFromString.length > 0 ) {
                if ( timestampString <= dateToString ) {
                    result.push( items[i] );
                }
            }
        }

        return result;
    };
});

angular.module( "crmApp" ).filter( "filterAssetIntangible", function() {
    return function( items, dateFrom, dateTo ) {
        if ( !items ) {
            return [];
        }

        var dateFromString = dateFrom;
        var dateToString = dateTo;

        var result = [];
        for ( var i = 0; i < items.length; i++ ) {
            var timestampString = items[i].creationDate;
            timestampString = timestampString.replace( /T/, " " ).replace( /Z/, "" ).replace( /\..+/, "" );

            if ( !dateFromString || !dateToString || dateFromString.length === 0 && dateToString.length === 0 ) {
                result.push( items[i] );
            }
            else if ( dateFromString.length > 0 && dateToString.length > 0 ) {
                if ( timestampString >= dateFromString && timestampString <= dateToString ) {
                    result.push( items[i] );
                }
            }
            else if ( dateFromString.length > 0 ) {
                if ( timestampString >= dateFromString ) {
                    result.push( items[i] );
                }
            }
            else if ( dateFromString.length > 0 ) {
                if ( timestampString <= dateToString ) {
                    result.push( items[i] );
                }
            }
        }

        return result;
    };
});

angular.module( "crmApp" ).filter( "filterDocumentCreation", function() {
    return function( items, dateFrom, dateTo ) {
        if ( !items ) {
           return [];
        }

        var dateFromString = dateFrom;
        var dateToString = dateTo;

        var result = [];
        for ( var i = 0; i < items.length; i++ ) {
            var timestampString = items[i].uploadDate;
            timestampString = timestampString.replace( /T/, " " ).replace( /Z/, "" ).replace( /\..+/, "" );

            if ( !dateFromString || !dateToString || dateFromString.length === 0 && dateToString.length === 0 ) {
                result.push( items[i] );
            }
            else if ( dateFromString.length > 0 && dateToString.length > 0 ) {
                if ( timestampString >= dateFromString && timestampString <= dateToString ) {
                    result.push( items[i] );
                }
            }
            else if ( dateFromString.length > 0 ) {
                if ( timestampString >= dateFromString ) {
                    result.push( items[i] );
                }
            }
            else if ( dateFromString.length > 0 ) {
                if ( timestampString <= dateToString ) {
                    result.push( items[i] );
                }
            }
        }

        return result;
    };
});

angular.module( "crmApp" ).filter( "filterAssetTangible", function() {
    return function( items, dateFrom, dateTo ) {
        if ( !items ) {
           return [];
        }

        var dateFromString = dateFrom;
        var dateToString = dateTo;

        var result = [];
        for ( var i = 0; i < items.length; i++ ) {
            var timestampString = items[i].creationDate;
            timestampString = timestampString.replace( /T/, " " ).replace( /Z/, "" ).replace( /\..+/, "" );

            if ( !dateFromString || !dateToString || dateFromString.length === 0 && dateToString.length === 0 ) {
                result.push( items[i] );
            }
            else if ( dateFromString.length > 0 && dateToString.length > 0 ) {
                if ( timestampString >= dateFromString && timestampString <= dateToString ) {
                    result.push( items[i] );
                }
            }
            else if ( dateFromString.length > 0 ) {
                if ( timestampString >= dateFromString ) {
                    result.push( items[i] );
                }
            }
            else if ( dateFromString.length > 0 ) {
                if ( timestampString <= dateToString ) {
                    result.push( items[i] );
                }
            }
        }

        return result;
    };
});

angular.module( "crmApp" ).filter( "filterDocumentOwner", function() {
    return function( items, owner ) {
        if ( !items ) {
            return [];
        }

        var result = [];
        for ( var i = 0; i < items.length; i++ ) {
            var currentDocument = items[i];

            if ( !owner ) {
                 result.push( currentDocument );
            }
            else if ( currentDocument.uploadUser._id === owner._id ) {
                result.push( currentDocument );
            }            
        }

        return result;
    };
});

angular.module( "crmApp" ).filter( "filterCreditorStatus", function() {
    return function( items, status ) {       
        if ( !items ) {
            return [];
        }    
   
        var result = [];
        for ( var i = 0; i < items.length; i++ ) {
            var currentCreditor = items[i];

            if ( !status || !status.id ) {
                 result.push( currentCreditor );
            }          
            else if ( currentCreditor.status.id === status.id ) {
                result.push( currentCreditor );
            }   
        }

        return result;
    };
});

angular.module( "crmApp" ).filter( "filterDebtorStatus", function() {
    return function( items, status ) {      
        if ( !items ) {
            return [];
        }    
   
        var result = [];
        for ( var i = 0; i < items.length; i++ ) {
            var currentDebtor = items[i];

            if ( !status || !status.id ) {
                 result.push( currentDebtor );
            }
            else if ( ( status.id ) && ( currentDebtor.status.id === status.id ) ) {
                result.push( currentDebtor );
            }   
        }

        return result;
    };
});

angular.module( "crmApp" ).filter( "filterAssetStatus", function() {
    return function( items, status ) {
        if ( !items ) {
            return [];
        }    
   
        var result = [];
        for ( var i = 0; i < items.length; i++ ) {
            var currentAsset = items[i];

            if ( !status || !status.id ) {
                 result.push( currentAsset );
            }          
            else if ( currentAsset.status.id === status.id ) {
                result.push( currentAsset );
            }   
        }

        return result;
    };
});


angular.module( "crmApp" ).filter( "filterUsersOutsideProjectGroup", function() {
    return function( items, projectGroup ) {
        if ( !items ) {
            return [];
        }

        var result = [];
        for ( var i = 0; i < items.length; i++ ) {
            var currentItem = items[i];           

            var userFound = false;
            for ( var j = 0; j < projectGroup.length; j++ ) {
                var currentUser = projectGroup[j];
                if ( currentUser._id === currentItem._id ) {
                    userFound = true;
                    break;
                }
            }
            if ( !userFound ) {
                result.push( currentItem );
            }       
        }
        return result;
    }
});

angular.module( "crmApp" ).filter( "filterTasksByUserRights", function() {
    return function( items, user, project ) {
		if ( !items ) {
            return [];
        }

        var result = [];
        for ( var i = 0; i < items.length; i++ ) {
			var currentItem = items[i];

			if ( !user._id ) {			
				continue;
			}

			if ( user.isRoot && ( user.isRoot === true ) ) {
				result.push( currentItem );
				continue;
			}

            // !!! ATTENTION !!! При использовании этой роли любой пользователь может просматривать все проекты
			if ( user.seeAllProjects ) {
				result.push( currentItem );
				continue;
			}

            if (
				project.manager && ( project.manager._id === user._id ) 
				|| project.owner && ( project.owner._id === user._id ) ) {
				result.push( currentItem );
                continue;
			}	
			
			if (
				currentItem.executor && ( currentItem.executor._id === user._id ) ) {
				result.push( currentItem );
			}		
        }

        return result;
    };
});

angular.module( "crmApp" ).filter( "filterTaskStatus", function() {
    return function( items, status ) {
        if ( !items ) {
            return [];
        }    
   
        var result = [];
        for ( var i = 0; i < items.length; i++ ) {
            var currentItem = items[i];

            if ( !status || !status.id ) {
                 result.push( currentItem );
            }          
            else if ( currentItem.status.id === status.id ) {
                result.push( currentItem );
            }   
        }

        return result;
    };
});


angular.module( "crmApp" )
.controller( "ProjectController", function ($resource, $rootScope, $scope, $http, $location, $routeParams, $filter, $modal, $log, ngTableParams, cfpLoadingBar, uuid2, GetProject, Project, Proposal, Comment, RolesHelper, Utils, Modals, CONFIG) {
    console.log( $location.path() );

    $scope.currentUser = {};
    
	if ( window.localStorage.profile ) {
        $scope.currentUser = JSON.parse( window.localStorage.profile );       
    }
    else {    
        window.location = "#/login";
		return;
    }

    $scope.documentsListTemplate = { name: "documentsList.html", url: "/views/templates/documentsList.html" };
    $scope.commentsListTemplate = { name: "commentsList.html", url: "/views/templates/commentsList.html" };

    function ProjectEntity() {				
        this._id = "";
        this.started = "";
        this.name = "";
        this.number = "";
        this.documents = [];
        this.address = "";
        this.mapAddress = {};          
        this.tasks = [];
        this.orgs = [];
        this.type = {};
        this.dueDate = "";          
        this.arbitrationManagers = [];  
        this.projectGroupList = [];
        this.assetsTangible = [];
        this.assetsIntangible = [];
        this.creditors = [];
        this.debtors = [];
        this.comments = [];
        this.manager = {};
        this.owner = {};
        this.init();
    }

    ProjectEntity.prototype = {
        init : function() {
        }
    }   
    
    $scope.newMode = $location.path().indexOf( "new" ) !== -1;
    $scope.editMode = $location.path().indexOf( "edit" ) !== -1 || $location.path().indexOf( "new" ) !== -1;

    $scope.currentProject = new ProjectEntity();

    $scope.$watch( "currentProject.manager", function( newValue, oldValue )  {
        if ( !oldValue._id ) {
            return;
        }      
       
        for ( var i = 0; i < $scope.currentProject.projectGroupList.length; i++ ) {
            var currentUser = $scope.currentProject.projectGroupList[i];
            if ( currentUser._id === oldValue._id ) {
                delete $scope.currentProject.projectGroupList.splice( i, 1 );
                break;
            }
        }

        delete newValue.roles; // NOTE Модель ролей требует рефакторинга и сейчас поле roles содержит некорректные данные
        newValue.roles = {};

        $scope.currentProject.projectGroupList.push( newValue );
    });
  
    $scope.creditorCreationDate = "";
    $scope.creditorCreationDateFrom = "";
    $scope.creditorCreationDateTo = "";

    $scope.debtorCreationDate = "";
    $scope.debtorCreationDateFrom = "";
    $scope.debtorCreationDateTo = "";

    $scope.assetIntangibleCreationDate = "";
    $scope.assetIntangibleCreationDateFrom = "";
    $scope.assetIntangibleCreationDateTo = "";

    $scope.assetTangibleCreationDate = "";
    $scope.assetTangibleCreationDateFrom = "";
    $scope.assetTangibleCreationDateTo = "";

    $scope.documentCreationDate = "";
    $scope.documentCreationDateFrom = "";
    $scope.documentCreationDateTo = "";

    $scope.filterCreditorStatus = {};
    $scope.filterDebtorStatus = {};
    $scope.filterTaskStatus = {};

    var mainProjectId = $routeParams.id;
    $rootScope.mainProjectId = mainProjectId;

    $scope.projectTypes = Utils.getProjectTypes();
    $scope.filter = {};
    $scope.search = {};


    window.localStorage.mainProjectId = $rootScope.mainProjectId;
    window.localStorage.projectId = $routeParams.id;  
    $scope.sortType = "docName";
    
    $scope.commentImportances = [
        {id: "normal", name: "Обычный"},  
        {id: "important", name: "Важный"},
        {id: "veryImportant", name: "Очень важный"}
    ];

    $scope.assetStatuses = [
        {id: "inPledge", name: "В залоге"},
        {id: "notInPledge", name: "Не в залоге"}
    ];

    $scope.creditorStatuses = $scope.debtorStatuses = [
        {id: "badLoan", name: "Просроченная ссуда"},
        {id: "notBadLoan", name: "Нет просрочки"}
    ];

    $scope.taskStatuses = [
        { id: "notScheduled", name: "Не запланирована" },
        { id: "running", name: "Выполняется" },
        { id: "finished", name: "Завершена" }
    ];

    $scope.findTaskStatusByID = function( id ) {
        for ( var i = 0; i < $scope.taskStatuses.length; i++ ) {
            var currentItem = $scope.taskStatuses[i];

            if ( currentItem.id === id ) {
                return currentItem;
            }
        }

        return null;      
    }
    
    $scope.mapManager = new MapManager( $http );

    var defaultMapAddress = {
        "point" : {
            "lat" : "55.753559",
            "lon" : "37.609218"
        }
    };

    $scope.newDoc = {};
    $scope.sortType  = "docName";

    $scope.isOverTime = function(task) {
        if (task.finished || !task.dueDate)
            return false;

        var now = ( new Date() ).toISOString();
        var dueDate = task.dueDate;

        return now > dueDate;
    };

    $scope.isLateFinish = function(task) {
        if (!task.finishingDate)
            return false;

        var finishedDate = task.finishingDate;
        var dueDate = task.dueDate;

        return finishedDate > dueDate;
    };

    $scope.viewComments = false;

    $scope.comments = [];
    $scope.post = {type: "success", message: ""};
    $scope.messageTypes = CONFIG.MESSAGE_TYPES;

    $scope.selected = { user: {} };
 
    $scope.notificationList = [];
    $scope.users = [];

    $scope.addOrg = function() {
        $scope.currentProject.orgs = $scope.currentProject.orgs || [];
        $scope.currentProject.orgs.push({
            _id: uuid2.newuuid(),
            name: "Предприятие " + ($scope.currentProject.orgs.length + 1)
        });          
    }

    $scope.addTangibleAsset = function() {
        var editorInstance = $modal.open( Modals.tangibleAsset( null ) );

         editorInstance.result.then(     
            function( asset ) {                
                $scope.currentProject.assetsTangible.push(asset);
                if ( $scope.$root.$$phase != "$apply" && $scope.$root.$$phase != "$digest" ) {
                    $scope.$apply();
                }
                saveChanges();
            },         
            function( result ) {
            }
        );      
    }

    $scope.editTangibleAsset = function( asset ) {
        var editorInstance = $modal.open( Modals.tangibleAsset( asset ) );

         editorInstance.result.then(
            function( asset ) {
                var key = asset.id;
                var objectList = $scope.currentProject.assetsTangible;
                for ( var i = 0; i < objectList.length; i++ ) {
                    var currentItem = objectList[i];
                    if ( currentItem.id === key ) {                         
                        objectList[i] = asset;
                        break;
                    }
                }

                if ( $scope.$root.$$phase != "$apply" && $scope.$root.$$phase != "$digest" ) {
                    $scope.$apply();
                }

                saveChanges();
            },        
            function (result) {
            }
        );
    }

    $scope.deleteTangibleAsset = function(asset) {
        alertify.confirm("Вы действительно хотите удалить данный актив?", function(e) {
            if ( e ) {
                var key = asset.id;
                var objectList = $scope.currentProject.assetsTangible;
                for ( var i = 0; i < objectList.length; i++ ) {
                    var currentItem = objectList[i];
                    if ( currentItem.id === key ) {                       
                        delete objectList.splice( i, 1 );
                        break;
                    }
                }

                if ( $scope.$root.$$phase != "$apply" && $scope.$root.$$phase != "$digest" ) {
                    $scope.$apply();
                }

                saveChanges();
            }
        });
    };

    $scope.addIntangibleAsset = function() {
        var editorInstance = $modal.open(Modals.intangibleAsset(null));

            editorInstance.result.then(
            //Сохранение
            function (asset) {                
                $scope.currentProject.assetsIntangible.push(asset);
                if ( $scope.$root.$$phase != "$apply" && $scope.$root.$$phase != "$digest" ) {
                    $scope.$apply();
                }
                saveChanges();
            },
            //Отмена
            function (result) {
            }
        );      
    }

    $scope.editIntangibleAsset = function(asset) {
        var editorInstance = $modal.open(Modals.intangibleAsset(asset));

        editorInstance.result.then(
            //Сохранение
            function (asset) {
                var key = asset.id;
                var objectList = $scope.currentProject.assetsIntangible;
                for ( var i = 0; i < objectList.length; i++ ) {
                    var currentItem = objectList[i];
                    if ( currentItem.id === key ) {                         
                        objectList[i] = asset;
                        break;
                    }
                }

                if ( $scope.$root.$$phase != "$apply" && $scope.$root.$$phase != "$digest" ) {
                    $scope.$apply();
                }

                saveChanges();
            },
            //Отмена
            function (result) {
            }
        );
    }

    $scope.deleteIntangibleAsset = function(asset) {
        alertify.confirm("Вы действительно хотите удалить данный актив?", function(e) {
            if ( e ) {
                var key = asset.id;
                var objectList = $scope.currentProject.assetsIntangible;
                for ( var i = 0; i < objectList.length; i++ ) {
                    var currentItem = objectList[i];
                    if ( currentItem.id === key ) {                       
                        delete objectList.splice( i, 1 );
                        break;
                    }
                }

                if ( $scope.$root.$$phase != "$apply" && $scope.$root.$$phase != "$digest" ) {
                    $scope.$apply();
                }

                saveChanges();
            }
        });
    };

    $scope.addCreditor = function() {    
        var editorInstance = $modal.open(Modals.creditor(null));  

        editorInstance.result.then(
            //Сохранение
            function (creditor) {                
                $scope.currentProject.creditors.push(creditor);
                if ( $scope.$root.$$phase != "$apply" && $scope.$root.$$phase != "$digest" ) {
                    $scope.$apply();
                }
                saveChanges();
            },
            //Отмена
            function (result) {
            }
        );
    }
    
    $scope.editCreditor = function(creditor) {       
        var editorInstance = $modal.open(Modals.creditor(creditor));     

        editorInstance.result.then(
            //Сохранение
            function (creditor) {
                var key = creditor.id;
                var objectList = $scope.currentProject.creditors;
                for ( var i = 0; i < objectList.length; i++ ) {
                    var currentItem = objectList[i];
                    if ( currentItem.id === key ) {                         
                        objectList[i] = creditor;
                        break;
                    }
                }

                if ( $scope.$root.$$phase != "$apply" && $scope.$root.$$phase != "$digest" ) {
                    $scope.$apply();
                }

                saveChanges();
            },
            //Отмена
            function (result) {
            }
        );
    }

    $scope.deleteCreditor = function(creditor) {
        alertify.confirm("Вы действительно хотите удалить данного кредитора?", function(e) {
            if ( e ) {
                var key = creditor.id;
                var objectList = $scope.currentProject.creditors;
                for ( var i = 0; i < objectList.length; i++ ) {
                    var currentItem = objectList[i];
                    if ( currentItem.id === key ) {                       
                        delete objectList.splice( i, 1 );
                        break;
                    }
                }

                if ( $scope.$root.$$phase != "$apply" && $scope.$root.$$phase != "$digest" ) {
                    $scope.$apply();
                }

                saveChanges();
            }
        });
    };

    $scope.addDebtor = function() {
        var editorInstance = $modal.open(Modals.debtor(null));

        editorInstance.result.then(
            //Сохранение
            function (debtor) {
                $scope.currentProject.debtors.push(debtor);
                if ( $scope.$root.$$phase != "$apply" && $scope.$root.$$phase != "$digest" ) {
                    $scope.$apply();
                }
                saveChanges();               
            },
            //Отмена
            function (result) {
            });
    }

    $scope.editDebtor = function(debtor) {
        var editorInstance = $modal.open(Modals.debtor(debtor));

        editorInstance.result.then(
            //Сохранение
            function (debtor) {
                var key = debtor.id;
                var objectList = $scope.currentProject.debtors;
                for ( var i = 0; i < objectList.length; i++ ) {
                    var currentItem = objectList[i];
                    if ( currentItem.id === key ) {                         
                        objectList[i] = debtor;
                        break;
                    }
                }

                if ( $scope.$root.$$phase != "$apply" && $scope.$root.$$phase != "$digest" ) {
                    $scope.$apply();
                }

                saveChanges();          
            },
            //Отмена
            function (result) {
            });
    }

    $scope.deleteDebtor = function(debtor) {
       alertify.confirm("Вы действительно хотите удалить данного дебитора?", function(e) {
            if ( e ) {
                var key = debtor.id;
                var objectList = $scope.currentProject.debtors;
                for ( var i = 0; i < objectList.length; i++ ) {
                    var currentItem = objectList[i];
                    if ( currentItem.id === key ) {                       
                        delete objectList.splice( i, 1 );
                        break;
                    }
                }

                if ( $scope.$root.$$phase != "$apply" && $scope.$root.$$phase != "$digest" ) {
                    $scope.$apply();
                }

                saveChanges();
            }
        });
    };

    $scope.openEditorTask = function(docList, doc) {
        $scope.createMode = !doc;
        $scope.objectCaption = ($scope.createMode) ? "Новый документ" : doc.name;
        $scope.doc = ($scope.createMode)
            ? { name: "123", htmlContent: "<h1>Введите текст документа...</h1><br/><br/><br/><br/><br/>", type: "intDoc", link: "", uploadUser:  Utils.getReducedUser($scope.currentUser) }
            : doc;

        var editorInstance = $modal.open(Modals.editor($scope));

        editorInstance.result.then(
            //Сохранение
            function (result) {
                if ($scope.createMode) {
                    docList.push(result);                 
                }             
            },
            //Отмена
            function (result) {
            });
    }

    $scope.removeOrg = function(org) {
        alertify.confirm("Вы действительно хотите удалить данное предприятие?", function(e) {
            if (e) {
                var indx = -1;
                var k = 0;
                $scope.currentProject.orgs.forEach(function(objOrg) {
                    if (objOrg._id == org._id)
                        indx = k;
                    k++;
                });

                if (indx > -1) {
                    $scope.currentProject.orgs.splice(indx, 1);
                }               
            }
        });
    }

    $scope.updateExecutors = function(task, roleItem) {
        if (roleItem==undefined) {
            task.filteredUsers = $scope.users;
            return;
        }

        task.filteredUsers = $scope.users.filter(function(fuser) {
            if (fuser.roles && fuser.roles.length > 0) {
                var foundedRole = fuser.roles.filter(function(frole) {
                    return frole.roleId === roleItem.id;
                })[0];
                if (foundedRole)
                    return true;
            }
            return false;
        });
    };
    
    $scope.savePlan = function() {
        saveChanges( function ( result ) {
            if ( result ) {
                $location.path( "projects/" + $scope.currentProject._id + "/tasks" );
            }
        });
    };

    $scope.uploadFile = function(docs, doc) {
        Utils.uploadFile(
            docs,
            null,
            $scope,
            function() {
                if ( $scope.$root.$$phase != "$apply" && $scope.$root.$$phase != "$digest" ) {
                    $scope.$apply();
                }

                if ( $scope.newMode === false ) {
                    saveChanges();
                }                
            },
            function(e) {               
                console.error(e);
            }
        );        
    };

    $scope.addLink = function(docs, doc) {
        doc.name = doc.name || "Ссылка";

        Utils.addLink(
            docs,
            doc,
            $scope.currentUser,
            function() {
                if ( $scope.$root.$$phase != "$apply" && $scope.$root.$$phase != "$digest" ) {
                    $scope.$apply();
                }

                if ( $scope.newMode === false ) {
                    saveChanges();
                }
            }
        );
        $scope.newDoc = {};
    };

    $scope.processDocument = function(doc) {
        var fileProps = {projectId: mainProjectId, id: $routeParams.id, fileUrl: doc.link};
        Utils.proccessDocument($scope, doc, fileProps);
    };

    alertify.set({ labels: {
        ok     : "Да",
        cancel : "Нет"
    }});

    $scope.getOrgName = function(orgId) {
        return Utils.getOrgName(orgId);
    }

    $scope.getOrgId = function(orgName) {
        return Utils.getOrgId(orgName);
    }

    $scope.orgMode = true;
    $scope.Orgs = {
        AbnId: $scope.getOrgId("АБН"),
        BusinessKrugId: $scope.getOrgId("Бизнес-КРУГ"),
        MosingprojectId: $scope.getOrgId("Мосинжпроект") };
  

    $scope.updateAddress = function(org) {
        return new Promise(function(resolve, reject){
            $scope.mapManager.updatePlace(org.address, 'map-' + org._id, null, function(address) {
                org.mapAddress = address;
                resolve(address);               
            });          
        });
    }

    $scope.changeAddress = function( org ) {
        if ( org.address && ( org.address.length !== 0 ) ) {
            var tickCount = 0;
            var intervalID = setInterval( function() {              
                if ( $scope.mapManager && $scope.mapManager.yandexMapAPIIsLoaded ) {                                      
                    $scope.updateAddress( org );
                    clearInterval( intervalID );
                    return;
                }

                tickCount++;
                if ( tickCount >= 10 ) {
                    clearInterval( intervalID );
                    console.error("Не удалось инициализировать Yandex Map API");
                    return;
                }
            }, 500 );           
        }        
    }

    $scope.loadSuggestion = function(org) {
        $('#org-address-' + org._id).suggestions({
            serviceUrl: 'https://dadata.ru/api/v2',
            token: '46d56b1479f511ec4473728687a2cbc45e967eee',
            type: 'ADDRESS',
            count: 5,
            onSelect: function(suggestion) {        
                org.address = suggestion.value;
                //$scope.updateAddress(org);
            }
        });
    };   

    $scope.deleteAsset = function(asset, isTangible) {
        if ( isTangible ) {
            $scope.deleteTangibleAsset( asset );
        }
        else {
            $scope.deleteIntangibleAsset( asset );
        }      
    };

    $scope.getUserPhoto = function(user) {
    	if (!user) {
    		return "";
    	}

    	if (!user.photo) {
    		return "/images/users/user1-128x128.jpg";
    	}

    	return user.photo;
    }   

    $scope.addUser = function() {
        if ( !$scope.selected || !$scope.selected.user || !$scope.selected.user._id ) {
            alertify.error( "Пользователь не выбран!" );
            return;
        }
        
        $scope.currentProject.projectGroupList.push( $scope.selected.user );   
       
        updateNotificationList();

        if ( $scope.$root.$$phase != "$apply" && $scope.$root.$$phase != "$digest" ) {
            $scope.$apply();
        }
        saveChanges(); 
    }

    $scope.deleteUser = function(user) {
        alertify.confirm("Вы действительно хотите удалить данного пользователя из проектной группы?", function(e) {
            if ( e ) {              
                var success = Utils.deleteElemById( $scope.currentProject.projectGroupList, user );
                if ( success ) {                  
                    updateNotificationList();
                }

                if ( $scope.$root.$$phase != "$apply" && $scope.$root.$$phase != "$digest" ) {
                    $scope.$apply();
                }
                saveChanges(); 
            }
        });
    }

    $scope.canDeleted = function(user) {
        return !RolesHelper.isInitWorkGroup(user, $scope.currentProject);
    }  

    var updateNotificationList = function() {
        $scope.notificationList = [];
        $scope.currentProject.projectGroupList.forEach(function(user) {
            $scope.notificationList.push(user);
        });
    }

    var updateProjectGroupList = function () {
        if ( $scope.currentProject.owner ) {
            $scope.currentProject.owner.projectRole = "создатель заявки";         
        }
        if ( $scope.currentProject.manager ) {
            $scope.currentProject.manager.projectRole = "руководитель";       
        }
        if ( $scope.currentProject.coordinator ) {
            $scope.currentProject.coordinator.projectRole = "координатор";            
        }
    
        updateNotificationList();
    }

    $scope.getLinkClass = function(link) {
        return (link && link.indexOf("cloud") > 0 && link.indexOf("abn-consult.ru") > 0)
            ? "cloudLink" : "";
    }

    $scope.getLinkFileIcon = function(link) {
        return (link && link.indexOf("cloud") > 0 && link.indexOf("abn-consult.ru") > 0)
            ? "font-up fa fa-arrow-down" : "font-up fa fa-arrow-right";
    }

    $scope.getLinkWord = function(link) {
        return (link && link.indexOf("cloud") > 0 && link.indexOf("abn-consult.ru") > 0)
        	? "Скачать" : "Перейти";
    }

    $scope.fileIcon = function(fileType) {
        return Utils.getFileIcon(fileType);
    };

    $scope.toLocalDateString = function( date ) {
        if ( !date ) {
            return null;
        }

        var timestampString = UTCToLocalTime( Date.parse( date ) ).toISOString().replace( /T/, " " ).replace( /Z/, "" ).replace( /\..+/, "" );
        timestampString = timestampString.replace( /(\d+)-(\d+)-(\d+)/, "$3.$2.$1" );      
        var splits = timestampString.split(/\s/);   
        return splits[0];
    };

    $scope.addTask = function() {     
        var editorInstance = $modal.open( Modals.taskEditor( null, $scope.users ) );
        editorInstance.result.then(       
            function( task ) {               
                if ( !task.startingDate ) {
                    task.startingDate = ( new Date() ).toISOString();
                }

                if ( !task.dueDate ) {
                    task.dueDate = ( new Date() ).toISOString();
                }          

                task.status = $scope.findTaskStatusByID( "running" );
                task.inProcess = true;     
                
                $scope.currentProject.tasks.push( task );

                if ( task.executor && task.executor._id ) {
                    var userFound = false;                  
                    for ( var i = 0; i < $scope.currentProject.projectGroupList.length; i++ ) {
                        var currentUser = $scope.currentProject.projectGroupList[i];
                        if ( currentUser._id === task.executor._id ) {
                            userFound = true;
                            break;
                        }
                    }
                    if ( !userFound ) {
                        $scope.currentProject.projectGroupList.push( task.executor );
                    }
                }                

                if ( $scope.$root.$$phase != "$apply" && $scope.$root.$$phase != "$digest" ) {
                    $scope.$apply();
                }

                saveChanges();
            },           
            function( result ) {
            }
        );
    }

    $scope.editTask = function(task) {        
        if ( task.finished ) {
            alertify.confirm("Задача завершена. Желаете ли вы снова открыть её для редактирования?", function(e) {
                if ( e ) {
                    task.finished = false;
                    if ( task.commentComplete ) {
                        delete task.commentComplete;
                    }

                    task.status = $scope.findTaskStatusByID( "notScheduled" );

                    var editorInstance = $modal.open( Modals.taskEditor( task, $scope.users ) );     

                    editorInstance.result.then(                    
                        function( task ) {
                            var key = task.id;
                            var objectList = $scope.currentProject.tasks;
                            for ( var i = 0; i < objectList.length; i++ ) {
                                var currentItem = objectList[i];
                                if ( currentItem.id === key ) {
                                    objectList[i] = task;
                                    break;
                                }
                            }

                            if ( task.executor && task.executor._id ) {
                                var userFound = false;
                                for ( var i = 0; i < $scope.currentProject.projectGroupList.length; i++ ) {
                                    var currentUser = $scope.currentProject.projectGroupList[i];
                                    if ( currentUser._id === task.executor._id ) {
                                        userFound = true;
                                        break;
                                    }
                                }
                                if ( !userFound ) {
                                    $scope.currentProject.projectGroupList.push( task.executor );
                                }
                            }       

                            if ( $scope.$root.$$phase != "$apply" && $scope.$root.$$phase != "$digest" ) {
                                $scope.$apply();
                            }

                            saveChanges();
                        },
                        function( result ) {
                        }
                    );
                }
            });
        }
        else {
            var editorInstance = $modal.open( Modals.taskEditor( task, $scope.users ) );

            editorInstance.result.then(                    
                function( task ) {
                    var key = task.id;
                    var objectList = $scope.currentProject.tasks;
                    for ( var i = 0; i < objectList.length; i++ ) {
                        var currentItem = objectList[i];
                        if ( currentItem.id === key ) {
                            objectList[i] = task;
                            break;
                        }
                    }

                    if ( task.executor && task.executor._id ) {
                        var userFound = false;
                        for ( var i = 0; i < $scope.currentProject.projectGroupList.length; i++ ) {
                            var currentUser = $scope.currentProject.projectGroupList[i];
                            if ( currentUser._id === task.executor._id ) {
                                userFound = true;
                                break;
                            }
                        }
                        if ( !userFound ) {
                            $scope.currentProject.projectGroupList.push( task.executor );
                        }
                    }       

                    if ( $scope.$root.$$phase != "$apply" && $scope.$root.$$phase != "$digest" ) {
                        $scope.$apply();
                    }

                    saveChanges();
                },
                function( result ) {
                }
            );
        }  
    }

    $scope.deleteTask = function(task, redirectURL) {
        alertify.confirm("Вы действительно хотите удалить данную задачу?", function(e) {
            if ( e ) {
                var key = task.id;
                var objectList = $scope.currentProject.tasks;
                for ( var i = 0; i < objectList.length; i++ ) {
                    var currentItem = objectList[i];
                    if ( currentItem.id === key ) {                       
                        delete objectList.splice( i, 1 );
                        break;
                    }
                }

                if ( $scope.$root.$$phase != "$apply" && $scope.$root.$$phase != "$digest" ) {
                    $scope.$apply();
                }
                
                if ( redirectURL && ( redirectURL.length > 0 ) ) {
                    saveChanges( function ( result ) {
                        if ( result ) {
                            $location.path( redirectURL );
                        }
                    });
                }
                else {
                    saveChanges();
                }                
            }
        });
    };

    $scope.removeTask = function(task) {
        $scope.deleteTask( task, "projects/" + $scope.currentProject._id + "/tasks" );
    }

    $scope.finishTask = function(task) {      
    	if ( !task.executor || !task.executor._id ) {
    		alertify.error( "Назначьте исполнителя!" );
    		return;
    	}        

        // var subTasks = $scope.subTasks( task );
        // for ( var i = 0; i < subTasks.length; i++ ) {
        //     var currentTask = subTasks[i];
        //     if ( currentTask.finished && ( currentTask.finished !== true ) ) {
        //         alertify.error("Данная задача для завершения требует завершения всех подзадач!");
        //         return;
        //     }
        // }

        alertify.confirm( "Вы уверены, что хотите отметить эту задачу как выполненную?", function ( e ) {
            if ( e ) {
                var editorInstance = $modal.open( Modals.taskFinish( task, $scope.users ) );     

                editorInstance.result.then(
                    //Сохранение
                    function ( task ) {
                        task.finished = true;
                        task.inProcess = false;
                        task.finishingDate = ( new Date() ).toISOString();
                        task.status = $scope.findTaskStatusByID( "finished" );

                        var key = task.id;
                        var objectList = $scope.currentProject.tasks;
                        for ( var i = 0; i < objectList.length; i++ ) {
                            var currentItem = objectList[i];
                            if ( currentItem.id === key ) {
                                objectList[i] = task;
                                break;
                            }
                        }

                        if ( $scope.$root.$$phase != "$apply" && $scope.$root.$$phase != "$digest" ) {
                            $scope.$apply();
                        }

                        saveChanges();
                    },
                    //Отмена
                    function ( result ) {
                    }
                );

                // var key = task.id;
                // var objectList = $scope.currentProject.tasks;
                // for ( var i = 0; i < objectList.length; i++ ) {
                //     var currentItem = objectList[i];
                //     if ( currentItem.id === key ) {
                //         objectList[i] = task;
                //         break;
                //     }
                // }

                // if ( $scope.$root.$$phase != "$apply" && $scope.$root.$$phase != "$digest" ) {
                //     $scope.$apply();
                // }

                // saveChanges();
            }
        });
    };
    
    var saveChanges = function( cb ) {
        $scope.currentProject.comments = $scope.comments;     

        // Обновление данных модели, которые в силу особенностей angular не обновляются автоматически
        // http://www.codelord.net/2014/05/10/understanding-angulars-magic-dont-bind-to-primitives/
        for ( var i = 0; i < $scope.currentProject.arbitrationManagers.length; i++ ) {
            var currentItem = $scope.currentProject.arbitrationManagers[i];            

            [].slice.call( document.querySelectorAll( "#arbitrationManagersTable #arbitrationManagersTableRow" ) ).forEach( function( el ) {             
                if ( el.dataset.objid == currentItem._id ) {
                    var arbitrationManagerRegDate = el.querySelector( "#arbitrationManager-regDate" ).value;
                    var arbitrationManagerAppointDate = el.querySelector( "#arbitrationManager-appointDate" ).value;
                    currentItem.regDate = arbitrationManagerRegDate;
                    currentItem.appointDate = arbitrationManagerAppointDate;
                    return;
                }
            });
        }

        // Коррекция даты в ISO8601, если она представлена в локальном формате
        for ( var i = 0; i < $scope.currentProject.tasks.length; i++ ) {
            var currentItem = $scope.currentProject.tasks[i];

            var timestampString = currentItem.creationDate;
            if ( /\d+\.\d+\.\d+/.test( timestampString ) ) { // ru-RU
                timestampString = timestampString.replace( /(\d+)\.(\d+)\.(\d+)/, "$3-$2-$1" );

                if ( !( /\d{2}:\d{2}:\d{2}/.test( timestampString ) ) ) {
                    timestampString += "T00:00:00.000Z";
                }

                currentItem.creationDate = timestampString;
            }

            timestampString = currentItem.dueDate;        
            if ( /\d+\.\d+\.\d+/.test( timestampString ) ) { // ru-RU
                timestampString = timestampString.replace( /(\d+)\.(\d+)\.(\d+)/, "$3-$2-$1" );

                if ( !( /\d{2}:\d{2}:\d{2}/.test( timestampString ) ) ) {
                    timestampString += "T00:00:00.000Z";
                }

                currentItem.dueDate = timestampString;
            }
        }

        for ( var i = 0; i < $scope.currentProject.arbitrationManagers.length; i++ ) {
            var currentItem = $scope.currentProject.arbitrationManagers[i];

            var timestampString = currentItem.regDate;
            if ( /\d+\.\d+\.\d+/.test( timestampString ) ) { // ru-RU
                timestampString = timestampString.replace( /(\d+)\.(\d+)\.(\d+)/, "$3-$2-$1" );

                if ( !( /\d{2}:\d{2}:\d{2}/.test( timestampString ) ) ) {
                    timestampString += "T00:00:00.000Z";
                }

                currentItem.regDate = timestampString;
            }

            timestampString = currentItem.appointDate;        
            if ( /\d+\.\d+\.\d+/.test( timestampString ) ) { // ru-RU
                timestampString = timestampString.replace( /(\d+)\.(\d+)\.(\d+)/, "$3-$2-$1" );

                if ( !( /\d{2}:\d{2}:\d{2}/.test( timestampString ) ) ) {
                    timestampString += "T00:00:00.000Z";
                }

                currentItem.appointDate = timestampString;
            }
        }      

        // Дополнительная валидация данных
        if ( !$scope.currentProject.name || ( $scope.currentProject.name.length === 0 ) ) {
            alertify.error("Не задано значение поля \"Название проекта\" !");
            return;
        }

        if ( !$scope.currentProject.number || ( $scope.currentProject.name.number === 0 )  ) {
            alertify.error("Не задано значение поля \"Номер проекта\" !");
            return;
        } 

        if ( !$scope.currentProject.manager._id  ) {
            alertify.error("Не задано значение поля \"Менеджер проекта\" !");
            return;
        } 

        var project = new Project( $scope.currentProject );
        if ( $scope.newMode ) {            
            project.$save( { id: $scope.currentProject._id }, function() {
                alertify.success( "Сохранено!" );              
                console.log( "Проект " + "id: " + $scope.currentProject._id + " сохранен успешно" );
                cb && cb(true);
            });
        }
        else {
            project.$update( { id: $scope.currentProject._id }, function() {
                alertify.success( "Сохранено!" );          
                console.log( "Проект " + "id: " + $scope.currentProject._id + " сохранен успешно" );
                cb && cb(true);
            });
        }        
    };

    $scope.isRightSide = function(comments, user) {
    	//Первый комментарий - слева
    	if (!comments || (comments && comments.length == 0))
    		return false;

    	//Последующие комментарии
    	var userWroteComment = false;
    	var isRightSide = undefined;
    	comments.forEach(function(c) {
    		if (c.owner._id === user._id) {
    			userWroteComment = true;
    			isRightSide = c.right;
    			return;
    		}
    	});

    	//Пользователь уже писал комментарий - берем ту же сторону
    	if (userWroteComment) {
    		return isRightSide;
    	}

    	return (comments[comments.length-1].right) ? !comments[comments.length-1].right : true;
    }

    $scope.addComment = function(objType, objID, commentText, commentImportance) {        
        var newComment = {
            id: uuid2.newuuid(),
            text: commentText,
            importance: commentImportance,
            creationDate: ( new Date() ).toISOString(),
            owner: $scope.currentUser,         
            right: false
        };    

        if ( objType ) {
            if ( objType === "currentProject" ) {
                newComment.right = $scope.isRightSide( $scope.comments, $scope.currentUser );
                $scope.currentProject.comments.push( newComment );                
            }
            else if ( objType === "task" ) {
                for ( var i = 0; i < $scope.currentProject.tasks.length; i++ ) {
                    var currentItem = $scope.currentProject.tasks[i];
                    if ( currentItem.id === objID ) {
                        newComment.right = $scope.isRightSide( currentItem.comments, $scope.currentUser );
                        currentItem.comments.push( newComment );
                        break;
                    }
                }
            }
            else if ( objType === "assetIntangible" ) {
                for ( var i = 0; i < $scope.currentProject.assetsIntangible.length; i++ ) {
                    var currentItem = $scope.currentProject.assetsIntangible[i];
                    if ( currentItem.id === objID ) {
                        currentItem.comments = currentItem.comments || [];
                        newComment.right = $scope.isRightSide( currentItem.comments, $scope.currentUser );
                        currentItem.comments.push( newComment );
                        break;
                    }
                }
            }
            else if ( objType === "assetTangible" ) {
                for ( var i = 0; i < $scope.currentProject.assetsTangible.length; i++ ) {
                    var currentItem = $scope.currentProject.assetsTangible[i];
                    if ( currentItem.id === objID ) {
                        currentItem.comments = currentItem.comments || [];
                        newComment.right = $scope.isRightSide( currentItem.comments, $scope.currentUser );
                        currentItem.comments.push( newComment );
                        break;
                    }
                }
            }
        }

        if ( $scope.$root.$$phase != "$apply" && $scope.$root.$$phase != "$digest" ) {
            $scope.$apply();
        }

        saveChanges();
    }

     $scope.deleteComment = function(objType, objID, comment) {
        alertify.confirm("Вы действительно хотите удалить этот комментарий?", function(e) {
            if ( e ) {
                if ( objType ) {
                    if ( objType === "currentProject" ) {
                        for ( var j = 0; j < $scope.currentProject.comments.length; j++ ) {
                            var currentComment = $scope.currentProject.comments[j];
                            if ( currentComment.id === comment.id ) {
                                delete $scope.currentProject.comments.splice( j, 1 );
                                break;
                            }
                        }                       
                    }
                    else if ( objType === "task" ) {
                        for ( var i = 0; i < $scope.currentProject.tasks.length; i++ ) {
                            var currentItem = $scope.currentProject.tasks[i];
                            if ( currentItem.id === objID ) {
                                for ( var j = 0; j < currentItem.comments.length; j++ ) {
                                    var currentComment = currentItem.comments[j];
                                    if ( currentComment.id === comment.id ) {
                                        delete currentItem.comments.splice( j, 1 );
                                        break;
                                    }
                                } 
                                break;
                            }
                        }
                    }
                    else if ( objType === "assetIntangible" ) {
                        for ( var i = 0; i < $scope.currentProject.assetsIntangible.length; i++ ) {
                            var currentItem = $scope.currentProject.assetsIntangible[i];
                            if ( currentItem.id === objID ) {
                                for ( var j = 0; j < currentItem.comments.length; j++ ) {
                                    var currentComment = currentItem.comments[j];
                                    if ( currentComment.id === comment.id ) {
                                        delete currentItem.comments.splice( j, 1 );
                                        break;
                                    }
                                } 
                                break;
                            }
                        }
                    }
                    else if ( objType === "assetTangible" ) {
                        for ( var i = 0; i < $scope.currentProject.assetsTangible.length; i++ ) {
                            var currentItem = $scope.currentProject.assetsTangible[i];
                            if ( currentItem.id === objID ) {
                                for ( var j = 0; j < currentItem.comments.length; j++ ) {
                                    var currentComment = currentItem.comments[j];
                                    if ( currentComment.id === comment.id ) {
                                        delete currentItem.comments.splice( j, 1 );
                                        break;
                                    }
                                } 
                                break;
                            }
                        }
                    }
                }

                if ( $scope.$root.$$phase != "$apply" && $scope.$root.$$phase != "$digest" ) {
                    $scope.$apply();
                }

                saveChanges();
            }
        });
    }   


    $scope.deleteDoc = function(docs, doc) {
    	alertify.confirm("Вы действительно хотите удалить этот файл?", function(e) {
            if (e) {
            	var index = 0;
            	var k = -1;
            	docs.forEach(function(d) {
            		if (d.id == doc.id) {
            			k = index;
            			return;
            		}
            		index++;
            	});

            	if (k>-1) {
            		docs.splice(k, 1);
                    $scope.$apply();	    		
    			}
    		}
    	});
    };

    var deleteDocuments = function(documentsContainer){       
        $scope.currentProject.tasks.forEach(function(t){
            if(t.id === documentsContainer.id){
                t.documents = documentsContainer.documents || [];
            }
        });
        renderTaskList();
    }

    var canCreateTasks = function(){
        return $scope.currentProject.tasks.every(function(t){
            return t.finished;
        });
    }

    var canSendToCRM = function(){
        var result = !!$scope.currentProject.tasks.filter(function(t){
            return !t.finished && t.status && t.status.title === "осмотр";
        })[0];
        return result;
    }

    var setRight = function(role){
        if(role.right === "view"){
            $scope.viewRight = true;
        } else if(role.right === "edit"){
            $scope.editRight = true;
        }
    }
    var setOwner = function(currentUser){
        if($scope.currentProject.manager || $scope.currentProject.owner){
            $scope.isOwner = false;
           if($scope.currentProject.owner){
                $scope.isOwner = $scope.currentProject.owner._id === currentUser._id;
           }
           if($scope.currentProject.manager){
                $scope.isOwner = $scope.isOwner || ($scope.currentProject.manager._id === currentUser._id);
           }
        }
    }

    var setRights = function(currentUser){
        if(!$scope.currentProject.status){
            $scope.editRight = true;
            $scope.viewRight = true;
            return;
        }
        if($scope.currentProject.status.roles){
            $scope.currentProject.status.roles.forEach(function(role){
                RolesHelper.getCurrentRoles(currentUser, $scope.currentProject).forEach(function(userRole){
                    if(userRole === role.role){
                        setRight(role);
                    }
                });
            });
        } else {
            //Если нет ролей разрешаем только просмотр
            $scope.viewRight = true;
        }

        if(currentUser.isRoot){
            $scope.editRight = true;
            $scope.viewRight = true;
        } else {
            $scope.editRight = $scope.isOwner || $scope.editRight;
            if($scope.editRight) $scope.viewRight = true;
            if(!$scope.viewRight) $scope.accessDeniedText = "В данный момент заявка имеет статус, не позволяющий вам просматривать ее";
            if(!$scope.editRight && $scope.viewRight){
                $scope.editDeniedText = "В данный момент заявка имеет статус, не позволяющий вам работать с задачами";
            }
        }
    }

    $scope.updateStatus = function(){      
        updateStatuses($scope.currentProject.status);
        alertify.success("Статус успешно обновлен!");
    }  

    var getStatuses = function(){
        $scope.statuses = [];
        if(!$scope.currentProject.status){
            return;
        }
        if($scope.currentProject.status.next){
            $scope.currentProject.status.next.forEach(function(id){
                $scope.currentProject.statuses.forEach(function(st){
                    if(st.id === id){
                        $scope.statuses.push(st);
                    }
                });
            });
        }
    }

    var updateStatuses = function(status){
        if(status.autoset){
            $scope.currentProject.status = status;
        }

        getStatuses();
        renderTaskList();
    }

    var setNextStatus = function(){
        if($scope.currentProject.status.next && $scope.currentProject.status.next.length === 1){
            var result;
            $scope.currentProject.statuses.forEach(function(st){
                if(st.id === $scope.currentProject.status.next[0]){
                    result = st;
                }
            });
            if(result){
                $scope.currentProject.status = result;
            }
        }
    }

    $scope.edit = function() {     
        $location.path( "projects/" + mainProjectId + "/edit" );
    };
    
    $scope.cancel = function() {
        if ( $scope.newMode ) {
            $location.path( "projects" );
        }
        else {
            $location.path( "projects/" + $scope.currentProject._id + "/main" );
        }
    };

    $scope.save = function() {      
        saveChanges( function ( result ) {
            if ( result ) {
                $location.path( "projects/" + $scope.currentProject._id + "/main" );
            }
        });       
    };   
    
    $scope.close = function() {
        $location.path( "/projects/" + $scope.currentProject._id );
    };

    var loadTaskDocuments = function(prop) {
        $scope.taskDocuments = [];
        prop.tasks.forEach(function(task) {
            if (task && task.documents) {
                task.documents.forEach(function(doc) {
                    $scope.taskDocuments.push(doc);
                });
            }
        });       
    };

    var checkDocuments = function (docs) {
        return new Promise(function(resolve, reject){
            var k = 1;
            docs.forEach(function(doc) {
                if (doc.link.indexOf("cloud") > 0 && doc.link.indexOf("abn-consult.ru") > 0) {
                    //Пытаемся получить имена файлов
                    var linkParts = doc.link.split("/");
                    var fileId = linkParts[linkParts.length-1];

                    $http.get(CONFIG.CLOUD_ADDRESS + "/api/files/" + fileId)
                        .success(function(data, status, headers, config) {
                            //Без расширения файла
                            //var fileName = data.file.Name;
                            //doc.name = fileName.substr(0, fileName.lastIndexOf("."));
                            var fileName = data.file.Name;
                            doc.type = Utils.getFileType(fileName);

                            if (!doc.name) {
                                doc.name = fileName;
                            }
                        }).error(function(data, status, headers, config) {
                            doc.name = "Файл" + k;
                            doc.type = "file";
                            k++;
                        });
                } else {
                    if (doc.name.length===0)
                        doc.name = "Файл" + k;

                    if (doc.type!=="intDoc")
                        doc.type = "link";
                    k++;
                }
            });
        });
    };    

    // GANNT
    $scope.useHour = false;
    $scope.showExecutors = false;
    $scope.first = true;

    $scope.toggleScale = function() {
        var weekScaleTemplate = function(date){
            var dateToStr = gantt.date.date_to_str("%d %M");
            var endDate = gantt.date.add(gantt.date.add(date, 1, "week"), -1, "day");
                return dateToStr(date) + " - " + dateToStr(endDate);
        };

        gantt.config.subscales = [
            {unit:"week", step:1, template:weekScaleTemplate},
            {unit:"day", step:1, date:"%D %d" },
        ];

        if ($scope.useHour) {
            gantt.config.subscales.push({unit:"hour", step:1, date:"%H" });
        }

        $scope.useHour = !$scope.useHour;
        gantt.refreshData();
        gantt.render();
    };

    $scope.toggleExecutors = function() {
        gantt.config.keep_grid_width = true;
        gantt.config.columns = [
            {name: "text", width: "260", tree: true},
            {name: "start_date", width: "100", align: "center"},
            {name: "end_date", width: "100", label: "Конец", align: "center"},
            {name: "duration", width: "100", align: "center"}
        ];

        if ($scope.showExecutors) {
            gantt.config.columns.push({name:"executor", label: "Исполнитель", align: "left", width: "160"});
            $scope.first = false;
        }

        if (!$scope.first) {
            gantt.refreshData();
            gantt.render();
        }

        $scope.showExecutors = !$scope.showExecutors;
    };
    
    $scope.updateGanttDiagramm = function() {
        var startDate = new Date($scope.currentProject.started);
        var endDate = new Date($scope.currentProject.started);
        var firstStartDate = ( new Date() ).toISOString();

        var res =  {
            data: [
                {
                    id: 1,
                    text: "Заявка",
                    start_date: startDate,
                    end_date: new Date(),
                    progress: 1.0,
                    type: gantt.config.types.project,
                    executor: "",
                    open: true
                }
            ],
            links: [
                {
                    id: 1,
                    source: 1,
                    target: 2,
                    type: "0"
                }
            ]
        };

        if ($scope.currentProject.tasks) {
            var tasks = Array.prototype.slice.call($scope.currentProject.tasks);
            tasks.reverse();

            var taskId = 2;
            var linkId = 1;
            tasks.forEach(function(t) {
                var newTask = {
                    id: taskId,
                    text: t.title,
                    start_date: new Date(t.creationDate),
                    progress: 1.0,
                    //duration:8,
                    //order:20,
                    parent: 1,
                    type: gantt.config.types.task,
                    executor: (t.executor) ? t.executor.fullName : ""
                    //open: true
                };

                if (t.finished && t.finishingDate) {
                    newTask.end_date = new Date(t.finishingDate);
                } else {
                    newTask.text += " (в процессе)";
                    newTask.end_date = new Date();
                }

                if (newTask.end_date > endDate) {
                    endDate = newTask.end_date;
                }

                var creationDate = t.creationDate;
                if (creationDate < firstStartDate) {
                    firstStartDate = creationDate;
                }

                //newTask.color = "green";
                //newTask.class = t.status.className;
                res.data.push(newTask);
                res.links.push({
                    id: linkId,
                    source: taskId,
                    target: taskId + 1,
                    type: "0"
                });
                taskId++;
                linkId++;
            });
            res.data[0].end_date = endDate;
        }

        var startGap = {
            text: "Этап до оценки",
            start_date: $scope.currentProject.started,
            end_date: firstStartDate//,
            //color: "orange"
        };

        gantt.config.keep_grid_width = true;
        gantt.config.readonly = false;

        $scope.toggleExecutors();

        gantt.templates.grid_date_format = function(date){
            return gantt.date.date_to_str("%d.%m %H:%i")(date);
        };

        gantt.init("interactiveGantt");

        gantt.config.scale_height = 90;
        gantt.config.scale_unit = "month";

        $scope.toggleScale();

        gantt.config.date_scale = "%F, %Y";
        gantt.config.min_column_width = 50;

        gantt.clearAll();
        gantt.parse(res);
    };

    $scope.$on( "ngRepeatFinished", function( ngRepeatFinishedEvent ) {
        [].slice.call( document.querySelectorAll( ".fc-datetime-widget" ) ).forEach( function( el ) {       
            $( el.querySelector( "input" ) ).datepicker({           
                format: {                
                    toDisplay: function( date, format, language ) {
                        var d = new Date( date );

                        var timestampString = d.toISOString().replace( /T/, " " ).replace( /Z/, "" ).replace( /\..+/, "" );
                        timestampString = timestampString.replace( /(\d+)-(\d+)-(\d+)/, "$3.$2.$1" );

                        if ( /\d{2}:\d{2}:\d{2}/.test( timestampString ) ) {
                            var re = /\s/;
                            var tokens = timestampString.split( re );
                            timestampString = tokens[0];
                        }

                        return timestampString;
                    },
                    toValue: function( date, format, language ) {
                        var timestampString = date;
                        if ( /\d+\.\d+\.\d+/.test( timestampString ) ) { // ru-RU
                            timestampString = timestampString.replace( /(\d+)\.(\d+)\.(\d+)/, "$3-$2-$1" );
                        }
                    
                        var result = new Date( timestampString );
                        return result;
                    }
                },         
                language: "ru-RU",
                autoclose: true
            })
            .on( "changeDate", function ( e ) {
                el.querySelector( ".fc-datetime-widget-clear-button" ).style.display = "block";
            });

            el.querySelector( ".fc-datetime-widget-clear-button" ).addEventListener( "click", function() {
                $( el.querySelector( "input" ) ).datepicker( "clearDates" );
                el.querySelector( ".fc-datetime-widget-clear-button" ).style.display = "none";
            });

            var date = el.querySelector( "input" ).value;
        
            if ( date.length === 0 ) {
                return;
            }            

            var timestampString = date;        
            if ( /\d+-\d+-\d+/.test( timestampString ) ) { // ISO8601
                timestampString = UTCToLocalTime( Date.parse( timestampString ) ).toISOString().replace( /T/, " " ).replace( /Z/, "" ).replace( /\..+/, "" );
                timestampString = timestampString.replace( /(\d+)-(\d+)-(\d+)/, "$3.$2.$1" );

                var re = /\s/;
                var tokens = timestampString.split( re );

                timestampString = tokens[0];
            }
            else if ( /\d+\.\d+\.\d+/.test( timestampString ) ) { // ru-RU
                timestampString = timestampString.replace( /(\d+)\.(\d+)\.(\d+)/, "$3-$2-$1" );
            }
            else {
                console.error( "Недопустимый формат даты " + timestampString );
                return;
            }     

            $( el.querySelector( "input" ) ).datepicker( "update", timestampString );
            el.querySelector( ".fc-datetime-widget-clear-button" ).style.display = "block"; 
        });
    });

    $scope.addArbitrationManager = function() {
        $scope.currentProject.arbitrationManagers = $scope.currentProject.arbitrationManagers || [];           
        $scope.currentProject.arbitrationManagers.push({
            _id: uuid2.newuuid(),				
            lastName : "",
            name : "Управляющий " + ($scope.currentProject.arbitrationManagers.length + 1),
            patronymic : "",
            INN : "",
            SNILS : "",
            regNum : "",
            regDate : "",
            SRO : "",
            appointDate : ""
        });
    }

    $scope.removeArbitrationManager = function(arbitrationManager) {
        alertify.confirm("Вы действительно хотите удалить данного управляющего?", function(e) {
            if (e) {
                var indx = -1;
                var k = 0;
                $scope.currentProject.arbitrationManagers.forEach(function(obj) {
                    if (obj._id == arbitrationManager._id)
                        indx = k;
                    k++;
                });

                if (indx > -1) {
                    $scope.currentProject.arbitrationManagers.splice(indx, 1);                    
                }
            }
        });
    }

    $scope.setConstantData = function(project) {
        var newTask = null;

        newTask = {
            id: "2daf23b3-89dd-4aea-9cee-bed1e9b4c46c",
            parentID: null,
            creationDate: ( new Date() ).toISOString(),
            startingDate: null,
            dueDate: null,
            inProcess: false,
            needsDocumentsOnFinish: false,
            status: $scope.findTaskStatusByID( "notScheduled" ),
            documents: [],
            description : "Отправка директору требования о передаче документов общества и распоряжения собственника",
            title: "Требование о передаче документов (Директору)",
            comments : [],
            executor: {}
        };
        project.tasks.push( newTask );
      
        newTask = {
            id: "bd952186-b244-42bb-89fd-d1b24668253c",
            parentID: null,
            creationDate: ( new Date() ).toISOString(),
            startingDate: null,
            dueDate: null,
            inProcess: false,
            needsDocumentsOnFinish: false,
            status: $scope.findTaskStatusByID( "notScheduled" ),
            documents: [],
            description: "Направить уведомления в известные кредитные организации о смене генерального директора",
            title: "Уведомление о смене генерального директора (Кредитные организации)",
            comments : [],
            executor: {}
        };
        project.tasks.push( newTask );

        newTask = {
            id: "9d07fd8e-fbf1-426d-8504-965587f2b78c",
            parentID: "bd952186-b244-42bb-89fd-d1b24668253c",
            creationDate: ( new Date() ).toISOString(),
            startingDate: null,
            dueDate: null,
            inProcess: false,
            needsDocumentsOnFinish: false,
            status: $scope.findTaskStatusByID( "notScheduled" ),
            documents: [],
            description: "Получить подтверждения от кредитных организации",
            title: "Подтверждения о смене генерального директора (Кредитные организации)",
            comments : [],
            executor: {}
        };
        project.tasks.push( newTask );

        newTask = {
            id: "047045d6-47af-4bce-8181-4799cbc5b4e9",
            parentID: null,
            creationDate: ( new Date() ).toISOString(),
            startingDate: null,
            dueDate: null,
            inProcess: false,
            needsDocumentsOnFinish: false,
            status: $scope.findTaskStatusByID( "notScheduled" ),
            documents: [],
            description: "Направить в адрес депозитария и регистратора, уведомление о смене генерального директора с просьбой не совершать записей по лицевым счетам АО",
            title: "Обращение к нотариусу",
            comments : [],
            executor: {}
        }
        project.tasks.push( newTask );
      
        newTask = {
            id: "7f501c69-4264-4812-9a37-50a193839f08",
            parentID: null,
            creationDate: ( new Date() ).toISOString(),
            startingDate: null,
            dueDate: null,
            inProcess: false,
            needsDocumentsOnFinish: false,
            status: $scope.findTaskStatusByID( "notScheduled" ),
            documents: [],
            description: "Направить в адрес депозитария и регистратора, уведомление о смене генерального директора с просьбой не совершать записей по лицевым счетам АО",
            title: "Обращение в ИФНС",
            comments : [],
            executor: {}
        };
        project.tasks.push( newTask );
     
        newTask = {           
            id: "348603e2-b9ce-4cb3-a3cd-e5f2464ba122",
            parentID: "7f501c69-4264-4812-9a37-50a193839f08",
            creationDate: ( new Date() ).toISOString(),
            startingDate: null,
            dueDate: null,
            inProcess: false,
            needsDocumentsOnFinish: false,
            status: $scope.findTaskStatusByID( "notScheduled" ),
            documents: [],
            description: "Направить в адрес депозитария и регистратора, уведомление о смене генерального директора с просьбой не совершать записей по лицевым счетам АО",
            title: "Регистрация в ЕГРЮЛ",
            comments : [],
            executor: {}
        };
        project.tasks.push( newTask );

        newTask = {
            id: "4deb8894-f2f9-4eea-a2a7-9f29023d0fb8",
            parentID: "7f501c69-4264-4812-9a37-50a193839f08",
            creationDate: ( new Date() ).toISOString(),
            startingDate: null,
            dueDate: null,
            inProcess: false,
            needsDocumentsOnFinish: false,
            status: $scope.findTaskStatusByID( "notScheduled" ),
            documents: [],
            description: "Направить в адрес депозитария и регистратора, уведомление о смене генерального директора с просьбой не совершать записей по лицевым счетам АО",
            title: "Смена банковских карточек",
            comments : [],
            executor: {}
        };
        project.tasks.push( newTask );

        newTask = {
            id: "f18ba711-27c6-4f2d-af42-474e23c94355",
            parentID: "7f501c69-4264-4812-9a37-50a193839f08",
            creationDate: ( new Date() ).toISOString(),
            startingDate: null,
            dueDate: null,
            inProcess: false,
            needsDocumentsOnFinish: false,
            status: $scope.findTaskStatusByID( "notScheduled" ),
            documents: [],
            description: "Направить в адрес депозитария и регистратора, уведомление о смене генерального директора с просьбой не совершать записей по лицевым счетам АО",
            title: "Смена прав первой и второй подписи",
            comments : [],
            executor: {}
        };
        project.tasks.push( newTask );

        newTask = {
            id: "1689e5a0-1b5e-436a-8ecc-f0a4ba8dda99",
            parentID: null,
            creationDate: ( new Date() ).toISOString(),
            startingDate: null,
            dueDate: null,
            inProcess: false,
            needsDocumentsOnFinish: false,
            status: $scope.findTaskStatusByID( "notScheduled" ),
            documents: [],
            description: "Направить в Росреестр уведомления о смене директоров с просьбой приостановить совершение регистрационных действий в части любого принадлежащего обществу недвижимого имущества и получить подтверждения",
            title: "Уведомления о смене директоров (Росреестр)",
            comments : [],
            executor: {}
        };
        project.tasks.push( newTask );

        newTask = {
            id: "6f32182f-f4b3-47ff-9379-a927724adee0",
            parentID: "1689e5a0-1b5e-436a-8ecc-f0a4ba8dda99",
            creationDate: ( new Date() ).toISOString(),
            startingDate: null,
            dueDate: null,
            inProcess: false,
            needsDocumentsOnFinish: false,
            status: $scope.findTaskStatusByID( "notScheduled" ),
            documents: [],
            description: "Направить в Росреестр уведомления о смене директоров с просьбой приостановить совершение регистрационных действий в части любого принадлежащего обществу недвижимого имущества и получить подтверждения",
            title: "Подтверждения о смене директоров (Росреестр)",
            comments : [],
            executor: {}
        };
        project.tasks.push( newTask );

        newTask = {
            id: "ba16f352-49b6-4e65-9b6a-a22101b9de0c",
            parentID: null,
            creationDate: ( new Date() ).toISOString(),
            startingDate: null,
            dueDate: null,
            inProcess: false,
            needsDocumentsOnFinish: false,
            status: $scope.findTaskStatusByID( "notScheduled" ),
            documents: [],
            description: "Опубликовать на федеральном ресурсе сведения об отзыве всех доверенностей выданных обществом",
            title: "Раскрытие корпоративной информации",
            comments : [],
            executor: {}
        }
        project.tasks.push( newTask );

        newTask = {
            id: "27006d14-3e06-4a51-8da3-60dc8bb13d95",
            parentID: null,
            creationDate: ( new Date() ).toISOString(),
            startingDate: null,
            dueDate: null,
            inProcess: false,
            needsDocumentsOnFinish: false,
            status: $scope.findTaskStatusByID( "notScheduled" ),
            documents: [],
            description: "Опубликовать на федеральном ресурсе сведения об отзыве всех доверенностей выданных обществом",
            title: "Сведения об отмене доверенностей в СМИ",
            comments : [],
            executor: {}
        };
        project.tasks.push( newTask );

        newTask = {
            id: "a9e991bc-23a6-4c99-99fc-72f3a7c52082",
            parentID: null,
            creationDate: ( new Date() ).toISOString(),
            startingDate: null,
            dueDate: null,
            inProcess: false,
            needsDocumentsOnFinish: false,
            status: $scope.findTaskStatusByID( "notScheduled" ),
            documents: [],
            description: "Обеспечить участие в текущих судебных разбирательствах. Отправить в суды уведомление об отзыве доверенностей за подписью уволенных директоров АО",
            title: "Уведомление об отзыве доверенностей (Суды)",
            comments : [],
            executor: {}
        };
        project.tasks.push( newTask );

        newTask = {
            id: "5f61e616-8b41-40da-b573-6738a3282ac7",
            parentID: null,
            creationDate: ( new Date() ).toISOString(),
            startingDate: null,
            dueDate: null,
            inProcess: false,
            needsDocumentsOnFinish: false,
            status: $scope.findTaskStatusByID( "notScheduled" ),
            documents: [],
            description: "Обеспечить участие в текущих судебных разбирательствах. Отправить в суды уведомление об отзыве доверенностей за подписью уволенных директоров АО",
            title: "Ознакомление с материалами судебных дел",
            comments : [],
            executor: {}
        };
        project.tasks.push( newTask );

        newTask = {
            id: "df42c022-63b7-430b-ae74-79ddcd6d024b",
            parentID: null,
            creationDate: ( new Date() ).toISOString(),
            startingDate: null,
            dueDate: null,
            inProcess: false,
            needsDocumentsOnFinish: false,
            status: $scope.findTaskStatusByID( "notScheduled" ),
            documents: [],
            description: "Обеспечить участие в текущих судебных разбирательствах. Отправить в суды уведомление об отзыве доверенностей за подписью уволенных директоров АО",
            title: "Обеспечение непрерывного участия исполнителей",
            comments : [],
            executor: {}
        };
        project.tasks.push( newTask );

        newTask = {
            id: "afd87b3a-dca8-4cdf-952a-c064cdc3b0f8",
            parentID: null,
            creationDate: ( new Date() ).toISOString(),
            startingDate: null,
            dueDate: null,
            inProcess: false,
            needsDocumentsOnFinish: false,
            status: $scope.findTaskStatusByID( "notScheduled" ),
            documents: [],
            description: "Восстановление Бухгалтерского и кадрового учёта, инвентаризация",
            title: "Принятие учредительных документов",
            comments : [],
            executor: {}
        };
        project.tasks.push( newTask );

        newTask = {
            id: "d8265f0d-d3bc-472e-a654-a1c0427e1f47",
            parentID: null,
            creationDate: ( new Date() ).toISOString(),
            startingDate: null,
            dueDate: null,
            inProcess: false,
            needsDocumentsOnFinish: false,
            status: $scope.findTaskStatusByID( "notScheduled" ),
            documents: [],
            description: "Восстановление Бухгалтерского и кадрового учёта, инвентаризация",
            title: "Принятие печатей",
            comments : [],
            executor: {}
        };
        project.tasks.push( newTask );

        newTask = {
            id: "dc2a3e57-3c22-4492-9d5c-177c32ed03e2",
            parentID: null,
            creationDate: ( new Date() ).toISOString(),
            startingDate: null,
            dueDate: null,
            inProcess: false,
            needsDocumentsOnFinish: false,
            status: $scope.findTaskStatusByID( "notScheduled" ),
            documents: [],
            description: "Восстановление Бухгалтерского и кадрового учёта, инвентаризация",
            title: "Принятие ключей от сайта",
            comments : [],
            executor: {}
        };
        project.tasks.push( newTask );

        newTask = {
            id: "5664f8c5-bd43-48db-8448-4f4c136f3436",
            parentID: null,
            creationDate: ( new Date() ).toISOString(),
            startingDate: null,
            dueDate: null,
            inProcess: false,
            needsDocumentsOnFinish: false,
            status: $scope.findTaskStatusByID( "notScheduled" ),
            documents: [],
            description: "Восстановление Бухгалтерского и кадрового учёта, инвентаризация",
            title: "Принятие по актам приема-передачи хозяйственных договоров",
            comments : [],
            executor: {}
        };
        project.tasks.push( newTask );

        newTask = {
            id: "b63715d9-cf5a-402a-8b0a-5be99bbd3173",
            parentID: null,
            creationDate: ( new Date() ).toISOString(),
            startingDate: null,
            dueDate: null,
            inProcess: false,
            needsDocumentsOnFinish: false,
            status: $scope.findTaskStatusByID( "notScheduled" ),
            documents: [],
            description: "Восстановление Бухгалтерского и кадрового учёта, инвентаризация",
            title: "Принятие по актам приема-передачи корпоративных документов",
            comments : [],
            executor: {}
        }
        project.tasks.push( newTask );

        newTask = {
            id: "ffb0973c-bf44-4a05-8aea-db9efa042e27",
            parentID: null,
            creationDate: ( new Date() ).toISOString(),
            startingDate: null,
            dueDate: null,
            inProcess: false,
            needsDocumentsOnFinish: false,
            status: $scope.findTaskStatusByID( "notScheduled" ),
            documents: [],
            description: "Восстановление Бухгалтерского и кадрового учёта, инвентаризация",
            title: "Принятие по акту приема-передачи претензионно-исковой документации",
            comments : [],
            executor: {}
        };
        project.tasks.push( newTask );

        newTask = {
            id: "d5e6f5d5-99f5-4a0d-8a6a-b782ec659854",
            parentID: null,
            creationDate: ( new Date() ).toISOString(),
            startingDate: null,
            dueDate: null,
            inProcess: false,
            needsDocumentsOnFinish: false,
            status: $scope.findTaskStatusByID( "notScheduled" ),
            documents: [],
            description: "Восстановление Бухгалтерского и кадрового учёта, инвентаризация",
            title: "Инвентаризация",
            comments : [],
            executor: {}
        }
        project.tasks.push( newTask );

        newTask = {
            id: "60e823a9-6a02-45ba-9b4f-b1ff738fa680",
            parentID: "d5e6f5d5-99f5-4a0d-8a6a-b782ec659854",
            creationDate: ( new Date() ).toISOString(),
            startingDate: null,
            dueDate: null,
            inProcess: false,
            needsDocumentsOnFinish: false,
            status: $scope.findTaskStatusByID( "notScheduled" ),
            documents: [],
            description: "Восстановление Бухгалтерского и кадрового учёта, инвентаризация",
            title: "Восстановление Бухгалтерского учёта",
            comments : [],
            executor: {}
        }
        project.tasks.push( newTask );        
        
        newTask = {
            id: "64295087-09a2-47f9-85d3-208046c54129",
            parentID: "d5e6f5d5-99f5-4a0d-8a6a-b782ec659854",
            creationDate: ( new Date() ).toISOString(),
            startingDate: null,
            dueDate: null,
            inProcess: false,
            needsDocumentsOnFinish: false,
            status: $scope.findTaskStatusByID( "notScheduled" ),
            documents: [],
            description: "Восстановление Бухгалтерского и кадрового учёта, инвентаризация",
            title: "Восстановление Кадрового учёта",
            comments : [],
            executor: {}
        }
        project.tasks.push( newTask );
    }

    $scope.subTasks = function( task ) {      
        var taskID = task.id;
        var result = [];
        for ( var i = 0; i < $scope.currentProject.tasks.length; i++ ) {
            var currentTask = $scope.currentProject.tasks[i];
            if ( currentTask.parentID && ( currentTask.parentID === taskID ) ) {
                result.push( currentTask );
            }
        }
        return result;
    }
    
    $scope.editTasks = function() {
     	window.location.href = "/#/projects/" + $routeParams.id + "/tasks-edit";
    }
  
    $http.get( CONFIG.REST_ADDRESS + "/api/users/brief" )
        .success( function( data, status, headers, config ) {
            $scope.users = $filter("orderBy")(data.users, "fullName");    
            $scope.managerUsers = $scope.users;       
        })
        .error( function( data, status, headers, config ) {      
            alertify.error("Не удалось загрузить список пользователей!");
        });

    
    // MAIN Точка входа
    if ( $scope.newMode === true ) {
        var newProjectEntity = new ProjectEntity();

        $scope.setConstantData(newProjectEntity);

        newProjectEntity._id = uuid2.newuuid();
        newProjectEntity.started = ( new Date() ).toISOString();

        delete $scope.currentUser.roles; // NOTE Модель ролей требует рефакторинга и сейчас поле roles содержит некорректные данные
        $scope.currentUser.roles = [];       

        newProjectEntity.manager = $scope.currentUser;
        newProjectEntity.owner = $scope.currentUser;
        if (newProjectEntity.owner) {            
            newProjectEntity.projectGroupList.push( newProjectEntity.owner );
        }
        if (newProjectEntity.manager) {           
            newProjectEntity.projectGroupList.push( newProjectEntity.manager );
        }

        $scope.currentProject = newProjectEntity;

        $rootScope.mainProjectId = $scope.currentProject._id; 
        $rootScope.title = 'Новый проект - AMS';
        $rootScope.subTitle = 'Новый проект';
    }
    else {      
        GetProject($scope, mainProjectId)
        .then( function( result ) {
            $scope.currentProject = result.project;

            console.log( "Проект id: " + $scope.currentProject._id + " загружен успешно" );           

            var rootProjects = Utils.getProjects();
            $scope.roles = Utils.getRoles();

            $scope.currentRole = RolesHelper.getRoleForProject($scope.currentUser, rootProjects[0]);
            $rootScope.currentRole = $scope.currentRole;

            $rootScope.projectId = $routeParams.id;
            $rootScope.title = '№' + $scope.currentProject.number + ' - ' + $scope.currentProject.name + ' - AMS';
            $rootScope.subTitle = 'Проект №' + $scope.currentProject.number + '. ' + $scope.currentProject.name;     

            updateProjectGroupList();

            checkDocuments( $scope.currentProject.documents );
            loadTaskDocuments( $scope.currentProject );      

            $scope.comments = $scope.currentProject.comments;
         
            $rootScope.mainProjectId = $scope.currentProject._id;
        });
    }
});