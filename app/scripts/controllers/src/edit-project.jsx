'use strict';

angular.module('crmApp')
  .controller('EditProjectCtrl', function ($scope, $http, $location, $routeParams, Template, Project) {
    var entityFactory = new EntityFactory(Template);
    var mapManager = new MapManager($http);

    Project.get({id: $routeParams.id}, function(project){
        if(project.$resolved){
            Template.get({id: project.templateId},function(template){
                if(template.$resolved){
                    $scope.project = project;
                    $scope.save = function(){
                        if($scope.project.name === ''){
                            $scope.project.name = 'Новый проект ' + $scope.project.started;
                        }
                        var project = new Project($scope.project);
                        project.$update({id: project._id},function(){
                            alertify.success("Сохранено!");
                            $location.path('/project/' + project._id);
                        });
                    }

                    var onCustomFieldsChanged = function(customFields){
                        $scope.project.customFields = customFields;
                    }

                    var components = entityFactory.parseTemplate(template);
                    React.render(<DfForm components={components} id={template.id} oldState={project.customFields} onChange={onCustomFieldsChanged}/>, document.getElementById('custom-fields'));
                    React.render(<DocsList items={project.documents} title={"Файлы и документы"}/>, document.getElementById('docs-list'));

                    setInterval(function(){
                        if($scope.project.address !== ''){
                            mapManager.viewPlace($scope.project.address,'map',null, function(address){
                                $scope.project.mapAddress = address;
                            });
                        }
                    },5000);
                }
            });
        }
    });
  });
