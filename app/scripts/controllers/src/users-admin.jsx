'use strict';

angular.module('crmApp')
    .controller('UsersAdminCtrl', function ($rootScope, $scope, Organization, User, $q, $location, $timeout, GetProject, Utils) {
        
        var mainProjectId = $rootScope.mainProjectId;
        var promise1 = GetProject($scope, mainProjectId),
            currentProject;

        var promise = $q.all({
            users: User.manage().$promise,
            orgs: Utils.getOrganizations(), //Organization.query().$promise,
            roles: Utils.getRoles()
        });

        $rootScope.title = 'Управление пользователями - AMS';
        $rootScope.subTitle = 'Управление пользователями';

        //TODO: получить список ролей по проекту
        //currentProject

        var onAdd = function(){
            $timeout(function(){
                $location.path('/admin/users/new');
                $scope.$apply();
            });
        };

        var onDelete = function(user){
            User.remove({id: user._id});
        };

        var onEdit = function(user){
            $timeout(function(){
                $location.path('/admin/users/'+user._id+'/edit');
                $scope.$apply();
            });
        };

        var onReset = function(user){
            $timeout(function(){
                $location.path('/admin/users/'+user._id+'/reset');
                $scope.$apply();
            });
        };

        var succ = function(data){
            if (data && data.users && data.users.users && data.users.users.length > 0) {
                data.users.users.forEach(function(user) {
                    //Получаем роль пользователя в проекте
                    //var userRole = user.roles.filter(function(role) {
                    //    return role.projectId === currentProject._id;
                    //})[0];

                    var userRole = user.roles[user.roles.length-1] || null;

                    if (currentProject) {
                        currentProject._id = "ams";
                        currentProject.roles = data.roles;
                    }

                    //Получаем имя роли
                    if (userRole) {
                        var projectRole = currentProject.roles.filter(function(role) {
                            return role.id === userRole.roleId;
                        })[0];
                        if (projectRole) {
                            user.roleName = projectRole.name.toLowerCase();
                        }
                    }
                });
            }
            //TODO: Смапить роли проекта на те что есть у пользователя
            React.render(<UsersList users={data.users.users} organizations={data.orgs}
                onAdd={onAdd} onDelete={onDelete} onEdit={onEdit} onReset={onReset}/>,
                document.getElementById('users-list'));
        };

        promise1.then(function(data){
            currentProject = data.project;
            $scope.roles = Utils.getRoles();
            promise.then(succ);
        });
});
