'use strict';

angular.module('crmApp')
    .controller('NewUserCtrl', function ($scope, Organization, Project, User, $q, $location, $timeout, Utils) {
        var promise = $q.all({
            users: User.query().$promise,
            orgs: Utils.getOrganizations(), //Organization.query().$promise
            projects: Utils.getProjects() //Project.query().$promise
        });

        var succ = function (data){

            var onCancel = function(){
                $timeout(function(){
                    $location.path('/admin/users');
                    $scope.$apply();
                });
            };
            var onSuccSave = function(resp){
                alertify.success('Сохранено!');
                $timeout(function(){
                    $location.path('/admin/users');
                    $scope.$apply();
                });
            };

            var onError = function(resp){
                if(resp.status ==400){
                    alertify.error(resp.data.message);
                }else if(resp.status == 500){
                    alertify.error("Невозможно добавить пользователя. Возможно, пользователь с таким email уже зарегистрирован.");
                }
            }

            var onSave = function (user) {
                user.fullName = user.firstName + ' ' + user.lastName;
                user.organizationId = Utils.getOrganizations()[0]._id;
                user = new User(user);
                User.save(user).$promise
                    .then(onSuccSave, onError);
            };

            React.render(<NewUserForm onSave={onSave} onCancel={onCancel}
                organizations={data.orgs} projects={data.projects}/>,
                document.getElementById('new-user-form'));
        };

        $scope.connectToCloud = function() {

            if (!$scope.cloud.login || !$scope.cloud.password) {
                alertify.error('Введите учетные данные для авторизации на Fincase Cloud!');
                return;
            }

            $http({method: 'POST', url: CONFIG.CLOUD_ADDRESS + '/oauth2/token',
                transformRequest: function(obj) {
                    var str = [];
                    for(var p in obj)
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data : {
                    username : $scope.cloud.login,
                    password : $scope.cloud.password,
                    grant_type : 'password',
                    client_id : CONFIG.CLOUD_CLIENT_ID
                },
                headers:{
                    'Accept': 'application/json'
                }})
                .success(function(data, status, headers, config) {
                    $scope.profile.cloudToken = data.access_token;
                    $scope.currentUser = $scope.profile;
                    $window.localStorage.profile = JSON.stringify($scope.profile);
                    onProfileSave($scope.profile);
                })
                .error(function(data, status, headers, config) {
                    $scope.profile.cloudToken = '';
                    alertify.error('Ошибка при проверке!');
                });
        }

        promise.then(succ);
    });
