'use strict';

angular.module('crmApp')
    .controller('NewProposalCtrl', function ($rootScope, $scope, $http, $routeParams, $window, $location, $filter, GetProject, Project, Proposal, Utils, CONFIG) {

        $rootScope.title = 'Новый проект - AMS';
        
        var entityFactory = new EntityFactory(),
            mapManager = new MapManager($http),
            components = [],
            promise = GetProject($scope, $routeParams.id),
            currentProject;
        
        var filter = {};

        var loadFilter = function() {
            if (Utils.isEmpty(filter) && $window.sessionStorage.newProposalFilter)
                filter = JSON.parse($window.sessionStorage.newProposalFilter);
            if (!Utils.isEmpty(filter.manager))
                $scope.proposal.manager = filter.manager;
            if (!Utils.isEmpty(filter.coordinator))
                $scope.proposal.coordinator = filter.coordinator;
        }

        $scope.orgMode = Utils.isOrgMode($routeParams.id);

        $scope.saveFilter = function() {
            filter.manager = ($scope.proposal.manager) ? $scope.proposal.manager : {};
            filter.coordinator = ($scope.proposal.coordinator) ? $scope.proposal.coordinator : {};
            $window.sessionStorage.newProposalFilter = JSON.stringify(filter);
        }

        $scope.updateDueDate = function() {
            var dateParts = $scope.dueDate.split('.');
            $scope.proposal.dueDate = new Date(dateParts[2], parseInt(dateParts[1])-1, dateParts[0]);
            //$scope.proposal.dueDate = Date.parse($scope.dueDate); //not valid
        }

        $http.get(CONFIG.REST_ADDRESS + '/api/users/brief').
            success(function(data, status, headers, config) {
                $scope.users = $filter('orderBy')(data.users, 'fullName');
                $scope.proposal = new ProposalEntity($routeParams.id);

                var dueDate = ( new Date() ).toISOString();
                dueDate.setDate(dueDate.getDate() + CONFIG.DUEDATE_PERIOD);
                $scope.dueDate = $filter('date')(dueDate,'dd.MM.yyyy');
                $scope.updateDueDate();
                loadFilter();

                var validateProposal = function(){
                    if($scope.proposal.number.length < 1){
                        alertify.error('Номер заявки должен состоять не менее чем из 1 симсола');
                        validate()
                        return false;
                    }else if($scope.proposal.address.length < 1){
                        alertify.error('Адрес заявки должен состоять не менее чем из 1 симсола');
                        validate()
                        return false;
                    } else {
                        return true;
                    }
                }

                $scope.save = function(){
                    if(validateProposal()){
                        if($scope.proposal.name === ''){
                            $scope.proposal.name = 'Новый проект ' + $scope.proposal.started;
                        }
                        if($scope.currentUser){
                            $scope.proposal.owner = $scope.currentUser;
                            Proposal.save({projectId:$routeParams.id}, $scope.proposal, function(item){
                                alertify.success("Сохранено!");
                                $scope.proposal = item;
                                $location.path('/projects/' + $routeParams.id + '/proposals/' + $scope.proposal._id);
                            });
                        } else {
                            alertify.error('Не удалось получить информацию о вашем профиле. Попробуйте обновить страницу (F5) и повторить операцию.');
                        }
                    }
                }

                $scope.cancel = function(){
                    $location.path('/projects/' + $routeParams.id);
                }

                $scope.changeNumber = function(){
                    if($scope.proposal.number.length < 1){
                        $scope.hasNumber = 'has-error';
                    } else {
                        $scope.hasNumber = '';
                    }
                }

                $scope.changeAddress = function(){
                    if($scope.proposal.address.length < 1){
                        $scope.hasAddress = 'has-error';
                    } else {
                        $scope.hasAddress = '';
                    }
                }

                $scope.updateAddress = function(){
                    if($scope.proposal.address !== ''){
                        mapManager.viewPlace($scope.proposal.address,'map',null, function(address){
                            $scope.proposal.mapAddress = address;
                        });
                    }
                }

                var validate = function(){
                    $scope.changeAddress();
                    $scope.changeNumber();
                }

                validate();

                var onCustomFieldsChanged = function(customFields){
                    $scope.proposal.customFields = customFields;
                }

                var onDocLinkAdded = function(docs){
                    $scope.proposal.documents = docs;
                }

                var functionLoadEntities = function(){
                    promise.then(function(data){
                        currentProject = data.project;
                        var address = CONFIG.PROJECT_CITIES[currentProject.name] + ' ';
                        if(address){
                            $scope.proposal.address = address;
                            validate();
                        }

                        $scope.proposal.status = currentProject.statuses.filter(function(st){
                            return st.default === true;
                        })[0];
                        components = entityFactory.parseTemplate(currentProject.proposalTemplate, currentProject);
                        $scope.proposal.templateId = currentProject.proposalTemplate._id;

                        React.render(<DfForm components={components} onChange={onCustomFieldsChanged} id={data.project.proposalTemplate.id} />, document.getElementById('custom-fields'));
                        React.render(<DocsList currentUser={$scope.currentUser} title={"Файлы и документы"} onChange={onDocLinkAdded}/>, document.getElementById('docs-list'));
                    });
                }

            functionLoadEntities();
        }).
        error(function(data, status, headers, config) {
            alertify.error('Не удалось загрузить список пользователей!');
        });
});
