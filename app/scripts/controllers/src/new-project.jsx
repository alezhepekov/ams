'use strict';

angular.module('crmApp')
  .controller('NewProjectCtrl', function ($scope, Project, Organization, $timeout, $location, $rootScope) {
    var promise = Organization.query().$promise;


    promise.then(function(data){

        var onRemove = function () {
            $timeout(function (){
                $location.path('/');
                $scope.$apply();
            });
        };
        var onSuccSave = function(resp){
            alertify.success('Сохранено!');
            console.log(resp);
            $timeout(function(){
                $rootScope.project = resp;
                $location.path('/projects/'+resp._id);
                $scope.$apply();
            });
        };
        var onError = function(resp){
            if(resp.status === 400){
                alertify.error(resp.data.message);
            }
        };
        var onSave = function(tmpProject){
            var project = new ProjectEntity();
            project.name = tmpProject.name;
            project.description = tmpProject.description;
            project.organizationId = tmpProject.organizationId;
            project.statuses = tmpProject.statuses;
            project.roles = tmpProject.roles;
            project = new Project(project);
            Project.save(project).$promise.then(onSuccSave,onError);
        };

        React.render(<NewProjectForm onRemove={onRemove} onSave={onSave} organizations={data.organizations}/>,
            document.getElementById('new-project-form'));
    });
  });
