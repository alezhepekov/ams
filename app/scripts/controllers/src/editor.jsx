'use strict';

angular.module('crmApp')
    .controller('EditorCtrl', function ($rootScope, $scope, $http, $routeParams, $modal, $modalInstance, parentScope, Proposal, CONFIG) {

	$scope.objectCaption = parentScope.objectCaption;
	$scope.doc = parentScope.doc;
    $scope.apiAddress = CONFIG.REST_ADDRESS;

    $scope.froalaOptions = {
        toolbarButtons : ["bold", "italic", "underline", "|", "align", "formatOL", "formatUL"]
    }

    $scope.save = function() {
    	if (!($scope.doc.name && $scope.doc.name.length!=0)) {
    		alertify.error('Введите название документа!');
    		return;
    	}

    	/*Proposal.saveDoc({projectId:$routeParams.id, id: $routeParams.propId}, $scope.doc, function(result) {
    		alertify.success('Документ успешно сохранен!');
    	});*/

        $scope.doc.uploadDate = ( new Date() ).toISOString();
        $modalInstance.close($scope.doc);
    };

    $scope.close = function () {
        $modalInstance.dismiss();
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

});