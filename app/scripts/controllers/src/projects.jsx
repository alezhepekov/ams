"use strict";

function UTCToLocalTime( utc ) {
    var timezoneOffset = ( new Date() ).getTimezoneOffset() * 60000;
    return new Date( utc - timezoneOffset );
}

angular.module( "crmApp" ).filter( "filterProjectCreation", function() {
    return function( items, dateFromString, dateToString ) {
        if ( !items ) {
            return [];
        }    

        var result = [];
        for ( var i = 0; i < items.length; i++ ) {
			var currentItem = items[i];		

            var timestampString = currentItem.started;

			if ( !timestampString || timestampString.length === 0 ) {
				result.push( currentItem );
				continue;
			}

            if ( !dateFromString || !dateToString || dateFromString.length === 0 && dateToString.length === 0 ) {
                result.push( currentItem );
            }
            else if ( dateFromString.length > 0 && dateToString.length > 0 ) {
                if ( timestampString >= dateFromString && timestampString <= dateToString ) {
                    result.push( currentItem );
                }
            }
            else if ( dateFromString.length > 0 ) {
                if ( timestampString >= dateFromString ) {
                    result.push( currentItem );
                }
            }
            else if ( dateFromString.length > 0 ) {
                if ( timestampString <= dateToString ) {
                    result.push( currentItem );
                }
            }
        }

        return result;
    };
});

angular.module( "crmApp" ).filter( "filterProjectFinished", function() {
    return function( items, dateFromString, dateToString ) {
        if ( !items ) {
            return [];
        }    

        var result = [];
        for ( var i = 0; i < items.length; i++ ) {
			var currentItem = items[i];		

            var timestampString = currentItem.dueDate;

			if ( !timestampString || timestampString.length === 0 ) {
				result.push( currentItem );
				continue;
			}

            if ( !dateFromString || !dateToString || dateFromString.length === 0 && dateToString.length === 0 ) {
                result.push( currentItem );
            }
            else if ( dateFromString.length > 0 && dateToString.length > 0 ) {
                if ( timestampString >= dateFromString && timestampString <= dateToString ) {
                    result.push( currentItem );
                }
            }
            else if ( dateFromString.length > 0 ) {
                if ( timestampString >= dateFromString ) {
                    result.push( currentItem );
                }
            }
            else if ( dateFromString.length > 0 ) {
                if ( timestampString <= dateToString ) {
                    result.push( currentItem );
                }
            }
        }

        return result;
    };
});

angular.module( "crmApp" ).filter( "filterProjectType", function() {
    return function( items, type ) {
		if ( !items ) {
            return [];
        }

        var result = [];
        for ( var i = 0; i < items.length; i++ ) {
			var currentItem = items[i];

			if ( !type.id ) {
				result.push( currentItem );
				continue;
			}

			if ( currentItem.type.id === type.id ) {
				result.push( currentItem );
			}
        }

        return result;
    };
});

angular.module( "crmApp" ).filter( "filterProjectManager", function() {
    return function( items, manager ) {
		if ( !items ) {
            return [];
        }

        var result = [];
        for ( var i = 0; i < items.length; i++ ) {
			var currentItem = items[i];

			if ( !manager._id ) {
				result.push( currentItem );
				continue;
			}

			if ( currentItem.manager._id === manager._id ) {
				result.push( currentItem );
			}
        }

        return result;
    };
});



angular.module( "crmApp" ).filter( "filterProjectsByUserRights", function() {
    return function( items, user ) {
		if ( !items ) {
            return [];
        }

        var result = [];
        for ( var i = 0; i < items.length; i++ ) {
			var currentItem = items[i];

			if ( !user._id ) {			
				continue;
			}

			if ( user.isRoot && ( user.isRoot === true ) ) {
				result.push( currentItem );
				continue;
			}

			// !!! ATTENTION !!! При использовании этой роли любой пользователь может просматривать все проекты
			if ( user.seeAllProjects ) {
				result.push( currentItem );
				continue;
			}

			if (
				currentItem.manager && ( currentItem.manager._id === user._id ) 
				|| currentItem.owner && ( currentItem.owner._id === user._id ) ) {
				result.push( currentItem );
			}		
        }

        return result;
    };
});

angular.module("crmApp")
.controller("ProjectsController", function ($rootScope, $scope, $routeParams, $q, $window, $filter, $location, User, Role, RolesHelper, Project, Proposal, GetProject, Utils, CONFIG) {
	$scope.currentUser = {};
	
	if ( window.localStorage.profile ) {
        $scope.currentUser = JSON.parse( window.localStorage.profile );       
    }
    else {    
        window.location = "#/login";
		return;
    }

	$scope.users = [];
	$scope.projects = [];

	$scope.filterProjectCreationDate = "";
    $scope.filterProjectCreationDateFrom = "";
    $scope.filterProjectCreationDateTo = "";

	$scope.filterProjectFinishedDate = "";
    $scope.filterProjectFinishedDateFrom = "";
    $scope.filterProjectFinishedDateTo = "";

	$scope.filterProjectType = {};
	$scope.filterProjectManager = {};

	$scope.projectTypes = Utils.getProjectTypes();
	$scope.filter = {};

	$scope.newProject = function() {
		$location.path( "#/projects/new" );
	};
	
	$scope.deleteProject = function( id ) {
		alertify.confirm( "Вы уверены, что хотите удалить данный проект?", function( e ) {
			if ( e ) {
				var project = new Project();
				project.$delete( { id: id }, function() {
					for ( var i = 0; i < $scope.projects.length; i++ ) {
						var currentItem = $scope.projects[i];
						if ( currentItem._id == id ) {
							delete $scope.projects.splice( i, 1 );
						}
					}

					alertify.success( "Проект удален успешно!" );
				});
			}
		});
	};

	$scope.toLocalDateString = function( date ) {
        if ( !date ) {
            return null;
        }

        var timestampString = UTCToLocalTime( Date.parse( date ) ).toISOString().replace( /T/, " " ).replace( /Z/, "" ).replace( /\..+/, "" );
        timestampString = timestampString.replace( /(\d+)-(\d+)-(\d+)/, "$3.$2.$1" );      
        var splits = timestampString.split(/\s/);   
        return splits[0];
    };
	
	$scope.newProject = function() {
		$location.path( "/projects/new" );
	};

	$scope.search = function( searchString, cb ) {
		if ( !searchString.length ) {
			return;
		}

		var queryParams = {		
			searchContext: "",
			searchString: searchString				
		};

		var resultDataSet = [];
		
		// Поиск с использованием разных методов и критериев определяемых в контексте
		queryParams.searchContext = "Project*";
		$q.all({
			projects: Project.query(queryParams).$promise
		}).then(function(data) {
			resultDataSet.push({
				SearchContext: "Project*",
				Data: data.projects.projects
			});

			queryParams.searchContext = "Project*.Document*";
			$q.all({
				projects: Project.query(queryParams).$promise
			}).then(function(data) {
				resultDataSet.push({
					SearchContext: "Project*.Document*",
					Data: data.projects.projects
				});

				queryParams.searchContext = "Project*.Task*";
				$q.all({					
					projects: Project.query(queryParams).$promise
				}).then(function(data) {				
					resultDataSet.push({
						SearchContext: "Project*.Task*",
						Data: data.projects.projects
					});

					// Обработка полученных результатов и их проекция для формирования конечного результата						
					var projectionDataSet = [];

					// Обработка результатов полученных при поиске по имени проекта
					var projects = resultDataSet[0].Data;
					for ( var i = 0; i < projects.length; i++ ) { // Projects
						var currentProject = projects[i];
						
						var projectID = currentProject._id;
						var projectName = currentProject.name;
						
						var re = new RegExp(searchString, "i");
						if ( currentProject.name.search( re ) != -1 ) {
							var objectID = currentProject._id;
							var objectName = currentProject.name;
							var objectType = "СВОЙСТВА";
							var objectCreationDateTime = ((currentProject.started) ? (currentProject.started) : (""));
							var objectDescription = ((currentProject.description) ? (currentProject.description) : (""));
							
							projectionDataSet.push({
								ObjectID: objectID,
								ObjectType: objectType,
								ObjectName: objectName,
								ObjectCreationDateTime: objectCreationDateTime.replace( /T/, " " ).replace( /Z/, "" ).replace( /\..+/, "" ),
								ObjectDescription: objectDescription,						
								ProjectID: projectID,
								ProjectName: projectName
							});								
						}
					}

					// Обработка результатов полученных при поиске по имени документа ( файла ) 
					var projects = resultDataSet[1].Data;
					for ( var i = 0; i < projects.length; i++ ) { // Projects
						var currentProject = projects[i];							

						var projectID = currentProject._id;
						var projectName = currentProject.name;

						for ( var j = 0; j < currentProject.documents.length; j++ ) { // Documents
							var currentDocument = currentProject.documents[j];
							
							var re = new RegExp(searchString, "i");
							if ( currentDocument.name.search( re ) != -1 ) {
								var objectID = currentDocument.id;
								var objectName = currentDocument.name;
								var objectType = "ДОКУМЕНТЫ";
								var objectCreationDateTime = ((currentDocument.uploadDate) ? (currentDocument.uploadDate) : (""));
								var objectDescription = ((currentDocument.description) ? (currentDocument.description) : (""));

								projectionDataSet.push({
									ObjectID: objectID,
									ObjectType: objectType,
									ObjectName: objectName,
									ObjectCreationDateTime: objectCreationDateTime.replace( /T/, " " ).replace( /Z/, "" ).replace( /\..+/, "" ),
									ObjectDescription: objectDescription,								
									ProjectID: projectID,
									ProjectName: projectName
								});
							}								
						}
					}

					// Обработка результатов полученных при поиске по имени задачи
					var projects = resultDataSet[2].Data;
					for ( var i = 0; i < projects.length; i++ ) { // Projects
						var currentProject = projects[i];							

						var projectID = currentProject._id;
						var projectName = currentProject.name;

						for ( var j = 0; j < currentProject.tasks.length; j++ ) { // Tasks
							var currentTask = currentProject.tasks[j];							

							var re = new RegExp(searchString, "i");
							if ( currentTask.title.search( re ) != -1 ) {
								var objectID = currentTask.id;
								var objectName = currentTask.title;
								var objectType = "ЗАДАЧИ";
								var objectCreationDateTime = ((currentTask.creationDate) ? (currentTask.creationDate) : (""));
								var objectDescription = ((currentTask.description) ? (currentTask.description) : (""));

								projectionDataSet.push({
									ObjectID: objectID,
									ObjectType: objectType,
									ObjectName: objectName,
									ObjectCreationDateTime: objectCreationDateTime.replace( /T/, " " ).replace( /Z/, "" ).replace( /\..+/, "" ),
									ObjectDescription: objectDescription,						
									ProjectID: projectID,
									ProjectName: projectName
								});
							}
						}
					}

					cb && cb( projectionDataSet );
				});

			});

		});
	};

	// MAIN
	$q.all({
		users: User.query( {} ).$promise,
		projects: Project.query( {} ).$promise
	})
	.then( function( data ) {	
		$scope.users = data.users.users;
		$scope.projects = data.projects.projects;
	});
});
