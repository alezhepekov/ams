/**
 * @jsx React.DOM
 */
'use strict';

angular.module('crmApp')
    .controller('LogOutCtrl', function ($scope, $timeout, $location, $window, $rootScope) {
        var onAccept = function(){
            delete $window.localStorage.token;
            delete $window.localStorage.profile;
            $rootScope.currentUser = null;
            $rootScope.project = null;
            $timeout(function(){
                $location.path('/login/');
                $scope.$apply();
            });
        }

        var onDecline  = function(){
            $timeout(function(){
                $location.path('/');
                $scope.$apply();
            });
        }
        React.render(<LogoutForm onAccept={onAccept} onDecline={onDecline} />,
            document.getElementById('logout-form'));
    });
