/**
 * @jsx React.DOM
 */
'use strict';

angular.module('crmApp')
  .controller('DevpageCtrl', function ($scope, Template) {
    var projectTemplate;
    var entityFactory = new EntityFactory(Template);
    var components = [];

    var functionLoadEntities = function(){
        Template.get(function(promise){
            promise.templates.forEach(function(t){
                if(t.type === 'project'){
                    projectTemplate = t;
                }
            });

            if(projectTemplate){
                components = entityFactory.parseTemplate(projectTemplate);
            }
            React.render(<DfForm components={components} onSave={function(data){console.log(data);}}/>, document.getElementById('component-test'));
        });
    }

    functionLoadEntities();
  });
