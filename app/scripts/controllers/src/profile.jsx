/**
 * @jsx React.DOM
 */
'use strict';

angular.module('crmApp')
    .controller('UserProfileCtrl', function ($rootScope, $scope, $window, $http, Profile, CONFIG) {

        $scope.cloud = {
            login : "",
            password : ""
        };

        $scope.profile = null;

        $rootScope.title = 'Профиль пользователя - AMS';

        var onProfileSave = function(profile){
            profile = new Profile(profile);
            profile.$update();
            alertify.success('Сохранено!');
        };

        var onPasswordReset = function(password){
            $http.put(CONFIG.REST_ADDRESS+'/api/users/profile/password', password)
            .success(function(resp){
                alertify.success('Новый пароль успешно сохранен!');
            })
            .error(function(resp) {
                alertify.error(resp.message);
            });
        };

        var promise = Profile.get().$promise;

        $scope.exitCloud = function(){
            $scope.profile.cloudToken = ''; 
            $scope.currentUser = $scope.profile;
            $window.localStorage.profile = JSON.stringify($scope.profile);
            onProfileSave($scope.profile);
        };

        $scope.checkCloudConfig = function() {

            if (!$scope.cloud.login || !$scope.cloud.password) {
                alertify.error('Введите учетные данные для авторизации на Fincase Cloud!');
                return;
            }

            $http({method: 'POST', url: CONFIG.CLOUD_ADDRESS + '/oauth2/token',
                transformRequest: function(obj) {
                    var str = [];
                    for(var p in obj)
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data : {
                    username : $scope.cloud.login,
                    password : $scope.cloud.password,
                    grant_type : 'password',
                    client_id : CONFIG.CLOUD_CLIENT_ID
                },
                headers:{
                    'Accept': 'application/json'
                }})
                .success(function(data, status, headers, config) {
                    $scope.profile.cloudToken = data.access_token;
                    $scope.currentUser = $scope.profile;
                    $window.localStorage.profile = JSON.stringify($scope.profile);
                    onProfileSave($scope.profile);
                })
                .error(function(data, status, headers, config) {
                    $scope.profile.cloudToken = '';
                    alertify.error('Ошибка при проверке!');
                });
        }

        promise.then(function (profile) {

            $scope.profile = profile;

            React.render(<UserProfile profile={$scope.profile}
                onProfileSave={onProfileSave} onPasswordReset={onPasswordReset}/>,
                document.getElementById('user-profile'));
        });
    });
