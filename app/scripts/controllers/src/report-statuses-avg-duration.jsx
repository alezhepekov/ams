'use strict';

angular.module('crmApp')
    .controller('ReportStatusesAvgDurationCtrl', function ($scope, $routeParams, $http, $filter, $q, Project, Report, ReportHelper, RolesHelper, CONFIG)  {

        $scope.series1 = [];
        $scope.projectId = $routeParams.id;
        $scope.tabData = ReportHelper.getTabsConfig($scope.projectId, RolesHelper.isCoordinator($scope.currentUser.roles));
        
        $scope.avgDurationLoaded = false;

        var mySeries = [{
            name: "Среденее время выполнения статуса",
            colorByPoint: true,
            data: []
        }];
        
        $scope.getDate = function(){
            if ($scope.projectId === undefined) {
                return;
            }

            Report.statusesAvgDuration({ projectId : $scope.projectId }, function(data){
                $scope.avgDurationLoaded = true;
                if(data.$resolved) {
                    $scope.statusStats = data.result;
                    if ($scope.statusStats && $scope.statusStats.length > 0) {
                         //$filter('orderBy')(data.result, 'avgDuration');
                        mySeries[0].data = [];
                        $scope.statusStats.forEach(function(st){
                            if (st.name!=='отложена')
                                mySeries[0].data.push({ name: st.name, y: st.percentage, avgDuration: st.avgDuration});
                        });
                        $('#container').highcharts({
                            chart: {
                                plotBackgroundColor: null,
                                plotBorderWidth: null,
                                plotShadow: false,
                                type: 'pie'
                            },
                            title: {
                                text: 'Средние сроки завершенных заявок'
                            },
                            tooltip: {
                                pointFormat: '{series.name}:  <b>{point.avgDuration} часов ({point.percentage:.1f}%)</b>'
                            },
                            plotOptions: {
                                pie: {
                                    allowPointSelect: true,
                                    cursor: 'pointer',
                                    dataLabels: {
                                        enabled: true,
                                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                        style: {
                                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                        }
                                    }
                                }
                            },
                            series: mySeries
                        });
                    }else {
                        //$scope.avgDurationLoaded = true;
                        $('#container').height(0);
                    }
                }
            });
        };

    $scope.getDate();

});