'use strict';

angular.module('crmApp')
    .controller('StatisticsCtrl', function ($scope, $window, $sce, $http, CONFIG) {
        var currentUser = JSON.parse(window.localStorage.profile);

        $scope.statisticsUrl = $sce.trustAsResourceUrl(CONFIG.STATISTICS_ADDRESS);
        $scope.statUserName = currentUser.email;
                
        document.formStats.children[0].value = currentUser.email;
		document.formStats.action = CONFIG.STATISTICS_ADDRESS;
        document.formStats.submit();
    });