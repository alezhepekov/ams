'use strict';

angular.module('crmApp')
    .controller('ReportDurationCtrl', function ($scope, $routeParams, $http, $q, Project, Report, ReportHelper, RolesHelper, CONFIG) {

        $scope.series = [];
        $scope.projectId = $routeParams.id;
        $scope.tabData = ReportHelper.getTabsConfig($scope.projectId, RolesHelper.isCoordinator($scope.currentUser.roles));

        $scope.durationLoaded = false;
        $scope.errorText = '';
        $scope.getTotalDurationDescr = function(totalDuration) {
            return totalDuration + ' ' + ReportHelper.getCorrectDayLocalName(totalDuration);
        };

        $scope.getDate = function() {
            if ($scope.projectId === undefined) {
                return;
            }

            //TODO: Сделать универсальным для разных видов проектов
            Report.duration({ projectId : $scope.projectId }, function(data){
                $scope.durationLoaded = true;
                if(data.$resolved){

                    if (data.result) {
                        var series = [];
            
                        data.result.forEach(function(i, projectIdx){
                            var tLen = i.tasksTypes.length;
                            var taskIdx, taskStartDt, lastTaskDt, totalDuration, taskEndDt, appraisalDuration,sroDuration, dgiDuration;
                            if (!tLen) {
                                console.log('reportDuration: no tasks', i);                                        
                                return;
                            }
                            i.tasksTypes.reverse();
                            i.tasksCreatedAt.reverse();
                            i.tasksCompletedAt.reverse();
                            
                            taskIdx = i.tasksTypes.indexOf('оценка');
                            if (taskIdx === -1) {
                                $scope.errorText = 'В заявках нет статуса "оценка"'
                                console.log('reportDuration: no task', i);
                                return;
                            }
                            taskStartDt = new Date(Date.parse(i.tasksCreatedAt[taskIdx]));
                            if (!taskStartDt) {
                                console.log('reportDuration: invalid taskStartDt date', i);
                                return;
                            }
                            lastTaskDt = new Date(Date.parse(i.tasksCompletedAt[tLen-1]));
                            if (!lastTaskDt) {
                                console.log('reportDuration: invalid lastTask date', i);
                                return;
                            }
                            totalDuration = 1 + lastTaskDt.daysSince(taskStartDt);
                            if (totalDuration<=0) {
                                console.log('reportDuration: invalid totalDuration', totalDuration, lastTaskDt, taskStartDt, i);
                                return;
                            }
                            taskIdx = i.tasksTypes.lastIndexOf('оценка');
                            taskEndDt = new Date(Date.parse(i.tasksCompletedAt[taskIdx]));
                            if (!taskEndDt) {
                                console.log('reportDuration: invalid taskEndDt date', i);
                                return;
                            }       
                            appraisalDuration = 1 + taskEndDt.daysSince(taskStartDt);
                        // calc sro
                            taskIdx = i.tasksTypes.indexOf('согласование в СРО');
                            if (taskIdx === -1) {
                                console.log('reportDuration: no sro task', i);
                                sroDuration = 0;
                            }else {
                                taskStartDt = new Date(Date.parse(i.tasksCreatedAt[taskIdx]));
                                if (!taskStartDt) {
                                    console.log('reportDuration: invalid sro taskStartDt date', i);
                                    return;
                                }
                                taskIdx = i.tasksTypes.lastIndexOf('согласование в СРО');
                                taskEndDt = new Date(Date.parse(i.tasksCompletedAt[taskIdx]));
                                if (!taskEndDt) {
                                    console.log('reportDuration: invalid taskEndDt date', i);
                                    return;
                                }                   
                                sroDuration =  1 + taskEndDt.daysSince(taskStartDt);
                            }
                        // calc dgi
                            taskIdx = i.tasksTypes.indexOf('согласование в ДГИМ');
                            if (taskIdx === -1) {
                                console.log('reportDuration: no dgi task', i);
                                dgiDuration = 0;
                            }else {
                                taskStartDt = new Date(Date.parse(i.tasksCreatedAt[taskIdx]));
                                if (!taskStartDt) {
                                    console.log('reportDuration: invalid dgi taskStartDt date', i);
                                    return;
                                }
                                taskIdx = i.tasksTypes.lastIndexOf('согласование в ДГИМ');
                                taskEndDt = new Date(Date.parse(i.tasksCompletedAt[taskIdx]));
                                if (!taskEndDt) {
                                    console.log('reportDuration: invalid taskEndDt dgi date', i);
                                    return;
                                }                   
                                dgiDuration =  1 + taskEndDt.daysSince(taskStartDt);
                            }
                            $scope.series.push({
                                _id: i._id,
                                projectNo: i.projectNo,
                                completedAt: lastTaskDt.format('{dd}.{MM}.{yy}'),
                                totalDuration: totalDuration,
                                appraisalDuration: appraisalDuration,
                                sroDuration: sroDuration,
                                dgiDuration: dgiDuration
                            });
                        });

                        if ($scope.series.length > 0)
                            $scope.errorText = '';
                    }
                    
                }
            });
        };

    $scope.getDate();

});