/**
 * @jsx React.DOM
 */
'use strict';

angular.module('crmApp')
    .controller('SMSCheckCtrl', function ($scope, $http, CONFIG, $location, $timeout, $routeParams, $window, $rootScope) {
        var onSubmit = function(code){
                var data = {
                    code: code,
                    id: $routeParams.id
                };
                $http.post(CONFIG.REST_ADDRESS + '/api/users/smscheck', data)
                .success(function(resp, status){
                    $window.localStorage.token = resp.token;
                    $window.localStorage.profile = JSON.stringify(resp.profile);
                    $rootScope.currentUser = resp.profile;
                    $timeout(function(){
                        $location.path('/');
                        $scope.$apply();
                    });
                })
                .error(function(resp, status){
                    if(status===401){
                        alertify.error("Неверный код!");
                    }
                });
            }
        React.render(<SMSCheck onSubmit={onSubmit} />, document.getElementById('smscheck-panel'));
        });
