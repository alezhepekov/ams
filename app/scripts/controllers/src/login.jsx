/**
 * @jsx React.DOM
 */
'use strict';

angular.module('crmApp')
  .controller('LogInCtrl', function ($scope, $rootScope, $http, CONFIG, $window, $location, $timeout) {
        $scope.loginPage = true;
        $scope.account = {};

        $scope.signIn = function(){
            $http.post(CONFIG.REST_ADDRESS + '/api/users/login', $scope.account)
                .success(function(resp, status){
                    /*if(status == 202){
                        $timeout(function(){
                            $location.path('/smscheck/'+resp.id);
                            $scope.$apply();
                        });
                        return;
                    }*/
                    $window.localStorage.token = resp.token;
                    $window.localStorage.profile = JSON.stringify(resp.profile);
                    $rootScope.currentUser = resp.profile;
                    $timeout(function(){
                        $location.path('/');
                        $scope.$apply();
                    });
                })
                .error(function(resp, status){
                    if(status===401){
                        alertify.error("Неверный логин или пароль!");
                    }
                });
        };
  });
