/**
 * @jsx React.DOM
 */
'use strict';

angular.module('crmApp')
  .controller('FormsCtrl', function ($scope, Template) {
        Template.get(function(promise){
            var links = [];

            promise.templates.forEach(function(t){
                if(t.type === 'form'){
                    links.push({id: t._id, text: t.name});
                }
            });

            React.render(<LinksList items={links} title='Список форм'></LinksList>, document.getElementById('forms'));
        });
  });
