'use strict';

angular.module('crmApp')
    .controller('ReportDgimCtrl', function ($scope, $http, $filter, $routeParams, Project, Report, $q, CONFIG, CsvExporter, ReportHelper, RolesHelper) {
       
        var now = ( new Date() ).toISOString();
        var startYear = new Date(now.getFullYear(), 0, 1);
         
         $scope.startDate = $filter('date')(startYear,'dd.MM.yyyy');
         $scope.endDate = $filter('date')(now,'dd.MM.yyyy');

        var currentUser = $scope.currentUser;
        $scope.projectId = $routeParams.id;
        $scope.tabData = ReportHelper.getTabsConfig($scope.projectId, RolesHelper.isCoordinator($scope.currentUser.roles));

        $scope.getReport = function(){

             if ($scope.projectId == undefined)
                return;

            Report.dgim(
                { 
                    projectId : $scope.projectId                    
                },
                {
                    startDate : $scope.startDate,
                    endDate: $scope.endDate
                }
                , function(data){
                    if(data.$resolved){                            
                       
                        if (data.result.length) {

                        alertify.success("Отчет успешно сформирован");

                        try {

                            var now = ( new Date() ).toISOString();
                            var saveTo = 'DgimReport-'+ now.getFullYear() + now.getMonth() + now.getDate() +'.csv';

                            var exporterConfig = [
                                    {path:'XXXXX',label:'№ п/п'}
                                    ,{path:'XXXXX',label:'Тип отчета об оценке'}
                                    ,{path:'data.sroNo',label:'№ заключения СРО'}
                                    ,{path:'data.reportNo',label:'Номер документа'}
                                    ,{path:'data.reportDate.$date',label:'Дата  документа'}
                                    ,{path:'data.docNo',label:'№ заявки Документоконтроля'}
                                    ,{path:'data.cadasterNoLand',label:'Кадастровый/условный номер земельного участка'}
                                    ,{path:'data.cadasterNo',label:'Кадастровый/условный номер улучшений'}
                                    ,{path:'data.title',label:'Адрес'}
                                    ,{path:'data.area',label:'Площадь, кв.м'}
                                    ,{path:'data.landMarketPrice',label:'Стоимость земельного участка без НДС, руб.'}
                                    ,{path:'data.marketPrice',label:'Стоимость объекта недвижимости без НДС, руб.'}
                                    ,{path:'data.reportComment',label:'Примечание'}
                                    ,{path:'data.files.-1.url',label:'Имя файла отчета'}
                                    ,{path:'completedAt.$date',label:'Дата завершения'}
                                    ,{path:'_id.$oid',label:'ID'}
                            ];                                

                            var result = data.result.map(function(p){

                                var completedAt = new Date(p.currentTask.finishingDate);
                                completedAt =  now.getDate() + now.getMonth() + now.getFullYear() + ' ' + now.getHours() + ":" + now.getMinutes();

                                if (!p.customFields || !p.customFields.value){
                                    return {
                                            _id : { $oid : p._id }
                                            ,data : undefined
                                            ,completedAt : {$date : completedAt}
                                        };
                                }

                                var customFieldsData = p.customFields.value.filter(function(x){ return x.id == '635fbd10-9a99-11e4-80e6-17c87fc056ce' });
                                if (customFieldsData && customFieldsData.length > 0){
                                    customFieldsData = customFieldsData[0].value;

                                    var area = ReportHelper.getFieldValue(customFieldsData, '7fa2b8e0-9a96-11e4-80e6-17c87fc056ce');
                                    var cadasterNo = ReportHelper.getFieldValue(customFieldsData, '5b3e3030-9a9e-11e4-80e6-17c87fc056ce');
                                    var sroNo = ReportHelper.getFieldValue(customFieldsData, '72ea3760-9a9e-11e4-80e6-17c87fc056ce');
                                    var docNo = ReportHelper.getFieldValue(customFieldsData, '6becaa60-9a9e-11e4-80e6-17c87fc056ce');
                                    var reportDate = ReportHelper.getFieldValue(customFieldsData, '82058e70-9a9e-11e4-80e6-17c87fc056ce');
                                    var reportNo = ReportHelper.getFieldValue(customFieldsData, '7b6c8fa0-9a9e-11e4-80e6-17c87fc056ce');
                                    var marketPrice = ReportHelper.getFieldValue(customFieldsData, '905d9580-9a9e-11e4-80e6-17c87fc056ce');
                                    var landMarketPrice = ReportHelper.getFieldValue(customFieldsData, '9b945f60-9a9e-11e4-80e6-17c87fc056ce');
                                    var reportComment = ReportHelper.getFieldValue(customFieldsData, 'a3c1e0e0-9a9e-11e4-80e6-17c87fc056ce');
                                    var cadasterNoLand = ReportHelper.getFieldValue(customFieldsData, '62717330-9a9e-11e4-80e6-17c87fc056ce');
                                }

                                return {
                                    _id : { $oid : p._id }
                                    ,data : {
                                        projectNo:"",
                                        title:p.name,
                                        area: area,
                                        files:[],
                                        marketPrice:marketPrice
                                        ,landMarketPrice:landMarketPrice
                                        ,reportNo:reportNo
                                        ,reportDate:reportDate
                                        ,reportComment:reportComment
                                        ,docNo:docNo
                                        ,sroNo:sroNo
                                        ,cadasterNo:cadasterNo
                                        ,cadasterNoLand:cadasterNoLand
                                    }                                        
                                    ,completedAt : {$date : completedAt}
                                };
                            });

                            CsvExporter.save(saveTo, exporterConfig, result);
                            return saveTo;
                        }
                        catch(ex) {
                            return;
                        }
                    }
                    else{
                        alertify.success("В заданном периоде нет выполненных заявок!");
                    }
                  }
                });

        };        

});