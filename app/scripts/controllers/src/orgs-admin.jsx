'use strict';

angular.module('crmApp')
    .controller('OrgsAdminCtrl', function ($rootScope, $scope, Organization, $q, $location, $timeout) {
        var promise = $q.all({
            organizations: Organization.query().$promise
        });

        $rootScope.title = 'Управление организациями - AMS';
        $rootScope.subTitle = 'Управление организациями';

        var onAdd = function(){
            $timeout(function(){
                $location.path('/admin/orgs/new');
                $scope.$apply();
            });
        };

        var onRemove = function(org){
            Organization.remove({id: org._id});
            alertify.success('Удалено');
        };

        var onEdit = function(org){
            $timeout(function(){
                $location.path('/admin/orgs/'+org._id+'/edit');
                $scope.$apply();
            });
        };

        var succ = function(data){
            React.render(<OrgsList organizations={data.organizations.organizations}
                onAdd={onAdd} onRemove={onRemove} onEdit={onEdit}/>,
                document.getElementById('orgs-list'));
        };

        promise.then(succ);
    });
