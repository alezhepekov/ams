'use strict';

angular.module('crmApp')
    .controller('ReportStatusesCtrl', function ($scope, $http, Project, Report, $routeParams, $q, CONFIG, ReportHelper, RolesHelper) {
    var currentUser = $scope.currentUser;
    $scope.projectId = $routeParams.id;
    var allProposalsQuery = { projectId: $scope.projectId };

    $scope.tabData = ReportHelper.getTabsConfig($scope.projectId, RolesHelper.isCoordinator(currentUser.roles));
    $scope.loadProject = function(query){

        if ($scope.projectId == undefined)
            return;

        Report.status(query, function(data){
            if(data.$resolved){
                $('#chartStatus').highcharts({
                    chart: {
                        type: 'bar'
                    },
                    title: {
                        text: 'Статус заявок'
                    },
                    xAxis: {
                        categories: data.statuses.map(function(v){ return v.name; }),
                        type: 'category'
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: null//'Кол-во заявок'
                        },
                        allowDecimals: false,
                        minTickInterval: 10
                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        bar: {
                            colorByPoint: true
                        }
                    },
                    series: [{
                        name: 'Заявок',
                        data: data.statuses
                    }]
                });
            }
        });
    }

    $scope.updateForYear = function(year) {
        if (year) {
            var startDate = new Date(year, 0, 1);
            var endDate = new Date(year, 11, 31);
            var query = { projectId: $scope.projectId, dateCreatedStart: startDate, dateCreatedEnd: endDate };
            $scope.loadProject(query);
        } else {
            $scope.loadProject(allProposalsQuery);
        }
    }

    $scope.loadProject(allProposalsQuery);
});