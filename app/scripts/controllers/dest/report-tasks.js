'use strict';

angular.module('crmApp')
    .controller('ReportTasksExcelCtrl', function ($scope, $http, $filter, Project, Report, $routeParams, $q, CONFIG, CsvExporter, ReportHelper, RolesHelper)  {

    $scope.projectId = $routeParams.id;
    $scope.tabData = ReportHelper.getTabsConfig($scope.projectId, RolesHelper.isCoordinator($scope.currentUser.roles));

    var now = ( new Date() ).toISOString();
    var startYear = new Date(now.getFullYear(), 0, 1);

    $scope.startDate = $filter('date')(startYear,'dd.MM.yyyy');
    $scope.endDate = $filter('date')(now,'dd.MM.yyyy');

    var exporterConfig = [
        {path:'data.projectNo',label:'Номер ДГИ'}
        ,{path:'data.projectNoCRM',label:'Номер CRM'}
        ,{path:'data.address',label:'Адрес'}
        ,{path:'data.buildingType',label:'Тип'}
        ,{path:'data.area',label:'Площадь'}
        ,{path:'data.projectStatus',label:'Статус'}
        ,{path:'data.taskType',label:'Задача'}
        ,{path:'data.taskAssignedTo',label:'Исполнитель'}
        ,{path:'data.taskDue',label:'Срок'}
        ,{path:'data.taskCreatedBy',label:'Постановщик'}
        ,{path:'data.taskCompletedAt',label:'Дата завершения'}
        ,{path:'data.taskDescription',label:'Комментарий'}
        ,{path:'_id.$oid',label:'ID'}
    ];

    $scope.getReport = function(){

    if ($scope.projectId == undefined)
        return;

        Report.tasksExcel(
            { 
                projectId : $scope.projectId                    
            },
            {
                startDate : $scope.startDate,
                endDate: $scope.endDate
            },
            function(data) {
                if(data.$resolved) {
                   
                    if (data.result.length) {

                        try {
                            var now = ( new Date() ).toISOString();
                            var saveTo = 'TasksReport-'+ now.getFullYear() + now.getMonth() + now.getDate() +'.csv';                            
                                                        
                            var result = data.result.map(function(p){
                                
                                var area = "";
                                var buildingType = "";
                                var cadasterNo = "";
                                var sroNo = "";
                                var docNo = "";
                                var reportDate = "";
                                var reportNo = "";
                                var marketPrice = "";
                                var landMarketPrice = "";
                                var reportComment = "";
                                var cadasterNoLand = "";
                                
                                if (p.customFields) {
                                    var customFieldsData = p.customFields.filter(function(x){ return x.id == '635fbd10-9a99-11e4-80e6-17c87fc056ce' });
                                    if (customFieldsData && customFieldsData.length > 0){
                                        customFieldsData = customFieldsData[0].value;

                                        area = ReportHelper.getFieldValue(customFieldsData, '7fa2b8e0-9a96-11e4-80e6-17c87fc056ce');
                                        buildingType = ReportHelper.getFieldValue(customFieldsData, '580b67d0-9a93-11e4-80e6-17c87fc056ce');                                        
                                        cadasterNo = ReportHelper.getFieldValue(customFieldsData, '5b3e3030-9a9e-11e4-80e6-17c87fc056ce');
                                        sroNo = ReportHelper.getFieldValue(customFieldsData, '72ea3760-9a9e-11e4-80e6-17c87fc056ce');
                                        docNo = ReportHelper.getFieldValue(customFieldsData, '6becaa60-9a9e-11e4-80e6-17c87fc056ce');
                                        reportDate = ReportHelper.getFieldValue(customFieldsData, '82058e70-9a9e-11e4-80e6-17c87fc056ce');
                                        reportNo = ReportHelper.getFieldValue(customFieldsData, '7b6c8fa0-9a9e-11e4-80e6-17c87fc056ce');
                                        marketPrice = ReportHelper.getFieldValue(customFieldsData, '905d9580-9a9e-11e4-80e6-17c87fc056ce');
                                        landMarketPrice = ReportHelper.getFieldValue(customFieldsData, '9b945f60-9a9e-11e4-80e6-17c87fc056ce');
                                        reportComment = ReportHelper.getFieldValue(customFieldsData, 'a3c1e0e0-9a9e-11e4-80e6-17c87fc056ce');
                                        cadasterNoLand = ReportHelper.getFieldValue(customFieldsData, '62717330-9a9e-11e4-80e6-17c87fc056ce');
                                    }
                                } else {
                                    console.log('no customfields');
                                }

                                return {
                                    _id : { $oid : p._id },
                                    data: {
                                        projectNo: p.projectNo,
                                        projectNoCRM: p.projectNoCRM,
                                        address: p.address,
                                        buildingType: buildingType,
                                        area: area,
                                        projectStatus: p.projectStatus,
                                        taskType: p.taskType,
                                        taskAssignedTo: p.taskAssignedTo,
                                        taskDue: p.taskDue ? new Date(p.taskDue).toLocaleDateString() : "",
                                        taskCreatedBy: p.taskCreatedBy,
                                        taskCompletedAt: p.taskCompletedAt ? new Date(p.taskCompletedAt).toLocaleDateString() : "",
                                        taskDescription: p.taskDescription,
                                        _id: { $oid : "" }
                                    }
                                };
                            });
                            CsvExporter.save(saveTo, exporterConfig, result);
                            alertify.success("Отчет успешно сформирован");
                            return saveTo;
                        }
                        catch(e){
                            alertify.error("Возникли ошибки при создании отчета");
                            return;
                        }
                    } else{
                        alertify.error("Не найдены заявки в зданнам периоде");
                    }
                }
            }
        );
    };
});