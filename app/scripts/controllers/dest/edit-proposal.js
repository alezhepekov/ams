'use strict';

angular.module( "crmApp" ).directive( "onFinishRender", function( $timeout ) {
    return {
        restrict: "A",
        link: function( scope, element, attr ) {
            if ( scope.$last === true ) {
                $timeout( function() {
                    scope.$emit( attr.onFinishRender );
                });
            }
        }
    }
});

angular.module('crmApp')
  .controller('EditProposalCtrl', function ($rootScope, $scope, $route, $routeParams, $templateCache, $http, $location, $filter, uuid2,
    FileUploader, GetProject, Project, Proposal, Utils, CONFIG) {

        var mainProjectId = $rootScope.mainProjectId;
        $scope.projectTypes = Utils.getProjectTypes();
        var currentUser = JSON.parse(window.localStorage.profile);

        var entityFactory = new EntityFactory(),
            mapManager = new MapManager($http),
            components = [],
            promise = GetProject($scope, mainProjectId),
            currentProject;

        var docsContainer = document.getElementById('docs-list');

        var uploader = $scope.uploader = new FileUploader({
            headers: {
                Authorization: 'Bearer ' + $scope.currentUser.cloudToken
            },
            url: CONFIG.CLOUD_ADDRESS + '/api/files/upload',
            method: 'POST',
            formData: [{ 'OnlyAuth': 'true' }]
        });

        var defaultMapAddress = {
            "point" : {
                "lat" : "55.753559",
                "lon" : "37.609218"
            }
        };

        $scope.orgMode = Utils.isOrgMode($routeParams.id);
        $scope.roles = [];

        $scope.editMode = true;

        $scope.updateDueDate = function() {
            var dateParts = $scope.dueDate.split('.');
            $scope.proposal.dueDate = new Date(dateParts[2], parseInt(dateParts[1])-1, dateParts[0]);
        }

        $scope.uploading = false;

        $scope.loadSuggestion = function(org) {
            $('#org-address-' + org._id).suggestions({
                serviceUrl: 'https://dadata.ru/api/v2',
                token: '46d56b1479f511ec4473728687a2cbc45e967eee',
                type: 'ADDRESS',
                count: 5,
                onSelect: function(suggestion) {
                    org.address = suggestion.value;
                    $scope.updateAddress(org);
                }
            });
        };
                
        $scope.addOrg = function() {
            $scope.proposal.orgs = $scope.proposal.orgs || [];
            var address = CONFIG.PROJECT_CITIES[currentProject.name] + ' ';
            $scope.proposal.orgs.push({ _id: uuid2.newuuid(), name: 'Предприятие ' + ($scope.proposal.orgs.length + 1), address: address });
            alertify.success('Предприятие успешно добавлено');
        }

        $scope.removeOrg = function(org) {
            if ($scope.proposal.orgs.length == 1) {
                alertify.error('Нельзя удалить единственное предприятие');
                return;
            }

            alertify.confirm('Вы действительно хотите удалить данное предприятие?', function(e) {
                if (e) {
                    var indx = -1;
                    var k = 0;
                    $scope.proposal.orgs.forEach(function(objOrg) {
                        if (objOrg._id == org._id)
                            indx = k;
                        k++;
                    });

                    if (indx > -1) {
                        $scope.proposal.orgs.splice(indx, 1);
                        $scope.$apply();
                        alertify.success('Предприятие успешно удалено');
                    }
                }
            });
        }
		
		$scope.addArbitrationManager = function() {
            $scope.proposal.arbitrationManagers = $scope.proposal.arbitrationManagers || [];           
            $scope.proposal.arbitrationManagers.push({
				_id: uuid2.newuuid(),				
				lastName : "",
				name : "Управляющий " + ($scope.proposal.arbitrationManagers.length + 1),
				patronymic : "",
				INN : "",
				SNILS : "",
				regNum : "",
				regDate : "",
				SRO : "",
				appointDate : ""
			});
            alertify.success("Новый управляющий добавлен успешно");
        }
		
		$scope.removeArbitrationManager = function(arbitrationManager) {
            alertify.confirm("Вы действительно хотите удалить данного управляющего?", function(e) {
                if (e) {
                    var indx = -1;
                    var k = 0;
                    $scope.proposal.arbitrationManagers.forEach(function(obj) {
                        if (obj._id == arbitrationManager._id)
                            indx = k;
                        k++;
                    });

                    if (indx > -1) {
                        $scope.proposal.arbitrationManagers.splice(indx, 1);
                        alertify.success("Менеджер успешно удален");
                        $scope.$apply();
                    }
                }
            });
        }

        $scope.toLocalDateString = function( date ) {
            if ( !date ) {
                return null;
            }

            var timestampString = UTCToLocalTime( Date.parse( date ) ).toISOString().replace( /T/, " " ).replace( /Z/, "" ).replace( /\..+/, "" );
            timestampString = timestampString.replace( /(\d+)-(\d+)-(\d+)/, "$3.$2.$1" );
            return timestampString;
        };

        $scope.toLocalDateStringOrDefault = function(dt) {
            if (dt) {
                var normalDate = new Date(dt);
                return moment(normalDate).locale('ru').format('L');
            }

            return '-';
        };
        
        $scope.fileIcon = function(fileType) {
            return Utils.getFileIcon(fileType);
        };

        $scope.getLinkClass = function(link) {
            return (link && link.indexOf('cloud') > 0 && link.indexOf('abn-consult.ru') > 0)
                ? 'cloudLink' : '';
        }

        $scope.getLinkFileIcon = function(link) {
            return (link && link.indexOf('cloud') > 0 && link.indexOf('abn-consult.ru') > 0)
                ? 'font-up fa fa-arrow-down' : 'font-up fa fa-arrow-right';
        }

        $scope.getLinkWord = function(link) {
            return (link && link.indexOf('cloud') > 0 && link.indexOf('abn-consult.ru') > 0)
                ? 'Скачать' : 'Перейти';
        }       

        $scope.uploadFile = function(documents, docName){
            Utils.uploadFile(documents, docName, $scope, function() {
                $scope.save2();
            }, function() {});
        };

        $scope.addLink = function(docs, doc) {
            doc.name = doc.name || "Ссылка";
            
            Utils.addLink(docs, doc, currentUser, function() { 
                $scope.save2();
            });
            $scope.newDoc = {};          
        };

        $scope.processDocument = function(doc) {
            var fileProps = {projectId: mainProjectId, id: $routeParams.id, fileUrl: doc.link};
            Utils.proccessDocument($scope, doc, fileProps);
        }
        var dummyFunc = function(list, element) {};

        $scope.save = function(){			
            $scope.proposal.tasks.forEach(function(task, i) {
                if (task.creationDate) {
                    task.creationDate = Utils.getISODate(task.creationDate);
                }
                if (task.dueDate) {
                    task.dueDate = Utils.getISODate(task.dueDate);
                }
            });

            var proposal = new Proposal($scope.proposal);			
			
			proposal.$update({projectId: mainProjectId, id: $routeParams.id}, function() {
				alertify.success("Сохранено!");
				$location.path('/projects/' + $routeParams.id + '/main');
			});
        }

        $scope.save2 = function(){			
            $scope.proposal.tasks.forEach(function(task, i) {
                if (task.creationDate) {
                    task.creationDate = Utils.getISODate(task.creationDate);
                }
                if (task.dueDate) {
                    task.dueDate = Utils.getISODate(task.dueDate);
                }
            });

            var proposal = new Proposal($scope.proposal);
                proposal.$update({projectId: mainProjectId, id: $routeParams.id}, function() {
                });
        }

        $http.get(CONFIG.REST_ADDRESS + '/api/users/brief').
            success(function(data, status, headers, config) {
                $scope.users = $filter('orderBy')(data.users, 'fullName');
                $scope.managerUsers = $scope.users;
                

                $scope.changeStatus = function(){
                    if($scope.proposal.hold || $scope.proposal.canceled) {
                        if ($scope.proposal.hold) { //Статус может быть только один
                            currentProject.statuses.forEach(function(status) {
                                if (status.title === 'отложена') {
                                    $scope.proposal.status = status;
                                }
                            });
                        } else if ($scope.proposal.canceled) {
                            currentProject.statuses.forEach(function(status) {
                                if (status.title === 'отменена') {
                                    $scope.proposal.status = status;
                                }
                            });
                        }
                    } else {
                        if ($scope.proposal.currentTask && $scope.proposal.currentTask.status) {
                            $scope.proposal.status = $scope.proposal.currentTask.status;
                        } else {
                            currentProject.statuses.forEach(function(status){
                                if(status.title === 'заявка'){
                                    $scope.proposal.status = status;
                                }
                            });
                        }
                    }
                }

                $scope.cancel = function(){
                    $location.path('/projects/' + $routeParams.id  + '/main');
                }

                $scope.changeAddress = function(org){
                    if(org.address.length < 1){
                        $scope.hasAddress = 'has-error';
                    } else {
                        $scope.hasAddress = '';
                    }
                }

                $scope.updateAddress = function(org) {
                    return new Promise(function(resolve, reject){
                            mapManager.updatePlace(org.address, 'map-' + org._id, null, function(address) {
                                org.mapAddress = address;
                                resolve(address);
                            });
                    });
                }

                var onCustomFieldsChanged = function(customFields){
                    $scope.proposal.customFields = customFields;
                }

                var onDocLinkAdded = function(docs){
                    $scope.proposal.documents = docs;
                }

                 var upload = function(docName){
                    console.log(docName);


                    var fileSelector = document.createElement('input');
                    fileSelector.setAttribute('type', 'file');

                    var fileProps = {projectId: mainProjectId, id: $routeParams.id};

                    fileSelector.onchange = function(e){
                        var fileList = fileSelector.files;
                        if (fileList.length > 0){

                            var file = fileList[0];

                            uploader.clearQueue();
                            uploader.addToQueue(file);
                            $scope.uploading = true;

                            uploader.onCompleteItem = function(item, response, status, headers){

                                console.log(response);

                                if (status == 200 && response.success){
                                    alertify.success('Файл успешно загружен!');

                                    $scope.proposal.documents.push({
                                        text: docName,
                                        link: response.file.Link,
                                        id: uuid.v1(),
                                        user: $scope.currentUser.fullName,
                                        date: ( new Date() ).toISOString(),
                                    });

                                    React.render(React.createElement(DocsList, {currentUser: $scope.currentUser, title: "Файлы и документы", items: $scope.proposal.documents, onChange: onDocLinkAdded, upload: upload}), docsContainer);

                                }
                                fileSelector.remove();

                                console.info('onCompleteItem', item, response, status, headers);
                                fileProps.result = 'done';
                                fileProps.fileUrl = response.file.Link;
                                fileProps.fileName = response.file.FileName;
                                fileProps.fileSize = response.file.ContentLength;
                                Proposal.uploadFileCloud(fileProps);
                                $scope.uploading = false;
                            };

                            uploader.onProgressItem = function(item, progress) {
                                console.info('onProgressItem', item, progress);
                            };
                            uploader.onProgressAll = function(progress) {
                                console.info('onProgressAll', progress);
                            };

                            uploader.onCompleteAll = function() {
                                console.info('Все файлы загружены');
                                $scope.uploading = false;
                            };

                            uploader.onErrorItem = function(item, response, status, headers){
                                if(status == 401){
                                    alertify.error('Не выполнен вход в Cloud');
                                    fileProps.result = 'noAuth';
                                    fileProps.fileUrl = (response.file.Link) ? response.file.Link : '';
                                    fileProps.fileName = (response.file.FileName) ? response.file.FileName : '';
                                    fileProps.fileSize = (response.file.ContentLength) ? response.file.ContentLength : '';
                                    Proposal.uploadFileCloud(fileProps);
                                }
                                else{
                                    alertify.error('Не удалось загрузить файл!');
                                    fileProps.result = 'error';
                                    fileProps.fileUrl = (response.file.Link) ? response.file.Link : '';
                                    fileProps.fileName = (response.file.FileName) ? response.file.FileName : '';
                                    fileProps.fileSize = (response.file.ContentLength) ? response.file.ContentLength : '';
                                    Proposal.uploadFileCloud(fileProps);
                                }
                                $scope.uploading = false;
                            };

                            uploader.uploadAll();
                        }
                    }

                    fileSelector.click();
                }

                var functionLoadEntities = function(){
                    promise.then(function(data){
                        currentProject = data.project;
                        $scope.roles = data.project.roles;
                        Proposal.get({projectId: mainProjectId, id: $routeParams.id}, function(result){
                            $scope.proposal = result;
                            $rootScope.title = '№' + $scope.proposal.number + ' - ' + $scope.proposal.name + ' - AMS';
                            $scope.dueDate = $filter('date')($scope.proposal.dueDate, 'dd.MM.yyyy');
                            if ($scope.proposal.tasks && $scope.proposal.tasks.length > 0) {
                                $scope.proposal.tasks.forEach(function(task, i) {
                                    task.creationDate = $filter('date')(task.creationDate, 'dd.MM.yyyy');
                                    task.dueDate = $filter('date')(task.dueDate, 'dd.MM.yyyy');
                                });
                            }

                            $scope.proposal.orgs.forEach(function(org) {
                                setTimeout(function(){
                                    if (org.mapAddress && org.mapAddress.point){
                                        mapManager.viewPlace(org.mapAddress, 'map-' + org._id, org.address);
                                    } else {
                                        mapManager.viewPlace(defaultMapAddress, 'map-' + org._id, 'Москва');
                                    }
                                }, 500);
                            });

                            components = entityFactory.parseTemplate(currentProject.proposalTemplate, currentProject);
                        })


                    });
                }

                functionLoadEntities();
            }).
            error(function(data, status, headers, config) {
                alertify.error('Не удалось загрузить список пользователей!');
            });

  });
