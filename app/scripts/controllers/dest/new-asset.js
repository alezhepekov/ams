'use strict';

angular.module('crmApp')
    .directive('onFinishRender', function ($timeout) {
        return {
            restrict: 'A',
            link: function (scope, element, attr) {
                if (scope.$last === true) {
                    $timeout(function () {
                        scope.$emit('ngRepeatFinished');
                    });
                }
            }
        }
    });

angular.module('crmApp')
    .controller('NewAssetCtrl', function ($rootScope, $scope, $http, $routeParams, $window, $location, $filter, GetProject, Project, Proposal, uuid2, Utils, CONFIG) {

        var mainProjectId = $rootScope.mainProjectId;
        var currentUser = JSON.parse(window.localStorage.profile);

        $rootScope.title = 'Новый проект - AMS';
        $scope.orgMode = Utils.isOrgMode(mainProjectId);
        $scope.projectTypes = Utils.getProjectTypes();
        $scope.selected = { role: {} };
        $scope.roles = [];

        $scope.documentsListTemplate = { name: 'documentsList.html', url: '/views/templates/documentsList.html' };
        $scope.commentsListTemplate = { name: 'commentsList.html', url: '/views/templates/commentsList.html' };

        var entityFactory = new EntityFactory(),
            mapManager = new MapManager($http),
            components = [],
            promise = GetProject($scope, mainProjectId),
            currentProject;

        var filter = {};

        $scope.newDoc = {};
        $scope.sortType  = 'docName';

        $scope.loadSuggestion = function(org) {
            $('#org-address-' + org._id).suggestions({
                serviceUrl: 'https://dadata.ru/api/v2',
                token: '46d56b1479f511ec4473728687a2cbc45e967eee',
                type: 'ADDRESS',
                count: 5,
                onSelect: function(suggestion) {
                    org.address = suggestion.value;
                    $scope.updateAddress(org);
                }
            });
        };

        $scope.addOrg = function() {
            $scope.proposal.orgs = $scope.proposal.orgs || [];
            var address = CONFIG.PROJECT_CITIES[currentProject.name] + ' ';
            $scope.proposal.orgs.push({ _id: uuid2.newuuid(), name: 'Предприятие ' + ($scope.proposal.orgs.length + 1), address: address });
            alertify.success('Предприятие успешно добавлено');
        }
        
        $scope.removeOrg = function(org) {
            if ($scope.proposal.orgs.length == 1) {
                alertify.error('Нельзя удалить единственное предприятие');
                return;
            }

            alertify.confirm('Вы действительно хотите удалить данное предприятие?', function(e) {
                if (e) {
                    var indx = -1;
                    var k = 0;
                    $scope.proposal.orgs.forEach(function(objOrg) {
                        if (objOrg._id == org._id)
                            indx = k;
                        k++;
                    });

                    if (indx > -1) {
                        $scope.proposal.orgs.splice(indx, 1);
                        alertify.success('Предприятие успешно удалено');
                        $scope.$apply();
                    }
                }
            });
        }
		
		$scope.addArbitrationManager = function() {
            $scope.proposal.arbitrationManagers = $scope.proposal.arbitrationManagers || [];           
            $scope.proposal.arbitrationManagers.push({
				_id: uuid2.newuuid(),				
				lastName : "",
				name : "Управляющий " + ($scope.proposal.arbitrationManagers.length + 1),
				patronymic : "",
				INN : "",
				SNILS : "",
				regNum : "",
				regDate : "",
				SRO : "",
				appointDate : ""
			});
            alertify.success("Новый управляющий добавлен успешно");
        }
		
		$scope.removeArbitrationManager = function(arbitrationManager) {
            alertify.confirm("Вы действительно хотите удалить данного управляющего?", function(e) {
                if (e) {
                    var indx = -1;
                    var k = 0;
                    $scope.proposal.arbitrationManagers.forEach(function(obj) {
                        if (obj._id == arbitrationManager._id)
                            indx = k;
                        k++;
                    });

                    if (indx > -1) {
                        $scope.proposal.arbitrationManagers.splice(indx, 1);
                        alertify.success("Менеджер успешно удален");
                        $scope.$apply();
                    }
                }
            });
        }

        $scope.loadDocList = function() {
            React.render(React.createElement(DocsList, 
                {currentUser: $scope.currentUser,
                    title: "Файлы и документы",
                    hover: true,
                    onChange: onDocLinkAdded,
                    onSave: saveDocuments,
                    onOpenEditor: dummyFunc, items: $scope.proposal.documents}),
                document.getElementById('docs-list'));
        };

        $scope.$on('ngRepeatFinished', function (ngRepeatFinishedEvent) {          
        });

        $scope.uploadFile = function(documents, docName) {
            Utils.uploadFile(
                documents, docName, $scope,
                function() {
                    $scope.$apply();
                },
                function() {});
        };
        
        $scope.addLink = function(docs, doc) {
            doc.name = doc.name || "Ссылка";
            Utils.addLink(docs, doc, currentUser, function() {});
            $scope.newDoc = {};
        };

        $scope.processDocument = function(doc) {
            var fileProps = {projectId: mainProjectId, id: 'new-asset', fileUrl: doc.link};
            Utils.proccessDocument($scope, doc, fileProps);
        };

        $scope.getLinkClass = function(link) {
            return (link && link.indexOf('cloud') > 0 && link.indexOf('abn-consult.ru') > 0)
                ? 'cloudLink' : '';
        }

        $scope.fileIcon = function(fileType) {
            return Utils.getFileIcon(fileType);
        };

        $scope.getLinkFileIcon = function(link) {
            return (link && link.indexOf('cloud') > 0 && link.indexOf('abn-consult.ru') > 0)
                ? 'font-up fa fa-arrow-down' : 'font-up fa fa-arrow-right';
        }

        $scope.getLinkWord = function(link) {
            return (link && link.indexOf('cloud') > 0 && link.indexOf('abn-consult.ru') > 0)
                ? 'Скачать' : 'Перейти';
        }

        $scope.toLocalDateString = function( date ) {
            if ( !date ) {
                return null;
            }

            var timestampString = UTCToLocalTime( Date.parse( date ) ).toISOString().replace( /T/, " " ).replace( /Z/, "" ).replace( /\..+/, "" );
            timestampString = timestampString.replace( /(\d+)-(\d+)-(\d+)/, "$3.$2.$1" );
            return timestampString;
        };
        
        $scope.toLocalDateStringOrDefault = function(dt) {
            if (dt) {
                var normalDate = new Date(dt);
                return moment(normalDate).locale('ru').format('L');
            }

            return '-';
        };

        $scope.updateExecutors = function(task, roleItem) {
            if (roleItem==undefined) {
                task.filteredUsers = $scope.users;
                return;
            }
            
            task.filteredUsers = $scope.users.filter(function(fuser) {
                if (fuser.roles && fuser.roles.length > 0) {
                    var foundedRole = fuser.roles.filter(function(frole) {
                        return frole.roleId === roleItem.id;
                    })[0];
                    if (foundedRole)
                        return true;
                }
                return false;
            });
        };

        var saveDocuments = function(doc){
            $scope.proposal.documents = $scope.proposal.documents || [];
            $scope.proposal.documents.push(doc);
        }

        var onDocLinkAdded = function(docs){
            $scope.proposal.documents = docs;
        }

        $scope.deleteDoc = function(docs, doc) {
            alertify.confirm('Вы действительно хотите удалить этот файл?', function(e) {
                if (e) {
                    var index = 0;
                    var k = -1;
                    docs.forEach(function(d) {
                        if (d.id == doc.id) {
                            k = index;
                            return;
                        }
                        index++;
                    });

                    if (k>-1) {
                        docs.splice(k, 1);
                        $scope.$apply();
                    }
                }
            });
        };

        var loadFilter = function() {
            if (Utils.isEmpty(filter) && $window.sessionStorage.newProposalFilter)
                filter = JSON.parse($window.sessionStorage.newProposalFilter);
            if (!Utils.isEmpty(filter.manager))
                $scope.proposal.manager = filter.manager;
            if (!Utils.isEmpty(filter.coordinator))
                $scope.proposal.coordinator = filter.coordinator;
        }       

        $scope.saveFilter = function() {
            filter.manager = ($scope.proposal.manager) ? $scope.proposal.manager : {};
            filter.coordinator = ($scope.proposal.coordinator) ? $scope.proposal.coordinator : {};
            $window.sessionStorage.newProposalFilter = JSON.stringify(filter);
        }

        $scope.updateDueDate = function() {
            var dateParts = $scope.dueDate.split('.');
            $scope.proposal.dueDate = new Date(dateParts[2], parseInt(dateParts[1])-1, dateParts[0]);
        }

        $scope.updateAddress = function(org) {
            return new Promise(function(resolve, reject){
                    mapManager.updatePlace(org.address, 'map-' + org._id, null, function(address) {
                        org.mapAddress = address;
                        resolve(address);
                    });
            });
        }
        var dummyFunc = function(list, element) {};

        $http.get(CONFIG.REST_ADDRESS + '/api/users/brief').
            success(function(data, status, headers, config) {
                $scope.users = $filter('orderBy')(data.users, 'fullName');
                $scope.managerUsers = $scope.users;
                $scope.proposal = new ProposalEntity(mainProjectId);
                $scope.proposal.orgs = $scope.proposal.orgs || [ { _id: uuid2.newuuid(), name: 'Предприятие 1' } ];
                $scope.proposal.type = $scope.projectTypes[0];

                var dueDate = ( new Date() ).toISOString();
                dueDate.setDate(dueDate.getDate() + CONFIG.DUEDATE_PERIOD);
                $scope.dueDate = $filter('date')(dueDate,'dd.MM.yyyy');
                $scope.updateDueDate();

                loadFilter();

                var validateProposal = function(){
                    if($scope.proposal.number.length < 1){
                        alertify.error('Номер проекта должен состоять не менее чем из 1 символа');
                        validate();
                        return false;
                    }

                    if($scope.proposal.name.length < 1){
                        alertify.error('Название проекта должно состоять не менее чем из 1 символа');
                        validate();
                        return false;
                    }

                    if($scope.proposal.orgs && $scope.proposal.orgs.length == 0){
                        alertify.error('В проекте должно быть хотя бы одно предприятие');
                        validate();
                        return false;
                    }

                    if (!($scope.proposal.orgs[0] && $scope.proposal.orgs[0].address && $scope.proposal.orgs[0].address.length > 0)) {
                        alertify.error('Заполните адрес предприятия');
                        validate();
                        return false;
                    }

                    if(!$scope.proposal.manager){
                        alertify.error('Выберите руководителя проекта!');
                        return false;
                    }
                    
                    return true;
                }

                $scope.save = function(){
					console.debug("new-asset.jsx >> $scope.save");
                    if(validateProposal()){
                        if($scope.proposal.name === ''){
                            $scope.proposal.name = 'NewCompany_' + moment($scope.proposal.started).locale('ru').format('DD_MM_YYYY_hh_mm_ss');
                        }

                        if($scope.currentUser){
                            $scope.proposal.owner = $scope.currentUser;
                            $scope.proposal.tasks.forEach(function(task, i) {
                                if (task.creationDate) {
                                    task.creationDate = Utils.getISODate(task.creationDate);
                                }
                                if (task.dueDate) {
                                    task.dueDate = Utils.getISODate(task.dueDate);
                                }
                                delete task["filteredUsers"];
                            });
                                Proposal.save({projectId: mainProjectId}, $scope.proposal, function(item) {
                                    alertify.success("Сохранено!");
                                    $scope.proposal = item;
                                    $location.path('/projects/' + mainProjectId + '/proposals/' + $scope.proposal._id);
                                });
                        } else {
                            alertify.error('Не удалось получить информацию о вашем профиле. Попробуйте обновить страницу (F5) и повторить операцию.');
                        }
                    }
                }

                console.log($scope.proposal);

                $scope.cancel = function(){
                    $location.path('/projects/' + mainProjectId);
                }

                $scope.changeNumber = function(){
                    if($scope.proposal.number.length < 1){
                        $scope.hasNumber = 'has-error';
                    } else {
                        $scope.hasNumber = '';
                    }
                }

                $scope.changeAddress = function(org){
                    if(org.address.length < 1){
                        $scope.hasAddress = 'has-error';
                    } else {
                        $scope.hasAddress = '';
                    }
                }

                var validate = function(org){
                    $scope.changeAddress(org);
                    $scope.changeNumber();
                }

                var onCustomFieldsChanged = function(customFields){
                    $scope.proposal.customFields = customFields;
                }

                var functionLoadEntities = function(){
                    promise.then(function(data){
                        currentProject = data.project;
                        $scope.roles = data.project.roles;
                        $scope.proposal.orgs = $scope.proposal.orgs || [ { _id: uuid2.newuuid(), name: 'Предприятие 1' } ];
                        var address = CONFIG.PROJECT_CITIES[currentProject.name] + ' ';
                        if(address){
                            $scope.proposal.orgs[0].address = address;
                            validate($scope.proposal.orgs[0]);
                        }
                        
                        if (currentProject.statuses && currentProject.statuses.length>0) {
                            $scope.proposal.tasks = new Array(currentProject.statuses.length);
                            var now = ( new Date() ).toISOString();
                            var creationDate = new Date(now.getFullYear(), now.getMonth(), now.getDate());
                            currentProject.statuses.forEach(function(status, i) {
                                $scope.proposal.tasks[i] = {
                                    id: status.id,
                                    status: status,
                                    parentId: undefined,
                                    finished: false,
                                    finishingDate: undefined,
                                    owner: undefined,
                                    executor: undefined,
                                    documents: [],
                                    creationDate: $filter('date')(creationDate,'dd.MM.yyyy')
                                };
                            });

                            $scope.proposal.documents = [];
                            currentProject.statuses.forEach(function(status, i) {
                                if (status.next && status.next.length>0) {
                                    status.next.forEach(function(nextId, j) {
                                        var propTasks = $scope.proposal.tasks.filter(function(task){
                                            return task.id===nextId;
                                        });

                                        propTasks.forEach(function(task) {
                                            task.parentId = status.id;
                                        });
                                    });
                                }
                            });
                        }

                        components = entityFactory.parseTemplate(currentProject.proposalTemplate, currentProject);
                        $scope.proposal.templateId = currentProject.proposalTemplate._id;

                        console.log($scope.proposal);
                                                
                    });
                }

                functionLoadEntities();
            }).
            error(function(data, status, headers, config) {
                alertify.error('Не удалось загрузить список пользователей!');
            });
    });