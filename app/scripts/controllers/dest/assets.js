'use strict';

angular.module('crmApp')
    .controller('AssetTangibleCtrl', function ($rootScope, $scope, $modalInstance, asset, uuid2, Utils, CONFIG) {
        $scope.documentsListTemplate = { name: 'documentsList.html', url: '/views/templates/documentsList.html' };
        $scope.currencies = CONFIG.CURRENCIES;
        $scope.newDoc = {};

        if (asset) {
            $scope.newMode = false;
            $scope.editMode = true;
            $scope.asset = asset;
        } else {
            $scope.newMode = true;
            $scope.editMode = false;
            $scope.asset = {
                 id: uuid2.newuuid()
            };
        }

        $scope.statuses = [
            {id: 'inPledge', name: 'В залоге'},
            {id: 'notInPledge', name: 'Не в залоге'}
        ];
        
        $scope.suggestLoaded = false;
        $scope.loadSuggestion = function() {
        	if ($scope.suggestLoaded)
        		return;

            $('#asset-address').suggestions({
                serviceUrl: 'https://dadata.ru/api/v2',
                token: '46d56b1479f511ec4473728687a2cbc45e967eee',
                type: 'ADDRESS',
                count: 5,
                onSelect: function(suggestion) {
                    $scope.asset.address = suggestion.value;
                }
            });

            $scope.suggestLoaded = true;
        };    

        $scope.hasError = function(field, validation) {
            if(validation){
                return $scope.form[field].$dirty && $scope.form[field].$error[validation];
            }
            return $scope.form[field].$dirty && $scope.form[field].$invalid;
        };

        $scope.getLinkClass = function(link) {
            return (link && link.indexOf('cloud') > 0 && link.indexOf('abn-consult.ru') > 0)
                ? 'cloudLink' : '';
        }

        $scope.getLinkFileIcon = function(link) {
            return (link && link.indexOf('cloud') > 0 && link.indexOf('abn-consult.ru') > 0)
                ? 'font-up fa fa-arrow-down' : 'font-up fa fa-arrow-right';
        }

        $scope.getLinkWord = function(link) {
            return (link && link.indexOf('cloud') > 0 && link.indexOf('abn-consult.ru') > 0)
                ? 'Скачать' : 'Перейти';
        }

        $scope.fileIcon = function(fileType) {
            return Utils.getFileIcon(fileType);
        };

        $scope.toLocalDateString = function( date ) {
            if ( !date ) {
                return null;
            }

            var timestampString = UTCToLocalTime( Date.parse( date ) ).toISOString().replace( /T/, " " ).replace( /Z/, "" ).replace( /\..+/, "" );
            timestampString = timestampString.replace( /(\d+)-(\d+)-(\d+)/, "$3.$2.$1" );      
            var splits = timestampString.split(/\s/);   
            return splits[0];
        }; 

        $scope.save = function() {
            if ( $scope.form.$invalid )
                return;

            if ( $scope.editMode ) {
                $scope.asset.modificationDates = $scope.asset.modificationDates || [];
                $scope.asset.modificationDates.push( ( new Date() ).toISOString() );
            } else {           
                $scope.asset.creationDate = ( new Date() ).toISOString();
            }           
            
	        $modalInstance.close( $scope.asset );
	    };

	    $scope.close = function () {
	        $modalInstance.dismiss();
	    };

	    $scope.cancel = function () {
	        $modalInstance.dismiss('cancel');
	    };

        $scope.uploadFile = function() {
            Utils.uploadFile(
                $scope.asset.documents,
                null,
                $scope,
                function() {
                    if ( $scope.$root.$$phase != "$apply" && $scope.$root.$$phase != "$digest" ) {
                        $scope.$apply();
                    }               
                },
                function(e) {               
                    console.error(e);
                }
            );
        };

        $scope.addLink = function(docs, doc) {
            doc.name = doc.name || "Ссылка";

            Utils.addLink(
                $scope.asset.documents,
                doc,
                $scope.currentUser,
                function() {
                    if ( $scope.$root.$$phase != "$apply" && $scope.$root.$$phase != "$digest" ) {
                        $scope.$apply();
                    }
                }
            );
            $scope.newDoc = {};
        };

        $scope.processDocument = function(doc) {        
            var fileProps = {projectId: mainProjectId, id: $routeParams.id, fileUrl: doc.link};
            Utils.proccessDocument($scope, doc, fileProps);
        };

        $scope.deleteDoc = function(docs, doc) {
            alertify.confirm("Вы действительно хотите удалить этот файл?", function(e) {
                if ( e ) {
                    var key = doc.id;
                    var objectList = docs;
                    for ( var i = 0; i < objectList.length; i++ ) {
                        var currentItem = objectList[i];
                        if ( currentItem.id === key ) {                         
                            delete objectList.splice( i, 1 );
                            break;
                        }
                    }

                    if ( $scope.$root.$$phase != "$apply" && $scope.$root.$$phase != "$digest" ) {
                        $scope.$apply();
                    }
                }
            });
        };
});