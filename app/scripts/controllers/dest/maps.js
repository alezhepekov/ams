'use strict';

angular.module('crmApp')
    .controller('MapsCtrl', function ($scope, $filter, $routeParams, $q, Project, Map, $location) {

    var currentUser = $scope.currentUser;
    var map = null;
    var points = null;
    $scope.projectId = $routeParams.id;
    $scope.project = {};

    var now = ( new Date() ).toISOString();
    var startYear = new Date(now.getFullYear(), 0, 1);

    $scope.selected = {
        startDate : $filter('date')(startYear,'dd.MM.yyyy'),
        endDate : $filter('date')(now,'dd.MM.yyyy')
    };

    $scope.isLoading = true;

    function init() {
        map = new ymaps.Map('projectMap', {
            center: [55.755768, 37.617671],
            zoom: 10,
            controls: ['zoomControl', 'typeSelector']
        });
        map.events.add('boundschange', recalcPrice);

        $scope.loadProject();
    }

    function median(values) {
        if (!Array.isArray(values)) {
            return null;
        }
        var arrlen = values.length;
        if (!arrlen) {
            return null;
        }
        
        var _arr = values.slice().sort( function(a,b) {return a - b;} );

        var median = !(arrlen & 1)
            ? (_arr[(arrlen / 2) - 1 ] + _arr[(arrlen / 2)]) / 2
            : _arr[(arrlen / 2) | 0 ];
        
        var devSum = 0;
        for (var i = arrlen - 1; i >= 0; i--)
            devSum += Math.abs(_arr[i] - median);
        var meddev = devSum / arrlen;
        
        return {
            median: median,
            meddev: meddev,
            medLower: median - (2*1.4826*meddev),
            medUpper: median + (2*1.4826*meddev),
            min: arrlen ? _arr[0] : 0,
            max: arrlen ? _arr[arrlen-1] : 0
        };
    };

    function recalcPrice(){

        var avgPPSqm = [], p, s
            , visiblePoints = points.searchInside(map)
            , i = visiblePoints.getIterator();
        
        $scope.visibleCount = visiblePoints.getLength();
        
        while ((p = i.getNext()) !== i.STOP_ITERATION) {
            s = parseInt(p.properties.get('dgiMarketPricePerSqm'), 10);
            if (!isNaN(s) && s>10000 && s<1000*1000) {
                avgPPSqm.push(s);
            }
        }

        var medianData = median(avgPPSqm); //avgPPSqm.average()

        $scope.avgPriceCount = avgPPSqm.length;

                if (medianData) {
                    $scope.avgPrice = medianData.median;//.round(-3);
                    $scope.intervalLower = medianData.min;//.round(-3);
                    $scope.intervalUpper = medianData.max;//.round(-3);
                } else {
                    $scope.avgPrice = 0;
                    $scope.intervalLower = 0;
                    $scope.intervalUpper = 0;
                }

        if(!$scope.$$phase) {
            $scope.$apply(function() {
                
            });
        }
    }

    $scope.updateMapFor = function(year) {
        $scope.selected.startDate = new Date(year, 0, 1).format('{dd}.{MM}.{yyyy}');
        $scope.selected.endDate = new Date(year, 11, 31).format('{dd}.{MM}.{yyyy}');
        $scope.loadProject();
    }

    $scope.loadProject = function() {
        $scope.isLoading = true;

        if ($scope.projectId == undefined)
            return;

        Project.get({id: $scope.projectId}, function(data){
            if(data.$resolved){
                $scope.project = data;                

                var queryParams = { projectId : $scope.projectId };
                if ($scope.selected.statuses!=undefined && $scope.selected.statuses.length>0) {
                    queryParams.statuses = $scope.selected.statuses;
                }
                
                if ($scope.selected.objectTypes!=undefined && $scope.selected.objectTypes.length>0) {
                    queryParams.objectTypes = $scope.selected.objectTypes;
                }

                if ($scope.selected.areaSizeTypes !=undefined && $scope.selected.areaSizeTypes.length>0) {
                    queryParams.areaSizeTypes = $scope.selected.areaSizeTypes;
                }

                if ($scope.selected.startDate !=undefined) {
                    queryParams.startDate = $scope.selected.startDate;
                }

                if ($scope.selected.endDate !=undefined) {
                    queryParams.endDate = $scope.selected.endDate;
                }

                Map.get(queryParams, function(data) {
                    if(data.$resolved) {

                        $scope.isLoading = false;                        

                        var result = ymaps.geoQuery({
                            type: 'FeatureCollection',
                            features: data.points
                        });
                    
                        map.geoObjects.removeAll();
                        points = result.search('geometry.type == "Point"');
                        map.geoObjects.add(points.clusterize({ clusterDisableClickZoom: true }));

                        $scope.totalCount = data.statistic.totalCount;

                        recalcPrice();
                    }
                });
            }
        });        
    }        

    setTimeout(function() {
        if (ymaps) {
            ymaps.ready(init);
        }
    }, 500);

});