'use strict';

angular.module('crmApp')
    .controller('RolesCtrl', function ($rootScope, $scope, Project) {

    $rootScope.title = 'Управление ролями - AMS';
    $rootScope.subTitle = 'Управление ролями';

    var render = function(){
        var projects;
        Project.get(function(result){
            if(result.$resolved){
                projects = result.projects;
                var saveRoles = function(resultProject){
                    var dbProject = new Project(resultProject);
                    dbProject.$update({id: dbProject._id}, function(){
                        alertify.success('Сохранено!');
                        render();
                    });
                }

                React.render(React.createElement(RolesManager, {onSave: saveRoles, projects: projects}), document.getElementById('roles-manager'));
            }
        });
    }
    render();
  });
