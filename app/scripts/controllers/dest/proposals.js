"use strict";

function UTCToLocalTime( utc ) {
    var timezoneOffset = ( new Date() ).getTimezoneOffset() * 60000;
    return new Date( utc - timezoneOffset );
}

angular.module( "crmApp" ).directive( "onFinishRender", function( $timeout ) {
    return {
        restrict: "A",
        link: function( scope, element, attrs ) {
            if ( scope.$last === true ) {
                $timeout( function() {
                    scope.$emit( attrs.onFinishRender );
                });
            }
        }
    }
});

angular.module( "crmApp" ).filter( "filterTaskActive", function() {
    return function( items ) {
        if ( !items ) {
            return [];
        }

        var result = [];
        for ( var i = 0; i < items.length; i++ ) {
            var currentItem = items[i];
            if ( !currentItem.removed ) {
                result.push( currentItem );
            }
        }
        return result;
    }
});

angular.module( "crmApp" ).filter( "filterCreditorCreation", function() {
    return function( items, dateFrom, dateTo ) {
        if ( !items ) {
            return [];
        }

        var dateFromString = dateFrom;
        var dateToString = dateTo;

        var result = [];
        for ( var i = 0; i < items.length; i++ ) {
            var timestampString = items[i].creationDate;
            timestampString = timestampString.replace( /T/, " " ).replace( /Z/, "" ).replace( /\..+/, "" );

            if ( !dateFromString || !dateToString || dateFromString.length === 0 && dateToString.length === 0 ) {
                result.push( items[i] );
            }
            else if ( dateFromString.length > 0 && dateToString.length > 0 ) {
                if ( timestampString >= dateFromString && timestampString <= dateToString ) {
                    result.push( items[i] );
                }
            }
            else if ( dateFromString.length > 0 ) {
                if ( timestampString >= dateFromString ) {
                    result.push( items[i] );
                }
            }
            else if ( dateFromString.length > 0 ) {
                if ( timestampString <= dateToString ) {
                    result.push( items[i] );
                }
            }
        }

        return result;
    };
});


angular.module( "crmApp" ).filter( "filterDebtorCreation", function() {
    return function( items, dateFrom, dateTo ) {
        if ( !items ) {
            return [];
        }

        var dateFromString = dateFrom;
        var dateToString = dateTo;

        var result = [];
        for ( var i = 0; i < items.length; i++ ) {
            var timestampString = items[i].creationDate;
            timestampString = timestampString.replace( /T/, " " ).replace( /Z/, "" ).replace( /\..+/, "" );

            if ( !dateFromString || !dateToString || dateFromString.length === 0 && dateToString.length === 0 ) {
                result.push( items[i] );
            }
            else if ( dateFromString.length > 0 && dateToString.length > 0 ) {
                if ( timestampString >= dateFromString && timestampString <= dateToString ) {
                    result.push( items[i] );
                }
            }
            else if ( dateFromString.length > 0 ) {
                if ( timestampString >= dateFromString ) {
                    result.push( items[i] );
                }
            }
            else if ( dateFromString.length > 0 ) {
                if ( timestampString <= dateToString ) {
                    result.push( items[i] );
                }
            }
        }

        return result;
    };
});

angular.module( "crmApp" ).filter( "filterAssetIntangible", function() {
    return function( items, dateFrom, dateTo ) {
        if ( !items ) {
            return [];
        }

        var dateFromString = dateFrom;
        var dateToString = dateTo;

        var result = [];
        for ( var i = 0; i < items.length; i++ ) {
            var timestampString = items[i].creationDate;
            timestampString = timestampString.replace( /T/, " " ).replace( /Z/, "" ).replace( /\..+/, "" );

            if ( !dateFromString || !dateToString || dateFromString.length === 0 && dateToString.length === 0 ) {
                result.push( items[i] );
            }
            else if ( dateFromString.length > 0 && dateToString.length > 0 ) {
                if ( timestampString >= dateFromString && timestampString <= dateToString ) {
                    result.push( items[i] );
                }
            }
            else if ( dateFromString.length > 0 ) {
                if ( timestampString >= dateFromString ) {
                    result.push( items[i] );
                }
            }
            else if ( dateFromString.length > 0 ) {
                if ( timestampString <= dateToString ) {
                    result.push( items[i] );
                }
            }
        }

        return result;
    };
});

angular.module( "crmApp" ).filter( "filterDocumentCreation", function() {
    return function( items, dateFrom, dateTo ) {
        if ( !items ) {
           return [];
        }

        var dateFromString = dateFrom;
        var dateToString = dateTo;

        var result = [];
        for ( var i = 0; i < items.length; i++ ) {
            var timestampString = items[i].uploadDate;
            timestampString = timestampString.replace( /T/, " " ).replace( /Z/, "" ).replace( /\..+/, "" );

            if ( !dateFromString || !dateToString || dateFromString.length === 0 && dateToString.length === 0 ) {
                result.push( items[i] );
            }
            else if ( dateFromString.length > 0 && dateToString.length > 0 ) {
                if ( timestampString >= dateFromString && timestampString <= dateToString ) {
                    result.push( items[i] );
                }
            }
            else if ( dateFromString.length > 0 ) {
                if ( timestampString >= dateFromString ) {
                    result.push( items[i] );
                }
            }
            else if ( dateFromString.length > 0 ) {
                if ( timestampString <= dateToString ) {
                    result.push( items[i] );
                }
            }
        }

        return result;
    };
});

angular.module( "crmApp" ).filter( "filterAssetTangible", function() {
    return function( items, dateFrom, dateTo ) {
        if ( !items ) {
           return [];
        }

        var dateFromString = dateFrom;
        var dateToString = dateTo;

        var result = [];
        for ( var i = 0; i < items.length; i++ ) {
            var timestampString = items[i].creationDate;
            timestampString = timestampString.replace( /T/, " " ).replace( /Z/, "" ).replace( /\..+/, "" );

            if ( !dateFromString || !dateToString || dateFromString.length === 0 && dateToString.length === 0 ) {
                result.push( items[i] );
            }
            else if ( dateFromString.length > 0 && dateToString.length > 0 ) {
                if ( timestampString >= dateFromString && timestampString <= dateToString ) {
                    result.push( items[i] );
                }
            }
            else if ( dateFromString.length > 0 ) {
                if ( timestampString >= dateFromString ) {
                    result.push( items[i] );
                }
            }
            else if ( dateFromString.length > 0 ) {
                if ( timestampString <= dateToString ) {
                    result.push( items[i] );
                }
            }
        }

        return result;
    };
});

angular.module( "crmApp" ).filter( "filterDocumentOwner", function() {
    return function( items, owner ) {
        if ( !items ) {
            return [];
        }

        var result = [];
        for ( var i = 0; i < items.length; i++ ) {
            var currentDocument = items[i];

            if ( !owner ) {
                 result.push( currentDocument );
            }
            else if ( currentDocument.uploadUser._id === owner._id ) {
                result.push( currentDocument );
            }            
        }

        return result;
    };
});

angular.module( "crmApp" )
.controller( "ProposalCtrl", function ($resource, $rootScope, $scope, $http, $location, $routeParams, $filter, $modal, $log, ngTableParams, cfpLoadingBar, uuid2, GetProject, Project, Proposal, Comment, RolesHelper, Utils, Modals, CONFIG) {
    console.log( $location.path() );
    
    $scope.newMode = $location.path().indexOf( "new" ) !== -1;
    $scope.editMode = $location.path().indexOf( "edit" ) !== -1 || $location.path().indexOf( "new" ) !== -1;

    $scope.taskViewMode = $location.path().indexOf( "tasks" ) !== -1;
    $scope.taskEditMode = $location.path().indexOf( "tasks-edit" ) !== -1;
  
    $scope.creditorCreationDate = "";
    $scope.creditorCreationDateFrom = "";
    $scope.creditorCreationDateTo = "";

    $scope.debtorCreationDate = "";
    $scope.debtorCreationDateFrom = "";
    $scope.debtorCreationDateTo = "";

    $scope.assetIntangibleCreationDate = "";
    $scope.assetIntangibleCreationDateFrom = "";
    $scope.assetIntangibleCreationDateTo = "";

    $scope.assetTangibleCreationDate = "";
    $scope.assetTangibleCreationDateFrom = "";
    $scope.assetTangibleCreationDateTo = "";

    $scope.documentCreationDate = "";
    $scope.documentCreationDateFrom = "";
    $scope.documentCreationDateTo = "";   

    var mainProjectId = $routeParams.id;
    $rootScope.mainProjectId = mainProjectId;

    $scope.projectTypes = Utils.getProjectTypes();
    $scope.filter = {};
    $scope.search = {};
    $scope.currentProject = {};
    $scope.currentProject.arbitrationManagers = [];
    $scope.currentProject.tasks = [];

    $scope.documentsListTemplate = { name: "documentsList.html", url: "/views/templates/documentsList.html" };
    $scope.commentsListTemplate = { name: "commentsList.html", url: "/views/templates/commentsList.html" };

    if ( window.localStorage.profile ) {
        $scope.currentUser = JSON.parse( window.localStorage.profile ) || {};       
    }
    else {
        $location.path("/");      
    }


    window.localStorage.mainProjectId = $rootScope.mainProjectId;
    window.localStorage.projectId = $routeParams.id;  
    $scope.sortType = "docName";

    $scope.assetStatuses = [
        {id: "inPledge", name: "В залоге"},
        {id: "notInPledge", name: "Не в залоге"}
    ];

    $scope.creditorStatuses = $scope.debtorStatuses = [
        {id: "badLoan", name: "Просроченная ссуда"},
        {id: "notBadLoan", name: "Нет просрочки"}
    ];
    
    var entityFactory = new EntityFactory(),
        mapManager = new MapManager($http);

    var defaultMapAddress = {
        "point" : {
            "lat" : "55.753559",
            "lon" : "37.609218"
        }
    };

    $scope.newDoc = {};
    $scope.sortType  = "docName";   

	$scope.updateExecutors = function(task, roleItem) {
        if (roleItem==undefined) {
            task.filteredUsers = $scope.users;
            return;
        }

        task.filteredUsers = $scope.users.filter(function(fuser) {
            if (fuser.roles && fuser.roles.length > 0) {
                var foundedRole = fuser.roles.filter(function(frole) {
                    return frole.roleId === roleItem.id;
                })[0];
                if (foundedRole)
                    return true;
            }
            return false;
        });
    };

    $scope.isOverTime = function(task) {
        if (task.finished || !task.dueDate)
            return false;

        var now = ( new Date() ).toISOString();
        var dueDate = task.dueDate;

        return now > dueDate;
    };

    $scope.isLateFinish = function(task) {
        if (!task.finishingDate)
            return false;

        var finishedDate = task.finishingDate;
        var dueDate = task.dueDate;

        return finishedDate > dueDate;
    };

    $scope.viewComments = false;
    $scope.downloading = false;

    $scope.comments = [];
    $scope.post = { type: "success", message: ""};
    $scope.messageTypes = CONFIG.MESSAGE_TYPES;

    $scope.selected = { user: {} };

    $scope.projectGroupList = [];
    $scope.notificationList = [];
    $scope.taskDocuments = [];
    $scope.users = [];
    $scope.filteredUsers = [];

    $scope.addOrg = function() {
        $scope.currentProject.orgs = $scope.currentProject.orgs || [];
        $scope.currentProject.orgs.push({ name: "Предприятие " + ($scope.currentProject.orgs.length + 1) });
    }

    $scope.addTangibleAsset = function() {
        var editorInstance = $modal.open(Modals.tangibleAsset(null));

        editorInstance.result.then(
            function (asset) {
                $scope.currentProject.assetsTangible = $scope.currentProject.assetsTangible || [];
                $scope.currentProject.assetsTangible.push(asset);
                saveChanges();
                alertify.success("Актив успешно добавлен!");
            },
            function (result) {
            });
    }

    $scope.editTangibleAsset = function(asset) {
        var editorInstance = $modal.open(Modals.tangibleAsset(asset));

        editorInstance.result.then(
            function (asset) {
                saveChanges();
                alertify.success("Актив успешно сохранен!");
            },
            function (result) {
            });
    }

    $scope.addIntangibleAsset = function() {
        var editorInstance = $modal.open(Modals.intangibleAsset(null));

        editorInstance.result.then(
            function (asset) {
                $scope.currentProject.assetsIntangible = $scope.currentProject.assetsIntangible || [];
                $scope.currentProject.assetsIntangible.push(asset);
                saveChanges();
                alertify.success("Актив успешно добавлен!");
            },
            function (result) {
            });
    }

    $scope.editIntangibleAsset = function(asset) {
        var editorInstance = $modal.open(Modals.intangibleAsset(asset));

        editorInstance.result.then(
            function (asset) {
                saveChanges();
                alertify.success("Актив успешно сохранен!");
            },
            function (result) {
            });
    }

    $scope.addCreditor = function() {
        var editorInstance = $modal.open(Modals.creditor(null));

        editorInstance.result.then(
            function (creditor) {
                $scope.currentProject.creditors = $scope.currentProject.creditors || [];
                $scope.currentProject.creditors.push(creditor);
                saveChanges();
                alertify.success("Кредитор успешно добавлен!");
            },
            function (result) {
            });
    }
    
    $scope.editCreditor = function(creditor) {
        var editorInstance = $modal.open(Modals.creditor(creditor));

        editorInstance.result.then(
            function (creditor) {
                saveChanges();
                alertify.success("Кредитор успешно сохранен!");
            },
            function (result) {
            });
    }

    $scope.deleteCreditor = function(creditor) {
        alertify.confirm("Вы действительно хотите удалить данного кредитора?", function(e) {
            if (e) {
                if (Utils.deleteElem($scope.currentProject.creditors, creditor)) {
                    saveChanges();
                    alertify.success("Кредитор успешно удален!");
                } else {
                    alertify.error("Возникла ошибка во время удаления!");
                }
            }
        });
    };

    $scope.addDebtor = function() {
        var editorInstance = $modal.open(Modals.debtor(null));

        editorInstance.result.then(
            function (debtor) {
                $scope.currentProject.debtors = $scope.currentProject.debtors || [];
                $scope.currentProject.debtors.push(debtor);
                saveChanges();
                alertify.success("Дебитор успешно добавлен!");
            },
            function (result) {
            });
    }

    $scope.editDebtor = function(debtor) {
        var editorInstance = $modal.open(Modals.debtor(debtor));

        editorInstance.result.then(
            function (debtor) {
                saveChanges();
                alertify.success("Кредитор успешно сохранен!");
            },
            function (result) {
            });
    }

    $scope.deleteDebtor = function(debtor) {
        alertify.confirm("Вы действительно хотите удалить данного дебитора?", function(e) {
            if (e) {
                if (Utils.deleteElem($scope.currentProject.debtors, debtor)) {
                    saveChanges();
                    alertify.success("Дебитор успешно удален!");
                } else {
                    alertify.error("Возникла ошибка во время удаления!");
                }
            }
        });
    };

    $scope.openEditorTask = function(docList, doc) {
        $scope.createMode = !doc;
        $scope.objectCaption = ($scope.createMode) ? "Новый документ" : doc.name;
        $scope.doc = ($scope.createMode)
            ? { name: "123", htmlContent: "<h1>Введите текст документа...</h1><br/><br/><br/><br/><br/>", type: "intDoc", link: "", uploadUser:  Utils.getReducedUser($scope.currentUser) }
            : doc;

        var editorInstance = $modal.open(Modals.editor($scope));

        editorInstance.result.then(
            function (result) {
                if ($scope.createMode) {
                    docList.push(result);
                }
                saveChanges();
            },
            function (result) {
            });
    }

    $scope.removeOrg = function(org) {
        if ($scope.currentProject.orgs.length == 1) {
            alertify.error("Нельзя удалить единственное предприятие");
            return;
        }

        alertify.confirm("Вы действительно хотите удалить данное предприятие?", function(e) {
            if (e) {
                var indx = -1;
                var k = 0;
                $scope.currentProject.orgs.forEach(function(objOrg) {
                    if (objOrg._id == org._id)
                        indx = k;
                    k++;
                });

                if (indx > -1) {
                    $scope.currentProject.orgs.splice(indx, 1);
                }

                saveChanges();
            }
        });
    }

    $scope.removeTask = function(task) {
    	if (task.finished) {
    		alertify.error("Нельзя удалить завершенную задачу");
    		return;
    	}

    	alertify.confirm("Вы действительно хотите удалить эту задачу?", function(e) {
            if (e) {
            	task.removed = { date: new Date().toISOString(), user: Utils.getReducedUser($scope.currentUser) };
            	saveChanges();
            }
        });
    }

    $scope.getRemoveTitle = function(task) {
    	return (task.finished) ? "Задача завершена" : "Удалить задачу";
    }  

    $scope.uploadFile = function(documents, docName) {
        Utils.uploadFile(documents, docName, $scope.currentUser, function() {
            saveChanges();
        }, function() {});
        
    };

    $scope.addLink = function(docs, doc) {
        Utils.addLink(docs, doc, $scope.currentUser, function() {
            saveChanges();
        });
        $scope.newDoc = {};
    };

    $scope.processDocument = function(doc) {
        var fileProps = {projectId: mainProjectId, id: $routeParams.id, fileUrl: doc.link};
        Utils.proccessDocument($scope, doc, fileProps);
    };

    alertify.set({ labels: {
        ok     : "Да",
        cancel : "Нет"
    }});

    $scope.getOrgName = function(orgId) {
        return Utils.getOrgName(orgId);
    }

    $scope.getOrgId = function(orgName) {
        return Utils.getOrgId(orgName);
    }

    $scope.orgMode = true;
    $scope.Orgs = {
        AbnId: $scope.getOrgId("АБН"),
        BusinessKrugId: $scope.getOrgId("Бизнес-КРУГ"),
        MosingprojectId: $scope.getOrgId("Мосинжпроект") };

    var loadComments = function() {
        $scope.viewComments = RolesHelper.canViewComments($scope.currentUser, $scope.currentProject);       

        Comment.query( {propId: $routeParams.id} )
            .$promise
            .then(function(data) {
                $scope.comments = data.comments;
            });
    }

    $scope.onOrgChange = function() {
        saveChanges();
    }

    $scope.postComment = function() {
        var newComment = {
            proposalId: $routeParams.id,
            projectId: mainProjectId,
            user: {id: $scope.currentUser._id, fullName: $scope.currentUser.fullName},
            type: $scope.post.type.code,
            message: $scope.post.message,
            notificationList: $scope.notificationList,
            time: ( new Date() ).toISOString()
        };

        Comment.save({}, newComment, function(item) {         
            $scope.post.message = "";
            loadComments();
            alertify.success("Комментарий успешно добавлен!");
        });
    }

    $scope.deleteComment = function(comment) {
        if (!comment._id)
            return;

        alertify.confirm("Вы действительно хотите удалить этот комментарий?", function(e) {
            if (e) {          
                comment.type = "deleted";
                comment.deletionTime = ( new Date() ).toISOString();
                Comment.update({id: comment._id}, comment, function(item) {                
                    comment = item;
                    alertify.success("Комментарий успешно удален!");
                });
            }
        });
    }

    $scope.deleteAsset = function(asset, isTangible) {
        alertify.confirm("Вы действительно хотите удалить этот актив?", function(e) {
            if (e) {
                var idx = -1;
                var link;
                if (isTangible) {
                    idx = $scope.currentProject.assetsTangible.indexOf(asset);
                    link = $scope.currentProject.assetsTangible;
                } else {
                    idx = $scope.currentProject.assetsIntangible.indexOf(asset);
                    link = $scope.currentProject.assetsIntangible;
                }

                if (idx > -1) {
                    link.splice(idx,1);
                    saveChanges();
                    alertify.success("Актив успешно удален!");
                }
            }
        });
    };

    $scope.getUserPhoto = function(user) {
    	if (!user) {
    		return "";
    	}

    	if (!user.photo) {
    		return "/images/users/user1-128x128.jpg";
    	}

    	return user.photo;
    }   

    $scope.addUser = function() {
        if (!$scope.currentProject.projectGroupList) {
            $scope.currentProject.projectGroupList = [];
        }
        $scope.projectGroupList.push($scope.selected.user);
        $scope.currentProject.projectGroupList.push($scope.selected.user);
        saveChanges();
        Utils.deleteElemById($scope.filteredUsers, $scope.selected.user);
        alertify.success("Пользователь успешно добавлен!");
        updateNotificationList();
    }

    $scope.deleteUser = function(user) {
        alertify.confirm("Вы действительно хотите удалить данного пользователя из проектной группы?", function(e) {
            if (e) {
                var success1 = Utils.deleteElemById($scope.projectGroupList, user);
                var success2 = Utils.deleteElemById($scope.currentProject.projectGroupList, user);
                $scope.filteredUsers.push(user);
                $scope.filteredUsers = $filter("orderBy")($scope.filteredUsers, "fullName");

                if (success1 && success2) {
                    saveChanges();
                    alertify.success("Пользователь успешно удален!");
                    updateNotificationList();
                }
            }
        });
    }

    $scope.canDeleted = function(user) {
        return !RolesHelper.isInitWorkGroup(user, $scope.currentProject);
    }

    var filterUserSelectList = function() {
        $scope.projectGroupList.forEach(function(user) {
            Utils.deleteElemById($scope.filteredUsers, user);
        });
    }

    var updateNotificationList = function() {
        $scope.notificationList = [];
        $scope.projectGroupList.forEach(function(user) {
            $scope.notificationList.push(user);
        });
    }

    $scope.justifySelection = function(checked) {
        if (checked) {
            updateNotificationList();
        }
        else {
            $scope.notificationList = [];
        }
    }

    $scope.justifyNotificationList = function (user) {
        var idx = $scope.notificationList.indexOf(user);

        if (idx > -1) {
            $scope.notificationList.splice(idx, 1);  //Было выбрано ранее
        } else {
            $scope.notificationList.push(user);
        }
    }

    var loadProjectGroupList = function () {
        if ($scope.currentProject.owner) {
            $scope.currentProject.owner.projectRole = "создатель заявки";
            $scope.projectGroupList.push($scope.currentProject.owner);
        }
        if ($scope.currentProject.manager) {
            $scope.currentProject.manager.projectRole = "руководитель";
            $scope.projectGroupList.push($scope.currentProject.manager);
        }
        if ($scope.currentProject.coordinator) {
            $scope.currentProject.coordinator.projectRole = "координатор";
            $scope.projectGroupList.push($scope.currentProject.coordinator);
        }

        if ($scope.currentProject.projectGroupList && $scope.currentProject.projectGroupList.length > 0) {
            $scope.currentProject.projectGroupList.forEach(function(user) {
                $scope.projectGroupList.push(user);
            });
        }

        updateNotificationList();
    }

    var updateComments = function() {
        Proposal.get({projectId: mainProjectId, id: $routeParams.id}, function(data){
            if (!$scope.currentProject)
                $scope.currentProject = data;

            var date = ( new Date() ).toISOString();
            $scope.currentProject.comments = data.comments;
            var timestampLocalString = UTCToLocalTime( Date.parse( date ) ).toISOString().replace( /T/, " " ).replace( /Z/, "" ).replace( /\..+/, "" );
            alertify.success( "Обновление произошло в: " + timestampLocalString );
        });
    }

    $scope.getCommentClass = function(type) {
        switch(type) {
            case "success": return "alert alert-success";
            case "info": return "alert alert-info";
            case "warning": return "alert alert-warning";
            case "danger": return "alert alert-danger";
            default: return "alert";
        }
    }

    $scope.getCommentIcon = function(type) {
        return "";
    }

    $scope.getLinkClass = function(link) {
        return (link && link.indexOf("cloud") > 0 && link.indexOf("abn-consult.ru") > 0)
            ? "cloudLink" : "";
    }

    $scope.getLinkFileIcon = function(link) {
        return (link && link.indexOf("cloud") > 0 && link.indexOf("abn-consult.ru") > 0)
            ? "font-up fa fa-arrow-down" : "font-up fa fa-arrow-right";
    }

    $scope.getLinkWord = function(link) {
        return (link && link.indexOf("cloud") > 0 && link.indexOf("abn-consult.ru") > 0)
        	? "Скачать" : "Перейти";
    }

    $scope.fileIcon = function(fileType) {
        return Utils.getFileIcon(fileType);
    };

    $scope.toLocalDateString = function( date ) {
        if ( !date ) {
            return null;
        }

        var timestampString = UTCToLocalTime( Date.parse( date ) ).toISOString().replace( /T/, " " ).replace( /Z/, "" ).replace( /\..+/, "" );
        timestampString = timestampString.replace( /(\d+)-(\d+)-(\d+)/, "$3.$2.$1" );
        return timestampString;
    };


    $scope.formatTime = function(dt) {
        var date = new Date(dt);
        return date.toLocaleString();
    };

    $scope.formatTimeFromNow = function(dt) {
        var date = new Date(dt);
        return moment(date).fromNow();
    };

    $scope.loadFinish = function() {
        alertify.success("Файл успешно загружен");
    };

    var oldNotificationColor = undefined;
    var oldJuristicColor = undefined;

    $scope.isDepended = function(task) {
    	return task.parentId;
    };

    $scope.addTask = function() {
        $scope.taskName = "Новая задача"
        $scope.taskDescription = "";
        var editorInstance = $modal.open(Modals.taskEditor($scope));

        editorInstance.result.then(
            function (task) {
                $scope.currentProject.tasks = $scope.currentProject.tasks || [];
                task.inProcess = true;
                $scope.currentProject.tasks.push(task);
                saveChanges();
                alertify.success("Задача успешно добавлена!");
            },
            function (result) {
            });
    }

    $scope.openTaskFinsh = function(task) {
        $scope.currentTask = task;
        var editorInstance = $modal.open(Modals.taskFinish($scope));

        editorInstance.result.then(
            function (result) {
               	task.finished = true;
                task.inProcess = false;
                task.finishingDate = ( new Date() ).toISOString();
                saveChanges();
            },
            function (result) {
            });
    }

    $scope.finishTask = function(task) {
    	if (!task.executor) {
    		alertify.error("Назначьте исполнителя!");
    		return;
    	}

    	if (task.mustDocs) {
    		$scope.openTaskFinsh(task);
    		return;
    	}

        alertify.confirm("Вы уверены, что хотите отметить эту задачу как выполненную?", function (e) {
            if (e) {
                task.finished = true;
                task.inProcess = false;
                task.finishingDate = ( new Date() ).toISOString();
                saveChanges();
            }
        });
    };

    var finishTask = function(id){
        $scope.currentProject.tasks.forEach(function(task){
            if(task.id === id){
                alertify.confirm("Вы уверены, что хотите отметить эту задачу как выполненную?", function (e) {
                    if (e) {
                        task.finished = true;
                        task.inProcess = false;
                        task.finishingDate = ( new Date() ).toISOString();
                        $scope.canSendToCRM = canSendToCRM();
                        saveChanges();
                        renderTaskList();
                    }
                });
            }
        });
    };

    var processTask = function(id, process) {
        $scope.currentProject.tasks.forEach(function(task){
            if(task.id === id){
                task.inProcess = process;
                if (!task.events) {
                    task.events = [];
                }
                task.events.push({ eventType: (process) ? "start" : "stop", dateTime: new Date().toISOString()});
                saveChanges();
                renderTaskList();
            }
        });
    };

    var deleteTask = function(id){
        alertify.confirm("Вы уверены, что хотите удалить данную задачу?", function(e){
            if(e){
                $scope.currentProject.tasks =  $scope.currentProject.tasks.filter(function(t){
                    return t.id !== id;
                });
                $scope.canSendToCRM = canSendToCRM();
                saveChanges();
                renderTaskList();
            }
        })
    }

    $scope.editTasks = function() {
    	window.location.href = "/#/projects/" + $routeParams.id + "/tasks-edit";
    };    

    $scope.savePlan = function() {
        $scope.currentProject.tasks.forEach(function(task, i) {
            if (task.creationDate) {
                task.creationDate = task.creationDate;
            }
            if (task.dueDate) {
                task.dueDate = task.dueDate;
            }
            delete task["filteredUsers"];
        });
        saveChanges();

        $location.path( "projects/" + $scope.currentProject._id + "/tasks" );      
    };   

    $scope.savePlan2 = function(){

            if($scope.currentUser){
                $scope.currentProject.owner = $scope.currentUser;
                $scope.currentProject.tasks.forEach(function(task, i) {
                    if (task.creationDate) {
                        task.creationDate = task.creationDate;
                    }
                    if (task.dueDate) {
                        task.dueDate = task.dueDate;
                    }
                    delete task["filteredUsers"];
                });
				
					
                    Proposal.save({projectId: mainProjectId}, $scope.currentProject, function(item) {
                        alertify.success("Сохранено!");						
                        $scope.currentProject = item;
                        $location.path("/projects/" + $scope.currentProject._id + "/tasks");
                    });
            } else {
                alertify.error("Не удалось получить информацию о вашем профиле. Попробуйте обновить страницу (F5) и повторить операцию.");
            }
    }

    var renderTaskList = function(){
        if($scope.viewRight){

            $(".notificationTask").unbind();
            $(".juristicTask").unbind();
            $("div.col-lg-7.status-title.status-tooltip").unbind();
            $("div.col-lg-7.taskOwnerInfo").unbind();
            $("[data-toggle=\"tooltip\"]").tooltip();      

            $("div.col-lg-7.status-title.status-tooltip").on("click", function () {
                $(this).parent().parent().parent().find(".taskDetails").slideToggle();
            });

            $("div.col-lg-7.taskOwnerInfo").on("click", function () {
                $(this).parent().parent().parent().find(".taskDetails").slideToggle();
            });
        }
    };
    
    var saveChanges = function() {   
        for ( var i = 0; i < $scope.currentProject.arbitrationManagers.length; i++ ) {
            var currentItem = $scope.currentProject.arbitrationManagers[i];            

            [].slice.call( document.querySelectorAll( "#arbitrationManagersTable #arbitrationManagersTableRow" ) ).forEach( function( el ) {             
                if ( el.dataset.objid == currentItem._id ) {
                    var arbitrationManagerRegDate = el.querySelector( "#arbitrationManager-regDate" ).value;
                    var arbitrationManagerAppointDate = el.querySelector( "#arbitrationManager-appointDate" ).value;
                    currentItem.regDate = arbitrationManagerRegDate;
                    currentItem.appointDate = arbitrationManagerAppointDate;
                    return;
                }
            });
        }
        for ( var i = 0; i < $scope.currentProject.tasks.length; i++ ) {
            var currentItem = $scope.currentProject.tasks[i];

            var timestampString = currentItem.creationDate;
            if ( /\d+\.\d+\.\d+/.test( timestampString ) ) { // ru-RU
                timestampString = timestampString.replace( /(\d+)\.(\d+)\.(\d+)/, "$3-$2-$1" );

                if ( !( /\d{2}:\d{2}:\d{2}/.test( timestampString ) ) ) {
                    timestampString += "T00:00:00.000Z";
                }

                currentItem.creationDate = timestampString;
            }

            timestampString = currentItem.dueDate;        
            if ( /\d+\.\d+\.\d+/.test( timestampString ) ) { // ru-RU
                timestampString = timestampString.replace( /(\d+)\.(\d+)\.(\d+)/, "$3-$2-$1" );

                if ( !( /\d{2}:\d{2}:\d{2}/.test( timestampString ) ) ) {
                    timestampString += "T00:00:00.000Z";
                }

                currentItem.dueDate = timestampString;
            }
        }

        for ( var i = 0; i < $scope.currentProject.arbitrationManagers.length; i++ ) {
            var currentItem = $scope.currentProject.arbitrationManagers[i];

            var timestampString = currentItem.regDate;
            if ( /\d+\.\d+\.\d+/.test( timestampString ) ) { // ru-RU
                timestampString = timestampString.replace( /(\d+)\.(\d+)\.(\d+)/, "$3-$2-$1" );

                if ( !( /\d{2}:\d{2}:\d{2}/.test( timestampString ) ) ) {
                    timestampString += "T00:00:00.000Z";
                }

                currentItem.regDate = timestampString;
            }

            timestampString = currentItem.appointDate;        
            if ( /\d+\.\d+\.\d+/.test( timestampString ) ) { // ru-RU
                timestampString = timestampString.replace( /(\d+)\.(\d+)\.(\d+)/, "$3-$2-$1" );

                if ( !( /\d{2}:\d{2}:\d{2}/.test( timestampString ) ) ) {
                    timestampString += "T00:00:00.000Z";
                }

                currentItem.appointDate = timestampString;
            }
        }

        var project = new Project( $scope.currentProject );
        if ( $scope.newMode ) {            
            project.$save( { id: $scope.currentProject._id }, function() {
                alertify.success( "Сохранено!" );
            });
        }
        else {
            project.$update( { id: $scope.currentProject._id }, function() {
                alertify.success( "Сохранено!" );
            });
        }        
    };

    var documetAdd = function(f){
        console.debug("documetAdd");
        console.debug(f);
    };

    var saveTask = function(task){
        if($scope.currentProject.tasks.filter(function(t){ return t.id === task.id; }).length){
            $scope.currentProject.tasks.forEach(function(t){
                if(t.id === task.id){
                    t = task;
                }
            });
        } else {
            task.creationDate = undefined;
            $scope.currentProject.tasks.push(task);
        }

        $scope.canSendToCRM = canSendToCRM();
        updateStatuses(task.status);
        saveChanges();
        $scope.taskEditMode = false;
    };

    var getCurrentStatuses = function(){
        var statusesForRole = calcStatuses();

        if (statusesForRole && statusesForRole.length > 0){
            return statusesForRole;
        }

        if (Utils.onlyCurrentStatuses(mainProjectId))
            return $scope.statuses;
        return $scope.currentProject.tasks.length > 0 ? $scope.currentProject.statuses : $scope.statuses;
    }

    var calcStatuses = function() {
        var role = $scope.currentUser.roles.filter(function(r){
            return r.projectId===mainProjectId;
        });
        if (role && role.length > 0) {
            var roleName = RolesHelper.getRoleNameById(role[0].roleId, $scope.currentProject.roles);

            if (roleName && roleName.length > 0) {
                return RolesHelper.getStatusesByRoleName(roleName[0].name, $scope.currentProject.statuses, CONFIG.STATUSES_FILTER);
            }
        }
        return $scope.currentProject.statuses;
    }

    var taskAgree = function(id){
        $scope.currentProject.tasks.forEach(function(task){
            if(task.id === id){
                alertify.confirm("Отметить как прочтенную вами?", function (e) {
                    if (e) {
                        task.executor.agree = (new Date).toISOString();
                        saveChanges();
                    } else {
                        task.executor.agree = "";
                    }
                    renderTaskList();
                });
            }
        });
    };

    $scope.isRightSide = function(comments, user) {
    	if (!comments || (comments && comments.length == 0))
    		return false;
    	var userWroteComment = false;
    	var isRightSide = undefined;
    	comments.forEach(function(c) {
    		if (c.owner._id === user._id) {
    			userWroteComment = true;
    			isRightSide = c.right;
    			return;
    		}
    	});
    	if (userWroteComment) {
    		return isRightSide;
    	}

    	return (comments[comments.length-1].right) ? !comments[comments.length-1].right : true;
    }

    $scope.addComment = function(obj) {
        obj.comments = obj.comments || [];
        if (obj.commentText) {
        	var resComment = {
        		text: obj.commentText,
        		creationDate: ( new Date() ).toISOString(),
        		owner: Utils.getReducedUser($scope.currentUser),
        		right: $scope.isRightSide(obj.comments, $scope.currentUser)
    		};
        	delete obj.commentText;
        	obj.comments.push(resComment);
        	saveChanges();
        }
    }

    var saveComments = function(commentsContainer){      
        $scope.currentProject.tasks.forEach(function(t){
            if(t.id === commentsContainer.id){
                t.comments = t.comments || [];
                t.comments.push(commentsContainer.comment);
            }
        });

        saveChanges();
        renderTaskList();
    }

    var saveDocuments = function(documentsContainer){       
        $scope.currentProject.tasks.forEach(function(t){
            if(t.id === documentsContainer.id){
                t.documents = t.documents || [];
                t.documents.push(documentsContainer.document);
            }
        });

        saveChanges();
        renderTaskList();
    };

    $scope.deleteDoc = function(docs, doc) {
    	alertify.confirm("Вы действительно хотите удалить этот файл?", function(e) {
            if (e) {
            	var index = 0;
            	var k = -1;
            	docs.forEach(function(d) {
            		if (d.id == doc.id) {
            			k = index;
            			return;
            		}
            		index++;
            	});

            	if (k>-1) {
            		docs.splice(k, 1);
	    			saveChanges();
    			}
    		}
    	});
    };

    var deleteDocuments = function(documentsContainer){       
        $scope.currentProject.tasks.forEach(function(t){
            if(t.id === documentsContainer.id){
                t.documents = documentsContainer.documents || [];
            }
        });

        saveChanges();
        renderTaskList();
    }

    var canCreateTasks = function(){
        return $scope.currentProject.tasks.every(function(t){
            return t.finished;
        });
    }

    var canSendToCRM = function(){
        var result = !!$scope.currentProject.tasks.filter(function(t){
            return !t.finished && t.status && t.status.title === "осмотр";
        })[0];
        return result;
    }

    var setRight = function(role){
        if(role.right === "view"){
            $scope.viewRight = true;
        } else if(role.right === "edit"){
            $scope.editRight = true;
        }
    }
    var setOwner = function(currentUser){
        if($scope.currentProject.manager || $scope.currentProject.owner){
            $scope.isOwner = false;
           if($scope.currentProject.owner){
                $scope.isOwner = $scope.currentProject.owner._id === currentUser._id;
           }
           if($scope.currentProject.manager){
                $scope.isOwner = $scope.isOwner || ($scope.currentProject.manager._id === currentUser._id);
           }
        }
    }

    var setRights = function(currentUser){
        if(!$scope.currentProject.status){
            $scope.editRight = true;
            $scope.viewRight = true;
            return;
        }
        if($scope.currentProject.status.roles){
            $scope.currentProject.status.roles.forEach(function(role){
                RolesHelper.getCurrentRoles(currentUser, $scope.currentProject).forEach(function(userRole){
                    if(userRole === role.role){
                        setRight(role);
                    }
                });
            });
        } else {
            $scope.viewRight = true;
        }

        if(currentUser.isRoot){
            $scope.editRight = true;
            $scope.viewRight = true;
        } else {
            $scope.editRight = $scope.isOwner || $scope.editRight;
            if($scope.editRight) $scope.viewRight = true;
            if(!$scope.viewRight) $scope.accessDeniedText = "В данный момент заявка имеет статус, не позволяющий вам просматривать ее";
            if(!$scope.editRight && $scope.viewRight){
                $scope.editDeniedText = "В данный момент заявка имеет статус, не позволяющий вам работать с задачами";
            }
        }
    }

    $scope.updateStatus = function(){
        saveChanges();
        updateStatuses($scope.currentProject.status);
        alertify.success("Статус успешно обновлен!");
    }  

    var getStatuses = function(){
        $scope.statuses = [];
        if(!$scope.currentProject.status){
            return;
        }
        if($scope.currentProject.status.next){
            $scope.currentProject.status.next.forEach(function(id){
                $scope.currentProject.statuses.forEach(function(st){
                    if(st.id === id){
                        $scope.statuses.push(st);
                    }
                });
            });
        }
    }

    var updateStatuses = function(status){
        if(status.autoset){
            $scope.currentProject.status = status;
        }

        getStatuses();
        renderTaskList();
    }

    var setNextStatus = function(){
        if($scope.currentProject.status.next && $scope.currentProject.status.next.length === 1){
            var result;
            $scope.currentProject.statuses.forEach(function(st){
                if(st.id === $scope.currentProject.status.next[0]){
                    result = st;
                }
            });
            if(result){
                $scope.currentProject.status = result;
            }
        }
    }

    $scope.edit = function() {     
        $location.path( "projects/" + mainProjectId + "/edit" );
    };
    
    $scope.cancel = function() {
        if ( $scope.taskViewMode === true || $scope.taskEditMode === true ) {
            $location.path( "projects/" + $scope.currentProject._id + "/tasks" );
        }
        else {
            $location.path( "projects/" + $scope.currentProject._id + "/main" );
        }
    };

    $scope.save = function() {
        saveChanges();     
        $location.path( "projects/" + $scope.currentProject._id + "/main" );      
    };
    
    $scope.close = function() {
        $location.path( "/projects/" + $scope.currentProject._id );
    };

    var loadTaskDocuments = function(prop) {
        $scope.taskDocuments = [];
        prop.tasks.forEach(function(task) {
            if (task && task.documents) {
                task.documents.forEach(function(doc) {
                    $scope.taskDocuments.push(doc);
                });
            }
        });       
    };

    var checkDocuments = function (docs) {
        return new Promise(function(resolve, reject){
            var k = 1;
            docs.forEach(function(doc) {
                if (doc.link.indexOf("cloud") > 0 && doc.link.indexOf("abn-consult.ru") > 0) {
                    var linkParts = doc.link.split("/");
                    var fileId = linkParts[linkParts.length-1];

                    $http.get(CONFIG.CLOUD_ADDRESS + "/api/files/" + fileId)
                        .success(function(data, status, headers, config) {
                            var fileName = data.file.Name;
                            doc.type = Utils.getFileType(fileName);

                            if (!doc.name) {
                                doc.name = fileName;
                            }
                        }).error(function(data, status, headers, config) {
                            doc.name = "Файл" + k;
                            doc.type = "file";
                            k++;
                        });
                } else {
                    if (doc.name.length===0)
                        doc.name = "Файл" + k;

                    if (doc.type!=="intDoc")
                        doc.type = "link";
                    k++;
                }
            });
        });
    };    

    $scope.useHour = false;
    $scope.showExecutors = false;
    $scope.first = true;

    $scope.toggleScale = function() {
        var weekScaleTemplate = function(date){
            var dateToStr = gantt.date.date_to_str("%d %M");
            var endDate = gantt.date.add(gantt.date.add(date, 1, "week"), -1, "day");
                return dateToStr(date) + " - " + dateToStr(endDate);
        };

        gantt.config.subscales = [
            {unit:"week", step:1, template:weekScaleTemplate},
            {unit:"day", step:1, date:"%D %d" },
        ];

        if ($scope.useHour) {
            gantt.config.subscales.push({unit:"hour", step:1, date:"%H" });
        }

        $scope.useHour = !$scope.useHour;
        gantt.refreshData();
        gantt.render();
    };

    $scope.toggleExecutors = function() {
        gantt.config.keep_grid_width = true;
        gantt.config.columns = [
            {name: "text", width: "260", tree: true},
            {name: "start_date", width: "100", align: "center"},
            {name: "end_date", width: "100", label: "Конец", align: "center"},
            {name: "duration", width: "100", align: "center"}
        ];

        if ($scope.showExecutors) {
            gantt.config.columns.push({name:"executor", label: "Исполнитель", align: "left", width: "160"});
            $scope.first = false;
        }

        if (!$scope.first) {
            gantt.refreshData();
            gantt.render();
        }

        $scope.showExecutors = !$scope.showExecutors;
    };
    
    $scope.updateGanttDiagramm = function() {
        var startDate = new Date($scope.currentProject.started);
        var endDate = new Date($scope.currentProject.started);
        var firstStartDate = ( new Date() ).toISOString();

        var res =  {
            data: [
                {
                    id: 1,
                    text: "Заявка",
                    start_date: startDate,
                    end_date: ( new Date() ).toISOString(),
                    progress: 1.0,
                    type: gantt.config.types.project,
                    executor: "",
                    open: true
                }
            ],
            links: [
                {
                    id: 1,
                    source: 1,
                    target: 2,
                    type: "0"
                }
            ]
        };

        if ($scope.currentProject.tasks) {
            var tasks = Array.prototype.slice.call($scope.currentProject.tasks);
            tasks.reverse();

            var taskId = 2;
            var linkId = 1;
            tasks.forEach(function(t) {
                var newTask = {
                    id: taskId,
                    text: t.status.title,
                    start_date: t.creationDate,
                    progress: 1.0,
                    parent: 1,
                    type: gantt.config.types.task,
                    executor: (t.executor) ? t.executor.fullName : ""
                };

                if (t.finished && t.finishingDate) {
                    newTask.end_date = t.finishingDate;
                } else {
                    newTask.text += " (в процессе)";
                    newTask.end_date = ( new Date() ).toISOString();
                }

                if (newTask.end_date > endDate) {
                    endDate = newTask.end_date;
                }

                var creationDate = t.creationDate;
                if (creationDate < firstStartDate) {
                    firstStartDate = creationDate;
                }
                res.data.push(newTask);
                res.links.push({
                    id: linkId,
                    source: taskId,
                    target: taskId + 1,
                    type: "0"
                });
                taskId++;
                linkId++;
            });
            res.data[0].end_date = endDate;
        }

        var startGap = {
            text: "Этап до оценки",
            start_date: $scope.currentProject.started,
            end_date: firstStartDate//,
        };

        gantt.config.keep_grid_width = true;
        gantt.config.readonly = false;

        $scope.toggleExecutors();

        gantt.templates.grid_date_format = function(date){
            return gantt.date.date_to_str("%d.%m %H:%i")(date);
        };

        gantt.init("interactiveGantt");

        gantt.config.scale_height = 90;
        gantt.config.scale_unit = "month";

        $scope.toggleScale();

        gantt.config.date_scale = "%F, %Y";
        gantt.config.min_column_width = 50;

        gantt.clearAll();
        gantt.parse(res);
    };

    $scope.$on( "ngRepeatFinished", function( ngRepeatFinishedEvent ) {
        [].slice.call( document.querySelectorAll( ".fc-datetime-widget" ) ).forEach( function( el ) {       
            $( el.querySelector( "input" ) ).datepicker({           
                format: {                
                    toDisplay: function( date, format, language ) {
                        var d = new Date( date );

                        var timestampString = d.toISOString().replace( /T/, " " ).replace( /Z/, "" ).replace( /\..+/, "" );
                        timestampString = timestampString.replace( /(\d+)-(\d+)-(\d+)/, "$3.$2.$1" );

                        if ( /\d{2}:\d{2}:\d{2}/.test( timestampString ) ) {
                            var re = /\s/;
                            var tokens = timestampString.split( re );
                            timestampString = tokens[0];
                        }

                        return timestampString;
                    },
                    toValue: function( date, format, language ) {
                        var timestampString = date;
                        if ( /\d+\.\d+\.\d+/.test( timestampString ) ) { // ru-RU
                            timestampString = timestampString.replace( /(\d+)\.(\d+)\.(\d+)/, "$3-$2-$1" );
                        }
                    
                        var result = new Date( timestampString );
                        return result;
                    }
                },         
                language: "ru-RU",
                autoclose: true
            })
            .on( "changeDate", function ( e ) {
                el.querySelector( ".fc-datetime-widget-clear-button" ).style.display = "block";
            });

            el.querySelector( ".fc-datetime-widget-clear-button" ).addEventListener( "click", function() {
                $( el.querySelector( "input" ) ).datepicker( "clearDates" );
                el.querySelector( ".fc-datetime-widget-clear-button" ).style.display = "none";
            });

            var date = el.querySelector( "input" ).value;
        
            if ( date.length === 0 ) {
                return;
            }            

            var timestampString = date;        
            if ( /\d+-\d+-\d+/.test( timestampString ) ) { // ISO8601
                timestampString = UTCToLocalTime( Date.parse( timestampString ) ).toISOString().replace( /T/, " " ).replace( /Z/, "" ).replace( /\..+/, "" );
                timestampString = timestampString.replace( /(\d+)-(\d+)-(\d+)/, "$3.$2.$1" );

                var re = /\s/;
                var tokens = timestampString.split( re );

                timestampString = tokens[0];
            }
            else if ( /\d+\.\d+\.\d+/.test( timestampString ) ) { // ru-RU
                timestampString = timestampString.replace( /(\d+)\.(\d+)\.(\d+)/, "$3-$2-$1" );
            }
            else {
                console.error( "Недопустимый формат даты " + timestampString );
                return;
            }     

            $( el.querySelector( "input" ) ).datepicker( "update", timestampString );
            el.querySelector( ".fc-datetime-widget-clear-button" ).style.display = "block"; 
        });
    });

    $scope.addArbitrationManager = function() {
        $scope.currentProject.arbitrationManagers = $scope.currentProject.arbitrationManagers || [];           
        $scope.currentProject.arbitrationManagers.push({
            _id: uuid2.newuuid(),				
            lastName : "",
            name : "Управляющий " + ($scope.currentProject.arbitrationManagers.length + 1),
            patronymic : "",
            INN : "",
            SNILS : "",
            regNum : "",
            regDate : "",
            SRO : "",
            appointDate : ""
        });
    }
    
    $scope.removeArbitrationManager = function(arbitrationManager) {
        alertify.confirm("Вы действительно хотите удалить данного управляющего?", function(e) {
            if (e) {
                var indx = -1;
                var k = 0;
                $scope.currentProject.arbitrationManagers.forEach(function(obj) {
                    if (obj._id == arbitrationManager._id)
                        indx = k;
                    k++;
                });

                if (indx > -1) {
                    $scope.currentProject.arbitrationManagers.splice(indx, 1);                    
                }
            }
        });
    }
  
    $http.get( CONFIG.REST_ADDRESS + "/api/users/brief" )
    .success( function( data, status, headers, config ) {
        $scope.users = $filter("orderBy")(data.users, "fullName");
        $scope.managerUsers = $scope.users;
        $scope.filteredUsers = $filter("orderBy")(data.users, "fullName");
        filterUserSelectList();     
    })
    .error( function( data, status, headers, config ) {      
        alertify.error("Не удалось загрузить список пользователей!");
    });
    if ( $scope.newMode === true ) {   
        var newProjectEntity = {
            "_id" : "",
            "started" : "",
            "name" : "",
            "number" : "",
            "documents" : [],
            "address" : "",
            "mapAddress" : {},
            "currentTask" : {
                "executor" : {}
            },
            "tasks" : [],
            "orgs" : [],
            "type" : {},
            "dueDate" : "",
            "manager" : {},
            "arbitrationManagers" : [],
            "administrator" : {},
            "owner" : {},
            "projectGroupList" : [],
            "assetsTangible" : [],
            "assetsIntangible" : [],
            "creditors" : [],
            "debtors" : []
        };

        newProjectEntity._id = uuid2.newuuid();
        newProjectEntity.started = ( new Date() ).toISOString();        
        $scope.currentProject = newProjectEntity;    
        $rootScope.mainProjectId = $scope.currentProject._id;      
    }
    else {      
        GetProject($scope, mainProjectId)
        .then( function( result ) {
            $scope.currentProject = result.project;
            $scope.currentRole = RolesHelper.getRoleForProject($scope.currentUser, result.project);
            $rootScope.currentRole = $scope.currentRole;       

            loadProjectGroupList();
            loadComments();
            checkDocuments( $scope.currentProject.documents );
            loadTaskDocuments( $scope.currentProject );
        });
    }   
});
