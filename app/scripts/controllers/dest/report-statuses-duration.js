'use strict';

angular.module('crmApp')
    .controller('ReportStatusesDurationCtrl', function ($scope, $http, Project, Report, $routeParams, $q, CONFIG, ReportHelper, RolesHelper)  {

        $scope.series1 = [];
        $scope.projectId = $routeParams.id;
        $scope.tabData = ReportHelper.getTabsConfig($scope.projectId, RolesHelper.isCoordinator($scope.currentUser.roles));
        $scope.tabStatusesData = ReportHelper.getTabsStatusesConfig();

        $scope.statusesDurationLoaded = false;

        $scope.formatTimeToLocal = function(ISODate) {
            var dt = new Date(ISODate);
            return dt.toLocaleString();
        }

        $scope.getDate = function(){
            if ($scope.projectId === undefined) {
                return;
            }

            Report.statusesDuration({ projectId : $scope.projectId }, function(data){
                $scope.statusesDurationLoaded = true;
                if(data.$resolved){
                    $scope.dataTable = data.result;
                }
            });
        };

    $scope.getDate();
});