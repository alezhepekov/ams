'use strict';

angular.module('crmApp')
    .controller('FeedbackCtrl', function ($scope, $rootScope, $http, CONFIG) {
    	$rootScope.title = 'Обратная связь - Asssets Management System';
    	$scope.feedback = { name: '', email: '', phoneNum: '123', message: ''};

    	$scope.sendFeedback = function() {
    		
    		if (!$scope.feedback) {
    			return;
    		}

	        if ($scope.feedback.name.length == 0) {
	            $('#feedbackNameReq').slideDown();
	        } else {
	            $('#feedbackNameReq').slideUp();
	        }
	        
	        var rePhoneCheck = /^(\+?\d[- .]*){7,13}$/;
	        var reEmailCheck = /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/;

	        if ($scope.feedback.phoneNum.length == 0 && $scope.feedback.email == 0) {
	            $('#feedbackPhoneNumReq').slideDown();
	            $('#feedbackEmailReq').slideDown();
	        } else {
	            if ($scope.feedback.phoneNum.length != 0) {
	                if (rePhoneCheck.test($scope.feedback.phoneNum))
	                    $('#feedbackPhoneNumReq').slideUp();
	                else {
	                    $('#feedbackPhoneNumReq').html('<span class="required">*</span>Введите номер телефона в корректном формате');
	                    $('#feedbackPhoneNumReq').slideDown();
	                }
	            }

	            if ($scope.feedback.email.length != 0) {
	                if (reEmailCheck.test($scope.feedback.email))
	                    $('#feedbackEmailReq').slideUp();
	                else {
	                    $('#feedbackEmailReq').html('<span class="required">*</span>Введите Email в корректном формате');
	                    $('#feedbackEmailReq').slideDown();
	                }
	            }
	        }

	        if ($scope.feedback.message.length == 0) {
	            $('#feedbackMessageReq').slideDown();
	        } else {
	            $('#feedbackMessageReq').slideUp();
	        }

	        if ($scope.feedback.name.length==0 || $scope.feedback.email.length==0 || $scope.feedback.message.length==0) {
    			alertify.error('Заполните корректно все поля помеченные звездочкой')
    			return;
    		}

	        $http.post(CONFIG.REST_ADDRESS+'/api/feedbacks', $scope.feedback)
		        .success(function(resp, status){
		            alertify.success('Спасибо! Ваше обращение успешно отправлено!');
		        })
		        .error(function (resp, status){
		            alertify.error("Во время отправки заявки возникла ошибка. Повторите попытку позднее...");
		        });
    	} 

});