angular.module('crmApp')
    .controller('ResetAdminCtrl', function ($scope, $http, CONFIG, $routeParams) {
        var onSave = function(password){
            $http.put(CONFIG.REST_ADDRESS+'/api/users/'+$routeParams.id+'/password', {password: password})
                .success(function(resp){
                    alertify.success('Новый пароль сохранен!');
                })
                .error(function(resp) {
                	alertify.error(resp.message);
                })
        };
        React.render(React.createElement(ResetAdmin, {onSave: onSave}), document.getElementById('reset-admin'));
    });
