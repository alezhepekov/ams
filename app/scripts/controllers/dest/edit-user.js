'use strict';

angular.module('crmApp')
    .controller('EditUserCtrl', function ($scope, $q, $location, $timeout, $routeParams, $http, CONFIG, Organization, User, Project, Utils) {
        $scope.cloudTokenExists = false;

        $scope.cloud = {
            login : "",
            password : ""
        };

        var userPromise = $q.all({
            user: User.query({id: $routeParams.id}).$promise,
            orgs: Utils.getOrganizations(), //Organization.query().$promise,
            projects: Utils.getProjects() //Project.query().$promise,
        });

        var onCancel = function(){
            $timeout(function(){
                $location.path('/admin/users');
                $scope.$apply();
            });
        };

        var onSuccSave = function(resp){
            alertify.success('Сохранено!');
            $timeout(function(){
                $location.path('/admin/users');
                $scope.$apply();
            });
        };

        var onError = function(resp){
            if(resp.status === 400) {
                alertify.error(resp.data.message);
            }
        }

        var onSave = function (user) {
            user = new User(user);
            user.organizationId = Utils.getOrganizations()[0]._id;
            User.update({id: user._id}, user).$promise
            .then(onSuccSave, onError);
        };

        var succ = function (data){
            $scope.cloudTokenExists = data.user.cloudToken;
            React.render(React.createElement(NewUserForm, {onSave: onSave, onCancel: onCancel, 
                organizations: data.orgs, user: data.user, projects: data.projects}),
                document.getElementById('new-user-form'));
        };

        $scope.disconnectFromCloud = function() {
            userPromise.then(function(data) {
                data.user.cloudToken = "";
                var user = new User(data.user);
                User.update({id: user._id}, user).$promise
                    .then(function() {
                        $scope.cloud.login = "";
                        $scope.cloud.password = "";
                        $scope.cloudTokenExists = false;
                        alertify.success('Сохранено!')
                    }, onError);
            });
        };

        $scope.connectToCloud = function() {

            if (!$scope.cloud.login || !$scope.cloud.password) {
                alertify.error('Введите учетные данные для авторизации на Fincase Cloud!');
                return;
            }

            $http({method: 'POST', url: CONFIG.CLOUD_ADDRESS + '/oauth2/token',
                transformRequest: function(obj) {
                    var str = [];
                    for(var p in obj)
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data : {
                    username : $scope.cloud.login,
                    password : $scope.cloud.password,
                    grant_type : 'password',
                    client_id : CONFIG.CLOUD_CLIENT_ID
                },
                headers:{
                    'Accept': 'application/json'
                }})
                .success(function(response, status, headers, config) {
                    userPromise.then(function(data) {
                        data.user.cloudToken = response.access_token;
                        var user = new User(data.user);
                        User.update({id: user._id}, user).$promise
                            .then(function() {
                                $scope.cloudTokenExists = true;
                                alertify.success('Сохранено!')
                            }, onError);
                    });
                })
                .error(function(data, status, headers, config) {
                    $scope.cloudTokenExists = false;
                    alertify.error('Ошибка при проверке!');
                });
        }

        userPromise.then(succ);
    });
