'use strict';

angular.module('crmApp')
    .controller('ReportErrorsCtrl', function ($scope, $http, Project, Report, $routeParams, $q, CONFIG, ReportHelper)  {
       $scope.projectId = $routeParams.id;       
       $scope.tabData = ReportHelper.getTabsConfig($scope.projectId);
});