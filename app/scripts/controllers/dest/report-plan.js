'use strict';


angular.module('crmApp')
    .controller('ReportPlanCtrl', function ($scope, $http, Project, Report, $routeParams, $q, CONFIG, ReportHelper, RolesHelper)  {

       $scope.projectId = $routeParams.id;     

       $scope.tabData = ReportHelper.getTabsConfig($scope.projectId, RolesHelper.isCoordinator($scope.currentUser.roles));

       var dtToday = Date.create().utc(true).reset();

            var getPlan = function(status) {
            switch(status) {
                case 'заявка': return [null, null];
                case 'осмотр': return [null, null];
                case 'оценка': return [6, 14];
                case 'исправление': return [5, 13];
                case 'проверка': return [4, 12];
                case 'проверка после исправления': return [4, 12];
                case 'согласование в ДГИМ':return [0, 9];
                case 'повторное согласование в ДГИМ':return [0, 10];
                case 'согласование в СРО': return [null, 7];
                case 'исправление ошибок': return [null, 6];
                case 'повторное согласование в СРО': return [null, 4];
                case 'подготовка экспертизы':return [null, 3];
                case 'повторная экспертиза':return [null, 3];
                case 'сдача':return [null, 2];
                case 'итоговое согласование в ДГИМ':return [null, 2];
                case 'печать':return [null, 2];
                case 'доставка':return [null, 1];
                case 'выполнена': return [null, null];
            }   
            return [null, null];
        };

       $scope.getData = function(){

            Report.plan({ projectId : $scope.projectId }, function(data){

                var planThreshold = 15, series = {
                        'dgim': new Array(planThreshold)
                        ,'dgimProjects': new Array(planThreshold)
                        ,'complete': new Array(planThreshold)
                        ,'completeProjects': new Array(planThreshold)
                    };
                    
                    for (var i = 0; i<planThreshold; i++){
                        series.dgim[i]=series.complete[i]=0; series.dgimProjects[i]=[]; series.completeProjects[i]=[];
                    }

                    data.result.forEach(function(i){
                        var daysIncr = getPlan(i.status)
                        , dayIdx;
                        
                        if (daysIncr[0] !== null){
                            dayIdx = Date.create(i.taskStartedAt).addDays(daysIncr[0]).daysSince(dtToday);
                            if (dayIdx>=0 && dayIdx<planThreshold) {
                                series.dgim[dayIdx]++;
                                series.dgimProjects[dayIdx].push(i.projectNo);
                            } else {
                                console.log('reportPlan: dgim index out of scope', dayIdx, i);
                            }
                        }

                        if (daysIncr[1] !== null){
                            dayIdx = Date.create(i.taskStartedAt).addDays(daysIncr[1]).daysSince(dtToday);
                            if (dayIdx>=0 && dayIdx<planThreshold) {
                                series.complete[dayIdx]++;
                                series.completeProjects[dayIdx].push(i.projectNo);
                            } else {
                                console.log('reportPlan: completed index out of scope', dayIdx, i);
                            }
                        }

                    });


                var chartData = [{
                            name: 'Согласование в ДГИМ',
                            pointInterval: 24 * 3600 * 1000,
                            pointStart: dtToday.utc().getTime(),
                            data: series.dgim,
                            options: series.dgimProjects
                        }
                        ,{
                            name: 'Выполнено',
                            pointInterval: 24 * 3600 * 1000,
                            pointStart: dtToday.utc().getTime(),
                            data: series.complete,
                            options: series.completeProjects
                        }];

                        $('#chartPlan').highcharts({
                            chart: {
                                type: 'areaspline'
                                ,zoomType: 'x'
                            },
                            title: {
                                text: ''
                            },
                            subtitle: {
                                text: document.ontouchstart === undefined ?
                                    'Нажимайте на статусы в легенде для фильтрации. Выделите область внутри графика для увеличения масштаба' :
                                    'Раздвиньте область внутри графика для увеличения масштаба'
                            },
                            xAxis: {
                                type: 'datetime',
                                minRange: 1 * 24 * 3600000, // 1 days
                                maxZoom: 48 * 3600 * 1000, // 2 days
                                tickInterval: 24 * 3600 * 1000
                            },
                            yAxis: {
                                min: 0,
                                title: {
                                    text: 'План'
                                }
                            },
                            series: chartData,
                            plotOptions: {
                                series: {
                                    stacking: 'normal'
                                }
                            }
                        });

            });

        };

       $scope.getData();
});