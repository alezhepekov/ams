'use strict';

angular.module('crmApp')
    .controller('NewOrgCtrl', function ($scope, Organization, $q, $location, $timeout) {
        var onCancel = function(){
            $timeout(function(){
                $location.path('/admin/orgs');
                $scope.$apply();
            });
        };
        var onSuccSave = function(){
            alertify.success('Сохранено!');
            $timeout(function(){
                $location.path('/admin/orgs');
                $scope.$apply();
            });
        };

        var onError = function (resp){
            if(resp.status === 400) {
                alertify.error(resp.data.message);
            }
        };

        var onSave = function (org) {
            org = new Organization(org);
            Organization.save(org).$promise.then(onSuccSave, onError);

        };

        React.render(React.createElement(NewOrgForm, {onSave: onSave, onCancel: onCancel}),
            document.getElementById('new-org-form'));

    });
