'use strict';
angular.module('crmApp')
    .controller('MainCtrl', function ($scope, $location, $rootScope, Project, User, $q) {

        $rootScope.project = null;

        var promise = $q.all({
            projects: Project.query().$promise,
            users: User.query().$promise
        });

        var succ = function(data){
            var projects = data.projects.projects;
            $rootScope.users = data.users;
            var onSelect = function (project){
                $rootScope.project = project;
                $location.path("/projects/"+project._id);
                $scope.$apply();
            };

            var onCreate = function(){
                $location.path('/new-project');
                $scope.$apply();
            };

            var onRemove = function(project){
                alertify.confirm('Вы уверены, что хотитие удалить данный поект?',function(e){
                    if(e){
                        Project.remove({id: project._id});
                        projects = projects.filter(function(p){
                            return p._id !== project._id;
                        });
                        renderProjectList(projects);
                    }
                })
            };

            var renderProjectList = function(projects){
                React.render(React.createElement(ProjectsList, {currentUser: $scope.currentUser, projects: projects, onSelect: onSelect, 
                    onRemove: onRemove, onNewClick: onCreate}),
                    document.getElementById('projects-select'));
            }

            renderProjectList(projects);
        };

        promise.then(succ);
    });
