'use strict';
angular.module('crmApp')
  .controller('Tv2Ctrl', function ($scope, $http, $timeout, CONFIG) {
        var loadTv2 = function () {
            $http({url: CONFIG.REST_ADDRESS + "/api/tv2s",
                method: "GET",
                contentType: "application/json"})
                .success(function(data){

                    for(var i=0; i<data.length; i++)
                        console.log(data[i]);

                    $("#host").html('');
                    $('#fp-nav').remove();

                    var sc = Math.floor(data.length / 2);
                    var scc = data.length - sc*2;

                    var t="";
                    for(var i=0; i<sc; i++)
                    {
                        t+='<div class="section" id="section' + i + '">';
                        for(var j=0; j<2; j++)
                        {
                            t+='<div class="col-md-4" style="width: 100%" >';
                            t+='<div class="panel panel-'+data[i*2+j].cssclass+'" style="height: 48%;">'; //*2+j
                            t+='<div class="panel-heading">';
                            t+='<span class="badge">'+data[i*2+j].AgreementNo+'</span>';
                            t+='<span class="pull-right"><i class="fa fa-clock-o margin-right-sm"></i><span>'+data[i*2+j].PlannedReadyDateTime+'</span></span>';
                            t+='</div>';
                            t+='<div class="panel-body">';
                            t+='<h4 class="text-'+data[i*2+j].cssclass+'">'+data[i*2+j].Address+'</h4>';
                            t+='<small class="pull-left label margin-right label-'+data[i*2+j].cssclass+'"><i class="fa fa-rocket margin-right-sm"></i><span>'+data[i*2+j].Started+'</span></small>';
                            t+='<p class="text-'+data[i*2+j].cssclass+'">'+data[i*2+j].status+'</p>';
                            t+='<div class="panel">';
                            t+='<span class="label label-info margin-right-sm"><i class="fa fa-user margin-right-sm"></i><span>'+data[i*2+j].CoordManager+'</span></span>';
                            t+='<span>';
                            var ar = data[i*2+j].Appraiser.split(';');
                            for (var a=0; a<ar.length; a++) {
                                t+='<span class="label label-primary margin-left-sm">'+ar[a]+'</span>';
                            }
                            t+='</span>';
                            t+='</div>';
                            t+='</div>';
                            t+='<div class="timeline-'+(i*2+j)+'"></div>';
                            t+='</div>';
                            t+='</div>';
                        }
                        t+='</div>';
                    }

                    if (scc > 0) {
                        t += '<div class="section" id="section' + sc + '">';
                        t += '<div class="col-md-4" style="width: 100%" >';
                        t += '<div class="panel panel-' + data[data.length - 1].cssclass + '" style="height: 48%;">';
                        t += '<div class="panel-heading">';
                        t += '<span class="badge">' + data[data.length - 1].AgreementNo + '</span>';
                        t += '<span class="pull-right"><i class="fa fa-clock-o margin-right-sm"></i><span>' + data[data.length - 1].PlannedReadyDateTime + '</span></span>';
                        t += '</div>';
                        t += '<div class="panel-body">';
                        t += '<h4 class="text-' + data[data.length - 1].cssclass + '">' + data[data.length - 1].Address + '</h4>';
                        t += '<small class="pull-left label margin-right label-' + data[data.length - 1].cssclass + '"><i class="fa fa-rocket margin-right-sm"></i><span>' + data[data.length - 1].Started + '</span></small>';
                        t += '<p class="text-' + data[data.length - 1].cssclass + '">' + data[data.length - 1].status + '</p>';
                        t += '<div class="panel">';
                        t += '<span class="label label-info margin-right-sm"><i class="fa fa-user margin-right-sm"></i><span>' + data[data.length - 1].CoordManager + '</span></span>';
                        t += '</div>';
                        t += '</div>';
                        t += '<div class="timeline-' + (data.length - 1) + '"></div>';
                        t += '</div>';
                        t += '</div>';
                        t += '</div>';
                    }

                    $('#host').append(t);

                    for(var i=0; i<data.length; i++) {
                            $('.timeline-'+i).multiTimeline({
                                start: data[i].graphstartdate,
                                end:  data[i].graphenddate,
                                maxLabelCount: 20,
                                mousewheelPan: false,
                                mousewheelZoom: false,
                                data: data[i].ntasks
                            });
                    }

                    $('#host').fullpage({navigation: true,
                        navigationPosition: 'left',
                        continuousVertical: true,
                        autoScrolling: true,
                        scrollingSpeed: 3000
                    });

                    $timeout(loadTv2, 300000);
                })
        }

        var autoScrol = function() {
            $.fn.fullpage.moveSectionDown();
            $timeout(autoScrol, 30000);
        }

        angular.element(document).ready(function () {
            loadTv2();

            $timeout(autoScrol, 30000);
        });
  });
