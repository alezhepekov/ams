'use strict';

angular.module('crmApp')
  .controller('FormsCtrl', function ($scope, Template) {
        Template.get(function(promise){
            var links = [];

            promise.templates.forEach(function(t){
                if(t.type === 'form'){
                    links.push({id: t._id, text: t.name});
                }
            });

            React.render(React.createElement(LinksList, {items: links, title: "Список форм"}), document.getElementById('forms'));
        });
  });
