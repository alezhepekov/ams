'use strict';

angular.module('crmApp')
    .controller('ReportPropExportCtrl', function ($scope, $http, $filter, $routeParams, Project, Report, $q, CONFIG, CsvExporter, ReportHelper, RolesHelper) {
       
        var now = ( new Date() ).toISOString();
        var startYear = new Date(now.getFullYear(), 0, 1);
         
         $scope.startDate = $filter('date')(startYear,'dd.MM.yyyy');
         $scope.endDate = $filter('date')(now,'dd.MM.yyyy');

        var currentUser = $scope.currentUser;
        $scope.projectId = $routeParams.id;
        $scope.tabData = ReportHelper.getTabsConfig($scope.projectId, RolesHelper.isCoordinator($scope.currentUser.roles));

        $scope.getReport = function(){

             if ($scope.projectId == undefined)
                return;

            Report.export(
                { 
                    projectId : $scope.projectId                    
                },
                {
                    startDate : $scope.startDate,
                    endDate: $scope.endDate
                }
                , function(data){
                        if(data.$resolved){                            
                           
                            if (data.result.length) {

                            alertify.success("Отчет успешно сформирован");

                            try {

                                var now = ( new Date() ).toISOString();
                                var saveTo = 'Export-'+ now.getFullYear() + now.getMonth() + now.getDate() +'.csv';

                                var exporterConfig = [
                                    {path:'data.projectNo',label:'Номер'}
                                    ,{path:'data.title',label:'Адрес'}
                                    ,{path:'data.description',label:'Контакты'}
                                    ,{path:'data.type',label:'Тип'}
                                    ,{path:'data.area',label:'Площадь'}
                                    ,{path:'data.floor',label:'Этаж'}
                                    ,{path:'data.status',label:'Статус'}
                                    ,{path:'data.projectNoCRM',label:'Номер CRM'}
                                    ,{path:'data.marketPrice',label:'Стоимость'}
                                    ,{path:'data.marketPricePerSqm',label:'Стоимость кв.м'}
                                    ,{path:'data.landMarketPrice',label:'Стоимость з/у'}
                                    ,{path:'data.currentUse',label:'Тек. использование'}
                                    ,{path:'data.placement',label:'Линия'}
                                    ,{path:'data.separateEntrance',label:'Отд. вход'}
                                    ,{path:'data.entrancePlacement',label:'Расположение входа'}
                                    ,{path:'data.condition',label:'Отделка'}
                                    ,{path:'data.municipDistrict',label:'Округ'}
                                    ,{path:'data.municipRegion',label:'Район'}
                                    ,{path:'data.actors.appraiser.name',label:'Оценщик'}
                                    ,{path:'data.actors.supervisor.name',label:'Руководитель'}
                                    ,{path:'data.coords.0',label:'Широта'}
                                    ,{path:'data.coords.1',label:'Долгота'}
                                    ,{path:'data.fileCaptions',label:'Файлы'}
                                    ,{path:'data.files.0.caption',label:'Ссылка 1 имя'}
                                    ,{path:'data.files.0.url',label:'Ссылка 1 адрес'}
                                    ,{path:'data.files.1.caption',label:'Ссылка 2 имя'}
                                    ,{path:'data.files.1.url',label:'Ссылка 2 адрес'}
                                    ,{path:'data.files.2.caption',label:'Ссылка 3 имя'}
                                    ,{path:'data.files.2.url',label:'Ссылка 3 адрес'}
                                    ,{path:'data.files.3.caption',label:'Ссылка 4 имя'}
                                    ,{path:'data.files.3.url',label:'Ссылка 4 адрес'}
                                    ,{path:'data.files.4.caption',label:'Ссылка 5 имя'}
                                    ,{path:'data.files.4.url',label:'Ссылка 5 адрес'}
                                    ,{path:'data.files.5.caption',label:'Ссылка 6 имя'}
                                    ,{path:'data.files.5.url',label:'Ссылка 6 адрес'}
                                    ,{path:'data.files.6.caption',label:'Ссылка 7 имя'}
                                    ,{path:'data.files.6.url',label:'Ссылка 7 адрес'}
                                    ,{path:'data.files.7.caption',label:'Ссылка 8 имя'}
                                    ,{path:'data.files.7.url',label:'Ссылка 8 адрес'}
                                    ,{path:'createdAt.$date',label:'Дата создания'}
                                    ,{path:'createdBy.name',label:'Кто создал'}
                                    ,{path:'modifiedAt.$date',label:'Дата изменения'}
                                    ,{path:'modifiedBy.name',label:'Кто изменил'}
                                    ,{path:'_id.$oid',label:'Номер в DGI'}
                                ];                                

                                data.result = data.result.filter(function(p){
                                    return p.customFields != undefined;
                                });

                                var result = data.result.map(function(p) {                                   

                                    var customFieldsData = p.customFields.value.filter(function(x){ return x.id == '635fbd10-9a99-11e4-80e6-17c87fc056ce' });
                                    if (customFieldsData && customFieldsData.length > 0) {
                                        customFieldsData = customFieldsData[0].value;

                                        var objectType = ReportHelper.getFieldValue(customFieldsData, '580b67d0-9a93-11e4-80e6-17c87fc056ce');

                                        var currentUse = ReportHelper.getFieldValue(customFieldsData, 'bd43da30-9a96-11e4-80e6-17c87fc056ce');
                                        var placement = ReportHelper.getFieldValue(customFieldsData, '7fa2b8e0-9a96-11e4-80e6-17c87fc056ce');
                                        var separateEntrance = ReportHelper.getFieldValue(customFieldsData, '7fa2b8e0-9a96-11e4-80e6-17c87fc056ce');
                                        var entrancePlacement = ReportHelper.getFieldValue(customFieldsData, '7fa2b8e0-9a96-11e4-80e6-17c87fc056ce');
                                        var condition = ReportHelper.getFieldValue(customFieldsData, '43b54160-9a9e-11e4-80e6-17c87fc056ce');
                                        var municipDistrict = ReportHelper.getFieldValue(customFieldsData, '1e8cadc0-9a98-11e4-80e6-17c87fc056ce');
                                        var municipRegion = ReportHelper.getFieldValue(customFieldsData, '4');

                                        var projectNoCRM = ReportHelper.getFieldValue(customFieldsData, '7fa2b8e0-9a96-11e4-80e6-17c87fc056ce');
                                        var floor = ReportHelper.getFieldValue(customFieldsData, '7fa2b8e0-9a96-11e4-80e6-17c87fc056ce');
                                        var area = ReportHelper.getFieldValue(customFieldsData, '7fa2b8e0-9a96-11e4-80e6-17c87fc056ce');
                                        var cadasterNo = ReportHelper.getFieldValue(customFieldsData, '5b3e3030-9a9e-11e4-80e6-17c87fc056ce');
                                        var sroNo = ReportHelper.getFieldValue(customFieldsData, '72ea3760-9a9e-11e4-80e6-17c87fc056ce');
                                        var docNo = ReportHelper.getFieldValue(customFieldsData, '6becaa60-9a9e-11e4-80e6-17c87fc056ce');
                                        var reportDate = ReportHelper.getFieldValue(customFieldsData, '82058e70-9a9e-11e4-80e6-17c87fc056ce');
                                        var reportNo = ReportHelper.getFieldValue(customFieldsData, '7b6c8fa0-9a9e-11e4-80e6-17c87fc056ce');
                                        var marketPrice = ReportHelper.getFieldValue(customFieldsData, '905d9580-9a9e-11e4-80e6-17c87fc056ce');
                                        var landMarketPrice = ReportHelper.getFieldValue(customFieldsData, '9b945f60-9a9e-11e4-80e6-17c87fc056ce');
                                        var reportComment = ReportHelper.getFieldValue(customFieldsData, 'a3c1e0e0-9a9e-11e4-80e6-17c87fc056ce');
                                        var cadasterNoLand = ReportHelper.getFieldValue(customFieldsData, '62717330-9a9e-11e4-80e6-17c87fc056ce');
                                    }                                   

                                    return {
                                        _id : { $oid : p._id },
                                        data : {
                                                projectNo: p.number,
                                                title: p.name,
                                                description:""
                                                ,type: objectType
                                                ,area: area,
                                                floor: floor
                                                ,status: (p.status) ? p.status.title : ""
                                                ,projectNoCRM: projectNoCRM
                                                ,marketPrice: marketPrice
                                                ,marketPricePerSqm: ""
                                                ,landMarketPrice: landMarketPrice
                                                ,currentUse: currentUse
                                                ,placement: placement
                                                ,separateEntrance: separateEntrance
                                                ,entrancePlacement: entrancePlacement
                                                ,condition: condition
                                                ,municipDistrict: municipDistrict
                                                ,municipRegion: municipRegion
                                                ,actors: "?"
                                                ,coords: (p.mapAddress && p.mapAddress.point) ? [ p.mapAddress.point.lat, p.mapAddress.point.lon ] : []
                                                ,fileCaptions: ""
                                                ,files:p.documents.map(function(x){  return { url : x.link, caption : x.text } })
                                                ,createdAt: p.started
                                                ,createdBy: (p.owner && p.owner.fullName) ? p.owner.fullName : ''
                                                ,modifiedAt: ""
                                                ,modifiedBy: ""
                                                ,_id: { $oid : "" }
                                        }
                                    }
                                });

                                CsvExporter.save(saveTo, exporterConfig, result);
                                return saveTo;
                            }
                            catch(ex) {
                                console.log(ex);
                                return;
                            }
                        }
                        else{
                            alertify.error("Не найдены заявки в зданнам периоде");
                        }

                      }
                        
                    });        

        };        

});