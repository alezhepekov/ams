'use strict';

angular.module('crmApp')
  .controller('ProjectTemplateCtrl', function ($scope, Template) {
    var projectTemplate;
    var formTemplates = [];

    var functionLoadEntities = function(){
        Template.get(function(promise){
            formTemplates.length = 0;
            promise.templates.forEach(function(t){
                if(t.type === 'project'){
                    projectTemplate = t;
                } else{
                    formTemplates.push(t);
                }
            });

            if(projectTemplate){
                React.render(React.createElement(EntitiesConstructor, {templates: formTemplates, entity: projectTemplate, stepOneText: "Шаг 1: сформируйте новое поле формы проекта", stepTwoText: "Шаг 2: назовите и сохраните новое поле в форму проекта", onSave: savingHandler}), document.getElementById('entities-constructor'));
            } else {
                React.render(React.createElement(EntitiesConstructor, {templates: formTemplates, stepOneText: "Шаг 1: сформируйте новое поле формы проекта", stepTwoText: "Шаг 2: назовите и сохраните новое поле в форму проекта", onSave: savingHandler}), document.getElementById('entities-constructor'));
            }
        });
    }

    var savingHandler = function(entity){
        entity.type = 'project';
        entity.id = entity.id || uuid.v1();
        Template.save(entity, function(item){
            entity = item;
            alertify.success("Сохранено!");
        });
    }

    functionLoadEntities();
});
