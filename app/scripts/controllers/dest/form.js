'use strict';

angular.module('crmApp')
  .controller('FormCtrl', function ($scope, $routeParams, Template, $location) {
    var savingHandler = function(entity){
        entity.type = 'form';
        entity.id = entity.id || uuid.v1();
        Template.save(entity,function(data){
            entity = data;
            alertify.success('Сохранено!');
        });
        $location.path('admin/forms');
    }

    Template.get(function(promise){
        var templates = promise.templates;
        if($routeParams.id){
            var entity;
            templates.forEach(function(t){
                if(t._id === $routeParams.id){
                    entity = t;
                }
            });

            React.render(React.createElement(EntitiesConstructor, {templates: templates, entity: entity, onSave: savingHandler}), document.getElementById('form-constructor'));
        } else {
            React.render(React.createElement(EntitiesConstructor, {templates: templates, onSave: savingHandler}), document.getElementById('form-constructor'));
        }
    });
  });
