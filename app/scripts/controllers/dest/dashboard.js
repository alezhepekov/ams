"use strict";

function UTCToLocalTime( utc ) {
    var timezoneOffset = ( new Date() ).getTimezoneOffset() * 60000;
    return new Date( utc - timezoneOffset );
}

angular.module( "crmApp" ).filter( "filterProjectsByUserRights", function() {
    return function( items, user ) {
		if ( !items ) {
            return [];
        }

        var result = [];
        for ( var i = 0; i < items.length; i++ ) {
			var currentItem = items[i];

			if ( !user._id ) {			
				continue;
			}

			if ( user.isRoot && ( user.isRoot === true ) ) {
				result.push( currentItem );
				continue;
			}
			if ( user.seeAllProjects ) {
				result.push( currentItem );
				continue;
			}

			if (
				currentItem.manager && ( currentItem.manager._id === user._id ) 
				|| currentItem.owner && ( currentItem.owner._id === user._id ) ) {
				result.push( currentItem );
			}		
        }

        return result;
    };
});

angular.module( "crmApp" ).filter( "filterTasksByUserRights", function() {
    return function( items, user, project ) {
		if ( !items ) {
            return [];
        }

        var result = [];
        for ( var i = 0; i < items.length; i++ ) {
			var currentItem = items[i];

			if ( !user._id ) {			
				continue;
			}

			if ( user.isRoot && ( user.isRoot === true ) ) {
				result.push( currentItem );
				continue;
			}
			if ( user.seeAllProjects ) {
				result.push( currentItem );
				continue;
			}

            if (
				project.manager && ( project.manager._id === user._id ) 
				|| project.owner && ( project.owner._id === user._id ) ) {
				result.push( currentItem );
                continue;
			}	
			
			if (
				currentItem.executor && ( currentItem.executor._id === user._id ) ) {
				result.push( currentItem );
			}		
        }

        return result;
    };
});

angular.module("crmApp")
.controller("DashboardController", function ($rootScope, $scope, $routeParams, $q, $window,  $filter, $location, User, Project, Utils, CONFIG) {
	$scope.currentUser = {};  

	if ( window.localStorage.profile ) {
        $scope.currentUser = JSON.parse( window.localStorage.profile );       
    }
    else {    
        window.location = "#/login";
		return;
    }

	$scope.users = [];
	$scope.projects = [];

    $scope.availableProjects = [];
    $scope.totalTaskCount = 0;

    $scope.availableTasks = [];

	$scope.toLocalDateString = function( date ) {		
		if ( !date ) {
			return null;
		}
		
		var timestampString = UTCToLocalTime( Date.parse( date ) ).toISOString().replace( /T/, " " ).replace( /Z/, "" ).replace( /\..+/, "" );
		timestampString = timestampString.replace( /(\d+)-(\d+)-(\d+)/, "$3.$2.$1" );
		return timestampString;
	};

    $scope.updateData = function() {       
        $scope.availableProjects = $filter( "filterProjectsByUserRights" )( $scope.projects, $scope.currentUser );        

        $scope.totalTaskCount = 0;    

        for ( var i = 0; i < $scope.projects.length; i++ ) {
            var currentProject = $scope.projects[i];

            for ( var j = 0; j < currentProject.tasks.length; j++ ) {
                var currentTask = currentProject.tasks[j];

                if ( 
                    currentProject.manager && ( currentProject.manager._id === $scope.currentUser._id ) 
                    || currentProject.owner && ( currentProject.owner._id === $scope.currentUser._id )
                    || currentTask.executor && ( currentTask.executor._id === $scope.currentUser._id ) ) {
                    $scope.availableTasks.push(
                        {
                            projectID: currentProject._id,
                            id: currentTask.id,
                            title: currentTask.title
                        }
                    );
                    $scope.totalTaskCount++;
                }
            }
        }
    }
	$q.all({
		users: User.query( {} ).$promise,
		projects: Project.query( {} ).$promise
	})
	.then( function( data ) {	
		$scope.users = data.users.users;
		$scope.projects = data.projects.projects;

        $scope.updateData();
	});
});
