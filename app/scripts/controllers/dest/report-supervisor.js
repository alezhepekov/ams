'use strict';

angular.module('crmApp')
    .controller('ReportSupervisorCtrl', function ($scope, $http, Project, Report, $routeParams, $q, CONFIG, ReportHelper, RolesHelper)  {


       $scope.projectId = $routeParams.id;
       $scope.projectStatuses = [];

       $scope.tabData = ReportHelper.getTabsConfig($scope.projectId, RolesHelper.isCoordinator($scope.currentUser.roles));

       $scope.getData = function(status, supervisor){
            var st = $scope.statuses.filter(function(x){ return x._id.status == status });
            if (st.length > 0){
               var sv = st[0].supervisors.filter(function(x) { return x.supervisor == supervisor});
               if (sv.length > 0){
                    return sv[0].proposals;
               }
            }

            return "";
       };

       $scope.getTotalBySupervisor = function(supervisor){
        var result = 0;

        $scope.statuses.forEach(function(s){
                                s.supervisors.forEach(function(sv){
                                    if (sv.supervisor == supervisor){
                                        result += sv.proposals;
                                    }                                    
                                });
                            });

            return result;
       };

       $scope.getTotalByStatus = function(status){
        var result = 0;

        $scope.statuses.forEach(function(s){
                    if (s._id.status == status){
                        s.supervisors.forEach(function(sv){
                            result += sv.proposals;
                        });
                    }
                });

            return result;
       };
       

        $scope.loadProject = function(){

            if ($scope.projectId == undefined)
                return;        

            Report.supervisor({ projectId : $scope.projectId }, function(data){
                        if(data.$resolved){
                           $scope.statuses = data.data;

                           Project.get({id: $scope.projectId}, function(projData){
                                
                                $scope.projectStatuses = projData.statuses;

                                $scope.supervisors = [];
                                $scope.statuses.forEach(function(s){
                                    s.supervisors.forEach(function(sv){
                                        var sup = $scope.supervisors.filter(function(x){ return x == sv.supervisor});
                                        if (sup.length == 0){
                                            $scope.supervisors.push(sv.supervisor);
                                        }
                                    });
                                });
                           });                           
                        }
                    });        
    }

    $scope.loadProject();

})