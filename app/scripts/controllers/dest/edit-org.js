'use strict';

angular.module('crmApp')
    .controller('EditOrgCtrl', function ($scope, Organization, $q, $location, $timeout, $routeParams) {            
        var promise = Organization.get({id: $routeParams.id}).$promise;
        
        promise.then(function(org){
            
            var onCancel = function(){
                $timeout(function(){
                    $location.path('/admin/orgs');
                    $scope.$apply();
                });
            };
            
            var onSave = function (org) {
                org = new Organization(org);
                org.$update({id:org._id});
                $timeout(function(){
                    $location.path('/admin/orgs');
                    $scope.$apply();
                });
            };

            React.render(React.createElement(NewOrgForm, {onSave: onSave, onCancel: onCancel, organization: org}),
                document.getElementById('new-org-form'));
        });
    });
