'use strict';

angular.module('crmApp')
  .controller('AdminCtrl', function ($scope, $resource, $http,  Project) {
    var saveProject = function(projectObj){
        var project = new Project(projectObj);
        project.$update({id: project._id},function(){
            loadProjects();
            alertify.success('Сохранено!');
        });
    }

    var loadProjects = function(){
        Project.query(function(promise){
            if(promise.$resolved){
                React.render(React.createElement(AdminPanel, {onSave: saveProject, projects: promise.projects}), document.getElementById('projects'));
            }
        });
    }

    loadProjects();
  });
