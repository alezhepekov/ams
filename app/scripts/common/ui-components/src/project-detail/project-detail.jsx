/**
 * @jsx React.DOM
 */
(function(){
    'use strict';
    var OrderDropdown = React.createClass({
        getInitialState: function(){
            return this.props;
        },
        onReversedClick: function(e){
            var field = this.state.sortedBy;
            var reversed = !this.state.reversed;
            this.setState({reversed: reversed, sortedBy: field});
            if(this.props.onChange){
                this.props.onChange(field, reversed);
            }
        },
        onSortedByChange: function(e){
            var field = e.target.value;
            var reversed = this.state.reversed;
            this.setState({reversed: reversed, sortedBy: field});
            if(this.props.onChange){
                this.props.onChange(field, reversed);
            }
        },
        renderReverseOrderButton: function(){
            var classes = 'btn btn-default';
            if(this.state.reversed){
                classes = 'btn btn-success active';
            }
            return  <span className='input-group-btn'>
                        <button type='button' onClick={this.onReversedClick} className={classes}>
                            <span className='glyphicon glyphicon-sort' aria-hidden='true'></span>
                        </button>
                    </span>;
        },
        render: function () {
            return  <div>
                        <div>
                            <label>Упорядочить</label>
                        </div>
                        <div className='input-group' role='group'>
                            {this.renderReverseOrderButton()}
                            <select className='form-control select' value={this.state.sortedBy}
                                onChange={this.onSortedByChange}>
                                <option value='name'>По названию</option>
                                <option value='started'>По дате начала</option>
                            </select>
                        </div>
                    </div>;
        }
    });

    var SearchField = React.createClass({
        onSearch: function(e){
            e.preventDefault();
            var q = this.refs.q.getDOMNode().value;
            this.props.onSubmit(q);
        },
        render: function(){
            return <form>
                        <label>Искать</label>
                        <div className="input-group">
                            <input type="text" placeholder="Запрос" defaultValue={this.props.value} className="form-control" ref="q"/>
                            <span className="input-group-btn">
                                <button type="submit" className="btn btn-primary" onClick={this.onSearch}>
                                    <span className="glyphicon glyphicon-search" />
                                </button>
                            </span>
                        </div>
                    </form>
        }
    });

    var UserDropdown = React.createClass({
        getInitialState: function(){
            return {
                selected: this.props.selected
            };
        },
        onSelect: function(e){
            e.preventDefault();
            if(this.props.onChange){
                this.props.onChange(e.target.value);
            }
        },
        renderOption: function(user){
            return <option value={user._id}>{user.firstName} {user.lastName}</option>
        },
        render: function(){
            return <div>
                        <label>{this.props.label}</label>
                        <div>
                            <select value={this.state.selected} onChange={this.onSelect} className="form-control">
                                <option value="any">Любой</option>
								<option value="notdefined">Не назначен</option>
                                {this.props.users.map(this.renderOption)}
                            </select>
                        </div>
                    </div>
        }
    });

	var UserDropdownCreator = React.createClass({
        getInitialState: function(){
            return {
                selected: this.props.selected
            };
        },
        onSelect: function(e){
            e.preventDefault();
            if(this.props.onChange){
                this.props.onChange(e.target.value);
            }
        },
        renderOption: function(user){
            return <option value={user._id}>{user.firstName} {user.lastName}</option>
        },
        render: function(){
            return <div>
                        <label>{this.props.label}</label>
                        <div>
                            <select value={this.state.selected} onChange={this.onSelect} className="form-control">
                                <option value="any">Любой</option>
								{this.props.users.map(this.renderOption)} 
                            </select>
                        </div>
                    </div>
        }
    });
	
    var StatusDropdown = React.createClass({
        getInitialState: function(){
            return {
                selected: this.props.selected
            };
        },
        onSelect: function(e){
            e.preventDefault();
            if(this.props.onChange){
                this.props.onChange(e.target.value);
            }
        },
        renderOption: function(st){
            return <option value={st.title} className={st.className}>{st.title}</option>
        },
        render: function(){
            if (this.props.disabled) {
                return <div />;
            }
            else if (this.props.anyStatus) {
                return <div>
                            <label>Статус</label>
                            <div>
                                <select value={this.state.selected} onChange={this.onSelect} className="form-control status-select">
                                    <option value="undefined">Любой</option>
                                    {this.props.statuses.map(this.renderOption)}
                                </select>
                            </div>
                        </div>
            } else {
                return <div>
                            <label>Статус</label>
                            <div>
                                <select value={this.state.selected} onChange={this.onSelect} className="form-control status-select">
                                    {this.props.statuses.map(this.renderOption)}
                                </select>
                            </div>
                        </div>
            }
        }
    });

    var ProjectTypeDropdown = React.createClass({
        getInitialState: function(){
            return {
                selected: this.props.selected
            };
        },
        onSelect: function(e){
            e.preventDefault();
            if(this.props.onChange){
                this.props.onChange(e.target.value);
            }
        },
        renderOption: function(projType){
            return <option value={projType.id}>{projType.name}</option>
        },
        render: function(){
            return <div>
                <label>Тип проекта</label>
                <div>
                    <select value={this.state.selected} onChange={this.onSelect} className="form-control status-select">
                        <option value="undefined">Любой</option>
                        {this.props.types.map(this.renderOption)}
                    </select>
                </div>
            </div>
        }
    });

    var DateInput = React.createClass({
        render: function() {
            return <div class="col-md-12">
                        <DfDateInput title={this.props.label} oldState={this.props.dateState} 
                            onChange={this.props.onChange} onChangeAdditional={this.props.onChange} onDateInvalidInput={this.props.onDateInvalidInput}
                            sm12={true}/>
                    </div>
        }
    });

    var ProjectDetail = React.createClass({
        onNewButtonClick: function(){
            if(this.props.onNewButtonClick){
                this.props.onNewButtonClick(this.props.project);
            }
        },
        onPaginationClick: function(e){
            e.preventDefault();
            var page = e.target.dataset.n;
            if(page != this.props.currentPage){
                this.props.onCriteriaChange(page, {field:this.props.sort, reversed: this.props.reversed}, this.props.filter)
            }
        },
        onSortChange: function(field, reversed){
            this.props.onCriteriaChange(1, {field:field, reversed:reversed}, this.props.filter);
        },
        onSearch: function(q){
            var f = this.props.filter;
            f.search = q;
            this.props.onCriteriaChange(1, {field:this.props.sort, reversed: this.props.reversed}, f);

        },
        onManagerSelect: function(id){
            var f = this.props.filter;
            f.manager = id;
            this.props.onCriteriaChange(1, {field:this.props.sort, reversed: this.props.reversed}, f);
        },
        onOwnerSelect: function(id){
            var f = this.props.filter;
            f.owner = id;
            this.props.onCriteriaChange(1, {field:this.props.sort, reversed: this.props.reversed}, f);
        },
        onCoordinatorSelect: function(id){
            var f = this.props.filter;
            f.coordinator = id;
            this.props.onCriteriaChange(1, {field:this.props.sort, reversed: this.props.reversed}, f);
        }, 
        onExecutorSelect: function(id){
            var f = this.props.filter;
            f.executor = id;
            this.props.onCriteriaChange(1, {field:this.props.sort, reversed: this.props.reversed}, f);
        },               
        onStatusSelect: function(type){
            var f = this.props.filter;
            f.projectType = type;
            this.props.onCriteriaChange(1, {field:this.props.sort, reversed: this.props.reversed}, f);
        },
        onProjectTypeSelect: function(type){
            var f = this.props.filter;
            f.projectType = type;
            this.props.onCriteriaChange(1, {field:this.props.sort, reversed: this.props.reversed}, f);
        },
        onDateCreatedStartChange: function(date){
            var f = this.props.filter;
            f.dateCreatedStart = date;
            this.props.onCriteriaChange(1, {field:this.props.sort, reversed: this.props.reversed}, f);
        },
        onDateCreatedEndChange: function(date){
            var f = this.props.filter;
            f.dateCreatedEnd = date;
            this.props.onCriteriaChange(1, {field:this.props.sort, reversed: this.props.reversed}, f);
        },
        onDateFinishedStartChange: function(date){
            var f = this.props.filter;
            f.dateFinishedStart = date;
            this.props.onCriteriaChange(1, {field:this.props.sort, reversed: this.props.reversed}, f);
        },
        onDateFinishedEndChange: function(date){
            var f = this.props.filter;
            f.dateFinishedEnd = date;
            this.props.onCriteriaChange(1, {field:this.props.sort, reversed: this.props.reversed}, f);
        },
        onDateCreatedStartInvalidInput: function(date){
            var f = this.props.filter;
            f.dateCreatedStart = undefined;
            this.props.onCriteriaChange(1, {field:this.props.sort, reversed: this.props.reversed}, f);
        },
        onDateCreatedEndInvalidInput: function(date){
            var f = this.props.filter;
            f.dateCreatedEnd = undefined;
            this.props.onCriteriaChange(1, {field:this.props.sort, reversed: this.props.reversed}, f);
        },
        onDateFinishedStartInvalidInput: function(date){
            var f = this.props.filter;
            f.dateFinishedStart = undefined;
            this.props.onCriteriaChange(1, {field:this.props.sort, reversed: this.props.reversed}, f);
        },
        onDateFinishedEndInvalidInput: function(date){
            var f = this.props.filter;
            f.dateFinishedEnd = undefined;
            this.props.onCriteriaChange(1, {field:this.props.sort, reversed: this.props.reversed}, f);
        },
        onUpdateForYear: function(year) {
            var f = this.props.filter;
            f.dateCreatedStart = {id: 1, value: {day: 1, month: 1, year: year}};
            f.dateCreatedEnd = {id: 2, value: {day: 31, month: 12, year: year}};
            this.props.onCriteriaChange(1, {field:this.props.sort, reversed: this.props.reversed}, f);
        },
        onUpdateForYear2014: function() {
            this.onUpdateForYear(2014);
        },
        onUpdateForYear2015: function() {
            this.onUpdateForYear(2015);
        },
        onUpdateAllOrgs: function() {
            this.onUpdateOrg('');
        },
        onUpdateAbnConsultOrg: function() {
            this.onUpdateOrg('АБН');
        },
        onUpdateBusinessKrugOrg: function() {
            this.onUpdateOrg('Бизнес-КРУГ');
        },
        onUpdateMosingprojectOrg: function() {
            this.onUpdateOrg('Мосинжпроект');
        },
        onUpdateOrg: function(org) {
            var f = this.props.filter;
            f.org = org;
            this.props.onCriteriaChange(1, {field:this.props.sort, reversed: this.props.reversed}, f);
        },
        /**
         * Renders list of proposals
         */
        renderProposals: function(proposal){
            return <ProposalSnippet proposal={proposal} executors={this.props.users} onDelete={this.props.onDelete} canDeleteProposal={this.props.canDeleteProposal} orgMode={this.props.orgMode}/>;
        },
        /**
         * Renders controls for order management
         */
        renderProjectInfo: function () {
            var proposalsCount = this.props.nProposals;
            if (this.props.isCompleted) {
                return <div className='col-md-4'>
                        <div className='pull-left'>
                            <label>
                                Проектов: {proposalsCount}, страница {this.props.currentPage} из {Math.ceil(this.props.nProposals/20)}
                            </label>
                        </div>
                    </div>
            } else {
                return <div className='col-md-4'>
                        <div className='pull-left'>
                            <label>
                                Проектов: {proposalsCount}, страница {this.props.currentPage} из {Math.ceil(this.props.nProposals/20)}
                            </label>
                            <button onClick={this.onNewButtonClick} type='button' className='btn btn-primary add-proposal'>
                                <span className='glyphicon glyphicon-plus'/> Новый проект
                            </button>
                        </div>
                    </div>
            }
        },
        /**
         * Renders controls for order management
         */
        renderFilterControls: function () {
            return <div>
                    <div className="row">
                        <div className="col-md-3">
                            <OrderDropdown reversed={this.props.reversed} sortedBy={this.props.sort} onChange={this.onSortChange}/>
                        </div>
                        <div className="col-md-3">
                            <SearchField onSubmit={this.onSearch} value={this.props.filter.search}/>
                        </div>
                        <div className="col-md-3">
                            <ProjectTypeDropdown selected={this.props.filter.projectType} types={this.props.projectTypes} anyType={true}
                                onChange={this.onProjectTypeSelect}/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-3">
                            <UserDropdown label="Исполнитель" selected={this.props.filter.executor} users={this.props.users} onChange={this.onExecutorSelect}/>
                        </div>
                        <div className="col-md-3">
                            <UserDropdownCreator label="Создатель" selected={this.props.filter.owner} users={this.props.users} onChange={this.onOwnerSelect}/>
                        </div>
                    </div> 
                    <div className="row">
                        <div className="col-md-3">
                            <DateInput label="Дата создания" selected={this.props.filter.dateCreatedStart} dateState={this.props.dateCreatedStartState}
                            onChange={this.onDateCreatedStartChange} onDateInvalidInput={this.onDateCreatedStartInvalidInput}/>
                        </div>
                        <div className="col-md-3">
                            <DateInput label="&nbsp;" selected={this.props.filter.dateCreatedEnd} dateState={this.props.dateCreatedEndState}
                            onChange={this.onDateCreatedEndChange} onDateInvalidInput={this.onDateCreatedEndInvalidInput}/>
                        </div>
                        <div className="col-md-3">
                            <DateInput label="Дата завершения" selected={this.props.filter.dateFinishedStart} dateState={this.props.dateFinishedStartState}
                            onChange={this.onDateFinishedStartChange} onDateInvalidInput={this.onDateFinishedStartInvalidInput}/>
                        </div>
                        <div className="col-md-3">
                            <DateInput label="&nbsp;" selected={this.props.filter.dateFinishedEnd} dateState={this.props.dateFinishedEndState}
                            onChange={this.onDateFinishedEndChange} onDateInvalidInput={this.onDateFinishedEndInvalidInput}/>
                        </div>
                    </div>
                </div>;
        },
        renderCustomFilter: function() {
            /*if (this.props.orgMode) {
                if (this.props.isUserRootOrg) {
                    return <div className="row">
                        {this.renderFilterOrganizations()}
                    </div>
                }
            } else {*/
                return <div className="row">
                    {this.renderFilterYears()}
                </div>
            //}
        },
        renderFilterYears: function() {
            return <div className="col-md-12">
                <button onClick={this.onUpdateForYear2014} type='button' className='btn btn-primary'>
                    2014
                </button>
                <button onClick={this.onUpdateForYear2015} type='button' className='btn btn-danger'>
                    2015
                </button>
            </div>
        },
        renderFilterOrganizations: function() {
            return <div className="col-md-12">
                <button onClick={this.onUpdateAllOrgs} type='button' className='btn btn-danger'>
                    Все
                </button>
                <button onClick={this.onUpdateAbnConsultOrg} type='button' className='btn btn-success'>
                    АБН-Консалт
                </button>
                <button onClick={this.onUpdateBusinessKrugOrg} type='button' className='btn btn-info'>
                    Бизнес-КРУГ
                </button>
                <button onClick={this.onUpdateMosingprojectOrg} type='button' className='btn btn-warning'>
                    Мосинжпроект
                </button>
            </div>
        },
        /**
         * Renders header for list of proposals
         */
        renderHeading: function (){
            var title = (this.props.isCompleted) ? "Выполненные проекты" : "Проекты";
                //title += " проекта \"" + this.props.project.name +"\"";
            return <div className='panel-heading'>
                        <div className="row">
                            <div className="col-lg-8">
                                <h4 className='heading'>{title}</h4>
                            </div>
                            {this.renderProjectInfo()}
                        </div>
                        <br/>
                        <div className="row">
                            <div className="col-lg-12">
                                {this.renderFilterControls()}
                            </div>
                        </div>
                     </div>;
        },
        renderItem: function(n){
            var current = this.props.currentPage;
            if((n-current > 3) ||(n-current <-3)){
                return false;
            }else{
                if(n == current){
                    return <li className="active"><a data-n={n} onClick={this.onPaginationClick}>{n}</a></li>
                }else{
                    return <li><a data-n={n} onClick={this.onPaginationClick}>{n}</a></li>
                }
            }
        },
        renderPagination: function(){
            var first = 1;
            var current = this.props.currentPage;
            var last= Math.ceil(this.props.nProposals/20);
            if(first==last || this.props.proposals.length == 0){
                return false;
            }
            var pages = [];
            for(var i=1;i<=last;i++){
                pages[i] = i;
            }
            var renderLeftSpace = function(){
                if (current > 3){
                    return <li><span>...</span></li>
                }else{
                    return false
                }
            };
            var renderRightSpace = function(){
                if (last - current > 3){
                    return <li><span>...</span></li>
                }else{
                    return false
                }
            };
            return(
            <div className="proposals-list-pagination row">
                <ul className="pagination">
                    <li><a data-n={first} onClick={this.onPaginationClick}>&laquo;</a></li>
                    {renderLeftSpace()}
                    {pages.map(this.renderItem)}
                    {renderRightSpace()}
                    <li><a data-n={last} onClick={this.onPaginationClick}>&raquo;</a></li>
                </ul>
            </div>);
        },
        /**
         * Renders component
         */
        render: function(){
            var proposals = this.props.proposals;
            return <div className='panel panel-default project-detail'>
                        {this.renderHeading()}
                        <div className='panel-body'>
                            <ul className='list-group'>
                                {proposals.map(this.renderProposals)}
                            </ul>
                            {this.renderPagination()}
                        </div>
                    </div>;
        }
	});

	this.ProjectDetail = ProjectDetail;
}).call(this);
