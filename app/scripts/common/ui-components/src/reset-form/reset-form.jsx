(function(){
    'use strict';
    var ResetAdmin = React.createClass({
        onSave: function(e){
            e.preventDefault();
            var newPass = this.refs.new.getDOMNode().value;
            var confirm = this.refs.confirm.getDOMNode().value;
            if((newPass === confirm) && (newPass.length >= 8)){
                this.props.onSave(newPass);
            }else{
                alertify.error("Пароли не совпадают или слишком короткие!");
            }
        },
        render: function(){
            return <form>
                        <div className="panel panel-default reset-form">
                            <div className="panel-heading">
                                <h1>Сброс пароля</h1>
                                <div className="pull-right">
                                    <button type="submit" className="btn btn-primary" onClick={this.onSave}>
                                        <span className="glyphicon glyphicon-floppy-save"/> Сохранить
                                    </button>
                                </div>
                            </div>
                            <div className="panel-body">
                                <div className="row">
                                    <div className="col-md-2">
                                        <label>Новый пароль</label>
                                    </div>
                                    <div className="col-md-4">
                                        <input type="password" ref="new" className="form-control"/>
                                    </div>
                                </div>
                                <br />
                                <div className="row">
                                    <div className="col-md-2">
                                        <label>Подтверждение</label>
                                    </div>
                                    <div className="col-md-4">
                                        <input type="password" ref="confirm" className="form-control"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
        }
    });
    this.ResetAdmin = ResetAdmin;
}).call(this);
