/**
 * @jsx React.DOM
 */

/**
 * EntitiesCounstructor component
 *
 * @prop {object}   entity      - Object with a collection of entities which represents custom fields for a form template
 * @prop {string}   stepOneText - Description for a first step of a creation new form process
 * @prop {string}   stepTwoText - Description for a second step of a creation new form process
 * @prop {function} onSave      - callback for a saving event handling
 * @prop {[object]} templates   - a templates' collection for selecting one of them in a lists
 */
(function(){
    'use strict';
    var types = {
        textField: 'text-field',
        listField: 'list-field',
        dateField: 'date-field',
        entityField: 'entity-field',
    };
    var subtypes = {
        emptyList: 'empty-list',
        constantList: 'constant-list',
        date: 'date',
        time: 'time',
    };

    var emptyListTypes = {
        textFieldList: 'text-fields-list',
        entitiesFieldsList: 'entities-fields-list'
    };

    var EntityForm = React.createClass({
        getInitialState: function(){
            return {
                entityType: types.textField,
                entitySubtype: '',
                entityName: '',
                emptyListType: '',
                listItems: [],
                entityId: '',
                placeholder: '',
                title: '',
                id: uuid.v1(),
            };
        },
        componentWillMount: function(){
            if(this.props.startId){
                this.setState({id: this.props.startId});
            }
        },
        onEntityTypeSelect: function(type){
            this.setState({entityType: type});
            if(type === types.listField){
                this.setState({
                    entitySubtype: subtypes.emptyList,
                    emptyListType: emptyListTypes.textFieldList
                });
            } else if(type === types.dateField){
                this.setState({entitySubtype: subtypes.date});
            } else {
                this.setState({
                    entitySubtype: '',
                    emptyListType: '',
                    entityId: '',
                    placeholder: ''
                });
            }
        },
        onEntitySubtypeSelect: function(subtype){
            this.setState({entitySubtype: subtype});
        },
        onEmptyListTypeSelect: function(emptyListType){
            this.setState({emptyListType: emptyListType});
        },
        onTextInput: function(e){
            this.setState({entityName: e.target.value});
        },
        onSave: function(){
            if(this.props.onSave){
                this.props.onSave(this.state);
                this.setState({
                    entityName: '',
                    entitySubtype: '',
                    emptyListType: '',
                    entityId: '',
                    placeholder: '',
                    title: '',
                    listItems: [],
                    id: uuid.v1()
                });
            }
        },
        renderTypesList: function(){
            return  <div className="btn-group btn-group-justified" data-toggle="buttons">
                      <label onClick={this.onEntityTypeSelect.bind(null, types.textField)} className="btn btn-info active">
                        <input type="radio" name="options" autocomplete="off" checked>Текстовое поле</input>
                      </label>
                      <label onClick={this.onEntityTypeSelect.bind(null, types.listField)} className="btn btn-info">
                        <input type="radio" name="options" autocomplete="off">Список</input>
                      </label>
                      <label onClick={this.onEntityTypeSelect.bind(null, types.entityField)} className="btn btn-info">
                        <input type="radio" name="options" autocomplete="off">Форма</input>
                      </label>
                      <label onClick={this.onEntityTypeSelect.bind(null, types.dateField)} className="btn btn-info">
                        <input type="radio" name="options" autocomplete="off">Дата</input>
                      </label>
                    </div>;
        },
        renderInputForm: function(entityName){
            return  <div className="input-group">
                        <input onChange={this.onTextInput} type="text" className="form-control" value={entityName} placeholder='Название элемента формы'></input>
                        <span className='input-group-btn'>
                            <button onClick={this.onSave} className="btn btn-default" type="button">Добавить</button>
                        </span>
                    </div>;
        },
        onTitleInput: function(e){
            this.setState({title: e.target.value});
        },
        renderTitleInput: function(entityTitle){
            return  <input onChange={this.onTitleInput} type="text" className="form-control" value={entityTitle} placeholder='Заголовок поля'></input>;

        },
        renderListTypes: function(){
            return  <div className="btn-group btn-group-justified" data-toggle="buttons">
                      <label id='list-type-1' onClick={this.onEntitySubtypeSelect.bind(null, subtypes.emptyList)} className="btn btn-default active">
                        <input type="radio" name="options" autocomplete="off" checked>Пустой список</input>
                      </label>
                      <label id='list-type-2' onClick={this.onEntitySubtypeSelect.bind(null, subtypes.constantList)} className="btn btn-default">
                        <input type="radio" name="options" autocomplete="off">Список сущностей</input>
                      </label>
                    </div>;
        },
        renderEmptyListTypes: function(){
            return  <div className="btn-group btn-group-justified" data-toggle="buttons">
                      <label id='empty-list-type-1' onClick={this.onEmptyListTypeSelect.bind(null, emptyListTypes.textFieldList)} className="btn btn-default active">
                        <input type="radio" name="options" autocomplete="off" checked>Список текстовых элементов</input>
                      </label>
                      <label id='empty-list-type-2' onClick={this.onEmptyListTypeSelect.bind(null, emptyListTypes.entitiesFieldsList)} className="btn btn-default">
                        <input type="radio" name="options" autocomplete="off">Пустой список сущностей</input>
                      </label>
                    </div>;
        },
        onTemplateSelected: function(e){
            var id = e.target.selectedOptions[0].value,
                entityId;

            this.props.templates.forEach(function(template){
                if(template.id === id){
                    entityId = id;
                }
            });
            this.setState({entityId: entityId});
        },
        onPlaceholderInput: function(e){
            this.setState({placeholder: e.target.value});
        },
        renderTemplateItem: function(template){
            return <option value={template.id}>{template.name}</option>;
        },
        renderEntitiesList: function(){
            var templates = this.props.templates || [];
            return <div className='btn-group btn-group-justified'>
                        <select onChange={this.onTemplateSelected} className='form-control'>
                            <option>Выбирите шаблон формы...</option>
                            {templates.map(this.renderTemplateItem)}
                        </select>
                   </div>;
        },
        renderEmptyListEntities: function(){
            if(this.state.emptyListType === emptyListTypes.entitiesFieldsList){
                return this.renderEntitiesList();
            }
        },
        renderDateTypes: function(){
            return  <div className="btn-group btn-group-justified" data-toggle="buttons">
                      <label onClick={this.onEntitySubtypeSelect.bind(null, subtypes.date)} className="btn btn-default active">
                        <input id='date-type-1' type="radio" name="options" autocomplete="off" checked>Дата</input>
                      </label>
                      <label onClick={this.onEntitySubtypeSelect.bind(null, subtypes.time)} className="btn btn-default">
                        <input id='date-type-2' type="radio" name="options" autocomplete="off">Время</input>
                      </label>
                    </div>;
        },
        renderPlaceholderInput: function(){
            var value = this.state.placeholder;
            return <input onChange={this.onPlaceholderInput} className="form-control" value={value} type="text" placeholder="Текст плэйсхолдера"></input>;
        },
        renderAdditionalOptions: function(){
            if(this.state.entityType === types.listField){
                return this.renderListTypes();
            } else if(this.state.entityType === types.entityField){
                return this.renderEntitiesList();
            } else if(this.state.entityType === types.dateField){
                return this.renderDateTypes();
            } else if(this.state.entityType === types.textField){
              return this.renderPlaceholderInput();
            }
        },
        renderEmptyListOptions: function(){
            if(this.state.entitySubtype === subtypes.emptyList){
                return this.renderEmptyListTypes();
            }
        },
        addListItem: function(itemName){
            var resultItems = this.state.listItems;
            resultItems.push({
                id: resultItems.length,
                value: itemName
            });
            this.setState({listItems: resultItems});
        },
        onListItemDelete: function(id){
            var resultItems = this.state.listItems.filter(function(item){
                return item.id !== id;
            });

            this.setState({listItems: resultItems});
        },
        renderAdditionalInput: function(){
            if(this.state.entitySubtype === subtypes.constantList){
                return <div>
                            <SimpleInput buttonText={'Добавить'} placeholder={'Название элемента'} onSave={this.addListItem}></SimpleInput>
                            <TagList onDelete={this.onListItemDelete} items={this.state.listItems}></TagList>
                        </div>;
            }
        },
        render: function(){
            var entityName = this.state.entityName;
            var entityTitle = this.state.title;
            var step1 = this.props.stepOneText || 'Шаг 1: сформируйте новое поле формы';
            var step2 = this.props.stepTwoText || 'Шаг 2: назовите и сохраните новое поле в форму';
            return  <div className="form-group">
                        <h4 className="entities-constructor-title">{step1}</h4>
                        {this.renderTypesList()}
                        {this.renderAdditionalOptions()}
                        {this.renderEmptyListOptions()}
                        {this.renderEmptyListEntities()}
                        {this.renderAdditionalInput()}
                        <br></br>
                        <h4 className="entities-constructor-title">{step2}</h4>
                        {this.renderTitleInput(entityTitle)}
                        {this.renderInputForm(entityName)}
                    </div>;
        }
    });

    var EnitiesList = React.createClass({
        renderEntities: function(entity){
            var id="entity-" + entity.id;
            var data = {
                id: entity.id,
                entity: entity,
                content: entity.entityName,
                labels: {
                    label: entity.entityType
                }
            };
            return <li id={id} key={uuid.v1()} className="list-group-item">
                        <ListItem editable={true} onUpdate={this.onFieldUpate} onDelete={this.onDelete} data={data}></ListItem>
                    </li>;
        },
        onDelete: function(entity){
            if(this.props.onDelete){
               this.props.onDelete(entity);
            }
        },
        onFieldUpate: function(wrappedEntity){
            if(this.props.onUpdate){
                this.props.onUpdate(wrappedEntity.entity);
            }
        },
        onFormNameSave: function(formName){
            if(this.props.onFormNameSave){
                this.props.onFormNameSave(formName);
            }
        },
        render: function(){
            var formName = this.props.initialFormName || 'Новая форма';
            return  <div className="panel panel-default">
                        <div className="panel-heading">
                            <EditableLabel onSave={this.onFormNameSave} initialValue={formName}></EditableLabel>
                        </div>
                        <div className="panel-body">
                            <ul className="list-group">
                              {this.props.entities.map(this.renderEntities)}
                            </ul>
                        </div>
                    </div>;
        }
    });

    var EntitiesConstructor = React.createClass({
        getInitialState: function(){
            return {
                entities: [],
                name: 'Новая форма'
            };
        },
        componentWillMount: function(){
            if(this.props.entity){
                this.setState({
                    entities: this.props.entity.fields,
                    name: this.props.entity.name
                });
            }
        },
        addNewEntity: function(data){
            var tmpEntities = this.state.entities.slice(0);
            tmpEntities.push(data);
            this.setState({entities: tmpEntities});
        },
        onDelete: function(entity){
            var tmpEntities = [];

            this.state.entities.forEach(function(item){
                if(item.id !== entity.id){
                    tmpEntities.push(item);
                }
            });
            this.setState({entities: tmpEntities});
        },
        onSave: function(){
            if(this.props.onSave){
                var obj;
                if(this.props.entity){
                    obj = this.props.entity;
                    obj.fields = this.state.entities;
                    obj.name = (obj.name || this.state.name) || 'Новый шаблон';
                    obj.id = obj.id || uuid.v1();
                } else {
                    obj = {
                        fields: this.state.entities.slice(0),
                        name: this.state.name,
                        id: uuid.v1()
                    };
                }
                this.props.onSave(obj);
            }
        },
        onUpdate: function(entity){
            var resultEntities = [];
            this.state.entities.forEach(function(e){
                if(e.id === entity.id){
                    resultEntities.push(entity);
                } else {
                    resultEntities.push(e);
                }
            });

            this.setState({entities: resultEntities});
        },
        onFormNameSave: function(formName){
            this.setState({name: formName});
        },
        render: function(){
            var templates = this.props.templates || [];
            return <div>
                        <EntityForm templates={templates} startId={this.state.entities.length} stepOneText={this.props.stepOneText} stepTwoText={this.props.stepTwoText} onSave={this.addNewEntity}></EntityForm>
                        <br/>
                        <br/>
                        <EnitiesList initialFormName={this.state.name} onFormNameSave={this.onFormNameSave} onUpdate={this.onUpdate} onDelete={this.onDelete} entities={this.state.entities}></EnitiesList>
                        <button onClick={this.onSave} className="btn btn-success pull-right entities-constructor-spaces" type="button">Сохранить форму</button>
                   </div>
        }
    });

    this.EntitiesConstructor = EntitiesConstructor;
}).call(this);
