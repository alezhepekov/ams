(function (){
    'use strict';
    var LoginForm = React.createClass({
        getInitialState: function () {
            return {
                login:    "",
                password: "",
                remember: false,
            };
        },
        onLoginChange: function (e) {
            var value = e.target.value;
            this.setState({login: value});
        },
        onPasswordChange: function (e) {
            var value = e.target.value;
            this.setState({password: value});
        },
        onRememberChange: function (e) {
            var value = e.target.checked;
            this.setState({remember: value});
        },
        onSignInClick: function (e){
            e.preventDefault();
            if(this.state.login === '' || this.state.password === ''){
                return;
            }
            if(this.props.onSignIn){
                this.props.onSignIn(this.state);
            }
        },
        render: function(){
            return  <div className="panel panel-default login-form">
                        <form>
                        <div className="panel-heading">
                            <h3>Авторизация</h3>
                        </div>
                        <div className="panel-body">
                            <div className="row">
                                <div className = "col-md-12">
                                    <label>Email</label>
                                    <input type="text" autocomplete="on" id="login" name="login" className="form-control"
                                        value={this.state.login} onChange={this.onLoginChange}/>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-12">
                                    <label>Пароль</label>
                                    <input type="password" autocomplete="on" id="password" name="password" className="form-control"
                                        value={this.state.password} onChange={this.onPasswordChange}/>
                                </div>
                            </div>
                            <br />
                            <div className="row">
                                <div className="col-md-4">
                                    <button type="submit" className="btn btn-primary"
                                        onClick={this.onSignInClick}>
                                        <span className="glyphicon glyphicon-user" /> Войти
                                    </button>
                                </div>
                                <div className="col-md-8">
                                    <span>
                                        <input id="remember-me"type="checkbox" className="checkbox"
                                            checked={this.state.remember} onChange={this.onRememberChange}/>
                                        <label htmlFor="remember-me">Запомнить меня</label>
                                    </span>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>;
        }
    });
    this.LoginForm = LoginForm;
}).call(this);
