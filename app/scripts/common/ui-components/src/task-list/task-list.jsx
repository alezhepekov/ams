/**
 * @jsx React.DOM
 */
(function(){
    var Task = React.createClass({
        getInitialState: function(){
            return {
                isActive: !this.props.mustDocs || (this.props.mustDocs && this.props.documents && this.props.documents.length > 0)
            }
        },
        renderCreationDate: function(){
            if(this.props.creationDate){
                return moment(this.props.creationDate).locale('ru').format('LL');
            } else {
                console.log('WARNING: task must have a creation date!');
            }
        },
        rednerOwner: function(){
            if(this.props.owner){
                return this.props.owner.firstName + ' ' + this.props.owner.lastName;
            } else {
                console.log('WARNING: task must have an owner!');
            }
        },
        renderOwnerInfoPanel: function(){
            return <div>
                   Создал задачу: {this.rednerOwner()}
                </div>
        },
        renderExecutorName: function(){
            if(this.props.executor) {
                return <span>
                        <span className="glyphicon glyphicon-user" aria-hidden="true"></span> {this.props.executor.firstName} {this.props.executor.lastName}
                    </span>
            }
        },
        renderDueDate: function(){
            if(this.props.dueDate){
                return <span><span className="glyphicon glyphicon-time" aria-hidden="true"></span> {moment(this.props.dueDate).locale('ru').format('LL')} </span>
            }
        },
        renderFinishingDate: function(){
            if(this.props.finishingDate){
                return <span>Выполнено: <span className="glyphicon glyphicon-ok-sign" aria-hidden="true"></span> {moment(this.props.finishingDate).locale('ru').format('LLL')} </span>
            }
        },
        onTaskAgreement: function(){
            if(this.props.onTaskAgreement){
                this.props.onTaskAgreement(this.props.id);
            }
        },
        onProcessTask: function() {
            this.props.inProcess = !this.props.inProcess;
            if (this.props.onProcessTask) {
                this.props.onProcessTask(this.props.id, this.props.inProcess);
            }
        },
        onFinish: function(){
            if(this.props.onFinish){
                this.props.onFinish(this.props.id);
            }
        },
        onEdit: function(){
            if(this.props.onEdit){
                this.props.onEdit(this.props.id);
            }
        },
        onDelete: function(){
            if(this.props.onDelete){
                this.props.onDelete(this.props.id);
            }
        },
		onReopen: function(){
			if(this.props.onReopen && this.canReopen()) {
				this.props.onReopen(this.props.id);
			}
		},
		canReopen: function() {
			return this.props.currentUser._id===this.props.executor._id || this.props.isManager || this.props.isCoordinator;
		},
        checkFields: function(){
            return this.props.currentUser && this.props.owner;
        },
        renderButton: function(options){
            var buttonsClass = this.state.isActive ? options.enabledClass : options.disabledClass,
                buttonType = this.props.finished ? options.finishedButtonGlyph : options.defaultButtonGlyph;
            if(this.props.finished){
                buttonsClass = options.finishedButtonClass;
            }
            if (options.visibilityValidation()) {
                if (this.state.isActive) {
                    return  <div className="task-list-buttons-container" onClick={options.onClick}>
                                <div className="task-list-buttons-wrapper">
                                    <div className={buttonsClass}>
                                        <span className={buttonType} aria-hidden="true"></span>
                                    </div>
                                </div>
                            </div>
                } {
                    return  <div className="task-list-buttons-container" data-toggle="tooltip"  data-placement="right" title={'Условие завершения: ' + this.props.description}>
                                <div className="task-list-buttons-wrapper">
                                    <div className={buttonsClass}>
                                        <span className={buttonType} aria-hidden="true"></span>
                                    </div>
                                </div>
                            </div>
                }
            }
        },
        renderStatusIcon: function(){
            if (this.props.status.type) {
                if (this.props.status.type==='notification') {
                    return <i className="fa fa-share"></i>
                } else {
                    return <i className="fa fa-graduation-cap"></i>
                }
             }

             return;
        },
        renderCompleteButton: function(){
            if(!this.checkFields()){
                return;
            }

            var self = this,
                action = this.props.finished ? this.onReopen : this.onFinish,
                executor = self.props.executor || {};

            var taskParentDone = (self.props.parentTask) ? self.props.parentTask.finished : true;
            return this.renderButton({
                enabledClass: 'task-list-ok-button-enabled',
                disabledClass: 'task-list-ok-button-disabled',
                finishedButtonClass: 'task-list-finished-mark',
                finishedButtonGlyph: 'glyphicon glyphicon-ok-sign',
                defaultButtonGlyph: 'glyphicon glyphicon-ok',
                visibilityValidation: function(){
                    return ((self.props.currentUser._id === self.props.owner._id || self.props.currentUser._id === executor._id)
                        || self.props.finished || self.props.canCompleteTask || self.props.currentUser.isRoot) && taskParentDone;
                },
                onClick: action
            });
        },
        renderEditButton: function(){
            if(!this.checkFields()){
                return;
            }

            var self = this;
            return this.renderButton({
                enabledClass: 'task-list-edit-button-enabled',
                disabledClass: 'task-list-edit-button-disabled',
                finishedButtonClass: 'task-list-edit-button-enabled',
                finishedButtonGlyph: 'glyphicon glyphicon-pencil',
                defaultButtonGlyph: 'glyphicon glyphicon-pencil',
                visibilityValidation: function(){
                    return ((self.props.currentUser._id === self.props.owner._id && !self.props.finished) || self.props.canEditTask || self.props.currentUser.isRoot) && self.props.editMode;
                },
                onClick: this.onEdit
            });
        },
        renderRemoveButton: function(){
            if(!this.checkFields()){
                return;
            }

            var self = this;
            return this.renderButton({
                enabledClass: 'task-list-remove-button-enabled',
                disabledClass: 'task-list-remove-button-disabled',
                finishedButtonClass: 'task-list-remove-button-enabled',
                finishedButtonGlyph: 'glyphicon glyphicon-remove',
                defaultButtonGlyph: 'glyphicon glyphicon-remove',
                visibilityValidation: function(){
                    return ((self.props.currentUser._id === self.props.owner._id && !self.props.finished) || self.props.canEditTask || self.props.currentUser.isRoot) && self.props.editMode;
                },
                onClick: this.onDelete
            });
        },
       /* onMouseOver: function(){
            this.setState({isActive: true});
        },
        onMouseOut: function(){
            this.setState({isActive: false});
        },*/
        onCommentsSave: function(comment){
            if(this.props.onCommentsSave){
                this.props.onCommentsSave({
                    comment: comment,
                    id: this.props.id
                });
            }
        },
        onDocumentsSave: function(document){
            console.log('st2');
            if(this.props.onDocumentsSave){
                this.props.onDocumentsSave({
                    document: document,
                    id: this.props.id
                });
            }
        },
        onDocumentsDelete: function(docs){
            console.log('onDocumentsDelete st2');
            if(this.props.onDocumentsDelete){
                this.props.onDocumentsDelete({
                    documents: docs,
                    id: this.props.id
                });
            }
        },
        rednerProcessButton: function() {
            if (this.props.finished)
                return;

            var taskParentDone = (this.props.parentTask) ? this.props.parentTask.finished : true;
            if (!taskParentDone)
                return;

            if (this.props.inProcess) {
                return <button type="button" className="btn btn-primary stopTask" onClick={this.onProcessTask}>Стоп</button>
            } else {
                return <button type="button" className="btn btn-primary startTask" onClick={this.onProcessTask}>Старт</button>
            }
        },
        renderTaskMainInfo: function(statusDescription, statusClass, statusTitle) {
            var creationDateStr = '-';
            var dueDateStr = '-';
            var statusStr = 'в процессе';
            var executorFullName = '-';

            if (this.props.creationDate) {
                var creationDate = new Date(this.props.creationDate);
                creationDateStr = moment(creationDate).locale('ru').format('L');
            }
                        
            if (this.props.dueDate) {
                var dueDate = new Date(this.props.dueDate);
                dueDateStr = moment(dueDate).locale('ru').format('L');
            }
                        
            if (this.props.executor && this.props.executor.fullName && this.props.executor.fullName.length > 0) {
                executorFullName = this.props.executor.fullName;
            }

            if (this.props.finished) {
                statusStr = 'завершено';
            }

            return <div className="col-lg-12">
                <div className="col-lg-7 status-title status-tooltip" title={statusDescription} data-toggle="tooltip" data-placement="top">
                    <span className={statusClass} >{statusTitle} {this.renderStatusIcon()}</span>
                </div>
                <div className="col-lg-4">
                    <div className="pull-right">
                    </div>
                </div>
                <div className="col-lg-1">
                    <div className="col-lg-12">
                        <div className="pull-right">
                            {this.renderCompleteButton()}
                        </div>
                    </div>
                    <div className="col-lg-12">
                        <div className="pull-right">
                            {this.renderEditButton()}
                        </div>
                    </div>
                    <div className="col-lg-12">
                        <div className="pull-right">
                            {this.renderRemoveButton()}
                        </div>
                    </div>
                </div>
                <hr className="groupLine"/>
                <div className="col-lg-12">
                    <div className="col-lg-3">
                        Дата начала: {creationDateStr}
                    </div>
                    <div className="col-lg-3">
                        Дата завершения: {dueDateStr}
                    </div>
                    <div className="col-lg-3">
                        Статус: {statusStr}
                    </div>
                    <div className="col-lg-3">
                        Исполнитель: <span className="glyphicon glyphicon-user" aria-hidden="true"></span> {executorFullName}
                    </div>
                </div>
            </div>
        },
        render: function(){
            var statusClass = 'label',
                statusTitle = '',
                statusDescription = '',
                snippetClass = 'alert alert-info';

            var statusIcon = '';

            if(this.props.status) {
                 statusClass = "task-list-task-label label label-" + this.props.status.className;
                 statusTitle = this.props.status.title;
                 if (this.props.status.description) {
                    statusDescription = this.props.status.description;
                 }
                 if (this.props.status.type) {
                    if (this.props.status.type==='notification') {
                        snippetClass += ' notificationTask';
                    } else {
                        snippetClass += ' juristicTask';
                    }
                }
            }

            var processTime = 0;
            var processTimePer = 0;
            var processTimeDescr = '';

            var overDueTime = 0;
            var overDueTimePer = 0;
            var overDueTimeDescr = '';

            var restTime = 0;
            var restTimePer = 0;
            var restTimeDescr = '';

            var isOverDue = false;

            var now = new Date();
            var creationDate = new Date(this.props.creationDate);

            if (this.props.dueDate) {
                var dueDate = new Date(this.props.dueDate);
                var fullTime = dueDate.getTime() - creationDate.getTime();               
                
                processTime = now.getTime() - creationDate.getTime();
                if (processTime > 0 && processTime < 86400000) processTime = 86400000;
                processTimePer = (processTime / fullTime) * 100;

                var overDueFromPotential = (this.props.finishingDate) ? new Date(this.props.finishingDate) : now;
                overDueTime = dueDate.getTime() - overDueFromPotential.getTime();
                isOverDue = overDueTime <= 0;
                if (isOverDue) {
                    overDueTime = Math.abs(overDueTime);
                    overDueTimePer = (overDueTime / processTime) * 100;
                    var overDueTimeDays = Math.ceil(overDueTime / 86400000);

                    processTimePer = (fullTime / processTime) * 100;
                    overDueTimeDescr = overDueTimeDays + ' дн.';
                    
                    //console.debug('overDueTime: ' + overDueTime + '/' + 'processTime: ' + processTime + ' = overDueTimePer:' + overDueTimePer);
                }             

                if (processTime > 0) {
                    var processTimeDays = Math.floor(processTime / 86400000);
                    if (processTimeDays == 0) {
                        processTimePer = 0;
                        processTimeDescr = '';
                    } else {
                        processTimeDescr = processTimeDays + ' дн.';
                    }
                }

                if (!isOverDue) {
                    restTime = dueDate - now.getTime();
                    var restTimeDays = Math.floor(restTime / 86400000);
                    if (restTimeDays == 0) {
                        restTimePer = 0;
                        restTimeDescr = '';
                    } else {
                        restTimePer = 100 - processTimePer;
                        restTimeDescr = restTimeDays + ' дн.';
                    }
                }

            } else {
                //Нет даты завершения
                /*freezeTime = now.getTime() - creationDate.getTime();
                var freezeTimeDays = freezeTime / 86400000;
                freezeTimePer = 100;
                freezeTimeDescr = 'Время окончания не назначено (' + freezeTimeDays.toFixed(1) + ' дн.)';*/
            }

            //console.debug('processTimePer: ' + processTimePer + ' overDueTimePer: ' + overDueTimePer);
            if(this.props.finished){
                snippetClass = 'alert alert-success';
            } else {
                if (isOverDue) {
                    snippetClass = 'alert over-due';
                }
                if (this.props.inProcess) {
                    snippetClass = 'alert in-process';
                }
            }
            if (this.props.parentTask) {
                if (this.props.parentTask.parentId)
                    snippetClass += ' dependedTask2';
                else
                    snippetClass += ' dependedTask';
            }// onMouseOver={this.onMouseOver} onMouseOut={this.onMouseOut}
            

            return <div key={this.props.id} className={snippetClass} role="alert">
                        <div className="row taskMain">
                            {this.renderTaskMainInfo(statusDescription, statusClass, statusTitle)}
                            <div className="col-lg-12">
                                <div className="progress">
                                    <div className="progress-bar progress-bar-success progress-bar-striped" style={{width: processTimePer + '%'}}>
                                        <span title={processTimeDescr}>{processTimeDescr}</span>
                                    </div>
                                    <div className="progress-bar progress-bar-warning progress-bar-striped" style={{width: restTimePer + '%'}}>
                                        <span title={restTimeDescr}>{restTimeDescr}</span>
                                    </div>
                                    <div className="progress-bar progress-bar-danger progress-bar-striped" style={{width: overDueTimePer + '%'}}>
                                        <span title={overDueTimeDescr}>{overDueTimeDescr}</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row taskDetails">
                            <div className="col-lg-12">
                                 <div className="col-lg-12 taskOwnerInfo">
                                    {this.renderOwnerInfoPanel()}
                                </div>                                
                                
                            </div>
                            <div className="row">
                                <div className="col-lg-12 taskDocs">
                                    <Documents
                                        hover={this.state.isActive}
                                        documents={this.props.documents}
                                        onSave={this.onDocumentsSave}
                                        onDelete={this.onDocumentsDelete}
                                        onLoadFinish={this.props.onLoadFinish}
                                        owner={this.props.currentUser}
                                        onOpenEditor={this.props.onOpenEditor}
                                        getLinkClass={this.props.getLinkClass}
                                        getFileIcon={this.props.getFileIcon}
                                        processDocument={this.props.processDocument}/>
                                </div>
                                <div className="col-lg-12 taskComments">
                                    <Comments hover={this.state.isActive} comments={this.props.comments} onSave={this.onCommentsSave} owner={this.props.currentUser}/>
                                </div>
                            </div>
                        </div>                        
                    </div>
        }
    });

    var TaskList = React.createClass({
        getInitialState: function(){
            return {
                newTaskMode: false,
                newTask: {
                    creationDate: null,
                    dueDate: null,
                    owner: null,
                    executor: null,
                    status: null,
                    finished: false,
                    description: ''
                },
                editMode: false,
                users: [],
            }
        },
        componentWillMount: function(){
            if(this.props.currentUser){
                var resultTask = this.state.newTask;
                resultTask.owner = this.props.currentUser;

                this.setState({
                    newTask: resultTask,
                });
            }
        },
        enableNewTaskMode: function(){
            this.setState({newTaskMode: true});
        },
        disableNewTaskMode: function(){
            this.setState({newTaskMode: false});
        },
        onSave: function(){
            var self = this;
            if(this.props.onSave){
                var save = function(){
                    var task = self.state.newTask;
                    task.creationDate = new Date();
                    task.id = uuid.v1();
                    self.props.onSave(task);
                    self.setState({
                        newTask: {
                            owner: self.props.currentUser
                        },
                        newTaskMode: false
                    });
                }

                if(self.state.newTask.dueDate == null){
                    alertify.error('Вы не выбрали дату завершения задачи!');
                } else if(self.state.newTask.status == null){
                    alertify.error('Вы не выбрали статус задачи!');
                } else {
                    if(self.state.newTask.description === '' || self.state.newTask.description == null){
                        alertify.confirm('Вы уверены, что хотите создать задачу без описания?', function(e){
                            if(e){
                                save();
                            }
                        })
                    } else {
                        save();
                    }
                }

            }
        },
        onDateInput: function(e){
            var value = new Date(e.value.year, e.value.month - 1, e.value.day),
                resultTask = this.state.newTask;

            resultTask.dueDate = value;

            this.setState({newTask: resultTask});
        },
        onUserSelect: function(e){
            var id = e.target.selectedOptions[0].value,
                resultTask = this.state.newTask,
                esultTask = this.state.newTask,
                user;
            this.props.users.forEach(function(u){
                if(u._id === id){
                    user = u;
                }
            });

            resultTask.executor = user;
            this.setState({newTask: resultTask});
        },
        onStatusSelect: function(e){
            var value = e.target.value,
                resultTask = this.state.newTask,
                status;

            this.props.statuses.forEach(function(st){
                if(st.title === value){
                    status = st;
                }
            });
            resultTask.status = status;
            users = this.filterUsers(resultTask);

            this.setState({
                newTask: resultTask,
                users: users
            });
            console.log(this.state);
        },
        onDescriptionInput: function(e){
            var value = e.target.value,
                resultTask = this.state.newTask;

            resultTask.description = value;

            this.setState({newTask: resultTask});
        },
        onFinish: function(id){
            if(this.props.onFinish){
                this.props.onFinish(id);
            }
        },
		onReopen: function(id){
			if(this.props.onReopen){
                this.props.onReopen(id);
            }
		},
        onCommentsSave: function(commentsContainer){
            if(this.props.onCommentsSave){
                this.props.onCommentsSave(commentsContainer);
            }
        },
        onDocumentsSave: function(documentsContainer){
            console.log('st1');
            if(this.props.onDocumentsSave){
                this.props.onDocumentsSave(documentsContainer);
            }
        },
        onDocumentsDelete: function(docs){
            console.log('onDocumentsDelete st1');
            if(this.props.onDocumentsDelete){
                this.props.onDocumentsDelete(docs);
            }
        },
        createTask: function(st){
            if(this.props.onSave){
                var today = new Date();
                this.props.onSave({
                    status: st,
                    id: uuid.v1(),
                    //creationDate: new Date(),
                    dueDate: today.setDate(today.getDate() + 1),
                    finished: false,
                    finishingDate: null,
                    owner: this.props.currentUser,
                    executor: null,
                    description: '',
                });
            }
        },
        editTask: function(id){
            var taskForEditing = this.props.tasks.filter(function(task){
                return task.id === id;
            })[0];
            var users = this.filterUsers(taskForEditing);
            this.setState({
                newTask: taskForEditing,
                newTaskMode: true,
                users: users,
            });
        },
        deleteTask: function(id){
            if(this.props.onDelete){
                this.props.onDelete(id);
            }
        },
        renderUserItem: function(user){
            return <option value={user._id} key={user._id}>{user.firstName} {user.lastName}</option>
        },
        renderStatusItem: function(status){
            return <option value={status.title} key={status.id}>{status.title}</option>
        },
        rednerDateInput: function(){
            if(this.state.newTask.dueDate){
                var dateObj = new Date(this.state.newTask.dueDate);
                var dfDateObj = {
                    value: {
                        month: dateObj.getUTCMonth() + 1, //months from 1-12
                        day: dateObj.getUTCDate() + 1,
                        year: dateObj.getUTCFullYear()
                    }
                };
                return <DfDateInput onChange={this.onDateInput} independent={true} oldState={dfDateObj}/>
            }
            return <DfDateInput onChange={this.onDateInput} independent={true}/>
        },
        renderUsersMockItem: function(){
            //if(!this.state.newTask.executor){
                return <option>Выберите исполнителя...</option>
            //}
        },
        renderStatusesMockItem: function(){
            if(!this.state.newTask.status){
                return <option>Выберите статус...</option>
            }
        },
        filterUsers: function(task){
            var users = this.props.users.slice(0),
                self = this,
                currentTask = task || self.state.newTask;
            if(self.props.roles &&
                currentTask.status &&
                currentTask.status.bindRoles &&
                currentTask.status.bindRoles.length > 0){
                users = users.filter(function(user){
                    var userResult = false;
                    var tmpRoles = user.roles.map(function(userRole){
                        var result;
                        self.props.roles.forEach(function(role){
                            if(role.id === userRole.roleId){
                                result = role.name;
                            }
                        });
                        return result;
                    }).filter(function(role){return role != null});
                    if(currentTask.status.bindRoles){
                        currentTask.status.bindRoles.forEach(function(role){
                            tmpRoles.forEach(function(userRole){
                                if(userRole === role){
                                    userResult = true;
                                }
                            });
                        });
                    }

                    return userResult;
                });
            }
            return users;
        },
        renderUserList: function(){
            if(this.state.newTask.executor){
                return <select defaultValue={this.state.newTask.executor._id} id="task-list-users-select" onChange={this.onUserSelect} className="form-control">
                            {this.renderUsersMockItem()}
                            {this.state.users.map(this.renderUserItem)}
                        </select>
            } else {
                return <select id="task-list-users-select" onChange={this.onUserSelect} className="form-control">
                            {this.renderUsersMockItem()}
                            {this.state.users.map(this.renderUserItem)}
                        </select>
            }
        },
        renderStatusesList: function(){
            if(this.state.newTask.status){
                return <select defaultValue={this.state.newTask.status.title} id="task-list-statuses-select" onChange={this.onStatusSelect} className="form-control">
                            {this.renderStatusesMockItem()}
                            {this.props.statuses.map(this.renderStatusItem)}
                        </select>
            } else {
                return <select id="task-list-statuses-select" onChange={this.onStatusSelect} className="form-control">
                            {this.renderStatusesMockItem()}
                            {this.props.statuses.map(this.renderStatusItem)}
                        </select>
            }
        },
        renderTaskForm: function(){
            if(this.state.newTaskMode){
                var dateValue = this.state.newTask.dueDate,
                    descriptionValue = this.state.newTask.description;
                return  <div>
                            <div className="col-lg-4">
                                <label htmlFor="task-list-user">Исполнитель</label>
                                <div className="input-group" id="task-list-user">
                                  <span className="input-group-addon glyphicon glyphicon-user"></span>
                                   {this.renderUserList()}
                                </div>
                            </div>
                            <div className="col-lg-4">
                                <label htmlFor="due-date">Выполнить к</label>
                                <div className="input-group" id="due-date">
                                  {this.rednerDateInput()}
                                </div>
                            </div>
                            <div className="col-lg-4">
                                <label htmlFor="task-list-status">Статус</label>
                                <div className="input-group" id="task-list-status">
                                  <span className="input-group-addon glyphicon glyphicon-tag"></span>
                                  {this.renderStatusesList()}
                                </div>
                            </div>
                            <div className="col-lg-12">
                                <label htmlFor="task-description">Описание задачи</label>
                                <textarea onChange={this.onDescriptionInput} value={descriptionValue} className="form-control" rows="5" id="task-description" placeholder="Краткое описание задачи"></textarea>
                            </div>
                        </div>
            }
        },
        getParentTask: function(task) {
            if (task && task.parentId) {
                var parentTask = this.props.tasks.filter(function(t){
                    return t.id===task.parentId;
                })[0];

                return parentTask;
            }

            return undefined;
        },
        renderTask: function(task){
            return <Task
                key={task.id}
                id={task.id}
                tasks={task.tasks}
                parentTask={this.getParentTask(task)}
                owner={task.owner}
                executor={task.executor}
                status={task.status}
                creationDate={task.creationDate}
                dueDate={task.dueDate}
                finishingDate={task.finishingDate}
                comments={task.comments || []}
                documents={task.documents || []}
                events={task.events}
                mustDocs={task.mustDocs}
                description={task.description}
                inProcess={task.inProcess}
                finished={task.finished}
                currentUser={this.props.currentUser}
                onTaskAgreement={this.props.onTaskAgreement}
                onProcessTask={this.props.onProcessTask}
                onFinish={this.onFinish}
                onEdit={this.editTask}
                onDelete={this.deleteTask}
				onReopen={this.onReopen}
                onCommentsSave={this.onCommentsSave}
                onDocumentsSave={this.onDocumentsSave}
                onDocumentsDelete={this.onDocumentsDelete}
                onOpenEditor={this.props.onOpenEditor}
                onLoadFinish={this.props.onLoadFinish}
                getLinkClass={this.props.getLinkClass}
                getFileIcon={this.props.getFileIcon}
                processDocument={this.props.processDocument}
                canCompleteTask={this.props.canCompleteTasks}
                canEditTask={this.props.canEditTasks}
                editMode={this.state.editMode}
				isManager={this.props.isManager}
				isCoordinator={this.props.isCoordinator}/>
        },
        renderTaskTree: function(){
            if(!this.state.newTaskMode){
                return <div>
                    {this.props.tasks.map(this.renderTask)}
                </div>
            }
        },
        renderDropdownStatuses: function(status){
            return <li><a onClick={this.createTask.bind(this,status)}>{status.title}</a></li>
        },
        enableEditMode: function(){
            this.setState({editMode: true});
        },
        disableEditMode: function(){
            this.setState({editMode: false});
        },
        renderEditButtons: function(){
            if(this.props.canEditTasks){
                if(!this.state.editMode){
                    return <button onClick={this.enableEditMode} type="button" className="btn btn-primary">Редактировать</button>
                } else {
                    return <button onClick={this.disableEditMode} type="button" className="btn btn-danger">Отключить редактирование</button>
                }
            }
        },
        renderCreateTaskButton: function(){
            if(this.props.canCreateTasks){
                return <button type="button" className="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            Добавить <span className="caret"></span>
                        </button>
            } else {
                return <button disabled type="button" className="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            Добавить <span className="caret"></span>
                        </button>
            }
        },
        render: function(){
            return  <div className="col-lg-12">
                        <div className="panel panel-default">
                          <div className="panel-heading">
                            <div className="row">
                                <div className="col-lg-12">
                                    <div className="pull-left">
                                        Список задач
                                    </div>
                                </div>
                            </div>
                          </div>
                          <div className="panel-body">
                                {this.renderTaskForm()}
                                {this.renderTaskTree()}
                          </div>
                        </div>
                    </div>
        }
    });

    this.TaskList = TaskList;
}).call(this);
