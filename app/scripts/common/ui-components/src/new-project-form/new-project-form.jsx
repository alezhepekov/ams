/**
 * @jsx React.DOM
 */
(function(){
    'use strict';
    var StatusBadge = React.createClass({
        onRemove: function(){
            if(this.props.onRemove){
                this.props.onRemove(this.props.status);
            }
        },
        render: function(){
            var className = "label label-"+this.props.status.className;
            return  <a className={className} onClick={this.onRemove} title="Один щелчек для удаления">
                        {this.props.status.title}
                    </a>

        }
    });
    var NewProjectForm = React.createClass({
        getInitialState: function(){
            return {
                project:{
                    name: '',
                    description: '',
                    organizationId: '',
                    statuses: [
                        {className: 'info', title:'OK'},
                        {className: 'danger', title:'Danger'}
                    ],
                    roles:[ ]
                }
            };
        },
        validate: function(project) {
            if(project.name === ''){
                return false;
            }
            if(project.description === ''){
                return false;
            }
            if(project.organizationId ===''){
                return false;
            }
            return true;
        },
        onSaveClick: function (e){
            if(this.props.onSave){
                this.props.onSave(this.state.project);
            }
        },
        onRemoveClick: function(e){
            if(this.props.onRemove){
                this.props.onRemove();
            }
        },
        onNameChange: function (e){
            var input = e.target.value;
            var pr = this.state.project;
            pr.name = input;
            this.setState({project: pr});
        },
        onDescriptionChange: function (e) {
            var input = e.target.value;
            var pr = this.state.project;
            pr.description = input;
            this.setState({project: pr});
        },
        onOrgSelect: function(e){
            var input = e.target.value;
            var project = this.state.project;
            project.organizationId = input;
            this.setState({project: project});
        },
        onAddStatus: function(e){
            var st={};
            st.title = this.refs.newStatusTitle.getDOMNode().value;
            this.refs.newStatusTitle.getDOMNode().value = "";
            st.className = this.refs.newStatusClassName.getDOMNode().value;
            this.refs.newStatusClassName.getDOMNode().value = "default";
            var project = this.state.project;

            var isUnique = project.statuses.filter(function(s){
                return st.title === s.title;
            }).length === 0;

            if(st.title && isUnique) {
                project.statuses.push(st);
                this.setState({project: project});
            }else{
                alertify.error('Заголовок должен быть не пустым и уникальным!');
            }
        },
        onStatusRemove: function(st){
            var project = this.state.project;
            project.statuses = project.statuses.filter(function(s){
                return st !== s;
            });
            this.setState({project: project});
        },
        renderOrgs: function(org){
            return <option value={org._id}> {org.name} </option>;
        },
        renderHeading: function(){
            return  <div className="row">
                        <div className="col-md-8">
                            <h4>Создать проект</h4>
                        </div>
                        <div className="col-md-4">
                            <div className="pull-right">
                                <button type="button" onClick={this.onRemoveClick} className="btn btn-danger">
                                    <span className="glyphicon glyphicon-trash"/> Отменить
                                </button>
                                <button type="button" onClick={this.onSaveClick} className="btn btn-success">
                                    <span className="glyphicon glyphicon-floppy-save"/> Сохранить
                                </button>
                            </div>
                        </div>
                    </div>
        },
        renderStatusBadge: function(st){
            return <li><StatusBadge status={st} onRemove={this.onStatusRemove}/></li>
        },
        render: function () {
            return  <div className="panel panel-default">
                        <div className="panel-heading">
                            {this.renderHeading()}
                        </div>
                        <div className="panel-body">
                            <label>Организация</label>
                            <select value={this.state.project.organizationId} className="form-control"
                                onChange={this.onOrgSelect}>
                                <option value="">Не выбрана</option>
                                {this.props.organizations.map(this.renderOrgs)}
                            </select>
                            <label>Название проекта</label>
                            <input type="text" className="form-control" placeholder="Название"
                                value={this.state.project.name} onChange={this.onNameChange}/>
                            <label>Краткое описание</label>
                            <textarea className="form-control" placeholder="Краткое описание"
                                value={this.state.project.description} onChange={this.onDescriptionChange}/>
                            <label>Возможные статусы</label>
                            <div className="statuses-list">
                                <ul className="row">
                                    {this.state.project.statuses.map(this.renderStatusBadge)}
                                </ul>
                                <div className="row">
                                    <div className="col-md-6">
                                        <input type="text" className="form-control" ref="newStatusTitle"/>
                                    </div>
                                    <div className="col-md-4">
                                        <select className="form-control" ref="newStatusClassName">
                                            <option value="default">Серый</option>
                                            <option value="info">Голубой</option>
                                            <option value="success">Зеленый</option>
                                            <option value="warning">Оранжевый</option>
                                            <option value="danger">Красный</option>
                                        </select>
                                    </div>
                                    <div className="col-md-2">
                                        <button type="button" className="btn btn-primary" onClick={this.onAddStatus}>
                                            Добавить
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>;
        }
    });
    this.NewProjectForm = NewProjectForm;
}).call(this);
