/**
 * @jsx React.DOM
 */
(function(){
  var DfForm = React.createClass({
    getInitialState: function(){
        return {
            data: [],
            id: '',
            viewBody: false
        }
    },
    componentWillMount: function(){
        this.setState({id: this.props.id});
        if(this.props.oldState){
            this.setState({data: this.props.oldState.value});
        }
    },
    onChange: function(componentState){
        var resultData = [];
        if (this.state.data.length == 0) {
            this.state.data.push(componentState);
        }
        this.state.data.forEach(function(elem){
            if(componentState.id === elem.id){
                elem.value = componentState.value;
                resultData.push(elem);
            } else {
                resultData.push(elem);
            }
        });
        var tmpResult = this.state.data.filter(function(elem){
            return elem.id === componentState.id;
        });
        if(tmpResult.length === 0){
            resultData.push(componentState);
        }

        this.setState({data: resultData});
        if(this.props.internalForm || this.props.onChange){
            this.props.onChange({id: this.state.id, value: this.state.data});
        }
    },
    onSave: function(){
        if(this.props.onSave){
            this.props.onSave(this.state);
        }
    },
    renderSavingButton: function(){
        if(this.props.onChange == null && !this.props.readonly){
            return   <div className="col-md-12">
                        <button onClick={this.onSave} className="btn btn-success pull-right entities-constructor-spaces" type="button">Сохранить форму</button>
                    </div>
        }
    },
    getOldStateOf: function(id){
        var result;
        if(this.props.oldState){
            this.props.oldState.value.forEach(function(st){
                if(st.id === id){
                    result = st;
                }
            });
        }
        return result;
    },
    renderComponent: function(field){
        if(field instanceof EntityFactory.fieldTypes.TextField){
            return <DfTextInput key={field.id} id={field.id} onChange={this.onChange}
                placeholder={field.placeholder} name={field.name} title={field.title} oldState={this.getOldStateOf(field.id)} readonly={this.props.readonly}/>

        } else if(field instanceof EntityFactory.fieldTypes.ConstantList){
            return <DfConstantList key={field.id} id={field.id} onChange={this.onChange}
                title={field.title} items={field.items} oldState={this.getOldStateOf(field.id)} readonly={this.props.readonly}/>

        } else if(field instanceof EntityFactory.fieldTypes.EmptyTextList){
            return <DfEmptyTextList key={field.id} id={field.id} onChange={this.onChange}
                title={field.title} items={field.items} oldState={this.getOldStateOf(field.id)} readonly={this.props.readonly}/>

        } else if(field instanceof EntityFactory.fieldTypes.EntityField){
            return <DfForm key={field.id} components={field.fields} title={field.title} id={field.id} internalForm={true} onChange={this.onChange} oldState={this.getOldStateOf(field.id)} readonly={this.props.readonly}/>

        } else if(field instanceof EntityFactory.fieldTypes.DateField){
            return <DfDateInput key={field.id} id={field.id} title={field.title}
                onChange={this.onChange} oldState={this.getOldStateOf(field.id)} readonly={this.props.readonly}/>

        } else if(field instanceof EntityFactory.fieldTypes.TimeField){
            return <DfTimeInput key={field.id} id={field.id} title={field.title}
                onChange={this.onChange} oldState={this.getOldStateOf(field.id)} readonly={this.props.readonly}/>

        } else if(field instanceof EntityFactory.fieldTypes.EmptyTextList){
            return <DfEmptyTextList key={field.id} id={field.id} title={field.title} oldState={this.getOldStateOf(field.id)} readonly={this.props.readonly}/>
        }
    },
    chnageVisibility: function(){
        this.setState({viewBody: !this.state.viewBody});
    },
    renderBody: function(){
        if(this.state.viewBody){
            return <div className="panel-body">
                        {this.props.components.map(this.renderComponent)}
                    </div>
        }
    },
    renderArrow: function(){
        if(this.state.viewBody){
            return <span className="glyphicon glyphicon-chevron-down pull-right" aria-hidden="true"></span>
        } else {
            return <span className="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span>
        }
    },
    render: function(){
        if(this.props.internalForm){
            var panelClasses = "panel panel-info";
            if(this.props.readonly){
                panelClasses = "panel panel-default";
            }
            return <div className="col-md-12">
                        <div className="field df-form">
                            <div className={panelClasses}>
                                <div onClick={this.chnageVisibility} className="panel-heading">
                                    <div className="row">
                                        <div className="col-lg-8">
                                            <h3 className="panel-title">{this.props.title}</h3>
                                        </div>
                                        <div className="col-lg-4">
                                            {this.renderArrow()}
                                        </div>
                                    </div>
                                </div>
                                {this.renderBody()}
                            </div>
                        </div>
                   </div>
        } else {
            return <div className="field df-form">
                        <div id={this.props.id} role="form">
                            {this.props.components.map(this.renderComponent)}
                        </div>
                        {this.renderSavingButton()}
                    </div>
        }
    }
  });

  this.DfForm = DfForm;
}).call(this);
