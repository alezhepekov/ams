/**
 * @jsx React.DOM
 */
(function(){
  var DfTextInput = React.createClass({
    getInitialState: function(){
        return {
            value: '',
            id: 0
        }
    },
    componentWillMount: function(){
        this.setState({id: this.props.id});
        if(this.props.oldState){
            this.setState({value: this.props.oldState.value});
        }
    },
    onTextInput: function(e){
        var value = e.target.value;
        if(this.props.onChange){
            this.props.onChange({
                id: this.state.id,
                value: value
            });
        }
        this.setState({value: value});
    },
    render: function(){
        var value = this.state.value;
        if(this.props.readonly){
            return  <div className="col-sm-12 col-md-6 col-lg-4">
                        <div className="field">
                            <b>{this.props.title}: </b> {value}
                        </div>
                    </div>
        } else {
            return  <div className="form-group col-sm-12 col-md-6 col-lg-4">
                        <div className="field">
                            <label htmlFor={this.state.id}>{this.props.title}</label>
                            <input onChange={this.onTextInput} type="text" value={value} className="form-control" id={this.state.id} placeholder={this.props.placeholder}/>
                        </div>
                    </div>
        }
    }
  });

  this.DfTextInput = DfTextInput;
}).call(this);
