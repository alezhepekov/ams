/**
 * @jsx React.DOM
 */
(function(){
  var DfConstantList = React.createClass({
    getInitialState: function(){
        return {
            value: '',
            id: 0
        }
    },
    componentWillMount: function(){
        this.setState({id: this.props.id});

        if(this.props.oldState){
            this.setState({value: this.props.oldState.value});
        }
    },
    onItemSelect: function(e){
        var value = e.target.selectedOptions[0].value
        this.setState({value: value});
        if(this.props.onChange){
            this.props.onChange({
                id: this.state.id,
                value: value
            })
        }
    },
    renderListItem: function(item){
        if(this.props.oldState && item.id === this.props.oldState.id){
            return <option selected onClick={this.onItemSelect.bind(null,item)}>{item.value}</option>
        } else {
            return <option onClick={this.onItemSelect.bind(null,item)}>{item.value}</option>
        }
    },
    renderFirstOption: function(){
        if(this.state.value === ''){
            return <option>Выберите элемент списка...</option>
        }
    },
    render: function(){
        if(this.props.readonly){
            return  <div className="col-sm-12 col-md-6 col-lg-4">
                        <div className="field">
                            <b>{this.props.title}: </b>{this.state.value}
                        </div>
                    </div>
        } else {
            return  <div className="form-group col-sm-12 col-md-6 col-lg-4">
                <div className="field">
                    <label htmlFor={this.state.id}>{this.props.title}</label>
                    <select id={this.state.id} onChange={this.onItemSelect} className="form-control">
                        {this.renderFirstOption()}
                        {this.props.items.map(this.renderListItem)}
                    </select>
                </div>
            </div>
        }
    }
  });

  this.DfConstantList = DfConstantList;
}).call(this);
