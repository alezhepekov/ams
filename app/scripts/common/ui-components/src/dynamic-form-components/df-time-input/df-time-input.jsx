/**
 * @jsx React.DOM
 */
(function(){
  var DfTimeInput = React.createClass({
    getInitialState: function () {
        return {timeString:null, ok:false, id: 0};
    },
    /**
     * validates time from 00:00 to 23:59
     */
    componentWillMount: function(){
        this.setState({id: this.props.id});
        if(this.props.oldState){
            var time = this.props.oldState.value;
            this.setState({
                timeString: time.hours + ':' + time.mins,
                ok: true
            });
            this.renderStatus();
        }
    },
    validateTime: function(input){
        if(!input.match("^([0-9]{1,2})([:])([0-9]{1,2})$")){
            return false;
        }
        time = {};
        time.hours = parseInt(input.split(":")[0])
        time.mins = parseInt(input.split(":")[1])
        if((time.hours >=0)&&(time.hours <=23)&&(time.mins >=0)&&(time.mins <=59)){
            return true;
        }
        return false;
    },
    /**
     * parses time from 00:00 to 23:59
     */
    parseTime: function (input){
        time = {};
        time.hours = parseInt(input.split(":")[0])
        time.mins = parseInt(input.split(":")[1])
        return time;
    },
    /**
     * if time in input is valid, calls onChange callback, given
     * to constructor and shows badge to user
     */
    onInput: function (e){
        e.preventDefault();
        var input = e.target.value;
        var time;
        if(this.validateTime(input)){
            time = this.parseTime(input);
            this.setState({timeString:input, ok: true});
            this.props.onChange({id:this.state.id, value:time});
        }else{
            this.setState({timeString:input, ok: false});
        }
    },
    /**
     * Renders the badge
     */
    renderStatus: function () {
        if(this.state.ok){
            return <span className="glyphicon glyphicon-ok"/>
        }else{
            return <span className="glyphicon glyphicon-ban-circle"/>
        }
    },
    /**
     * Renders the component
     */
    render: function(){
        if(this.props.readonly){
            return  <div className="col-sm-12 col-md-6 col-lg-4">
                        <div className="field">
                            <b>{this.props.title}:</b> {this.state.timeString}
                        </div>
                    </div>
        } else {
            return  <div className="form-group df-time-input col-sm-12 col-md-6 col-lg-4">
                <div className="field">
                    <label htmlFor="df-form-timepicker" >{this.props.title}</label>
                    <div className="input-group">
                        <input type="text" size="10" maxLength="5" id="df-form-timipicker"
                            className="form-control" onChange={this.onInput} placeholder="HH:MM"
                            value={this.state.timeString}/>
                        <span className="input-group-addon">
                          {this.renderStatus()}
                        </span>
                    </div>
                </div>
            </div>
        }
    }
  });

  this.DfTimeInput = DfTimeInput;
}).call(this);
