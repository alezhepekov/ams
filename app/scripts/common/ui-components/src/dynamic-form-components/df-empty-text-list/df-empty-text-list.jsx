/**
 * @jsx React.DOM
 */
(function(){
    var DfEmptyTextList = React.createClass({
        getInitialState: function () {
            return {items:[], input:'', id: uuid.v1()}
        },
        componentWillMount: function(){
            this.setState({id: this.props.id});

            if(this.props.oldState){
                this.setState({items: this.props.oldState.value});
            }
        },
        onInput: function(e){
            e.preventDefault();
            var value = e.target.value;
            this.setState({input: value});
        },
        addItem: function(){
            var input = this.state.input;
            var items = this.state.items;
            items.push({id: uuid.v1(), value: input});
            this.setState({input: "", items: items});
            this.props.onChange({id:this.state.id, value: items});
        },
        onEnter: function (e){
            if(e.keyCode === 13){
                this.addItem();
            }
        },
        onDelete: function(id){
            var result = this.state.items.filter(function(item){
                return item.id !== id;
            });
            this.setState({items: result});
            if(this.props.onChange){
                this.props.onChange({id:this.state.id, value: result});
            }
        },
        onButtonClick: function(e){
            e.preventDefault();
            this.addItem();
        },
        toReadOnly: function(item){
            return item.value;
        },
        render: function(){
            if(this.props.readonly){
                return  <div className="col-sm-12 col-md-6 col-lg-4">
                        <div className="field">
                            <b>{this.props.title}: </b>{this.state.items.map(this.toReadOnly).join(', ')}
                        </div>
                    </div>
            } else {
                return  <div className="col-sm-12 col-md-6 col-lg-4">
                    <div className="field df-empty-text-list">
                        <label htmlFor={this.state.id}>{this.props.title}</label>
                        <div id={this.state.id} className="input-group">
                            <input type="text" className="form-control" value={this.state.input}
                             onChange={this.onInput} onKeyUp={this.onEnter}/>
                            <span className="input-group-btn">
                                <button className="btn btn-default" type="button"
                                    onClick={this.onButtonClick}>
                                    <span className="glyphicon glyphicon-plus"/>
                                </button>
                            </span>
                         </div>
                        <TagList items={this.state.items} onDelete={this.onDelete}/>
                    </div>
                </div>
            }
        }
    });

    this.DfEmptyTextList = DfEmptyTextList;
}).call(this);
