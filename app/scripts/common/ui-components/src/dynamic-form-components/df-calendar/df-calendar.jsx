/**
 * @jsx React.DOM
 */
(function(){
  var DfCalendar = React.createClass({
    /**
     * Returns Initial State
     */
    getInitialState: function(){
        var currYear = new Date().getFullYear();
        var currMonth = new Date().getMonth();
        var currDate = new Date().getDate();
        return {month:currMonth, year:currYear, date:currDate};
    },
    /**
     * Don't even touch, it's a kind of woodoo magic
     * Months are 0-based
     */
    buildCalendar: function(year, month){
        if (month > 11){
            throw new Error();
        }
        var nDays = new Date(year, month+1,0).getDate();		
        var firstDay = new Date(year, month,1).getDay()-1;
		if (firstDay < 0)
			firstDay = 6
        var nWeeks = Math.ceil((firstDay+nDays)/7);
        M = [];
        for(i=1;i<=nWeeks;i++){
            M[i] = [];
            for(j=1;j<=7;j++){
                M[i][j] = null;
            }
        }
        var d;
        for(i=1;i<=nDays; i++){
            d = new Date(year, month,i);
			var nday = d.getDay()-1;
			if (nday < 0)
				nday = 6;
            M[Math.ceil((firstDay+i)/7)][nday+1] = i;
        }
        return M;
    },
    /**
     * Triggered when year is typed into form.
     * if year is valid, rebuild calendar
     */
    onYearInput: function(e) {
        e.preventDefault();
        var year = parseInt(e.target.value);
        if((year>1900) && (year <2099)){
            this.setState({year: year});
        }
    },
    /**
     * Triggered when month is selected.
     */
    onMonthSelect: function(e){
        e.preventDefault();
        var month = parseInt(e.target.value);

        if((month >= 0) && (month < 12)){
            this.setState({month: month});
        }
    },
    onDayClick: function(e, date){
        e.preventDefault();
        this.setState({date: date});
        var data = this.state;
        data.date = date;
        data.month = data.month+1;
        this.props.onChange(data);
    },
    /**
     * Renders form for selecting year and month
     */
    renderForm: function(){
        return  <div>
                    <select defaultValue={this.state.month} onChange={this.onMonthSelect}>
                        <option value="0">Январь</option>
                        <option value="1">Февраль</option>
                        <option value="2">Март</option>
                        <option value="3">Апрель</option>
                        <option value="4">Май</option>
                        <option value="5">Июнь</option>
                        <option value="6">Июль</option>
                        <option value="7">Август</option>
                        <option value="8">Сентябрь</option>
                        <option value="9">Октябрь</option>
                        <option value="10">Ноябрь</option>
                        <option value="11">Декабрь</option>
                    </select>
                    <input type="text" defaultValue={this.state.year} size="5"
                        onChange={this.onYearInput} maxLength="4"/>
                </div>
    },
    /**
     * Renders link with day number
     */
    renderDay: function(d){
        var handler = function(e){
            this.onDayClick(e,d);
        }.bind(this);
        if(d!= null){
            if(d == this.state.date){
                return <a className="calendar-day selected" key={d}
                    onClick={handler}> {d} </a>
            }else{
                return <a className="calendar-day" key={d}
                    onClick={handler}> {d} </a>
            }
        }else{
            return <a className="calendar-empty-day"></a>
        }
    },
    /**
     * Renders 7 days
     */
    renderWeek: function(w){
        return <li>{w.map(this.renderDay)}</li>
    },
    /**
     * Renders calendar
     */
    render: function(){
        weeks = this.buildCalendar(this.state.year,this.state.month);
        return  <div className="df-calendar">
                    <h6>Выберите дату:</h6>
                    {this.renderForm()}
                    <ul>
                        <li>                            
                            <a className="calendar-top"> Пн </a>
                            <a className="calendar-top"> Вт </a>
                            <a className="calendar-top"> Ср </a>
                            <a className="calendar-top"> Чт </a>
                            <a className="calendar-top"> Пт </a>
                            <a className="calendar-top"> Сб </a>
							<a className="calendar-top"> Вс </a>
                        </li>
                        {weeks.map(this.renderWeek)}
                    </ul>
                </div>
    },
  });

  this.DfCalendar = DfCalendar;
}).call(this);
