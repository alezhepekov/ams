/**
 * @jsx React.DOM
 */
(function(){
  var DfDateInput = React.createClass({
    getInitialState: function(){
        return {ok:false, dateString: null, calendarVisible:false, id: 0}
    },
    /**
     * Validates dates from 1.1.1901 to 31.12.2099
     * @TODO fix bugs like 31'st february
     */
    componentWillMount: function(){
        this.setState({id: this.props.id});

        if(this.props.oldState){
            var date = this.props.oldState.value;
            this.setState({dateString: date.day + '.' + date.month +'.' + date.year});
        }
    },
    validateDate: function (input){
        if(!input.match("^([0-9]{1,2})([.])([0-9]{1,2})([.])([0-9]{4})$")){
            return false;
        }
        date = {};
        date.day = parseInt(input.split(".")[0])
        date.month = parseInt(input.split(".")[1])
        date.year = parseInt(input.split(".")[2])
        if((!date.day) || (!date.month) || (!date.year))
            return false;
        if((date.day > 31) || (date.day<=0))
            return false;
        if((date.month > 12) || (date.month<=0))
            return false;
        if((date.year > 2099) || (date.year<=1900))
            return false;
        return true;
    },
    /**
     * Parses dates, see previous method
     */
    parseDate: function (input){
        date = {};
        date.day = parseInt(input.split(".")[0])
        date.month = parseInt(input.split(".")[1])
        date.year = parseInt(input.split(".")[2])
        if(date.year <= 99){
            date.year = date.year + 2000;
        }
        return date;
    },
    /**
     * If date in input is valid, invokes callback,
     * given to constructor and shows badge to user
     */
    onInput: function (e) {
        e.preventDefault();
        var input = e.target.value;
        if(this.validateDate(input)){
            var date = this.parseDate(input);
            this.setState({dateString:input, ok: true});
            this.props.onChange({id:this.state.id, value:date});
            if (this.props.onChangeAdditional)
                this.props.onChangeAdditional();
        }else{
            this.setState({dateString:input, ok: false});
            if (this.props.onDateInvalidInput)
                this.props.onDateInvalidInput();
        }
    },
    onCalendarSelect: function(data){
        var str = data.date+'.'+data.month+'.'+data.year;
        this.setState({dateString:str, ok: true});
        this.props.onChange({id:this.state.id, value:this.parseDate(str)});
        this.toggleCalendar();
    },
    clearData: function(e) {
        this.setState({dateString:"", ok: false});
        this.props.onChangeAdditional();
    },
    /**
     * Renders badge
     */
    renderStatus: function(){
        if(this.state.ok){
            return <span className="glyphicon glyphicon-ok" onClick={this.clearData}/>
        }else{
            return <span className="glyphicon glyphicon-ban-circle" onClick={this.clearData}/>
        }
    },
    /**
     * Hides or shows calendar
     */
    toggleCalendar: function(visible){
        this.setState({calendarVisible: !this.state.calendarVisible});
    },
    /**
     * Renders popup calendar
     */
    renderCalendar: function(){
        var className;
        if(this.state.calendarVisible){
            className = "df-date-input-calendar-wrapper visible";
        }else{
            className = "df-date-input-calendar-wrapper hidden";
        }
        return  <div className={className}>
                    <DfCalendar onChange={this.onCalendarSelect}/>
                </div>
    },
    /**
     * Renders the whole component
     */
    renderComponent: function(){
        if(this.props.independent){
            return  <div className="field">
                        <div>
                            <span className="input-group">
                                <span className="input-group-btn">
                                    <button type="button" className="btn btn-primary" onClick={this.toggleCalendar}>
                                        <span className="glyphicon glyphicon-calendar"></span>
                                    </button>
                                </span>
                                <input type="text" className="form-control" onChange={this.onInput}
                                placeholder="DD.MM.YYYY" value={this.state.dateString} maxLength="10"/>
                                <span className="input-group-addon">
                                    {this.renderStatus()}
                                </span>
                            </span>
                            {this.renderCalendar()}
                        </div>
                    </div>
        } else {
            if (this.props.sm12) {
                return <div className="form-group df-date-input col-sm-12">
                        <div className="field">
                            <label>{this.props.title}</label>
                            <div>
                                <span className="input-group">
                                    <span className="input-group-btn">
                                        <button type="button" className="btn btn-primary" onClick={this.toggleCalendar}>
                                            <span className="glyphicon glyphicon-calendar"></span>
                                        </button>
                                    </span>
                                    <input type="text" className="form-control" onChange={this.onInput}
                                    placeholder="DD.MM.YYYY" value={this.state.dateString} maxLength="10"/>
                                    <span className="input-group-addon">
                                        {this.renderStatus()}
                                    </span>
                                </span>
                                {this.renderCalendar()}
                            </div>
                        </div>
                    </div>
                } else {
                    return <div className="form-group df-date-input col-sm-12 col-md-6 col-lg-4">
                        <div className="field">
                            <label>{this.props.title}</label>
                            <div>
                                <span className="input-group">
                                    <span className="input-group-btn">
                                        <button type="button" className="btn btn-primary" onClick={this.toggleCalendar}>
                                            <span className="glyphicon glyphicon-calendar"></span>
                                        </button>
                                    </span>
                                    <input type="text" className="form-control" onChange={this.onInput}
                                    placeholder="DD.MM.YYYY" value={this.state.dateString} maxLength="10"/>
                                    <span className="input-group-addon">
                                        {this.renderStatus()}
                                    </span>
                                </span>
                                {this.renderCalendar()}
                            </div>
                        </div>
                    </div>
                }
        }
    },
    render: function(){
        if(this.props.readonly){
            return  <div className="col-sm-12 col-md-6 col-lg-4">
                        <div className="field">
                            <b>{this.props.title}: </b>{this.state.dateString}
                        </div>
                    </div>
        } else {
            return this.renderComponent();
        }
    }
  });

  this.DfDateInput = DfDateInput;
}).call(this);
