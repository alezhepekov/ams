(function(){
    'use strict';
    var SimpleInput = React.createClass({
        getInitialState: function(){
            return {
                value: ''
            };
        },
        componentWillMount: function(){
            if(this.state.initialValue){
                this.setState({value: this.state.initialValue});
            }
        },
        componentWillReceiveProps: function(nextProps) {
          this.setState({
            value: nextProps.value
          });
        },
        onInput: function(e){
            if(this.props.onChange){
                this.props.onChange(e.target.value);
            }
            this.setState({value: e.target.value});
        },
        onSave: function(){
            this.props.onSave(this.state.value);
            this.setState({value: ''});
        },
        render: function(){
            var value = this.state.value;
            var placeholder = this.props.placeholder || 'Введите текст...';
            var buttonText = this.props.buttonText || 'Ok';
            
            if(this.props.withoutButton){
                return <input onChange={this.onInput} value={value} type='text' className='form-control' placeholder={placeholder}></input>;
            } else {
                return  <div className='input-group'>
                            <input onChange={this.onInput} value={value} type='text' className='form-control' placeholder={placeholder}>
                            </input>
                            <span className='input-group-btn'>
                                <button onClick={this.onSave} className='btn btn-default' type='button'>{buttonText}</button>
                            </span>
                        </div>;
            }
        }
    });

    this.SimpleInput = SimpleInput;
}).call(this);
