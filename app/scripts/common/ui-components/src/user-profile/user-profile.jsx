(function(){
    'use strict';
    var UserProfile = React.createClass({
        getInitialState: function(){
            return {
                profile: this.props.profile,
                password: {
                    old: '',
                    new: '',
                    confirm: ''
                }
            };
        },
        onFirstNameChange: function (e){
            var input = e.target.value;
            var profile = this.state.profile;
            profile.firstName = input;
            this.setState({profile: profile});
        },
        onLastNameChange: function (e){
            var input = e.target.value;
            var profile = this.state.profile;
            profile.lastName = input;
            this.setState({profile: profile});
        },
        onPositionChange: function (e){
            var input = e.target.value;
            var profile = this.state.profile;
            profile.position = input;
            this.setState({profile: profile});
        },
        onEmailChange: function (e){
            var input = e.target.value;
            var profile = this.state.profile;
            profile.email = input;
            this.setState({profile: profile});
        },
        onPhoneChange: function (e){
            var input = e.target.value;
            var profile = this.state.profile;
            profile.phone = input;
            this.setState({profile: profile});
        },
        onProfileSaveClick: function(e){
            /*if(!validate.profile(this.state.profile)){
                alertify.error('Не все поля заполнены валидно.');
                return;
            }*/
            if(this.props.onProfileSave){
                this.props.onProfileSave(this.state.profile);
            }
        },
        onOldPasswordChange: function(e){
            var input = e.target.value;
            var password = this.state.password;
            password.old = input;
            this.setState({password: password});
        },
        onNewPasswordChange: function(e){
            var input = e.target.value;
            var password = this.state.password;
            password.new = input;
            this.setState({password: password});
        },
        onConfirmPasswordChange: function(e){
            var input = e.target.value;
            var password = this.state.password;
            password.confirm = input;
            this.setState({password: password});
        },
        onPasswordResetClick: function(e){
            if((this.state.password.new !== this.state.password.confirm)){
                alertify.error('Пароли не совпадают');
                return;
            }
            if(this.state.password.new.length < 6){
                alertify.error('Слишком короткий пароль');
                return;
            }
            if(this.props.onPasswordReset){
                this.props.onPasswordReset(this.state.password);
            }
        },
        render: function() {
            return  <div className="panel panel-default user-profile">
                        {/*<div className="panel-heading">
                            <h4>Профиль</h4>
                        </div>*/}
                        <div className="panel-body">
                            <div className="row">
                                <h4> Личная и контактная информация</h4>
                            </div>
                            <div className="row">
                                <div className="col-md-2">
                                    <label>Имя</label>
                                </div>
                                <div className="col-md-6">
                                <input type="text" className="form-control"
                                    value={this.state.profile.firstName} onChange={this.onFirstNameChange}/>
                                </div>
                                <div className="col-md-4">
                                    <span className="hint">Только буквы, не менее 3 и не более 20.</span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-2">
                                    <label>Фамилия</label>
                                </div>
                                <div className="col-md-6">
                                <input type="text" className="form-control"
                                    value={this.state.profile.lastName}  onChange={this.onLastNameChange}/>
                                </div>
                                <div className="col-md-4">
                                    <span className="hint">Только буквы, не менее 3 и не более 20.</span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-2">
                                    <label>Должность</label>
                                </div>
                                <div className="col-md-6">
                                <input type="text" className="form-control"
                                    value={this.state.profile.position}  onChange={this.onPositionChange}/>
                                </div>
                                <div className="col-md-4">
                                    <span className="hint">Только буквы, не менее 3 и не более 50.</span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-2">
                                    <label>Email</label>
                                </div>
                                <div className="col-md-6">
                                <input type="text" className="form-control"
                                    value={this.state.profile.email}  onChange={this.onEmailChange}/>
                                </div>
                                <div className="col-md-4">
                                    <span className="hint">Валидный Email, например user@email.com</span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-2">
                                    <label>Номер телефона</label>
                                </div>
                                <div className="col-md-6">
                                <input type="text" className="form-control"
                                    value={this.state.profile.phone}  onChange={this.onPhoneChange}/>
                                </div>
                                <div className="col-md-4">
                                    <span className="hint">Только цифры.</span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-2">
                                    <button className="btn btn-primary" onClick={this.onProfileSaveClick}>
                                        <span className="glyphicon glyphicon-floppy-save" /> Сохранить
                                    </button>
                                </div>
                            </div>
                            <div className="row">
                                <h4>Смена пароля</h4>
                            </div>
                            <div className="row">
                                <div className="col-md-2">
                                    <label>Старый пароль</label>
                                </div>
                                <div className="col-md-6">
                                <input type="password" className="form-control"
                                    value={this.state.password.old} onChange={this.onOldPasswordChange}/>
                                </div>
                                <div className="col-md-4">
                                    <span className="hint">
                                        Необходим для проверки подлинности.
                                    </span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-2">
                                    <label>Новый пароль</label>
                                </div>
                                <div className="col-md-6">
                                <input type="password" className="form-control"
                                    value={this.state.password.new} onChange={this.onNewPasswordChange}/>
                                </div>
                                <div className="col-md-4">
                                    <span className="hint">Новый пароль. Не короче 8-ми символов.</span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-2">
                                    <label>Подтверждение</label>
                                </div>
                                <div className="col-md-6">
                                <input type="password" className="form-control"
                                value={this.state.password.confirm} onChange={this.onConfirmPasswordChange}/>
                                </div>
                                <div className="col-md-4">
                                    <span className="hint">Должно совпасть с новым паролем.</span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-2">
                                    <button className="btn btn-primary" onClick={this.onPasswordResetClick}>
                                        <span className="glyphicon glyphicon-floppy-save" /> Сменить пароль
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>;
        }
    });
    this.UserProfile = UserProfile;
}).call(this);
