/**
 * @jsx React.DOM
 */
(function(){
  var Comments = React.createClass({
    getInitialState: function(){
        return {
            newCommentMode: false,
            expanded: false,
            comment: {
                id: uuid.v1(),
                text: '',
                owner: null,
                creationDate: null,
            },
        }
    },
    newComment: function(){
        this.setState({newCommentMode: true});
    },
    onCommentInput: function(e){
        var value = e.target.value,
            resultComment = this.state.comment;

        resultComment.text = value;
        this.setState({comment: resultComment});
    },
    onSave: function(){
        var resultComment = this.state.comment;
        resultComment.owner = this.props.owner;
        resultComment.creationDate = new Date();

        this.setState({
            comment: {
                id: uuid.v1(),
            },
            newCommentMode: false,
            expanded: true,
        });
        if(this.props.onSave){
            this.props.onSave(resultComment);
        }
    },
    onCancel: function(){
        this.setState({
            newCommentMode: false,
        });
    },
    onExpandButtonClick: function(){
        this.setState({expanded: !this.state.expanded});
    },
    renderCommentPanel: function(comment){
        return <div>
                    <div className="col-lg-6">
                        <span className="label label-primary comments-big-label">Написал {comment.owner.firstName} {comment.owner.lastName}</span>
                    </div>
                    <div className="col-lg-6">
                        <div className="pull-right">
                            <span className="label label-primary comments-big-label">{moment(comment.creationDate).locale('ru').format('LLL')}</span>
                        </div>
                    </div>
                </div>
    },
    renderCommentBody: function(comment){
        return <div className="col-lg-12">
                    <div className="comments-comment-body">
                        {comment.text}
                    </div>
                </div>
    },
    renderComments: function(comment){
        return <div key={this.props.id} className="panel panel-default">
                    <div className="panel-body">
                        {this.renderCommentPanel(comment)}
                        {this.renderCommentBody(comment)}
                    </div>
                </div>
    },
    renderCommentForm: function(){
        if(this.state.newCommentMode){
            var text = this.state.comment.text;
            return <div className="col-lg-12">
                        <div className="col-lg-12">
                            <label htmlFor="comment-text">Новый комментарий</label>
                            <textarea onChange={this.onCommentInput} value={text} className="form-control" rows="5" id="comment-text" placeholder="Ваш комментарий..."></textarea>
                        </div>
                        <div className="col-lg-12">
                            <div className="pull-right">
                                <div className="btn-group" role="group">
                                  <button onClick={this.onCancel} type="button" className="btn btn-danger">Отмена</button>
                                  <button onClick={this.onSave} type="button" className="btn btn-success">Сохранить</button>
                                </div>
                            </div>
                        </div>
                    </div>
        }
    },
    renderMainPanel: function(){
        var wrapperClass = "comments",
            arrowClass = "glyphicon glyphicon-chevron-down comments-button";
        if(this.state.expanded){
            arrowClass = "glyphicon glyphicon-chevron-up comments-button";
        }

		if (this.props.comments.length == 0){
		
		return <div>
            <div className={wrapperClass}>
                <div className="col-lg-4">
                    <button onClick={this.newComment} type="button" className="btn btn-default">
                        <span className="glyphicon glyphicon-edit comments-button" aria-hidden="true"></span> Написать комментарий
                    </button>
                </div>
                <div className="col-lg-4">
                    <div className="comments-count-wrapper">
                        <div className="comments-count">
                            {this.props.comments.length} комментариев
                        </div>
                    </div>
                </div>
            </div>
        </div>
		
		}		
		
        return <div>
            <div className={wrapperClass}>
                <div className="col-lg-4">
                    <button onClick={this.newComment} type="button" className="btn btn-default">
                        <span className="glyphicon glyphicon-edit comments-button" aria-hidden="true"></span> Написать комментарий
                    </button>
                </div>
                <div className="col-lg-4">
                    <div className="comments-count-wrapper">
                        <div className="comments-count">
                            {this.props.comments.length} комментариев
                        </div>
                    </div>
                </div>
                <div className="col-lg-4">
                    <div className="pull-right">
                        <button onClick={this.onExpandButtonClick} type="button" className="btn btn-default">
                          <span className={arrowClass} aria-hidden="true"></span> Развернуть комментарии
                        </button>
                    </div>
                </div>
            </div>
        </div>
    },
    rednerCommentsList: function(){
        if(!this.state.newCommentMode){
            return this.props.comments.sort(function(a,b){
                                                return (-1 * (new Date(b.creationDate) - new Date(a.creationDate)));
                                            }).map(this.renderComments);
        }
    },
    renderBody: function(){
        if(this.state.expanded || this.state.newCommentMode){
            return <div className="panel panel-default">
                        <div className="panel-body">
                            <div className="comments-comment">
                                {this.rednerCommentsList()}
                                {this.renderCommentForm()}
                            </div>
                        </div>
                    </div>
        }
    },
    render: function(){
      return <div className="col-lg-12">
               {this.renderMainPanel()}
                <div className="col-lg-12">
                    {this.renderBody()}
                </div>
             </div>
    }
  });

  this.Comments = Comments;
}).call(this);
