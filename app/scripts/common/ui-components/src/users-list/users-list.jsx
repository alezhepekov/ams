(function(){
    'use strict';
    var UsersListItem = React.createClass({
        onDeleteClick: function(e){
            if(this.props.onDelete){
                this.props.onDelete(this.props.user);
            }
        },
        onEditClick: function(e){
            if(this.props.onEdit){
                this.props.onEdit(this.props.user);
            }
        },
        onResetClick: function(){
            if(this.props.onReset){
                this.props.onReset(this.props.user);
            }
        },
        render: function() {
            return  <li className="list-group-item user-list-item">
                        <div className="row">
                            <div className="col-md-3">
                                <p> <span className="glyphicon glyphicon-user" />
                                    {this.props.user.firstName} {this.props.user.lastName}
                                </p>
                                <p>
                                    {this.props.user.position} ({this.props.user.roleName ? this.props.user.roleName : 'не назначено'})
                                </p>
                            </div>
                            <div className="col-md-4">
                                <p><span className="glyphicon glyphicon-phone" />
                                    {this.props.user.phone}
                                </p>
                                <p><span className="glyphicon glyphicon-envelope" />
                                    {this.props.user.email}
                                </p>
                            </div>
                            <div className="col-md-1">
                                {(this.props.user.cloudToken ? <i className='fa fa-soundcloud fa-lg'></i> : <span></span>)}
                            </div>
                            <div className="col-md-4">
                                <div className="pull-right">
                                    <button className="btn btn-primary" onClick={this.onEditClick}>
                                        <span className="glyphicon glyphicon-pencil"/>
                                        Изменить
                                    </button>
                                    <button className="btn btn-default" onClick={this.onResetClick}>
                                        <span className="glyphicon glyphicon-pencil"/>
                                        Сброс пароля
                                    </button>
                                    <button className="btn btn-danger" onClick={this.onDeleteClick}>
                                        <span className="glyphicon glyphicon-trash"/>
                                        Удалить
                                    </button>
                                </div>
                            </div>
                        </div>
                    </li>;
        }
    });
    var UsersList = React.createClass({
        getInitialState: function(){
            return {
                users:this.props.users
            };
        },
        onAddClick: function (e) {
            if(this.props.onAdd){
                this.props.onAdd();
            }
        },
        onDelete: function(user){
            if(this.props.onDelete){
                this.props.onDelete(user);
            }
            var users = this.state.users.filter(function(u){
                return user._id !== u._id;
            });
            this.setState({users: users});
        },
        renderOrg: function(org){
            return  <option> {org.name} </option>;
        },
        renderUser: function(usr){
            return <UsersListItem user={usr} onEdit={this.props.onEdit} onDelete={this.onDelete} onReset={this.props.onReset}/>;
        },
        render: function(){
            return  <div className="panel panel-default user-list">
                        <div className="panel-heading">
                            <div className="row">
                                {/*<div className="col-md-5">
                                    <h3>Сотрудники</h3>
                                </div>
                                <div className="col-md-4">
                                    <select className="form-control">
                                        {this.props.organizations.map(this.renderOrg)}
                                    </select>
                                </div>*/}
                                <div className="col-md-12">
                                    <button className="btn btn-primary pull-right" onClick={this.onAddClick}>
                                        <span className="glyphicon glyphicon-plus"/>
                                        Добавить
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div className="panel-body">
                            <ul className="list-group">
                                {this.state.users.map(this.renderUser)}
                            </ul>
                        </div>
                    </div>;
        }
    });
    this.UsersList = UsersList;
}).call(this);
