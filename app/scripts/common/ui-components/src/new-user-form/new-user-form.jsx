(function(){
    'use strict';
    var RoleSelect = React.createClass({
        onSelect: function(e){
            e.preventDefault();
            var input = e.target.value;
            if(this.props.onSelect){
                this.props.onSelect(this.props.projectId, input);
            }
        },
        renderRoleOption: function(role){
            return <option value={role.id}>{role.name}</option>;
        },
        render: function(){
            return <select className="form-control" value={this.props.oldRoleId} onChange={this.onSelect}>
                        <option value="null"> &ndash; </option>
                        {this.props.roles.map(this.renderRoleOption)}
                    </select>
        }
    });

    var ProjectListForm = React.createClass({
        onRoleSelect: function(projId, roleId) {
            if (this.props.onChange) {
                this.props.onChange({projectId: projId, roleId: roleId});
            }
        },
        renderRoleSelect: function(project){
            var oldRole = this.props.roles.filter(function(r){
                return r.projectId === project._id;
            })[0];
            var oldRoleId;
            if(oldRole){
                oldRoleId = oldRole.roleId;
            }
            return <div className="row">
                        {/*<div className="col-md-3">
                            <a className="project-name-link" href={"#/projects/"+project._id}>{project.name}</a>
                        </div>*/}
                        <div className="col-md-8 roleLeftTrick">
                            <RoleSelect roles={project.roles} oldRoleId={oldRoleId} projectId={project._id} onSelect={this.onRoleSelect}/>
                        </div>
                    </div>
        },
        render: function(){
            return <div className="row">
                        <div className="col-md-2">
                            <label>Роли</label>
                        </div>
                        <div className="col-md-6">
                                {this.props.projects.map(this.renderRoleSelect)}
                        </div>
                    </div>
        }
    });
    var NewUserForm = React.createClass({
        getInitialState: function() {
            var emptyUser = {
                organizationId: "",
                firstName: "",
                lastName: "",
                email: "",
                position: "",
                phone: "",
                password: "",
                confirm: "",
                seeAllProjects: false,
                active: true,
                needsSMS: false,
                roles: [ ],
                cloud: {}
            };
            if(!this.props.user){
                return {
                    action: 'create',
                    user: emptyUser
                };
            }else{
                return {
                    action:'update',
                    user: this.props.user
                };
            }
        },
        onCancelClick: function(e){
            if(this.props.onCancel){
                this.props.onCancel();
            }
        },
        onSaveClick: function(e){
            e.preventDefault();
            if(this.props.onSave){
                this.props.onSave(this.state.user);
            }
        },
        onFirstNameChange: function(e){
            var input = e.target.value;
            var user = this.state.user;
            user.firstName = input;
            this.setState({user: user});
        },
        onLastNameChange: function(e){
            var input = e.target.value;
            var user = this.state.user;
            user.lastName = input;
            this.setState({user: user});
        },
        onPositionChange: function(e){
            var input = e.target.value;
            var user = this.state.user;
            user.position = input;
            this.setState({user: user});
        },
        onEmailChange: function(e){
            var input = e.target.value;
            var user = this.state.user;
            user.email = input;
            this.setState({user: user});
        },
        onPhoneChange: function(e){
            var input = e.target.value;
            var user = this.state.user;
            user.phone = input;
            this.setState({user: user});
        },
        onPasswordChange: function(e){
            var input = e.target.value;
            var user = this.state.user;
            user.password = input;
            this.setState({user: user});
        },
        onConfirmChange: function(e){
            var input = e.target.value;
            var user = this.state.user;
            user.confirm = input;
            this.setState({user: user});
        },
        onOrganizationSelect: function(e){
            var input = e.target.value;
            var user = this.state.user;
            user.organizationId = input;
            this.setState({user: user});
        },
        onRolesChange: function(role){
            var user = this.state.user;
            user.roles = user.roles.filter(function(r){
                return r.projectId !== role.projectId;
            });
            user.roles.push(role);
            this.setState({user: user});
        },
        onSeeAllProjectsChange: function(e){
            var u = this.state.user;
            u.seeAllProjects = e.target.checked;
            this.setState({user: u});
        },
        onActiveChange: function(e){
            var u = this.state.user;
            u.active = e.target.checked;
            this.setState({user: u});
        },
        onNeedsSMSChange: function(e){
            var u = this.state.user;
            u.needsSMS = e.target.checked;
            this.setState({user: u});
        },
        onCloudLoginChange: function(e){
            var user = this.state.user;
            user.cloud.login = e.target.value;
            this.setState({user: user});
        },
        onCloudPasswordChange: function(e){
            var user = this.state.user;
            user.cloud.password = e.target.value;
            this.setState({user: user});
        },
        renderHeading: function(){
            var title;
            if(this.state.action === 'create'){
                title = 'Добавить профиль';
            }else if(this.state.action === 'update'){
                title = 'Изменить профиль';
            }
            return  <div className="row">
                        <div className="col-md-8">
                            <h3>{title}</h3>
                        </div><div className="col-md-4">
                            <button type="button" className="btn btn-danger"
                                onClick={this.onCancelClick}>
                                <span className="glyphicon glyphicon-trash"/> Отменить
                            </button>
                            <button type="submit" className="btn btn-success"
                                onClick={this.onSaveClick}>
                                <span className="glyphicon glyphicon-floppy-save"/> Сохранить
                            </button>
                        </div>
                    </div>;
        },
        renderOrg: function (org){
            return <option value={org._id}>{org.name}</option>;
        },
        renderOrganizationSelect: function(){
            return  <div className="row">
                        <div className="col-md-2">
                            <label>Организация <span className="required" aria-hidden="true">*</span></label>
                        </div>
                        <div className="col-md-6">
                            <select onChange={this.onOrganizationSelect}
                                value={this.state.user.organizationId} className="form-control" required>
                                <option>Не выбрана.</option>
                                {this.props.organizations.map(this.renderOrg)}
                            </select>
                        </div>
                        <div className="col-md-4">
                            <span className="hint">Выберите организацию из списка.</span>
                        </div>
                    </div>;
        },
        renderFirstNameInput: function() {
            return  <div className="row">
                        <div className="col-md-2">
                            <label>Имя <span className="required" aria-hidden="true">*</span></label>
                        </div>
                        <div className="col-md-6">
                            <input type="text" className="form-control"
                                value={this.state.user.firstName} onChange={this.onFirstNameChange} required minlength="3" maxlength="20" />
                        </div>
                        <div className="col-md-4">
                            <span className="hint">Только буквы, не менее 3 и не более 20.</span>
                        </div>
                    </div>;
        },
        renderLastNameInput: function(){
            return  <div className="row">
                        <div className="col-md-2">
                            <label>Фамилия <span className="required" aria-hidden="true">*</span></label>
                        </div>
                        <div className="col-md-6">
                            <input type="text" className="form-control"
                                value={this.state.user.lastName} onChange={this.onLastNameChange}/>
                        </div>
                        <div className="col-md-4">
                            <span className="hint">Только буквы, не менее 3 и не более 20.</span>
                        </div>
                    </div>;
        },
        renderPositionInput: function(){
            return  <div className="row">
                        <div className="col-md-2">
                            <label>Должность <span className="required" aria-hidden="true">*</span></label>
                        </div>
                        <div className="col-md-6">
                            <input type="text" className="form-control"
                                value={this.state.user.position} onChange={this.onPositionChange}/>
                        </div>
                        <div className="col-md-4">
                            <span className="hint">Только буквы, не менее 3 и не более 50.</span>
                        </div>
                    </div>;
        },
        renderEmailInput: function(){
            return  <div className="row">
                        <div className="col-md-2">
                            <label>Email <span className="required" aria-hidden="true">*</span></label>
                        </div>
                        <div className="col-md-6">
                            <input type="email" className="form-control"
                                value={this.state.user.email} onChange={this.onEmailChange}/>
                        </div>
                        <div className="col-md-4">
                            <span className="hint">
                                Валидный Email, например user@example.org.
                            </span>
                        </div>
                    </div>;
        },
        renderPhoneInput: function(){
            return  <div className="row">
                        <div className="col-md-2">
                            <label>Номер телефона <span className="required" aria-hidden="true">*</span></label>
                        </div>
                        <div className="col-md-6">
                            <input type="text" className="form-control"
                                value={this.state.user.phone} onChange={this.onPhoneChange}/>
                        </div>
                        <div className="col-md-4">
                            <span className="hint">Только цифры.</span>
                        </div>
                    </div>;
        },
        renderPasswordInput: function(){
            if(this.state.action === 'update'){
                return;
            }
            return  <div className="row">
                        <div className="col-md-2">
                            <label>Пароль <span className="required" aria-hidden="true">*</span></label>
                        </div>
                        <div className="col-md-6">
                            <input type="password" className="form-control"
                                value={this.state.user.password} onChange={this.onPasswordChange} pattern="^[a-zA-Z0-9]+$"/>
                        </div>
                        <div className="col-md-4">
                            <span className="hint">Любая строка, не короче 8 символов.</span>
                        </div>
                    </div>;
        },
        renderConfirmInput: function(){
            if(this.state.action === 'update'){
                return;
            }
            return <div className="row">
                        <div className="col-md-2">
                            <label>Подтверждение <span className="required" aria-hidden="true">*</span></label>
                        </div>
                        <div className="col-md-6">
                            <input type="password" className="form-control"
                                value={this.state.user.confirm} onChange={this.onConfirmChange} pattern="^[a-zA-Z0-9]+$"/>
                        </div>
                        <div className="col-md-4">
                            <span className="hint">Пароль еще раз.</span>
                        </div>
                    </div>;
        },
        renderRoleSelect: function(){
            return <ProjectListForm projects={this.props.projects} roles={this.state.user.roles}
                        onChange={this.onRolesChange} />
        },
        renderSeeAllProjectsCheckbox: function(){
            return <div className="row">
                        <div className="col-md-2">
                            <label>Может видеть все проекты</label>
                        </div>
                        <div className="col-md-6 checkbox">
                            <input type="checkbox" className="checkbox"
                                checked={this.state.user.seeAllProjects} onChange={this.onSeeAllProjectsChange}/>
                        </div>
                        <div className="col-md-4">
                        </div>
                    </div>
        },
        renderActiveCheckbox: function(){
            return <div className="row">
                <div className="col-md-2">
                    <label>Активен</label>
                </div>
                <div className="col-md-6 checkbox">
                    <input type="checkbox" className="checkbox"
                        checked={this.state.user.active} onChange={this.onActiveChange}/>
                </div>
                <div className="col-md-4">
                </div>
            </div>
        },
        renderNeedsSMSCheckbox: function(){
            return <div className="row">
                <div className="col-md-2">
                    <label>Двухфакторная аутентификаци</label>
                </div>
                <div className="col-md-6 checkbox">
                    <input type="checkbox" className="checkbox"
                        checked={this.state.user.needsSMS} onChange={this.onNeedsSMSChange}/>
                </div>
                <div className="col-md-4">
                </div>
            </div>  
        },
        
        
        render: function(){
            return  <form>
                    <div className="panel panel-default new-user-form">
                        <div className="panel-heading">
                            {this.renderHeading()}
                        </div>
                        <div className="panel-body">
                            {/*this.renderOrganizationSelect()*/}
                            {this.renderLastNameInput()}
                            {this.renderFirstNameInput()}
                            {this.renderPositionInput()}
                            {this.renderEmailInput()}
                            {this.renderPhoneInput()}
                            {this.renderPasswordInput()}
                            {this.renderConfirmInput()}
                            {this.renderRoleSelect()}
                            {this.renderSeeAllProjectsCheckbox()}
                            {this.renderActiveCheckbox()}
                            {this.renderNeedsSMSCheckbox()}
                        </div>
                    </div>
                    </form>;
        }
    });
    this.NewUserForm = NewUserForm;
}).call(this);
