/**
 * @jsx React.DOM
 */
(function(){
    'use strict';
    var Tag = React.createClass({
        onDelete: function(){
            if(this.props.onDelete){
                this.props.onDelete(this.props.id);
            }
        },
        render: function(){
            return <div className='tag-list-badge badge'>
                        <table className='tag-list-badge-table'>
	                     <tr>
	                         <td><span className='tag-list-badge-text label'> {this.props.value}</span></td>
	                         <td><span onClick={this.onDelete} className='glyphicon glyphicon-remove tag-list-remove-tag'></span></td>
	                     </tr>
	                 </table>
                    </div>
        }
    });

    /**
     * [TagList - a list whose elements are tags]
     * @props {[{value: '', id: ''}]} items [an array of objects with two fields: value and id]
     * @props {[function]} onDelete [element deleting callback]
     */
    var TagList = React.createClass({
        onDelete: function(id){
            if(this.props.onDelete){
                this.props.onDelete(id);
            }
        },
        renderItems: function(item){
            return <Tag key={uuid.v1()} onDelete={this.onDelete} id={item.id} value={item.value}></Tag>;
        },
        renderTagsPanel: function(){
            if(this.props.items && this.props.items.length > 0){
                return  <div className='panel panel-default'>
                          <div className='panel-body'>
                            {this.props.items.map(this.renderItems)}
                          </div>
                        </div>
            }
        },
        render: function(){
            return <div>
                        {this.renderTagsPanel()}
                   </div>
        }
    });

    this.TagList = TagList;
}).call(this);
