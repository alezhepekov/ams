/**
 * @jsx React.DOM
 */
(function(Utils){
    'use strict';
    var LinksList = React.createClass({
        renderItem: function(item){
            var link,
                userAndDate;
            //if(item.id){ Зачем это нужно было?
            //    link = "#/admin/forms/" + item.id;
            //} else if(item.link){
                link = item.link;
            //}
            
            var uploadDate = '-';
            var uploadUser = '-';
            if (item.uploadDate) {
                uploadDate = moment(item.uploadDate).locale('ru').format('MM.DD.YYYY hh:mm');
            }

            if (item.uploadUser) {
                uploadUser = item.uploadUser.fullName;
            }

            var linkClass = this.props.getLinkClass(link);
            var fileIcon = this.props.getFileIcon(item.type);
            return <div className="row linkRow" key={uuid.v1()}>
                        <div className="col-lg-6">
                            <i className={fileIcon}></i> {item.name}
                        </div>
                        <div className="col-lg-2">
                            {uploadDate}
                        </div>
                        <div className="col-lg-2">
                            <span className="glyphicon glyphicon-user" aria-hidden="true"></span> {uploadUser}
                        </div>
                        <div className="col-lg-1">
                            <a onClick={this.props.processDocument.bind(this, item)} className={linkClass}>
                                <i className="fa fa-download fa-lg links-list-download-link-button"></i>
                            </a>
                        </div>
                        <div className="col-lg-1">
                            {this.renderDeleteButton(item)}
                        </div>
                    </div>
        },
        renderItems: function() {
            if (this.props.items)
                this.props.items.map(this.renderItem);
        },
        onDelete: function(item){
            console.log('links-list onDelete');
            if(this.props.onDelete){
                this.props.onDelete(item);
            }
        },
        renderTitle: function(){
            return this.props.title || 'Список';
        },
        renderDeleteButton: function(item){
            if(this.props.editable){
                return <span onClick={this.onDelete.bind(this, item)}
                    className="glyphicon glyphicon-remove pull-right links-list-delete-link-button" aria-hidden="true"></span>
            }
        },
        render: function(){
            return <div className="panel panel-default">
                        <div className="panel-heading">
                            <h3 className="panel-title">{this.renderTitle()}</h3>
                        </div>
                        <div className="panel-body">
                            <div className="row">
                                <div className="col-lg-6">
                                    Название
                                </div>
                                <div className="col-lg-2">
                                    Дата добавления
                                </div>
                                <div className="col-lg-2">
                                    Кто добавил
                                </div>
                                <div className="col-lg-1">
                                    Скачать
                                </div>
                                <div className="col-lg-1">
                                </div>
                            </div>
                            <div className="list-group">
                                {this.props.items.map(this.renderItem)}
                            </div>
                        </div>
                    </div>
        }
    });

    this.LinksList = LinksList;
}).call(this);
