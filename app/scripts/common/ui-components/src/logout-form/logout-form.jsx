(function (){
    'use strict';
    var LogoutForm = React.createClass({
        onAcceptClick: function(e){
            if(this.props.onAccept){
                this.props.onAccept();
            }
        },
        onDeclineClick: function(e){
            if(this.props.onDecline){
                this.props.onDecline();
            }
        },
        render: function(){
            return  <div className="panel panel-default logout-form">
                        <div className="panel-heading">
                            <h3>Уверены?</h3>
                        </div>
                        <div className="panel-body">
                            <button type="button" className="btn btn-primary" onClick={this.onDeclineClick}>
                                Отмена
                            </button>
                            <button type="button" className="btn btn-warning" onClick={this.onAcceptClick}>
                                Выйти
                            </button>
                        </div>
                    </div>;
        }
    });
    this.LogoutForm = LogoutForm;
}).call(this);