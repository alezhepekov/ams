(function(){
    'use strict';
    var NewOrgForm = React.createClass({
        getInitialState: function(){
            if(this.props.organization){
                return {
                    org: this.props.organization
                };
            }

            return {
                org:{
                    name:'',
                    description:''
                }
            };
        },
        onCancelClick: function(e){
            if(this.props.onCancel){
                this.props.onCancel();
            }
        },
        onSaveClick: function(e){
            if(this.props.onSave){
                this.props.onSave(this.state.org);
            }
        },
        onNameChange: function(e){
            var input = e.target.value;
            var org = this.state.org;
            org.name = input;
            this.setState({org: org});
        },
        onDescChange: function(e){
            var input = e.target.value;
            var org = this.state.org;
            org.description = input;
            this.setState({org: org});
        },
        render: function(){
            return  <div className="panel panel-default new-org-form">
                        <div className="panel-heading">
                            <div className="row">
                                <div className="col-md-8">
                                    <h3>Добавить организацию</h3>
                                </div>
                                <div className="col-md-4">
                                    <div className="pull-right">
                                         <button type="button" className="btn btn-danger"
                                            onClick={this.onCancelClick}>
                                            <span className="glyphicon glyphicon-trash"/> Отменить
                                        </button>
                                        <button type="submit" className="btn btn-success"
                                            onClick={this.onSaveClick}>
                                            <span className="glyphicon glyphicon-floppy-save"/> Сохранить
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="panel-body">
                            <div className="row">
                                <div className="col-md-2">
                                    <label>Название</label>
                                </div>
                                <div className="col-md-6">
                                    <input placeholder="Название огранизации" type="text" className="form-control"
                                        value={this.state.org.name} onChange={this.onNameChange}/>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-2">
                                    <label>Описание</label>
                                </div>
                                <div className="col-md-6">
                                    <textarea placeholder="Описание организации" className="form-control" value={this.state.org.description}
                                        onChange={this.onDescChange}/>
                                </div>
                            </div>
                        </div>
                    </div>;
        }
    });
    this.NewOrgForm = NewOrgForm;
}).call(this);
