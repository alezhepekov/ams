/**
 * @jsx React.DOM
 */

/**
 * EditableLabel component
 * 
 * @prop {function} onSave - saving callback
 * @prop {string} initialValue - a text which will be set as initial value 
 */
(function(){
    'use strict';
    var EditableLabel = React.createClass({
        getInitialState: function(){
            return {
                value: 'Текст',
                mouseOverLabel: false,
                editMode: false
            };
        },
        componentWillMount: function(){
            if(this.props.initialValue){
                this.setState({value: this.props.initialValue});
            }
        },
        onMouseOverLabel: function(){
            this.setState({mouseOverLabel: true});
        },
        mouseOutLabel: function(){
            this.setState({mouseOverLabel: false});
        },
        editModeOn: function(){
            this.setState({editMode: true});
        },
        saveValue: function(){
            this.setState({editMode: false});
            if(this.props.onSave){
                this.props.onSave(this.state.value);
            }
        },
        renderButton: function(){
            var classes = "glyphicon glyphicon-pencil editable-label-button";
            var clickAction = this.editModeOn;

            if(this.state.editMode){
                clickAction = this.saveValue;
                classes = "glyphicon glyphicon-ok editable-label-button-dark";
            } else {
                if(this.state.mouseOverLabel){
                    classes = "glyphicon glyphicon-pencil editable-label-button-dark";
                }
            }
            return <span onClick={clickAction} className={classes} aria-hidden="true"></span>;
        },
        onChangeValue: function(e){
            this.setState({value: e.target.value});
        },
        renderLabel: function(){
            if(this.state.editMode){
                var value = this.state.value;
                return <input onChange={this.onChangeValue} className="editable-label-input" type="text" value={value}></input>;
            } else {
                return <span className="editable-label-value">{this.state.value}</span>;
            }
        },
        render: function(){
            return  <div onMouseOver={this.onMouseOverLabel} onMouseOut={this.mouseOutLabel}>
                        {this.renderLabel()}
                        {this.renderButton()}
                    </div>;
        }
    });

    this.EditableLabel = EditableLabel;
}).call(this);