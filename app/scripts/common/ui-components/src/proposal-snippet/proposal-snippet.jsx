/**
 * @jsx React.DOM
 */
(function(){
    'use strict';
    var ProposalSnippet = React.createClass({
        getInitialState: function(){
            return {
                viewAdditionalInfo: false
            }
        },
        renderBadge: function(){
            var lastFinishedTask;
            if(this.props.proposal.tasks.length > 0 &&
                this.props.proposal.tasks[0].finished &&
                this.props.proposal.tasks[0].status &&
                this.props.proposal.status &&
                ((this.props.proposal.tasks[0].status.id === this.props.proposal.status.id) ||
                (this.props.proposal.status && this.props.proposal.status.complete))){
                lastFinishedTask = this.props.proposal.tasks[0];
            }
            var st = this.props.proposal.status;
            if(st == null){
                return <span className='pull-right label label-default'>Статус не был установлен</span>;
            }else if(lastFinishedTask){
                var className = 'pull-right label label-' + st.className;
                return <div>
                            <span className={className}><span className="glyphicon glyphicon-ok-sign" aria-hidden="true"></span> {moment(lastFinishedTask.finishingDate).locale('ru').format('LLL')}</span>
                            <span className={className}>{st.title}</span>
                        </div>;
            }else{
                var className = 'pull-right label label-' + st.className;
                return <span className={className}>{st.title}</span>;
            }
        },
		renderButton: function(options){
            var buttonsClass = options.enabledClass, /*this.state.isActive ? options.enabledClass : options.disabledClass,*/
                buttonType = this.props.finished ? options.finishedButtonGlyph : options.defaultButtonGlyph;
            if(this.props.finished){
                buttonsClass = options.finishedButtonClass;
            }
            if(options.visibilityValidation()){
                return  <div className="task-list-buttons-container" onClick={options.onClick}>
                            <div className="task-list-buttons-wrapper">
                                <div className={buttonsClass}>
                                    <span className={buttonType} aria-hidden="true"></span>
                                </div>
                            </div>
                        </div>
            }
        },
		renderRemoveButton: function(){
			if (!this.props.canDeleteProposal)
				return;

            /*if(!this.checkFields()){
                return;
            }

            var self = this;*/
            return this.renderButton({
                enabledClass: 'task-list-remove-button-enabled',
                disabledClass: 'task-list-remove-button-disabled',
                finishedButtonClass: 'task-list-remove-button-enabled',
                finishedButtonGlyph: 'glyphicon glyphicon-remove',
                defaultButtonGlyph: 'glyphicon glyphicon-remove',
                visibilityValidation: function() {return true;}, /*function(){
                    return ((self.props.currentUser._id === self.props.owner._id && !self.props.finished) || self.props.canEditTask || self.props.currentUser.isRoot) && self.props.editMode;
                },*/
                onClick: this.onDelete
            });
        },
        formatDate: function(date){
            return moment(date).locale('ru').format('LL')
        },
        getExecutor: function (id){
            var e = this.props.executors.filter(function(u){
                return u._id === id;
            })[0];
            return e;
        },
        getSpecialMark: function(){
            var result = '';
            if(this.props.proposal){
                if(this.props.proposal.lawCourt){   //статус может быть только один
                    result = 'proposal-snippet-lawCourt';
                } else if(this.props.proposal.sale){
                    result = 'proposal-snippet-sale';
                } else if(this.props.proposal.urgent){
                    result = 'proposal-snippet-urgent';
                } else if(this.props.proposal.status && this.props.proposal.status.complete){
                    result = 'proposal-snippet-complete';
                } else if(this.props.proposal.hold){
                    result = 'proposal-snippet-hold';
                } else if(this.props.proposal.canceled){
                    result = 'proposal-snippet-canceled';
                }
            }
            return result;
        },
		onDelete: function(){
            if(this.props.onDelete){
                this.props.onDelete(this.props.proposal._id);
            }
        },
        renderLiquidationSign: function(){
            if(this.props.proposal && this.props.proposal.type.id === 'liquidation') {
                return <span className="glyphicon glyphicon-off" aria-hidden="true" style={{color: '#f00', fontSize: '20px'}} title="Ликвидация"></span>
            }
        },
        renderReorganizationSign: function(){
            if(this.props.proposal && this.props.proposal.type.id === 'reorganization') {
                return <span className="glyphicon glyphicon-tree-deciduous" aria-hidden="true" style={{color: 'green', fontSize: '20px'}} title="Реорганизация"></span>
            }
        },
        renderSaleSign2: function(){
            if(this.props.proposal && this.props.proposal.type.id === 'sale') {
                return <span className="glyphicon glyphicon-euro" aria-hidden="true" style={{color: '#003153', fontSize: '20px'}} title="Продажа"></span>
            }
        },
        renderUrgentSign: function(){
            if(this.props.proposal){
                if(this.props.proposal.urgent){
                    return <span className="glyphicon glyphicon-flash" aria-hidden="true" style={{color: '#EFAA48', fontSize: '20px'}} title="Срочная"></span>
                }
            }
        },
        renderSaleSign: function(){
            if(this.props.proposal){
                if(this.props.proposal.sale){
                    return <span className="glyphicon glyphicon-briefcase" aria-hidden="true" style={{color: '#003153', fontSize: '20px'}} title="Продажа"></span>
                }
            }
        },
        renderLawCourtSign: function(){
            if(this.props.proposal){
                if(this.props.proposal.lawCourt){
                    return <span className="glyphicon glyphicon-education" aria-hidden="true" style={{color: '#003153', fontSize: '24px'}} title="Суд"></span>
                }
            }
        },
        renderHoldSign: function(){
            if(this.props.proposal){
                if(this.props.proposal.hold){
                    return <span className="glyphicon glyphicon-pause" aria-hidden="true" style={{color: '#353535', fontSize: '20px'}} title="Отложена"></span>
                }
            } else {
                console.log('nope');
            }
        },
        renderCanceledSign: function(){
            if(this.props.proposal){
                if(this.props.proposal.canceled){
                    return <span className="glyphicon glyphicon-ban-circle" aria-hidden="true" style={{color: '#CB3D39', fontSize: '20px'}} title="Отменена"></span>
                }
            }
        },
        renderOverdueSign: function(){
            if(this.props.proposal.tasks.length > 0 && this.props.proposal.tasks[0].dueDate && this.props.proposal.status && !this.props.proposal.status.complete){
                var dueDate = new Date(this.props.proposal.tasks[0].dueDate);
                if(dueDate.setDate(dueDate.getDate() + 1) <= new Date()){
                    return <span className="glyphicon glyphicon-exclamation-sign" aria-hidden="true" style={{color: '#CB3D39', fontSize: '20px'}} title="Просрочена"></span>
                }
            }
        },
        renderAgreementSign: function() {
            if(this.props.proposal && this.props.proposal.tasks) {
                var task = this.props.proposal.tasks.filter(function(t) {
                    return t.executor && t.executor.agree;
                })[0];

                if (task)
                    return <span className="glyphicon glyphicon-check" aria-hidden="true" style={{color: '#003153', fontSize: '20px'}} title="Ознакомлено"></span>
            }
        },
        renderCancelHoldReason: function(){
            if((this.props.proposal.hold || this.props.proposal.canceled) && this.props.proposal.cancelReason) {
                return <div className="col-lg-12">Заявка отменена/отложена по причине: <span style={{color: "#CB3D39"}}>{this.props.proposal.cancelReason}</span></div>
            }
        },
        renderAdditionalInfo: function(){
            var manager,
                owner,
                coordinator;

            if(this.props.proposal.manager) {
                manager = this.props.proposal.manager.firstName +' '+ this.props.proposal.manager.lastName;
            }else{
                manager = "-";
            }
            if(this.props.proposal.coordinator) {
                coordinator = this.props.proposal.coordinator.firstName +' '+ this.props.proposal.coordinator.lastName;
            }else{
                coordinator = "-";
            }
            if(this.props.proposal.owner) {
                owner = this.props.proposal.owner.firstName +' '+ this.props.proposal.owner.lastName;
            }else{
                owner = "-";
            }
            if(this.state.viewAdditionalInfo){
                return <div>
                            <div className='proposal-snippet-brief-info'>
                                <span className='glyphicon glyphicon-user'/>
                                <span>Супервайзер: <span className="label label-success">{manager}</span></span>
                            </div>
                            <div className='proposal-snippet-brief-info'>
                                <span className='glyphicon glyphicon-user'/>
                                <span>Координатор: <span className="label label-success">{coordinator}</span></span>
                            </div>
                            <div className='proposal-snippet-brief-info'>
                                <span className='glyphicon glyphicon-user'/>
                                <span>Создал заявку: <span className="label label-success">{owner}</span></span>
                            </div>
                        </div>
            }
        },
        changeAdditionalInfoVisibility: function(){
            this.setState({viewAdditionalInfo: !this.state.viewAdditionalInfo});
        },
        renderCloud: function(){
            if(this.props.proposal.DgiProjectNo){
                return <span className="glyphicon glyphicon-cloud proposal-snippet-special-mark" aria-hidden="true"></span>
            }
        },
        renderPhoto: function(){
            if(this.props.proposal.documents.some(function(d){return d.text === 'фото'})){
                return <span className="glyphicon glyphicon-camera proposal-snippet-special-mark" aria-hidden="true"></span>
            }
        },
        renderDocument: function(){
            if(this.props.proposal.documents.some(function(d){return d.text === 'документы'})){
                return <span className="glyphicon glyphicon-file proposal-snippet-special-mark" aria-hidden="true"></span>
            }
        },
        renderReport: function(){
            if(this.props.proposal.documents.some(function(d){return d.text === 'отчёт'})){
                return <span className="glyphicon glyphicon-folder-open proposal-snippet-special-mark" aria-hidden="true"></span>
            }
        },
        renderAdditionalInfoButton: function(){
            var type = 'default',
                text = 'Показать дополнительную информацию';
            if(this.state.viewAdditionalInfo){
                type = 'danger';
                text = 'Скрыть дополнительную информацию';
            }
            return  <button onClick={this.changeAdditionalInfoVisibility} type="button" className={"proposal-snippet-brief-info btn btn-xs btn-" + type}>{text}</button>
        },
        renderSnippet: function(){
            var link = '#/projects/' + this.props.proposal._id + '/main',
                itemClass = 'list-group-item ' + this.getSpecialMark(),
                owner,
                dueDate;

            if(this.props.proposal.owner) {
                owner = this.props.proposal.owner.firstName +' '+ this.props.proposal.owner.lastName;
            }else{
                owner = "-";
            }

            /*if(this.props.proposal.currentTask && !this.props.proposal.currentTask.finished && this.props.proposal.currentTask.executor) {
                executor = this.props.proposal.currentTask.executor.fullName;
            }else{
                executor = "-";
            }*/
            if(this.props.proposal.tasks.length > 0 && this.props.proposal.tasks[0].dueDate){
                dueDate = this.props.proposal.tasks[0].dueDate;
            }

            var renderDueDate = function(){
                if(dueDate){
                    return  <div className='proposal-snippet-brief-info'>
                                <span className='glyphicon glyphicon-time'/>
                                <span>Завершить до: {this.formatDate(dueDate)}</span>
                            </div>
                }
            }

            var showName = (this.props.orgMode) ? this.props.proposal.name : this.props.proposal.address;

            return  <li key={this.props.proposal.id} className={itemClass}>
                        <div className='row'>
                            <div className='col-md-6'>
                                <h4 className='proposal-snippet-title'>
                                    <span className='label label-primary'> № {this.props.proposal.number}</span> <a href={link}>{showName}</a>
                                </h4>
                                <div className="col-lg-12">
                                    {this.renderLiquidationSign()}
                                    {this.renderReorganizationSign()}
                                    {this.renderSaleSign2()}
                                    {this.renderLawCourtSign()}
                                    {this.renderSaleSign()}
                                    {this.renderUrgentSign()}
                                    {this.renderHoldSign()}
                                    {this.renderCanceledSign()}
                                </div>
                                {this.renderCancelHoldReason()}
                            </div>
                            <div className='col-md-4'>
                                <div className='proposal-snippet-brief-info'>
                                    <span className='glyphicon glyphicon-time'/>
                                    <span>Создана: {this.formatDate(this.props.proposal.started)}</span>
                                </div>
                                {renderDueDate.call(this)}
                                <div className='proposal-snippet-brief-info'>
                                    <span className='glyphicon glyphicon-user'/>
                                    <span>Создатель: <span className="label label-success">{owner}</span></span>
                                </div>
                            </div>
                            <div className='col-md-2'>
								<div className='pull-right'>
									{this.renderRemoveButton()}
								</div>
                                <div className='col-lg-12'>
                                    {this.renderBadge()}
                                </div>
                                <br/>
                                <div className='col-lg-12'>
                                    <div className='pull-right'>
										{this.renderCloud()}
										{this.renderDocument()}
										{this.renderPhoto()}
										{this.renderReport()}
										{this.renderOverdueSign()}
                                        {this.renderAgreementSign()}
                                    </div>
                                </div>
                            </div>
                       </div>
                   </li>;
        },
        render: function(){
            return <div className='row'>
                        <div className='col-md-12'>
                            {this.renderSnippet()}
                        </div>
                   </div>;
        }
    });

    this.ProposalSnippet = ProposalSnippet;
}).call(this);
