/**
 * @jsx React.DOM
 */
(function(){
    var TemplateList = React.createClass({
        onSelect: function(item){
            if(this.props.onSelect){
                this.props.onSelect(item);
            }
        },
        renderItem: function(item){
            return <li className="list-group-item"><button onClick={this.onSelect.bind(null,item)} className="btn btn-default form-control">{item.name}</button></li>;
        },
        newItem: function(){
            if(this.props.newItem){
                this.props.newItem();
            }
        },
        render: function(){
            return <div className="panel panel-default">
                        <div className="panel-heading">
                            <div className="row">
                                <div className="col-lg-8">
                                    <h3 className="panel-title">Список шаблонов форм</h3>
                                </div>
                                <div className="col-lg-4">
                                    <button onClick={this.newItem} className="btn btn-success pull-right" type="button">+</button>
                                </div>
                            </div>
                        </div>
                        <div className="panel-body">
                            <ul className="list-group">
                                {this.props.items.map(this.renderItem)}
                            </ul>
                        </div>
                    </div>
        }
    });

    var ProjectsList = React.createClass({
        onChange: function(e){
            var id = e.target.selectedOptions[0].value,
                project;

            if(this.props.items){
                this.props.items.forEach(function(item){
                    if(item._id === id){
                        project = item;
                    }
                });
            }
            if(project != null && this.props.onChange != null){
                this.props.onChange(project);
            }
        },
        renderItem: function(item){
            return <option value={item._id}>{item.name}</option>
        },
        render: function(){
            return  <select onChange={this.onChange} className="form-control">
                        <option>Выберите проект...</option>
                        {this.props.items.map(this.renderItem)}
                    </select>
        }
    });

    var AdminPanel = React.createClass({
        getInitialState: function(){
            return {
                selectedProject: null,
                selectedFormTemplate: null,
                proposalTemplateEditMode: false,
                templatesEditMode: false,
                newFormMode: false,
                editFormTeplateMode: false,
            }
        },
        onProposalTemplateSave: function(template){
            var tmpProject = this.state.selectedProject;
            tmpProject.proposalTemplate = template;
            this.setState({selectedProject: tmpProject});
            if(this.props.onSave){
                this.props.onSave(this.state.selectedProject);
            }
        },
        onProjectSelected: function(project){
            if(project){
                this.setState({selectedProject: project});
            }
        },
        onFormTemplateSelect: function(formTepmlate){
            if(formTepmlate){
                this.setState({
                    selectedFormTemplate: formTepmlate,
                    proposalTemplateEditMode: false,
                    templatesEditMode: true,
                    newFormMode: false,
                    editFormTeplateMode: true,
                });
            }
        },
        onFormTemplateSave: function(formTepmlate){
            var tmpProject = this.state.selectedProject,
                result = [];
            tmpProject.templates.forEach(function(t){
                if(t.id === formTepmlate.id){
                    result.push(formTepmlate);
                } else {
                    result.push(t);
                }
            });

            var tmpForms = tmpProject.templates.filter(function(t){
                return t.id === formTepmlate.id;
            });
            if(tmpForms.length === 0){
                result.push(formTepmlate);
            }

            tmpProject.templates = result;

            this.setState({
                selectedProject: tmpProject,
                newFormMode: false,
                editFormTeplateMode: false,
            });
            if(this.props.onSave){
                this.props.onSave(this.state.selectedProject);
            }
        },
        editProposalTemplate: function(){
            this.setState({
                proposalTemplateEditMode: true,
                templatesEditMode: false,
                newFormMode: false,
                editFormTeplateMode: false,
            });
        },
        editFormTemplates: function(){
            this.setState({
                proposalTemplateEditMode: false,
                templatesEditMode: true,
                newFormMode: false,
                editFormTeplateMode: false,
            });
        },
        renderProposalTemplateEditor: function(){
            if(this.state.proposalTemplateEditMode){
                return <EntitiesConstructor stepOneText={"Шаг 2: выберите тип нового поля заявки"} stepTwoText={"Шаг 3: назовите и сохраните новое поле в форму заявки"} onSave={this.onProposalTemplateSave} entity={this.state.selectedProject.proposalTemplate} templates={this.state.selectedProject.templates}/>
            }
        },
        renderFormTeplateEditor: function(){
            if(this.state.newFormMode){
                return <EntitiesConstructor stepOneText={"Шаг 2: выберите тип нового поля формы"} stepTwoText={"Шаг 3: назовите и сохраните новое поле в форму"} onSave={this.onFormTemplateSave} templates={this.state.selectedProject.templates}/>
            } else if(this.state.editFormTeplateMode){
                return <EntitiesConstructor stepOneText={"Шаг 2: выберите тип нового поля формы"} stepTwoText={"Шаг 3: назовите и сохраните новое поле в форму"} onSave={this.onFormTemplateSave} entity={this.state.selectedFormTemplate} templates={this.state.selectedProject.templates}/>
            }
        },
        onNewFormTemplate: function(){
            this.setState({
                proposalTemplateEditMode: false,
                templatesEditMode: true,
                newFormMode: true,
                editFormTeplateMode: false,
            });
        },
        renderTemplatesList: function(){
            if(!this.state.newFormMode && !this.state.editFormTeplateMode){
                return <div>
                            <h4>Шаг 2: добавьте новую форму или выберите из списка для редактирования</h4>
                            <TemplateList onSelect={this.onFormTemplateSelect} items={this.state.selectedProject.templates} newItem={this.onNewFormTemplate}/>
                        </div>
            }
        },
        renderTemplatesEditor: function(){
            if(this.state.templatesEditMode){
                return <div>
                            <div>
                                {this.renderTemplatesList()}
                                {this.renderFormTeplateEditor()}
                            </div>
                        </div>
            }
        },
        renderButtons: function(){
            if(this.state.selectedProject != null){
                return  <div className="btn-group">
                            <button onClick={this.editProposalTemplate} className="btn btn-default" type="button">Редактировать шаблон заявки</button>
                            <button onClick={this.editFormTemplates} className="btn btn-default" type="button">Редактировать шаблоны форм</button>
                        </div>
            }
        },
        render: function(){
          return <div className="row">
                    <div className="col-lg-12">
                        <h4>Шаг 1: выберите проект из списка и режим редактирования</h4>
                        <div className="form-group">
                            <div className="btn-group btn-group-justified">
                                <ProjectsList onChange={this.onProjectSelected} items={this.props.projects}/>
                            </div>
                        </div>
                        {this.renderButtons()}
                        <br/>
                        <br/>
                        {this.renderProposalTemplateEditor()}
                        {this.renderTemplatesEditor()}
                    </div>
                </div>
        }
    });

  this.AdminPanel = AdminPanel;
}).call(this);
