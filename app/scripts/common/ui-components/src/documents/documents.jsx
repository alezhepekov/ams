/**
/**
 * @jsx React.DOM
 */
(function(){
    var Documents = React.createClass({
            getInitialState: function(){
                return {
                    newDocumentMode: false,
                    expanded: false,
                    document: {
                        id: uuid.v1(),
                        name: '',
                        link: '',
                        uploadUser: null,
                        uploadDate: null
                    },
                }
            },
            newDocument: function(){
                this.setState({newDocumentMode: true});
            },
            onDocumentInput: function(e){
                var value = e.target.value,
                    resultDocument = this.state.document;

                resultDocument.name = value;
                this.setState({document: resultDocument});
            },
            onSave: function(){
                var resultDocument = this.state.document;
                resultDocument.uploadUser = this.props.owner;
                resultDocument.uploadDate = new Date();

                console.log('SAVE');
                this.setState({
                    document: {
                        id: uuid.v1(),
                    },
                    newDocumentMode: false,
                    expanded: true,
                });
                if(this.props.onSave){
                    console.log('props.save');
                    this.props.onSave(resultDocument);
                    this.props.onLoadFinish();
                }
            },
            onCancel: function(){
                this.setState({
                    newDocumentMode: false,
                });
            },
            upload: function(docName){
                var fileSelector = document.createElement('input');
                fileSelector.setAttribute('type', 'file');

                //fileSelector.setAttribute('Bearer', this.props.owner.cloudToken);
                //var bearer = this.props.owner.cloudToken;
                var self = this;

                fileSelector.onchange = function(e){
                    var fileList = fileSelector.files;
                    if (fileList.length > 0){
                        var file = fileList[0];

                        //var bearer = this.getAttribute('Bearer');

                        var fd = new FormData();
                        fd.append('OnlyAuth', 'true');
                        fd.append("file", file);

                        var xhr = new XMLHttpRequest();
                        xhr.open("POST", 'http://cloud.abn-consult.ru/api/files/upload');
                        xhr.setRequestHeader("Authorization", 'Bearer ' + self.props.owner.cloudToken);
                        xhr.send(fd);

                        xhr.onload = function () {
                            try {
                                var res = JSON.parse(xhr.responseText);
                                self.state.document.name = (docName) ? docName : res.file.FileName;
                                self.state.document.link = res.file.Link;
                                self.onSave();
                            } catch (e) {
                                console.error(e);
                                return this.onError();
                            }
                            //console.log(res);
                        };
                    }
                }
                fileSelector.click();
            },
            addLink: function(docName, docLink){
                console.debug('addlink');

                if (docName.length==0) {
                    alertify.error('Введите название документа');
                    return;
                }

                var urlCheck = /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/;
                if (docLink.length==0) {
                    alertify.error('Введите адрес URL');
                    return;
                }

                if (!urlCheck.test(docLink)) {
                    alertify.error('Введите корректный адрес URL');
                    return;
                }

                this.state.document.name = docName;
                this.state.document.link = docLink;
                this.onSave();
            },
            onDelete: function (item) {
                console.log('documents onDelete');
                if(this.props.onDelete){
                    this.props.onDelete(item);
                }
            },
            /*renderMainPanel: function(){
                var wrapperClass = "comments",
                    arrowClass = "glyphicon glyphicon-chevron-down comments-button";
                if(this.state.expanded){
                    arrowClass = "glyphicon glyphicon-chevron-up comments-button";
                }

                    return <div>
                    <div className={wrapperClass}>
                        <div className="col-lg-12">
                            <DocsList currentUser={this.props.currentUser} title={"Файлы и документы"} items={this.props.documents} onChange={this.onSave} upload={this.upload} onSave={this.addLink} onDelete={this.onDelete}/>
                        </div>
                    </div>
                    </div>
            },*/
            render: function(){
                return  <div className="col-lg-12">
                            <DocsList
                                currentUser={this.props.currentUser} title={"Файлы и документы"}
                                items={this.props.documents}
                                onChange={this.onSave} upload={this.upload} onSave={this.addLink} onDelete={this.onDelete}
                                onLoadFinish={this.props.onLoadFinish}
                                onOpenEditor={this.props.onOpenEditor}
                                getLinkClass={this.props.getLinkClass}
                                getFileIcon={this.props.getFileIcon}
                                processDocument={this.props.processDocument}/>
                        </div>
            }
});

this.Documents = Documents;
}).call(this);
