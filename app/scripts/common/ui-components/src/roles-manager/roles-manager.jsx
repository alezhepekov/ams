/**
 * @jsx React.DOM
 */
(function(){
    var RolesEditor = React.createClass({
        getInitialState: function(){
            return {
                id: uuid.v1(),
                name: '',
                seeAllProposals: false,
                seeAllProjects: false,
                editAllProposals: false,
                editAllTasks: false,
                removeProposals: false,
                completeAllTasks: false,
                seeStatistic: false,
                seeMaps: false,
                dataExprot: false
            }
        },
        componentWillMount: function(){
            if(this.props.role){
                this.setState({
                    id: this.props.role.id,
                    name: this.props.role.name,
                    seeAllProposals: this.props.role.seeAllProposals,
                    seeAllProjects: this.props.role.seeAllProjects,
                    editAllProposals: this.props.role.editAllProposals,
                    editAllTasks: this.props.role.editAllTasks,
                    removeProposals: this.props.role.removeProposals,
                    completeAllTasks: this.props.role.completeAllTasks,
                    seeStatistic: this.props.role.seeStatistic,
                    seeMaps: this.props.role.seeMaps,
                    dataExprot: this.props.role.dataExprot
                });
            }
        },
        onSeeAllProposalsClick: function(){
            this.setState({seeAllProposals: !this.state.seeAllProposals});
        },
        onSeeAllProjectsClick: function(){
            this.setState({seeAllProjects: !this.state.seeAllProjects});
        },
        onEditAllProposalsClick: function(){
            this.setState({editAllProposals: !this.state.editAllProposals});
        },
        onRemoveProposalsClick: function(){
            this.setState({removeProposals: !this.state.removeProposals});
        },
        onCompleteAllTasksClick: function(){
            this.setState({completeAllTasks: !this.state.completeAllTasks});
        },
        onEditAllTasksClick: function(){
            this.setState({editAllTasks: !this.state.editAllTasks});
        },
        onSeeStatisticClick: function(){
            this.setState({seeStatistic: !this.state.seeStatistic});
        },
        onSeeMapsClick: function(){
            this.setState({seeMaps: !this.state.seeMaps});
        },
        onDataExportClick: function(){
            this.setState({dataExprot: !this.dataExprot});
        },
        onNameInput: function(e){
            this.setState({name: e.target.value});
        },
        onSave: function(){
            if(this.props.onSave){
                this.props.onSave(this.state);
                this.setState({
                    name: '',
                    seeAllProposals: false,
                    seeAllProjects: false,
                    editAllProposals: false,
                    editAllTasks: false,
                    removeProposals: false,
                    completeAllTasks: false,
                    seeStatistic: false,
                    seeMaps: false,
                    dataExprot: false
                });
            }
        },
        renderRoleForm: function(){
            var name = this.state.name;
            return <div>
                        <div className="col-lg-12">
                            <div className="input-group">
                                <span className="input-group-addon"><span className="glyphicon glyphicon-user" aria-hidden="true"></span></span>
                                <input onChange={this.onNameInput} type="text" className="form-control" placeholder="Название роли" value={name}/>
                                <span className="input-group-btn">
                                    <button onClick={this.onSave} className="btn btn-success" type="button">Сохранить</button>
                                </span>
                            </div>
                        </div>
                        <div className="col-lg-12">
                            <div className="col-lg-4">
                               <div className="checkbox">
                                   <label>
                                       <input type="checkbox" checked={this.state.seeAllProposals} onChange={this.onSeeAllProposalsClick}/> Просмотр всех заявок
                                   </label>
                               </div>
                            </div>
                            <div className="col-lg-4">
                               <div className="checkbox">
                                   <label>
                                       <input type="checkbox" checked={this.state.seeAllProjects} onChange={this.onSeeAllProjectsClick}/> Просмотр всех проектов
                                   </label>
                               </div>
                            </div>
                            <div className="col-lg-4">
                               <div className="checkbox">
                                   <label>
                                       <input type="checkbox" checked={this.state.editAllProposals} onChange={this.onEditAllProposalsClick}/> Редактирование всех заявок
                                   </label>
                               </div>
                            </div>
                            <div className="col-lg-4">
                               <div className="checkbox">
                                   <label>
                                       <input type="checkbox" checked={this.state.removeProposals} onChange={this.onRemoveProposalsClick}/> Удаление заявок
                                   </label>
                               </div>
                            </div>
                            <div className="col-lg-4">
                               <div className="checkbox">
                                   <label>
                                       <input type="checkbox" checked={this.state.completeAllTasks} onChange={this.onCompleteAllTasksClick}/> Завершение всех задач
                                   </label>
                               </div>
                            </div>
                            <div className="col-lg-4">
                               <div className="checkbox">
                                   <label>
                                       <input type="checkbox" checked={this.state.editAllTasks} onChange={this.onEditAllTasksClick}/> Редактирование всех задач
                                   </label>
                               </div>
                            </div>
                            <div className="col-lg-4">
                               <div className="checkbox">
                                   <label>
                                       <input type="checkbox" checked={this.state.seeStatistic} onChange={this.onSeeStatisticClick}/> Просмотр отчетов
                                   </label>
                               </div>
                            </div>
                            <div className="col-lg-4">
                               <div className="checkbox">
                                   <label>
                                       <input type="checkbox" checked={this.state.seeMaps} onChange={this.onSeeMapsClick}/> Просмотр карт
                                   </label>
                               </div>
                            </div>
                            <div className="col-lg-4">
                               <div className="checkbox">
                                   <label>
                                       <input type="checkbox" checked={this.state.dataExprot} onChange={this.onDataExportClick}/> Выгрузка данных
                                   </label>
                               </div>
                            </div>
                        </div>
                   </div>
        },
        render: function(){
            return <div className="col-lg-12">
                       {this.renderRoleForm()}
                   </div>
        }
    });
    var RolesManager = React.createClass({
        getInitialState: function(){
            return {
                editMode: false,
                selectedProject: null,
                roleForEdit: null,
            }
        },
        onSave: function(role){
            var resultProject = this.state.selectedProject;

            if(resultProject.roles.filter(function(r){return role.id === r.id;}).length > 0){
                var resultRoles = [];
                resultProject.roles.forEach(function(r){
                    if(r.id === role.id){
                        resultRoles.push(role);
                    } else {
                        resultRoles.push(r);
                    }
                });
                resultProject.roles = resultRoles;
            } else {
                resultProject.roles.push(role);
            }

            this.setState({
                selectedProject: resultProject,
                editMode: false,
                roleForEdit: null,
            });
            if(this.props.onSave){
                this.props.onSave(resultProject);
            }
        },
        onProjectSelect: function(e){
            var value = e.target.selectedOptions[0].value,
                project;

            this.props.projects.forEach(function(p){
                if(p._id === value){
                    p.roles = p.roles ? p.roles : [];
                    project = p;
                }
            });
            if(project){
                this.setState({selectedProject: project});
            }
        },
        changeMode: function(){
            this.setState({editMode: !this.state.editMode});
        },
        editRole: function(role){
            this.setState({
                editMode: true,
                roleForEdit: role,
            });
        },
        deleteRole: function(roleId){
            var self = this;
            alertify.confirm('Вы уверены, что хотите удалить эту роль?', function(e){
                if(e){
                    var resultProject = self.state.selectedProject;
                    resultProject.roles = resultProject.roles.filter(function(r){
                        return r.id !== roleId;
                    });

                    self.setState({selectedProject: resultProject});
                    if(self.props.onSave){
                        self.props.onSave(resultProject);
                    }
                }
            });
        },
        renderButtons: function(){
            var buttonClasses = "btn btn-primary pull-right",
                glyphiconClasses = "glyphicon glyphicon-edit",
                text = "Добавить роль";
            if(this.state.editMode){
                buttonClasses = "btn btn-danger pull-right";
                glyphiconClasses = "glyphicon glyphicon-remove";
                text = "Отмена";
            }
            return <button onClick={this.changeMode} type="button" className={buttonClasses}>
                        <span className={glyphiconClasses} aria-hidden="true"></span> {text}
                    </button>
        },
        renderRoleMark: function(flag){
            if(flag){
                return <div className="roles-manager-role-mark">
                            <span className="glyphicon glyphicon-ok" style={{color: "#77AC4E"}} aria-hidden="true"></span>
                        </div>
            } else {
                return <div className="roles-manager-role-mark">
                            <span className="glyphicon glyphicon-remove" style={{color: "#C9302C"}} aria-hidden="true"></span>
                        </div>
            }
        },
        renderRoleRow: function(role){
            return <tr key={role.id}>
                        <th scope="row">{role.name}</th>
                        <td>{this.renderRoleMark(role.seeAllProposals)}</td>
                        <td>{this.renderRoleMark(role.seeAllProjects)}</td>
                        <td>{this.renderRoleMark(role.editAllProposals)}</td>
                        <td>{this.renderRoleMark(role.removeProposals)}</td>
                        <td>{this.renderRoleMark(role.completeAllTasks)}</td>
                        <td>{this.renderRoleMark(role.editAllTasks)}</td>
                        <td>{this.renderRoleMark(role.seeStatistic)}</td>
                        <td>{this.renderRoleMark(role.seeMaps)}</td>
                        <td>{this.renderRoleMark(role.dataExprot)}</td>
                        <td>
                            <div className="roles-manager-role-mark">
                                <button onClick={this.editRole.bind(null, role)} type="button" className="btn btn-primary">
                                  <span className="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                </button>
                            </div>
                        </td>
                        <td>
                            <div className="roles-manager-role-mark">
                                <button onClick={this.deleteRole.bind(null, role.id)} type="button" className="btn btn-danger">
                                  <span className="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                </button>
                            </div>
                        </td>
                    </tr>
        },
        renderRolesTable: function(){
            var roles = [];
            if(this.state.selectedProject){
                roles = this.state.selectedProject.roles;
            }
            return <table className="table table-bordered">
                      <thead>
                        <tr>
                            <th>Роль</th>
                            <th>Просмотр всех заявок</th>
                            <th>Просмотр всех проектов</th>
                            <th>Редактирование всех заявок</th>
                            <th>Удаление заявок</th>
                            <th>Завершение всех задач</th>
                            <th>Редактирование всех задач</th>
                            <th>Просмотр отчетов</th>
                            <th>Просмотр карт</th>
                            <th>Выгрузка данных</th>
                            <th>Редактировать</th>
                            <th>Удалить</th>
                        </tr>
                      </thead>
                      <tbody>
                        {roles.map(this.renderRoleRow)}
                      </tbody>
                    </table>
        },
        renderBody: function(){
            if(this.state.editMode){
                return <RolesEditor onSave={this.onSave} role={this.state.roleForEdit}/>
            } else {
                return this.renderRolesTable();
            }
        },
        renderProjectItem: function(project){
            return <option key={project._id} value={project._id}>{project.name}</option>
        },
        renderProjectsList: function(){
            return  <select onChange={this.onProjectSelect} className="form-control">
                        <option>Выберите проект...</option>
                        {this.props.projects.map(this.renderProjectItem)}
                    </select>
        },
        render: function(){
            return <div className="panel panel-default">
                        <div className="panel-heading">
                            <div className="row">
                                {/*<div className="col-lg-4">
                                    <h4 className="panel-title">Список ролей</h4>
                                </div>
                                <div className="col-lg-4">
                                    {this.renderProjectsList()}
                                </div>*/}
                                <div className="col-lg-12">
                                    {this.renderButtons()}
                                </div>
                            </div>
                        </div>
                        <div className="panel-body">
                            {this.renderBody()}
                        </div>
                    </div>

        }
    });

  this.RolesManager = RolesManager;
}).call(this);
