(function (){
    'use strict';
    var SMSCheck = React.createClass({
        getInitialState: function(){
            return {code: ""};
        },
        onCodeChange: function(e){
            var input = e.target.value;
            this.setState({code: input});
        },
        onSubmit: function(e){
            e.preventDefault();
            if(this.props.onSubmit){
                this.props.onSubmit(parseInt(this.state.code));
            }
        },
        render: function(){
            return  <div className="panel panel-default smscheck">
                        <div className="panel-heading">
                            <h3>Введите код, полученный в СМС</h3>
                        </div>
                        <div className="panel-body">
                            <form onSubmit={this.onSubmit}>
                                <div className="row">
                                    <div className="col-md-9">
                                        <input type="password" value={this.state.code} className="form-control" onChange={this.onCodeChange}/>
                                    </div>
                                    <div className="col-md-3">
                                        <button type="submit" className="btn btn-primary">
                                            <span className="glyphicon glyphicon-user"/> Войти
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>;
        }
    });
    this.SMSCheck = SMSCheck;
}).call(this);
