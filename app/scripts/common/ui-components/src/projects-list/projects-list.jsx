/**
 * @jsx React.DOM
 */
(function(){
    'use strict';
    var Project = React.createClass({
        onSelectClick: function(e){
            if(this.props.onSelect){
                this.props.onSelect(this.props.project);
            }
        },
        onRemoveClick: function(e){
            if(this.props.onRemove){
                this.props.onRemove(this.props.project);
            }
        },
        renderDeleteButton: function(){
            if(this.props.currentUser && this.props.currentUser.isRoot){
                return  <button type="button" className="btn btn-danger pull-right" onClick={this.onRemoveClick}>
                            <span className="glyphicon glyphicon-trash" /> Удалить
                        </button>
            }
        },
        render: function() {
            return  <li className='list-group-item'>
                        <div className="row">
                            <div className="col-md-9">
                                <h3><a onClick={this.onSelectClick}>{this.props.project.name}</a></h3>
                                <span>{this.props.project.description}</span>
                            </div>
                            <div className="col-md-3">
                                {this.renderDeleteButton()}
                            </div>
                        </div>
                    </li>;
        }
    });
    var ProjectsList = React.createClass({
        onRemove: function(project){
            this.props.onRemove(project);
        },
        renderProject: function (project){
            return <Project currentUser={this.props.currentUser} key={uuid.v1()} onRemove={this.onRemove}
                project={project} onSelect={this.props.onSelect}/>;
        },
        renderCreateButton: function(){
            if(this.props.currentUser && this.props.currentUser.isRoot){
                return  <button onClick={this.props.onNewClick} type='button' className='btn btn-primary pull-right'>
                            <span className='glyphicon glyphicon-plus' /> Создать
                        </button>
            }
        },
        render: function () {
            return  <div className='panel panel-default projects-select'>
                        <div className='panel-heading'>
                            <div className='row'>
                                <div className='col-md-12'>
                                    <div className='col-md-4'>
                                        <h3 className='heading'>Выберите проект:</h3>
                                    </div>
                                    <div className='col-md-6'>
                                    </div>
                                    <div className='col-md-2'>
                                        {this.renderCreateButton()}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='panel-body'>
                            <ul className='list-group'>
                                {this.props.projects.map(this.renderProject)}
                            </ul>
                        </div>
                    </div>;
        }
    });
    this.ProjectsList = ProjectsList;
}).call(this);

