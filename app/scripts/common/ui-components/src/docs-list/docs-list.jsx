/**
 * @jsx React.DOM
 */
(function(){
  'use strict';
  var DocsList = React.createClass({
    getInitialState: function(){
        return {
            id: uuid.v1(),
            docName: '',
            docLink: '',
            docs: (this.props.items && this.props.items.length > 0) ? this.props.items : []
        };
    },
    componentWillMount: function(){
        if(this.props.items){
            this.setState({docs: this.props.items});
        }
    },
    onDocNameChange: function(docName){
        this.setState({docName: docName});
    },
    onDocLinkChange: function(docLink){
        this.setState({docLink: docLink});
    },
    onDelete: function(item){
        var self = this;
        console.log('doc-list onDelete');
        console.log(item);
        alertify.confirm('Вы уверены, что хотите удалить данный документ?', function (e) {
            if (e) {
                var resultDocs = self.state.docs.filter(function (doc) {
                    if (doc.id) {
                        return item.id !== doc.id;
                    } else {
                        return (item.name !== doc.name && item.link !== doc.link);
                    }
                });

                console.log(resultDocs);
                self.setState({
                    d: uuid.v1(),
                    docName: '',
                    docLink: '',
                    docs: resultDocs
                });
                console.log('state');
                console.log(self.state);
                self.onChange();

                if(self.props.onDelete){
                    self.props.onDelete(resultDocs);
                }
            }
        });
    },
    onSave: function(){
        if(this.props.onSave){
            console.log('onSave1');
            this.props.onSave(this.state.docName, this.state.docLink);
            this.state.docName = "";
            this.state.docLink = "";
        } else {
            console.log('onSave2');
            var result = this.state.docs;
            result.push({
                name: this.state.docName,
                link: this.state.docLink,
                id: this.state.id,
                user: this.props.currentUser.fullName,
                date: new Date(),
            });
            this.setState({
                id: uuid.v1(),
                docName: '',
                docLink: '',
                docs: result
            });
            this.onChange();
        }
    },
    onChange: function(){
        console.log('onChange start');
        if(this.props.onChange){
            console.log('onChange docs');
            this.props.onChange(this.state.docs);
        }
    },


    upload: function(){
        if(this.props.upload){
            this.props.upload(this.state.docName);
            this.state.docName = "";
            //this.render();
            //this.props.onLoadFinish();
        }
    },

    render: function(){
        var task = {a: '1'};
        var doc = null; //{ name: '123', htmlContent: '<h1>Введите текст документа...</h1><br/><br/><br/><br/><br/>', type: 'intDoc', link: '' };
      return <div className="col-lg-12">                
                <div className="col-lg-6">
                    <SimpleInput placeholder={"Введите название документа"} value={this.state.docName} buttonText="Загрузить" onChange={this.onDocNameChange} onSave={this.upload}/>
                </div>
                <div className="col-lg-6">
                    <SimpleInput placeholder={"Введите url-адрес документа"} value={this.state.docLink} buttonText="Добавить" onChange={this.onDocLinkChange} onSave={this.onSave}/>
                </div>
                <div className="col-lg-12">
                    <button type="button" className="btn btn-default openDocumentEditor" onClick={this.props.onOpenEditor.bind(this, this.props.items, doc)}>
                        <span className="glyphicon glyphicon-plus" aria-hidden="true"></span> Добавить документ в редакторе
                    </button>
                </div>
            
            <div className="col-lg-12 docTable">
                <LinksList items={this.props.items} title={"Список документов"} editable={true} onDelete={this.onDelete}
                    getLinkClass={this.props.getLinkClass}
                    getFileIcon={this.props.getFileIcon}
                    processDocument={this.props.processDocument}/>
            </div>
         </div>
    }
  });

  this.DocsList = DocsList;
}).call(this);
