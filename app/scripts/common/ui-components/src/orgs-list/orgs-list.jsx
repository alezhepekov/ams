(function(){
    'use strict';
    var OrgListItem = React.createClass({
        onEditClick: function(){
            if(this.props.onEdit){
                this.props.onEdit(this.props.org);
            }
        },
        onRemoveClick: function(){
            if(this.props.onRemove){
                this.props.onRemove(this.props.org);
            }
        },
        render: function(){
            return  <li className="list-group-item">
                        <div className="row">
                            <div className="col-md-9">
                                <h4>{this.props.org.name}</h4>
                            </div>
                            <div className="col-md-3">
                                <div className="pull-right">
                                    <button className="btn btn-primary" onClick={this.onEditClick}>
                                        <span className="glyphicon glyphicon-pencil" /> Изменить
                                    </button>
                                    <button className="btn btn-danger" onClick={this.onRemoveClick}>
                                        <span className="glyphicon glyphicon-trash" /> Удалить
                                    </button>
                                </div>
                            </div>
                        </div>
                    </li>;
        }
    });
    var OrgsList = React.createClass({
        getInitialState: function(){
            return {orgs: this.props.organizations};
        },
        onAddClick: function(){
            if(this.props.onAdd){
                this.props.onAdd();
            }
        },
        onRemove: function(org){
            var orgs = this.state.orgs.filter(function(o){
                return o._id !== org._id;
            });
            this.setState({orgs:orgs});
            if(this.props.onRemove){
                this.props.onRemove(org);
            }
        },
        onEdit: function(org){
            if(this.props.onEdit){
                this.props.onEdit(org);
            }
        },
        renderOrg:function (org) {
            return <OrgListItem org={org} onEdit={this.onEdit} onRemove={this.onRemove}/>;
        },
        render: function(){
            return  <div className="panel panel-default">
                        <div className="panel-heading">
                            <div className="row">
                                {/*<div className="col-md-9">
                                    <h3>Список организаций</h3>
                                </div>*/}
                                <div className="col-md-12">
                                    <button className="btn btn-primary pull-right" onClick={this.onAddClick}>
                                        <span className="glyphicon glyphicon-plus"/>
                                        Добавить
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div className="panel-body">
                            <ul className="list-group">
                                {this.state.orgs.map(this.renderOrg)}
                            </ul>
                        </div>
                    </div>;
        }
    });

    this.OrgsList = OrgsList;
}).call(this);
