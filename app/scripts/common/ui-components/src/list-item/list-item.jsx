/**
 * @jsx React.DOM
 */
(function(){
    'use strict';
    var ListItemEditor = React.createClass({
        getInitialState: function(){
            return {
                item: {}
            };
        },
        componentWillMount: function(){
            if(this.props.item){
                this.setState({item: this.props.item});
            }
        },
        onNameChange: function(e){
            var resultItem = this.state.item;
            resultItem.entity.entityName = e.target.value;
            this.setState({item: resultItem});
        },
        onListItemDelete: function(id){
            var resultListItems = this.state.item.entity.listItems.filter(function(item){
                return item.id !== id;
            });

            var resultItem = this.state.item;
            resultItem.entity.listItems = resultListItems;
            this.setState({item: resultItem});
        },
        renderAdditionalEditor: function(){
            if(this.state.item.entity.listItems && this.state.item.entity.listItems.length > 0){
                return <div className='col-md-12'>
                            <h5> Элементы списка </h5>
                            <TagList items={this.state.item.entity.listItems} onDelete={this.onListItemDelete} />
                        </div>;
            }
        },
        onSave: function(){
            if(this.props.onSave){
                this.props.onSave(this.state.item);
            }
        },
        onCancel: function(){
            if(this.props.onCancel){
                this.props.onCancel();
            }
        },
        render: function(){
            var name = this.state.item.entity.entityName;
            return  <div>
                        <div className='row'>
                            <div className='col-md-12'>
                                <h5> Название поля </h5>
                                <input type="text" onChange={this.onNameChange} value={name} className="form-control" placeholder="Название поля"></input>
                            </div>
                            {this.renderAdditionalEditor()}
                        </div>
                        <br></br>
                        <br></br>
                        <div className='row'>
                            <div className='col-md-12'>
                                <button onClick={this.onCancel} className="btn btn-danger pull-right entities-constructor-spaces" type="button">Отменить</button>
                                <button onClick={this.onSave} className="btn btn-success pull-right entities-constructor-spaces" type="button">Сохранить</button>
                            </div>
                        </div>
                    </div>;
        }
    });

    var ListItem = React.createClass({
        getInitialState: function(){
            return {
                mouseOverMenuButton: false,
                mouseOverEditButton: false,
                mouseOverRemoveButton: false,
                menuOpened: false,
                editMode: false,
            };
        },
        onMenuButtonClick: function(){
            this.setState({menuOpened: !this.state.menuOpened});
        },
        getMenuButton: function(){
            var classes = 'glyphicon glyphicon-cog list-item-big-icon list-item-with-space-around list-item-menu-button';
            if(this.state.menuOpened){
                classes = 'glyphicon glyphicon-cog list-item-big-icon list-item-with-space-around list-item-menu-button-active';
            }

            return <span onClick={this.onMenuButtonClick} className={classes}></span>;
        },
        getEditButton: function(){
            var classes = 'glyphicon glyphicon-pencil list-item-big-icon list-item-with-space-around list-item-edit-button';
            return <span onClick={this.onEditButtonClick} className={classes}></span>;
        },
        onEditButtonClick: function(){
            this.setState({editMode: true});
        },
        getRemoveButton: function(){
            var classes = 'glyphicon glyphicon-remove list-item-big-icon list-item-with-space-around list-item-remove-button';

            if(!this.state.editable){
                classes += ' list-item-gray';
            }
            return <span onClick={this.onRemoveButtonClick} className={classes}></span>;
        },
        onRemoveButtonClick: function(){
            if(this.props.onDelete){
                this.props.onDelete(this.props.data);
            }
        },
        createLabel: function(label){
            var classes = label.classes + ' list-item-with-space-around';
            return <span className={classes}>{label.content}</span>;
        },
        getContent: function(data){
            var labels = [],
                content = data.content || '';
            if(this.props.data.labels === null){
                return <span className='list-item-big-text'>{content}</span>;
            }
            if(this.props.data.labels.label){
                labels.push({classes: 'label label-primary', content: this.props.data.labels.label});
            }
            if(this.props.data.labels.labelGreen){
                labels.push({classes: 'label label-success', content: this.props.data.labels.labelGreen});
            }
            if(this.props.data.labels.labelRed){
                labels.push({classes: 'label label-danger', content: this.props.data.labels.labelRed});
            }
            if(this.props.data.labels.labelYellow){
                labels.push({classes: 'label label-warning', content: this.props.data.labels.labelYellow});
            }

            return <span className='big-text'>{content}{labels.map(this.createLabel)}</span>;
        },
        getMenuItems: function(){
            var classes = 'hidden';
            if(this.state.menuOpened){
                classes = '';
            }
            return <div className={classes}>
                        {this.getEditButton()}
                        {this.getRemoveButton()}
                   </div>;
        },
        getMenu: function(){
            if(this.state.editMode){
                return;
            }
            if(this.props.editable){
                return  <div className='pull-right'>
                            <div className='list-item-line-element'>
                                {this.getMenuButton()}
                            </div>
                            <div className='list-item-line-element'>
                                {this.getMenuItems()}
                            </div>
                        </div>;
            } else {
                return  <div className='pull-right'>
                            {this.getRemoveButton()}
                        </div>;
            }
        },
        onItemUpdate: function(item){
            if(this.props.onUpdate){
                this.props.onUpdate(item);
            }
            this.setState({editMode: false});
        },
        onItemEditCancel: function(){
            this.setState({editMode: false});
        },
        renderEditor: function(){
            if(this.state.editMode){
                return <ListItemEditor onSave={this.onItemUpdate} onCancel={this.onItemEditCancel} item={this.props.data}></ListItemEditor>;
            }
        },
        renderElement: function(){
            if(!this.state.editMode){
                return  <div className='row'>
                            <div className='col-md-8'>
                                {this.getContent(this.props.data)}
                            </div>
                            {this.getMenu()}
                        </div>;
            }
        },
        render: function(){
            return <div>
                        {this.renderElement()}
                        {this.renderEditor()}
                    </div>;
        }
    });

    this.ListItemEditor = ListItemEditor;
    this.ListItem = ListItem;
}).call(this);
