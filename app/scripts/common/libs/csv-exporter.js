angular.module('crmApp').factory('CsvExporter',['$q',
    function ($q){
      var csvDecimal = ',' //'.'
    , csvSeparator = ';' //','
    , surroundRE = new RegExp('("|'+csvSeparator+'|\n)', 'g')
    , quoteRE = /"/g
    ;

    function csvEncode(val) {
        var valType = typeof(val);
        if (valType === 'undefined' || val === null || valType === 'object' || valType === 'function') {
            val = '';
        }else if (valType === 'number') {
            //val = isNaN(val) ? '':val.format(undefined, '', csvDecimal);
            val = isNaN(val) ? '':val;
        }else {
            if (valType !== 'string') {
                val = val.toString();
            }
            // escape
            val = val.replace(quoteRE, '""');
            if (val.search(surroundRE) >= 0) {
                val = '"' + val + '"';
            }else if (val.substr(-1)==='Z') {
                //val = Date.create(val).format('{dd}.{MM}.{yyyy} {hh}:{mm}:{ss}');
                val = new Date(val);
            }
        }
        return val;
    }   
    
    var isSupported = !!new Blob && !!window['URL'];
    
    return {
        isSupported: isSupported,
        
        save: function(filename, fldList, data) {
            var rows = [], i, l, c, cl, r, rl, val, row=[], pathComponents, csv;
            
            for (i = 0, l = fldList.length; i<l; i++){
                val = fldList[i].label;
                row.push(csvEncode(val));
            }
            rows.push(row.join(csvSeparator));

            for (r = 0, rl = data.length; r<rl; r++) {
                row = [];
                for (i = 0, l = fldList.length; i<l; i++){
                    pathComponents  = fldList[i].path.split('.'), val = data[r];
                    for (c = 0, cl = pathComponents.length; c<cl; c++){
                        val = val[pathComponents[c]];
                        if (typeof(val) === 'undefined' || !val) {
                            break; // stop going further if the path is broken
                        }
                    }
                    row.push(csvEncode(val));
                }
                rows.push(row.join(csvSeparator));
            }
            csv = rows.join("\n");
            var csvBlob = new Blob(['\uFEFF'+csv], {type: 'text/csv;charset=UTF-8'});

            //var a = document.createElementNS("http://www.w3.org/1999/xhtml", "a");a.href='';a.download='';
            var a = $('<a/>', {
                'download': filename
                ,'href': window.URL.createObjectURL(csvBlob)
            })[0];
            a.click();
            a.remove();
            // $link.attr("href", 'data:Application/octet-stream,' + encodeURIComponent(csv))[0].click();

            //return Actions.File.saveFile(Date.create().format('{yyyy}-{MM}-{dd}-{hh}{mm}{ss}'), '\uFEFF'+csv, 'text/csv; charset=UTF-8');     
        }
    };


    }
])
