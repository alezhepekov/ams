 $(document).ready(function() {
 	/* Push the body and the nav over by 285px over */
 	$('.icon-menu').click(function() {
 		$('.icon-menu').animate({
 			width: '240px'
 		});
 		$('.menu').css("width", "240px");

 		$('.menu-item').css("display", "inline-block");

 		$('.active-item').css("background-color", "#1e282c");
 	});
 	$('.icon-menu').mouseleave(function() {
 		$('.icon-menu').animate({
 			width: '50px'
 		});
 		$('.menu').css("width", "50px");
 		$('.menu-item').css("display", "none");
 		$('.active-item').css("background-color", "transparent");
 		$('.sub-menu').css("display", "none");
 	});
 	$('.dropdown-list').click(function() {
 		$('.sub-menu').css("display", "block");
 	});
 	$('.sub-menu').mouseleave(function() {
 		$('.sub-menu').css("display", "none");
 	});
 });