angular.module('crmApp').factory('Utils',['$filter', '$http', 'cfpLoadingBar', 'uuid2', 'Proposal', 'CONFIG',
    function ($filter, $http, cfpLoadingBar, uuid2, Proposal, CONFIG) {
    	
    	var getFileType = function(fileName) {
    		var pointIndex = fileName.lastIndexOf('.');
            if (pointIndex>0) {
                var potentialExt = fileName.substr(pointIndex + 1, fileName.length - pointIndex);
                if (potentialExt==='txt')
                    return 'text';
                if (potentialExt==='png' || potentialExt==='jpg' || potentialExt==='bmp' || potentialExt==='gif')
                    return 'image';
                if (potentialExt==='rar' || potentialExt==='zip' || potentialExt==='7z')
                    return 'archive';
                if (potentialExt==='doc' || potentialExt==='docx' || potentialExt==='odt')
                    return 'document';
                if (potentialExt==='xls' || potentialExt==='xlsx')
                    return 'etable';
                if (potentialExt==='ppt' || potentialExt==='pptx')
                    return 'presentation';

                return 'file';
            }

            return 'link';
    	};

    	return {
            isEmpty: function(obj) {
                if (obj == undefined) return true;
                for(var key in obj) {
                    if (obj.hasOwnProperty(key)) {
                        return false;
                    }
                }
                return true;
            },
            deleteElemById: function(arr, object) {
                var indx = -1;
                var k = 0;
                arr.forEach(function(arrObject) {
                    if (arrObject._id == object._id)
                        indx = k;
                    k++;
                });

                if (indx > -1) {
                    arr.splice(indx, 1);
                    return true;
                }

                return false;
            },
            deleteElem: function(arr, object) {
            	if (!arr || !object)
            		return false;

            	if (arr.length == 0)
            		return false;

                var idx = arr.indexOf(object);
                if (idx > -1) {
                    arr.splice(idx, 1);
                    return true;
                }
                return false;
            },
            onlyCurrentStatuses: function(id) {
                var res = false;
                CONFIG.ONLY_CURRENT_STATUSES.forEach(function(projectId) {
                    if (projectId === id)
                        res = true;
                });
                return res;
            },
            isOrgMode: function(id) {
                var res = false;
                CONFIG.ORG_MODE.forEach(function(projectId) {
                    if (projectId === id)
                        res = true;
                });
                return res;
            },
            getOrgName: function(orgId) {
                //TODO: Сделать загрузку списка организаций с сервера
                var org = CONFIG.ORGS.filter(function(o) {
                    return o.id == orgId;
                })[0];
                return (org) ? org.name : '';
            },
            getOrgId: function(orgName) {
                var org = CONFIG.ORGS.filter(function(o) {
                    return o.name == orgName;
                })[0];
                return (org) ? org.id : '';
            },
            isRootOrg: function(orgId) {
                var rootOrgIds = [
                    '54c53693a8a6e6d9a0866e0a',
                    '54c5eaa45079053c0350d085',
                    '55372317852193b41657450f',
                    '55a66bd3369a9b8c1d8ae45a',
                    '55ad04a658e88f78186a5ab6'];
                return rootOrgIds.filter(function(o){
                    return o == orgId;
                })[0];
            },
            getProjectTypes: function() {
                return [ {id: 'liquidation', name: 'Ликвидация предприятия'}, {id: 'reorganization', name: 'Реорганиазация'}, {id: 'sale', name: 'Продажа' }];
            },
            getISODate: function(strDate) {
            	if (strDate[strDate.length-1]==='Z') {
            		return strDate;
            	}
                var dArr = strDate.split(".");
                var normalDate = new Date(parseInt(dArr[2]), parseInt(dArr[1])-1, parseInt(dArr[0]));
                return normalDate.toISOString();
            },
            getFileType: function(fileName) {
                return getFileType(fileName);
            },
            getFileIcon: function(fileType) {
                if (fileType==='link')
                    return 'fa fa-external-link';
                if (fileType==='intDoc')
                    return 'fa fa-th-large';
                if (fileType==='image')
                    return 'fa fa-file-image-o';
                if (fileType==='archive')
                    return 'fa fa-file-archive-o';
                if (fileType==='text')
                    return 'fa fa-file-text-o';
                if (fileType==='document')
                    return 'fa fa-file-word-o';
                if (fileType==='etable')
                    return 'fa fa-file-excel-o';
                if (fileType==='presentation')
                    return 'fa fa-file-powerpoint-o';
                
                return 'fa fa-file';
            },
            lightenDarkenColor: function(col, amt) {
                var usePound = false;
              
                if (col[0] == "#") {
                    col = col.slice(1);
                    usePound = true;
                }
             
                var num = parseInt(col,16);
             
                var r = (num >> 16) + amt;
             
                if (r > 255) r = 255;
                else if  (r < 0) r = 0;
             
                var b = ((num >> 8) & 0x00FF) + amt;
             
                if (b > 255) b = 255;
                else if  (b < 0) b = 0;
             
                var g = (num & 0x0000FF) + amt;
             
                if (g > 255) g = 255;
                else if (g < 0) g = 0;
             
                return (usePound?"#":"") + (g | (b << 8) | (r << 16)).toString(16);
            },
            getReducedUser: function(user) {
                var resUser = {
                    _id: user._id,
                    organizationId: user.organizationId,
                    firstName: user.firstName,
                    lastName: user.lastName,
                    fullName: user.fullName,
                    email: user.email,
                    position: user.position,
                    active: user.active,
                    phone: user.phone,
                    isRoot: user.isRoot,
                    seeAllProjects: user.seeAllProjects
                };

                return resUser;
            },
            uploadFile: function(documents, docName, scope, success, error, isComplete) {
            	if (!(scope && scope.currentUser && scope.currentUser.cloudToken && scope.currentUser.cloudToken.length > 0)) {
		    		alertify.error('Авторизуйтесь в Cloud!');
		    		return;
		    	}

            	var fileSelector = document.createElement('input');
		        fileSelector.setAttribute('type', 'file');
                
		        fileSelector.onchange = function(e) {
		            if (fileSelector.files.length > 0) {                     
		                var file = fileSelector.files[0];
		                var fileType = getFileType(file.name);

		                var fd = new FormData();
		                fd.append('OnlyAuth', 'true');
		                fd.append("file", file);
                      
                        var XHR = ("onload" in new XMLHttpRequest()) ? XMLHttpRequest : XDomainRequest;
                        var xhr = new XHR();

                        xhr.upload.onprogress = function(event) {
                            //console.log('Загружено на сервер ' + event.loaded + '/' + event.total);
                            scope.uploadedPercents = Math.floor((event.loaded / event.total) * 100);
                            scope.$apply();
                        }

                        xhr.upload.onerror = function() {
                            console.log('Произошла ошибка при загрузке данных на сервер!');
                        }

		                xhr.onload = function () {
		                    try {
		                        var res = JSON.parse(xhr.responseText);
		                        var newDoc = {
		                        	id: uuid2.newuuid(),
		                        	name: res.file.FileName,
                                    size: res.file.ContentLength,
		                        	link: res.file.Link,
		                        	type: fileType,
		                        	uploadDate: new Date().toISOString(),
		                        	uploadUser: scope.currentUser
		                        };

		                        documents.push(newDoc);
                                scope.uploadedPercents = 0;
                                
		                        success();
		                    } catch (e) {		                    
		                        error(e);		                       
		                    }
		                };

                        scope.uploadedPercents = 0;
                        xhr.open('POST', CONFIG.CLOUD_ADDRESS + '/api/files/upload');
                        xhr.setRequestHeader("Authorization", 'Bearer ' + scope.currentUser.cloudToken);
                        xhr.send(fd);
		            }
		        }
		        fileSelector.click();
            },
            checkCloud: function() {
                var XHR = ("onload" in new XMLHttpRequest()) ? XMLHttpRequest : XDomainRequest;
                var xhr = new XHR();

                xhr.upload.onerror = function() {
                    console.log("Cloud is off-line");
                }
                
                xhr.onload = function () {
                    console.log("Cloud is on-line");
                };

                xhr.open('GET', CONFIG.CLOUD_ADDRESS, true);
                xhr.send();
            },
            addLink: function(docs, doc, currentUser, success, isComplete) {
            	if (!(doc && doc.link && doc.link.length > 0)) {
		            alertify.error('Введите адрес URL');
		            return;
		        }

		        var urlCheck = /^(ftp|https?):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?$/;
		        if (!urlCheck.test(doc.link)) {
		            alertify.error('Введите корректный адрес URL');
		            return;
		        }
		      
		        doc.id = uuid2.newuuid();
		        doc.uploadDate = new Date().toISOString();
		        doc.uploadUser = currentUser;

		        if (doc.isComplete)
		        	doc.isComplete = true;

		        docs = docs || [];

		        docs.push(doc);
		        success();		      
            },
            proccessDocument: function(scope, doc, fileProps) {
            	if (doc.type==='intDoc') {
		            scope.openEditor(doc);
		            return;
		        }

		        var link = doc.link;
		        if (!(link.indexOf('cloud') > 0 && link.indexOf('abn-consult.ru') > 0)) {
		            window.open(link, '_blank');
		            fileProps.result = 'doneAnother';
		            Proposal.downloadFileCloud(fileProps);
		        } else {
		            var linkParts = link.split('/');
		            var fileId = linkParts[linkParts.length-1];

		            $http.get(CONFIG.CLOUD_ADDRESS + '/api/files/' + fileId)
		                .success(function(data, status, headers, config) {
		                    fileProps.fileName = data.file.Name;
		                    fileProps.fileSize = data.file.Length;

		                    alertify.confirm('Вы действительно хотите сохранить файл <b>' + fileProps.fileName + '</b> (' + fileProps.fileSize + ') из Fincase.Cloud?', function (e) {
		                        if (e) {
		                            scope.downloading = true;
		                            cfpLoadingBar.includeBar = true;
		                            cfpLoadingBar.includeSpinner = false;
		                            cfpLoadingBar.start();

		                            $http({method: 'GET', url: data.file.Link,		                            
		                                headers:{
		                                    'Accept': 'application/json'
		                                }, responseType: 'blob'})
		                                .success(function(data, status, headers, config) {
		                                    scope.downloading = false;
		                                    cfpLoadingBar.complete();
		                                    var a = document.createElement("a");
		                                    document.body.appendChild(a);
		                                    a.style.display = "none";
		                                    var url = window.URL.createObjectURL(data);
		                                    a.href = url;
		                                    a.download = fileProps.fileName;
		                                    a.click();
		                                    //window.open(url);
		                                    window.URL.revokeObjectURL(url);
		                                    fileProps.result = 'done';
		                                    Proposal.downloadFileCloud(fileProps);
		                                })
		                                .error(function(data, status, headers, config) {
		                                    scope.downloading = false;
		                                    alertify.error('Ошибка при скачаивании файла!');
		                                    fileProps.result = 'error';
		                                    Proposal.downloadFileCloud(fileProps);
		                                });
		                        }
		                    });
		                })
		                .error(function(data, status, headers, config) {
		                    scope.downloading = false;
		                    alertify.error('Не удалось сохранить файл!')
		                    fileProps.result = 'error2';
		                    Proposal.downloadFileCloud(fileProps);
		            });
		        }
            },
            getRoles: function() {
                return [ 
                    {
                        "name" : "Руководитель проекта",
                        "createNewProject" : true,
                        "seeAllProposals" : true,
                        "seeAllProjects" : true,
                        "editProjectCard" : true,
                        "seeAllTasks" : true,
                        "editAllProposals" : true,
                        "createNewTask" : true,
                        "editAllTasks" : true,
                        "removeProposals" : true,
                        "completeAllTasks" : true,
                        "seeReports" : true,
                        "seeMaps" : true,
                        "dataExprot" : true,
                        "id" : "4ae44500-fca7-11e5-939f-43d1ff18ea7e"
                    }, 
                    {
                        "name" : "Исполнитель",
                        "seeAllProposals" : false,
                        "seeAllProjects" : false,
                        "editProjectCard" : false,
                        "seeAllTasks" : false,
                        "editAllProposals" : true,
                        "editAllTasks" : false,
                        "removeProposals" : false,
                        "completeAllTasks" : false,
                        "seeReports" : false,
                        "seeMaps" : true,
                        "id" : "4ae44501-fca7-11e5-939f-43d1ff18ea7e"
                    }, 
                    {
                        "name" : "Секретарь",
                        "seeAllProposals" : true,
                        "seeAllProjects" : true,
                        "seeAllTasks" : false,
                        "editAllProposals" : false,
                        "editAllTasks" : false,
                        "removeProposals" : false,
                        "completeAllTasks" : false,
                        "seeReports" : false,
                        "seeMaps" : false,
                        "id" : "4ae44502-fca7-11e5-939f-43d1ff18ea7e"
                    }
                ];
            },
            getOrganizations: function() {
                return [
                    {
                        "_id" : "54c53693a8a6e6d9a0866e0a",
                        "name" : "Развитие активов (РА)",
                        "description" : "Развитие активов (РА)"
                    }
                ]
            },
            getProjects: function() {
                return [{
                    _id: 'ams',
                    roles: this.getRoles()
                }]
            }
    	}
    }]
);