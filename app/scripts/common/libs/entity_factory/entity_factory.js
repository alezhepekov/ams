(function() {
    function EntityFactory(currentProject) {
        this.currentProject = currentProject;
    }
    EntityFactory.fieldTypes = {
        TextField: function(title, name, fieldId, placeholder) {
            this.title = title;
            this.name = name;
            this.id = fieldId;
            this.placeholder = placeholder;
        },
        ConstantList: function(title, name, fieldId, items) {
            this.title = title;
            this.name = name;
            this.id = fieldId;
            this.items = items;
        },
        EmptyTextList: function(title, name, fieldId) {
            this.title = title;
            this.name = name;
            this.id = fieldId;
        },
        EmptyEntityList: function(title, name, fieldId, templateId) {
            this.title = title;
            this.name = name;
            this.id = fieldId;
            this.templateId = templateId;
        },
        DateField: function(title, name, fieldId) {
            this.title = title;
            this.name = name;
            this.id = fieldId;
        },
        TimeField: function(title, name, fieldId) {
            this.title = title;
            this.name = name;
            this.id = fieldId;
        },
        EntityField: function(title, name, fieldId, entityId, fields) {
            this.title = title;
            this.name = name;
            this.id = fieldId;
            this.entityId = entityId;
            this.fields = fields;
        }
    }

    EntityFactory.prototype.parseField = function(rawField) {
        var self = this;
        if (rawField.entityType === 'text-field') {
            return new EntityFactory.fieldTypes.TextField(rawField.title, rawField.entityName, rawField.id, rawField.placeholder);
        } else if (rawField.entityType === 'list-field' && rawField.entitySubtype === 'constant-list') {
            return new EntityFactory.fieldTypes.ConstantList(rawField.title, rawField.entityName, rawField.id, rawField.listItems);
        } else if (rawField.entityType === 'list-field' && rawField.entitySubtype === 'empty-list' && rawField.emptyListType === 'text-fields-list') {
            return new EntityFactory.fieldTypes.EmptyTextList(rawField.title, rawField.entityName, rawField.id);
        } else if (rawField.entityType === 'list-field' && rawField.entitySubtype === 'empty-list' && rawField.emptyListType === 'entities-fields-list') {
            return new EntityFactory.fieldTypes.EmptyEntityList(rawField.title, rawField.entityName, rawField.id, rawField.entityId);
        } else if (rawField.entityType === 'date-field' && rawField.entitySubtype === 'date') {
            return new EntityFactory.fieldTypes.DateField(rawField.title, rawField.entityName, rawField.id);
        } else if (rawField.entityType === 'date-field' && rawField.entitySubtype === 'time') {
            return new EntityFactory.fieldTypes.TimeField(rawField.title, rawField.entityName, rawField.id);
        } else if (rawField.entityType === 'entity-field') {
            var entity = self.project.templates.filter(function(t) {
                return rawField.entityId === t.id;
            })[0];
            if (entity) {
                var fields = self.parseTemplate(entity, self.project);
                var id = entity.id;
                return new EntityFactory.fieldTypes.EntityField(rawField.title, rawField.entityName, rawField.id, id, fields);
            }
        }
    }

    EntityFactory.prototype.parseTemplate = function(template, project) {
        this.project = project;
        return template.fields.map(this.parseField.bind(this));
    }

    this.EntityFactory = EntityFactory;
}).call(this);
