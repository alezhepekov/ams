(function() {
    function MapManager( http ) {        
        this.http = http;
        this.place = null;
        this.lastQuery = '';
        this.map = null;
        this.yandexMapAPIIsLoaded = false;

        var self = this;
        if ( ymaps ) {        
            ymaps.ready( function() {                
                self.yandexMapAPIIsLoaded = true;
                self.map = ymaps;
            });
        }       
    }

    MapManager.prototype._createAddress = function(geoObject,point , queryPlace) {
        if ( !this.yandexMapAPIIsLoaded ) {
            console.error("Yandex Map API не загружен");
            return;
        }
        
        var result = {},
            geocoder,
            country;

        result.point = {
            lat: point[0],
            lon: point[1]
        }
        geocoder = geoObject.metaDataProperty.GeocoderMetaData;
        result.precision = geocoder.precision;
        country = geocoder.AddressDetails.Country;

        result.country = (country.CountryName) ? country.CountryName : '';
        result.state = (country.AdministrativeArea.AdministrativeAreaName) ? country.AdministrativeArea.AdministrativeAreaName : '';
        result.city = (country.AdministrativeArea.SubAdministrativeArea)
            ? country.AdministrativeArea.SubAdministrativeArea.Locality.LocalityName
            : (country.AdministrativeArea.Locality.LocalityName) ? country.AdministrativeArea.Locality.LocalityName : '';
        result.foundPlace = geocoder.text;
        result.queryPlace = queryPlace;
        return result;
    }

    MapManager.prototype.updatePlace = function(place, tagId, description, cb) {
        if ( !this.yandexMapAPIIsLoaded ) {
            console.error("Yandex Map API не загружен");
            return;
        }      

        var self = this;
        if (place == null || self.lastQuery === place) {
            return;
        }

        self.currentMaps = self.currentMaps || [];
        var geocode = ymaps.geocode(place);
        geocode.then(function(data) {          
            var mapObject = data.geoObjects.get(0);
            var geoObject = mapObject.properties.getAll();
            if (self.map != null && geoObject) {              
                var point = mapObject.geometry.getCoordinates();
                var desc = description || geoObject.balloonContent;

                document.getElementById(tagId).innerHTML = '';
                var currentMap = new self.map.Map(tagId, {
                    center: [point[0], point[1]],
                    zoom: 13
                });

                var placemark = new self.map.Placemark([point[0], point[1]], {
                    balloonContent: desc
                });

                currentMap.geoObjects.add(placemark);
                self.currentMaps.push(currentMap);
                self.lastQuery = place;
                var result = self._createAddress(geoObject, point, place);               

                cb && cb( result );
            }
        });
    }

    MapManager.prototype.viewPlace = function(place, tagId, description) {
        if ( !this.yandexMapAPIIsLoaded ) {
            console.error("Yandex Map API не загружен");
            return;
        }  

        var self = this;
        if (place == null || place.point == null || self.lastQuery === place) {
            return;
        }

        self.currentMaps = self.currentMaps || [];
        if (self.map != null) {
            //if (self.currentMap) {
            //    self.currentMap.destroy();
            //}
            var point = place.point;

            document.getElementById(tagId).innerHTML = '';
            var currentMap = new self.map.Map(tagId, {
                center: [point.lat, point.lon],
                zoom: 13
            });
            
            var placemark = new self.map.Placemark([point.lat, point.lon], {
                balloonContent: description
            });

            currentMap.geoObjects.add(placemark);
            self.currentMaps.push(currentMap);
        }
    }

    this.MapManager = MapManager;
}).call(this);
