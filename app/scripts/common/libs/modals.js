angular.module('crmApp').factory('Modals',['$filter', 'CONFIG',
    function ($filter, CONFIG){
    	return {
    		taskEditor: function(task, users) {
    			return {
			        backdrop: false,
			        keyboard: true,
			        backdropClick: true,
			        size: 'lg',
			        resolve: {
						task: function() {
			            	return task;
			            },
						users: function() {
			            	return users;
			            }
			        },
			        templateUrl: '/views/templates/task-edit.html',
			        controller: 'TaskEditCtrl'
			    }
    		},
    		taskFinish: function(task, users) {
    			return {
			        backdrop: false,
			        keyboard: true,
			        backdropClick: true,
			        size: 'lg',
			        resolve: {
						task: function() {
			            	return task;
			            },
						users: function() {
			            	return users;
			            }
			        },
			        templateUrl: '/views/templates/task-finish.html',
			        controller: 'TaskFinishCtrl'
			    }
    		},
            tangibleAsset: function(asset) {
            	return {
			        backdrop: false,
			        keyboard: true,
			        backdropClick: true,
			        size: 'md',
			        resolve: {
			            asset: function() {
			            	return asset;
			            }
			        },
			        templateUrl: '/views/templates/asset-tangible-edit.html',
			        controller: 'AssetTangibleCtrl'
			    }
		    },
		    intangibleAsset: function(asset) {
		    	return {
			        backdrop: false,
			        keyboard: true,
			        backdropClick: true,
			        size: 'md',
			        resolve: {
			            asset: function() {
			            	return asset;
			            }
			        },
			        templateUrl: '/views/templates/asset-intangible-edit.html',
			        controller: 'AssetTangibleCtrl'
			    }
		    },
		    creditor: function(creditor) {
		    	return {
			        backdrop: false,
			        keyboard: true,
			        backdropClick: true,
			        size: 'md',
			        resolve: {
			            creditor: function() {
			            	return creditor;
			            }
			        },
			        templateUrl: '/views/templates/creditor-edit.html',
			        controller: 'CreditorCtrl',
			        windowClass: 'creditor-modal-window'
			    }
		    },
		    debtor: function(debtor) {
		    	return {
			        backdrop: false,
			        keyboard: true,
			        backdropClick: true,
			        size: 'md',
			        resolve: {
			            debtor: function() {
			            	return debtor;
			            }
			        },
			        templateUrl: '/views/templates/debtor-edit.html',
			        controller: 'DebtorCtrl',
			        windowClass: 'debtor-modal-window'
			    }
		    },
		    editor: function(scope) {
		    	return {
			        backdrop: false,
			        keyboard: true,
			        backdropClick: true,
			        size: 'lg',
			        resolve: {
			            parentScope: function () {
			                return scope;
			            }
			        },
			        templateUrl: '/views/templates/editor.html',
			        controller: 'EditorCtrl',
			        windowClass: 'editor-modal-window'
			    }
		    }
        }
    }]
);