(function(){
    function ProposalEntity(projectId){
        this.started = new Date();
        this.projectId = projectId;
        this.name = '';
        this.number = '';
        this.templateId = '';
        this.documents = [];
        this.address = '';
        this.mapAddress = {};
        this.currentTask = {
            executor: {}
        };
        this.tasks = [];
    }

    this.ProposalEntity = ProposalEntity;
}).call(this);
