(function(){
    function ProjectEntity(){
        this.started = new Date();
        this.name = '';
        this.number = '';
        this.description = '';
        this.organizationId = '';
        this.documents = [];
        this.address = '';
        this.mapAddress = {};
        this.proposalTemplate = {
            fields: [],
        };
        this.templates = [];
        this.statuses = [];
    }

    this.ProjectEntity = ProjectEntity;
}).call(this);
