angular.module('crmApp').factory('authInterceptor', function ($rootScope, $q, $window, $location, $timeout, CONFIG) {
    'use strict';
    return {
        request: function (config) {
            config.headers = config.headers || {};
            var token = "";
            
            if($window.localStorage.profile && $window.localStorage.profile != "undefined"){
                $rootScope.currentUser = JSON.parse($window.localStorage.profile);
            }
            
            if (config.url.indexOf(CONFIG.CLOUD_ADDRESS) >= 0){
                token = $rootScope.currentUser.cloudToken;
            }
            else{
                if ($window.localStorage.token != undefined && $window.localStorage.token != "undefined") {
                    token = $window.localStorage.token;
                }
            }            

            config.headers.Authorization = ('Bearer ' + token).trim();

            return config;
        },
        responseError: function(response){
            if(response.status === 401){
                $timeout(function (){
                    $location.path('/login');
                });
            }
            return $q.reject(response);
        }
    };
});
angular.module('crmApp').config(function($httpProvider){
    $httpProvider.interceptors.push('authInterceptor');
});
