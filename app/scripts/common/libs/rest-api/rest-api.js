angular.module('crmApp').factory('Organization', function($resource, CONFIG, authInterceptor) {
    'use strict';
    return $resource(CONFIG.REST_ADDRESS + '/api/organizations/:id', {}, {
        'get': {
            method: 'GET',
            interceptor: authInterceptor
        },
        'save': {
            method: 'POST',
            interceptor: authInterceptor
        },
        'update':{
            method: 'PUT',
            interceptor: authInterceptor
        },
        'query': {
            method: 'GET',
            interceptor: authInterceptor
        },
        'remove': {
            method: 'DELETE',
            interceptor: authInterceptor
        },
        'delete': {
            method: 'DELETE',
            interceptor: authInterceptor
        }
    });
});

angular.module('crmApp').factory('Project', function($resource, CONFIG, authInterceptor) {
    'use strict';
    return $resource(CONFIG.REST_ADDRESS + '/api/projects/:id', {}, {
        'get': {
            method: 'GET',
            interceptor: authInterceptor
        },
        'save': {
            method: 'POST',
            interceptor: authInterceptor
        },
        'update':{
            method: 'PUT',
            interceptor: authInterceptor
        },
        'query': {
            method: 'GET',
            interceptor: authInterceptor
        },
        'remove': {
            method: 'DELETE',
            interceptor: authInterceptor
        },
        'delete': {
            method: 'DELETE',
            interceptor: authInterceptor
        }
    });
});

angular.module('crmApp').factory('Comment', function($resource, CONFIG, authInterceptor) {
    'use strict';
    return $resource(CONFIG.REST_ADDRESS + '/api/comments/:id', {}, {
        'get': {
            method: 'GET',
            interceptor: authInterceptor
        },
        'save': {
            method: 'POST',
            interceptor: authInterceptor
        },
        'update':{
            method: 'PUT',
            interceptor: authInterceptor
        },
        'query': {
            method: 'GET',
            interceptor: authInterceptor
        },
        'remove': {
            method: 'DELETE',
            interceptor: authInterceptor
        },
        'delete': {
            method: 'DELETE',
            interceptor: authInterceptor
        }
    });
});

angular.module('crmApp').factory('Proposal', function($resource, CONFIG, authInterceptor) {
    'use strict';
    return $resource(CONFIG.REST_ADDRESS + '/api/projects/:projectId/proposals/:id', {}, {
        'get': {
            method: 'GET',
            interceptor: authInterceptor
        },
        'save': {
            method: 'POST',
            interceptor: authInterceptor
        },
        'uploadFileCloud': {
            method: 'POST',
            //url: CONFIG.REST_ADDRESS + '/api/projects/:projectId/proposals/:id/uploadFileCloud',
            url: CONFIG.REST_ADDRESS + '/api/projects/uploadFileCloud',
            interceptor: authInterceptor
        },
        'downloadFileCloud': {
            method: 'POST',
            //url: CONFIG.REST_ADDRESS + '/api/projects/:projectId/proposals/:id/downloadFileCloud',
            url: CONFIG.REST_ADDRESS + '/api/projects/downloadFileCloud',
            interceptor: authInterceptor
        },
        'update':{
            method: 'PUT',
            interceptor: authInterceptor
        },
        'query': {
            method: 'GET',
            interceptor: authInterceptor
        },
        'remove': {
            method: 'DELETE',
            interceptor: authInterceptor
        },
        'delete': {
            method: 'DELETE',
            interceptor: authInterceptor
        },
        'saveDoc': {
            method: 'POST',
            url: CONFIG.REST_ADDRESS + '/api/projects/:projectId/proposals/:id/saveDoc',
            interceptor: authInterceptor
        },
    });
});

angular.module('crmApp').factory('User', function($resource, CONFIG, authInterceptor) {
    'use strict';
    return $resource(CONFIG.REST_ADDRESS + '/api/users/:id', {}, {
        'get': {
            method: 'GET',
            interceptor: authInterceptor
        },
        'save': {
            method: 'POST',
            interceptor: authInterceptor
        },
        'update':{
            method: 'PUT',
            interceptor: authInterceptor
        },
        'query': {
            method: 'GET',
            interceptor: authInterceptor
        },
        'manage': {
            method: 'GET',
            url: CONFIG.REST_ADDRESS + '/api/users/manage',
            interceptor: authInterceptor
        },
        'remove': {
            method: 'DELETE',
            interceptor: authInterceptor
        },
        'delete': {
            method: 'DELETE',
            interceptor: authInterceptor
        }
    });
});

angular.module('crmApp').factory('Role', function($resource, CONFIG, authInterceptor) {
    'use strict';
    return $resource(CONFIG.REST_ADDRESS + '/api/roles/:id', {}, {
        'get': {
            method: 'GET',
            interceptor: authInterceptor
        },
        'save': {
            method: 'POST',
            interceptor: authInterceptor
        },
        'update':{
            method: 'PUT',
            interceptor: authInterceptor
        },
        'query': {
            method: 'GET',
            interceptor: authInterceptor
        },
        'remove': {
            method: 'DELETE',
            interceptor: authInterceptor
        },
        'delete': {
            method: 'DELETE',
            interceptor: authInterceptor
        }
    });
});

angular.module('crmApp').factory('Profile', function($resource, CONFIG, authInterceptor) {
    'use strict';
    return $resource(CONFIG.REST_ADDRESS + '/api/users/profile', {}, {
        'get': {
            method: 'GET',
            interceptor: authInterceptor
        },
        'update':{
            method: 'PUT',
            interceptor: authInterceptor
        }
    });
});


angular.module('crmApp').factory('Report', function($resource, CONFIG, authInterceptor) {
    'use strict';
    return $resource(CONFIG.REST_ADDRESS + '/api/reports/:projectId', {}, {
        'status': {
            url: CONFIG.REST_ADDRESS + '/api/reports/status/:projectId',
            method: 'GET',
            interceptor: authInterceptor
        },
        'supervisor': {
            url: CONFIG.REST_ADDRESS + '/api/reports/supervisor/:projectId',
            method: 'GET',
            interceptor: authInterceptor
        },
        'dgim': {
            url: CONFIG.REST_ADDRESS + '/api/reports/dgim/:projectId',
            method: 'POST',
            params: {projectId: '@projectId'},
            interceptor: authInterceptor
        },
        'export': {
            url: CONFIG.REST_ADDRESS + '/api/reports/export/:projectId',
            method: 'POST',
            params: {projectId: '@projectId'},
            interceptor: authInterceptor
        },
        'duration': {
            url: CONFIG.REST_ADDRESS + '/api/reports/duration/:projectId',
            method: 'GET',
            interceptor: authInterceptor
        },
        'statusesDuration': {
            url: CONFIG.REST_ADDRESS + '/api/reports/statusesDuration/:projectId',
            method: 'GET',
            interceptor: authInterceptor
        },
        'statusesAvgDuration': {
            url: CONFIG.REST_ADDRESS + '/api/reports/statusesAvgDuration/:projectId',
            method: 'GET',
            interceptor: authInterceptor
        },
        'plan': {
            url: CONFIG.REST_ADDRESS + '/api/reports/plan/:projectId',
            method: 'GET',
            interceptor: authInterceptor
        },
        'tasksExcel': {
            url: CONFIG.REST_ADDRESS + '/api/reports/tasksExcel/:projectId',
            method: 'POST',
            params: {projectId: '@projectId', startDate: '@startDate', endDate: '@endDate'},
            interceptor: authInterceptor
        },
    });
});

angular.module('crmApp').factory('Map', function($resource, CONFIG, authInterceptor) {
    'use strict';
    return $resource(CONFIG.REST_ADDRESS + '/api/projects/:projectId/map', {}, {
        'get': {
            method: 'GET',
            interceptor: authInterceptor
        }
    });
});
