angular.module( "crmApp" ).factory( "GetProject",["$q","User", "Project",
    function( $q, User, Project ) {
        return function( $scope, id ) {
            if ( !$scope.project || !$scope.users || $scope.project._id != id ) {
                /*if there is no project and users in $scope, checkout from server*/
                var promise = $q.all({
                    project: Project.get( {id: id} ).$promise,
                    users: User.query().$promise
                });
                return promise;
            }
            else {
                var promise = {};
                promise.resolved = true;
                promise.project = $scope.project;
                promise.users = $scope.users;
                promise.then = function( f ) {
                    f(this);
                }
                return promise;
            }
        }
    }
])
