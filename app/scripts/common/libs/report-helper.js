angular.module('crmApp').factory('ReportHelper',['Utils', 'CONFIG',
    function (Utils, CONFIG){
        var reportStatuses = {
            heading: 'Статус',
            route:   'reports.statuses'
        };

        var reportSupervisors = {
            heading: 'Супервайзеры',
            route:   'reports.supervisor'
        };

        var reportTasksExcel = {
            heading: 'Задачи',
            route:   'reports.tasksexcel'
        };

        var reportDuration = {
            heading: 'Сроки',
            route:   'reports.duration'
        };

        var reportStatusesDuration = {
            heading: 'Сроки по статусам',
            route:   'reports.statuses-duration'
        };

        var reportStatusesAvgDuration = {
            heading: 'Средние сроки',
            route:   'reports.statuses-avg-duration'
        };

        //TODO: Ошибки + Активность

        var reportPlan = {
            heading: 'План',
            route:   'reports.plan'
        };

        var reportDgim = {
            heading: 'ДГИМ',
            route:   'reports.dgim'
        };

        var reportPropExport = {
            heading: 'Экспорт',
            route:   'reports.propexport'
        };

        var reportStatistics = {
            heading: 'Статистика',
            route: 'reports.statistics'
        }

        return {
            getTabsConfig: function(projectId, isCoordinator){
                reportStatuses.params = { id: projectId };
                reportSupervisors.params = { id: projectId };
                reportTasksExcel.params = { id: projectId };
                reportDuration.params = { id: projectId };
                reportStatusesDuration.params = { id: projectId };
                reportStatusesAvgDuration.params = { id: projectId };
                reportPlan.params = { id: projectId };
                reportDgim.params = { id: projectId };
                reportPropExport.params = { id: projectId };
                reportStatistics.params = { id: projectId };

                /*if (isCoordinator) {
                    return  [ reportStatuses, reportSupervisors ];
                } else {
                    if (Utils.isOrgMode(projectId)) {
                        return  [
                            reportStatuses, reportStatusesDuration, reportPlan, 
                            reportStatistics, reportSupervisors, reportDuration, reportStatusesAvgDuration,
                            reportTasksExcel, reportDgim, reportPropExport
                        ];
                    } else {
                        return  [
                            reportStatuses, reportStatusesDuration, reportPlan,
                            reportStatistics, reportSupervisors, reportDuration, reportStatusesAvgDuration,
                            reportTasksExcel, reportDgim, reportPropExport
                        ];
                    }
                }*/

                return  [ reportStatuses, reportStatistics, reportPropExport ];
            },
            getTabsStatusesConfig: function(){
                var res = [];
                CONFIG.STATUSES_REPORTS.forEach(function(st){
                    var report1 = {
                        heading: st,
                        route: 'reports'
                    };

                    res.push(report1);
                });
                return res;
            },
            getFieldValue : function(values, id){
                var value = values.filter(function(x){ return x.id == id });
                if (value && value.length > 0){
                    value = value[0].value;
                }
                return value;
            },
            getCorrectDayLocalName: function(daysNum) {
                return (daysNum % 10 == 1 && (daysNum > 20 || daysNum < 10)) 
                    ? 'день' : ((daysNum % 10 < 5 && daysNum > 20) ? 'дня' : (daysNum>1 && daysNum<5) ? 'дня' : 'дней');
            }
        };
        }    
])