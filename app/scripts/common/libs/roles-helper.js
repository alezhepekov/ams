angular.module('crmApp').factory('RolesHelper',['$filter', 'CONFIG',
    function ($filter, CONFIG){
    	return {
    		isCoordinator: function(roles) {
    			var res = false;
    			roles.forEach(function(role) {
    				if (role.roleId=="6ba9e551-a4c0-11e4-8c38-97efb816853a")
    					res = true;
    			})
    			return res;
    		},
    		getRoleForProject: function(currentUser, project) {
    			// Роль одна в проекте
    			var currentRole = currentUser.roles.filter(function(userRole){
                    return userRole.projectId === project._id;
                })[0];

                if (currentRole && project.roles && project.roles.length > 0) {
                	return project.roles.filter(function(projectRole) {
                		return projectRole.id === currentRole.roleId;
                	})[0];
                }

                return null;
    		},
            getCurrentRoles: function(currentUser, project) {
                var result = currentUser.roles.filter(function(userRole){
                    return userRole.projectId === project._id;
                }).map(function(userRole){
                    var r;
                    project.roles.forEach(function(projectRole){
                        if(userRole.roleId === projectRole.id){
                            r = projectRole.name;
                        }
                    });

                    return r;
                }).filter(function(r){
                    return r != null;
                });

                return result;
            },
            getRoleNameById: function(id, roles) {
                return result = roles.filter(function(r){ return r.id==id; });
            },
            //TODO: Оставить одну функцию
            getRoleNameById2: function(id, roles) {
                var result = undefined;
                $.each(roles, function(index, role) {
                    if (role.id==id)
                        result = role.name;
                });
                return result;
            },
            getStatusesByRoleName: function(roleName, statuses, FILTERS) {
                var statusFilter = FILTERS.filter(function(s){ return s.roleName == roleName});
                var result = [];

                //TODO: использовать регулярные выражения
                if (statusFilter && statusFilter.length > 0) {
                    statusFilter[0].tasksStatuses.forEach(function(ts){
                        statuses.forEach(function(s){
                            if(ts==s.title){
                                result.push(s);
                            }
                        });
                    });
                }

                return result;
            },
            canEdit: function(user, project, $scope){
                var result = false;
                project.roles.forEach(function(pr){
                    user.roles.forEach(function(ur){
                        if(pr.id === ur.roleId){
                            if(pr.editAllProposals){
                                result = true;
                            }
                        }
                    });
                });
                return result || $scope.currentUser.isRoot || $scope.editRight;
            },
            canCompleteTasks: function(user, project, editRight) {
                var result = false;
                project.roles.forEach(function(pr){
                    user.roles.forEach(function(ur){
                        if(pr.id === ur.roleId){
                            if(pr.completeAllTasks){
                                result = true;
                            }
                        }
                    });
                });
                return result || (editRight || user.isRoot);
            },
            canEditTasks: function(user, project, editRight) {
                var result = false;
                project.roles.forEach(function(pr){
                    user.roles.forEach(function(ur){
                        if(pr.id === ur.roleId){
                            if(pr.editAllTasks){
                                result = true;
                            }
                        }
                    });
                });
                return result || (editRight || user.isRoot);
            },
            canViewComments: function(currentUser, proposal) {
                var isRoot = currentUser.isRoot;
                var isOwner = false;
                var isManager = false;
                var isCoordinator = false;
                var isInProjectGroup = false;
                var isSupervisorToView = currentUser.email==='supervisor@abn-consult.ru' || currentUser.email==='superv@f-case.ru';

                if (proposal.owner) {
                    isOwner = proposal.owner._id === currentUser._id;
                } 

                if (proposal.manager) {
                    isManager = proposal.manager._id === currentUser._id;
                }

                if (proposal.coordinator) {
                    isCoordinator = proposal.coordinator._id === currentUser._id;
                }

                if (proposal.projectGroupList) {
                    proposal.projectGroupList.forEach(function(user) {
                        if (user._id === currentUser._id)
                            isInProjectGroup = true;
                    });
                }

                return isRoot || isOwner || isManager || isCoordinator || isInProjectGroup || isSupervisorToView;
            },
            isInitWorkGroup: function(currentUser, proposal) {
                var isOwner = false;
                var isManager = false;
                var isCoordinator = false;

                if (proposal.owner) {
                    isOwner = proposal.owner._id === currentUser._id;
                }

                if (proposal.manager) {
                    isManager = proposal.manager._id === currentUser._id;
                }

                if (proposal.coordinator) {
                    isCoordinator = proposal.coordinator._id === currentUser._id;
                }

                return isOwner || isManager || isCoordinator;
            }
    	}
    }]
);