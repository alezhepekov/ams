angular.module('crmApp').constant('CONFIG', {  
    REST_ADDRESS: 'http://localhost:3000',
    CLOUD_ADDRESS: 'http://cloud.abn-consult.ru',
    CLOUD_CLIENT_ID: '01dff085a6de46ce8cf358f26bfe8a91',
    STATISTICS_ADDRESS: 'http://glog.f-case.ru',
    PROJECT_CITIES: {
		'Управление активами': 'Москва,'
    },
    STATUSES_FILTER: [
    	{
    		roleName: 'СРО',
    		proposalsStatuses: ['замечания от СРО', 'согласование в СРО', 'повторное согласование в СРО', 
    			'исправление ошибок', 'подготовка экспертизы', 'повторная экспертиза'],
    		tasksStatuses: ['замечания от СРО', 'согласование в СРО', 'повторное согласование в СРО', 
    			'исправление ошибок', 'подготовка экспертизы', 'повторная экспертиза']
    	}
    ],
    CURRENCIES: [ {name: 'руб.', code: 'rub'}, {name: 'дол.', code: 'usd'}, {name: 'евр.', code: 'eur'} ],
    STATUSES_REPORTS: [
        'оценка', 'согласование в СРО' , 'повторное согласование в СРО', 'замечания от СРО', 'согласование в ДГИМ',
        'повторное согласование в ДГИМ', 'замечания от ДГИМ', 'проверка', 'исправление', 'проверка после исправления'
    ],
    ONLY_CURRENT_STATUSES: [
        '5555c4a152c591d01b76ab51',
        '5555c4d652c591d01b76ab5f'
    ],
    ORG_MODE: [
        '55a3708e043dcaac1570122b'
    ],
    COMMENTS_UPDATE_INTERVAL: 5000,
    MESSAGE_TYPES: [
        {name: 'Обычно', code: 'success'},
        {name: 'Обычно2', code: 'info'},
        {name: 'Важно', code: 'warning'},
        {name: 'Критично', code: 'danger'}
    ],
    ORGS: [
        {id: '54c53693a8a6e6d9a0866e0a', name: 'АБН'},
        {id: '54c5eaa45079053c0350d085', name: 'ДГИМ'},
        {id: '55372317852193b41657450f', name: 'СРО'},
		{id: '5555c43c52c591d01b76ab3c', name: 'ДГС'},
        {id: '5583e61d8c80873021f31b50', name: 'УГС'},
        {id: '55a52e10e3d3a94818105980', name: 'Бизнес-КРУГ'},
        {id: '55a66bd3369a9b8c1d8ae45a', name: 'Мосинжпроект'},
        {id: '55ad04a658e88f78186a5ab6', name: 'ДГП'},
        {id: '55b5f095dd994d081b4defda', name: 'МПСИ'}
    ],
    DUEDATE_PERIOD: 5
});