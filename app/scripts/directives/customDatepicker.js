'use strict';

angular.module('crmApp')
.directive('customDatepicker',function($compile,$timeout){
        return {
            templateUrl: '../../views/templates/custom-datepicker.html',
            replace:true,            
            scope: {
                ngModel: '=',
                dateOptions: '@',
                dateDisabled: '@',
                opened: '=',
                min: '@',
                max: '@',
                popup: '@',
                options: '@',
                name: '@',
                id: '@'
            },
            link: function($scope, $element, $attrs, $controller){

            }    
        };
    });