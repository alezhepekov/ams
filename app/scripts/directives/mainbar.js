'use strict';

angular.module('crmApp')
    .directive('mainbar', ['$rootScope', '$routeParams', '$location', 'Project', function($rootScope, $routeParams, location, Project) {
        return {
            templateUrl: '../../views/templates/mainbar.html',
            restrict: 'E',
            link: function (scope, element, attrs) {
                scope.$location = location;
                
                if ($routeParams.id) {
                    //scope.projectId = window.localStorage.projectId;
                    scope.projectId = $routeParams.id;
                }/* else {
                    window.location.href = '/';
                }*/

                scope.isActive = function(str) {
                    return location.$$url.indexOf(str)>0;
                };

                scope.$watch('$location.path()', function(locationPath) {
                    if (window.localStorage.profile) {
                        
                        if (!$rootScope.project) {
                            var proj = Project.get({id: $rootScope.mainProjectId}).$promise;
                            proj.then(function(data) {
                                $rootScope.project = data;
                            });
                        }

                        var canSeeReports = function(user, project) {
                            var result = false;
                            if (project && user) {
                                project.roles.forEach(function(pr) {
                                    user.roles.forEach(function(ur) {
                                        if (pr.id === ur.roleId) {
                                            if (pr.seeStatistic) {
                                                result = true;
                                            }
                                        }
                                    });
                                });
                            }
                            return result;
                        };

                        var canSeeMaps = function(user, project) {
                            var result = false;
                            if (project && user) {
                                project.roles.forEach(function(pr) {
                                    user.roles.forEach(function(ur) {
                                        if (pr.id === ur.roleId) {
                                            if (pr.seeMaps) {
                                                result = true;
                                            }
                                        }
                                    });
                                });
                            }
                            return result;
                        };

                        $rootScope.$watch('currentUser', function(){
                            scope.currentUser = $rootScope.currentUser;
                        });

                        $rootScope.$watch('project', function(){
                            scope.project = $rootScope.project;
                        });

                        scope.isRoot = function(){
                            return scope.currentUser && scope.currentUser.isRoot;
                        };
                        scope.canSeeReports = function(){
                            return canSeeReports(scope.currentUser, scope.project);
                        };
                        scope.canSeeMaps = function(){
                            return canSeeMaps(scope.currentUser, scope.project);
                        };
                    }
                });
            }
        };
    }]);